## a
### aqt (cross)


| Description | *aqt: Another (unofficial) Qt CLI Installer on multi-platforms* |
| -- | -- |
| Homepage | [https://github.com/miurahr/aqtinstall](https://github.com/miurahr/aqtinstall) |
| License | MIT |
| Versions |  |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [aqt/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/a/aqt/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] aqt
```

##### Integration in the project (xmake.lua)

```lua
add_requires("aqt")
```


### argh (cross)


| Description | *Argh! A minimalist argument handler.* |
| -- | -- |
| Homepage | [https://github.com/adishavit/argh](https://github.com/adishavit/argh) |
| License | BSD-3-Clause |
| Versions | v1.3.2 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [argh/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/a/argh/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] argh
```

##### Integration in the project (xmake.lua)

```lua
add_requires("argh")
```


### argparse (cross)


| Description | *A single header argument parser for C++17* |
| -- | -- |
| Homepage | [https://github.com/p-ranav/argparse](https://github.com/p-ranav/argparse) |
| License | MIT |
| Versions | 2.6, 2.7, 2.8, 2.9 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [argparse/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/a/argparse/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] argparse
```

##### Integration in the project (xmake.lua)

```lua
add_requires("argparse")
```


### asio (cross)


| Description | *Asio is a cross-platform C++ library for network and low-level I/O programming that provides developers with a consistent asynchronous model using a modern C++ approach.* |
| -- | -- |
| Homepage | [http://think-async.com/Asio/](http://think-async.com/Asio/) |
| License | BSL-1.0 |
| Versions | 1.20.0, 1.21.0, 1.24.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [asio/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/a/asio/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] asio
```

##### Integration in the project (xmake.lua)

```lua
add_requires("asio")
```


### autoconf (cross)


| Description | *An extensible package of M4 macros that produce shell scripts to automatically configure software source code packages.* |
| -- | -- |
| Homepage | [https://www.gnu.org/software/autoconf/autoconf.html](https://www.gnu.org/software/autoconf/autoconf.html) |
| Versions | 2.68, 2.69, 2.71 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [autoconf/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/a/autoconf/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] autoconf
```

##### Integration in the project (xmake.lua)

```lua
add_requires("autoconf")
```


### automake (cross)


| Description | *A tool for automatically generating Makefile.in files compliant with the GNU Coding Standards.* |
| -- | -- |
| Homepage | [https://www.gnu.org/software/automake/](https://www.gnu.org/software/automake/) |
| Versions | 1.15.1, 1.16.1, 1.16.4, 1.9.5, 1.9.6 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [automake/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/a/automake/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] automake
```

##### Integration in the project (xmake.lua)

```lua
add_requires("automake")
```



## b
### bazel (cross)


| Description | *A fast, scalable, multi-language and extensible build system* |
| -- | -- |
| Homepage | [https://bazel.build/](https://bazel.build/) |
| Versions | 5.0.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [bazel/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/b/bazel/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] bazel
```

##### Integration in the project (xmake.lua)

```lua
add_requires("bazel")
```


### better-enums (cross)


| Description | *C++ compile-time enum to string, iteration, in a single header file* |
| -- | -- |
| Homepage | [http://aantron.github.io/better-enums](http://aantron.github.io/better-enums) |
| License | BSD-2-Clause |
| Versions | 0.11.3 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [better-enums/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/b/better-enums/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] better-enums
```

##### Integration in the project (xmake.lua)

```lua
add_requires("better-enums")
```


### bin2c (cross)


| Description | *A simple utility for converting a binary file to a c application* |
| -- | -- |
| Homepage | [https://github.com/gwilymk/bin2c](https://github.com/gwilymk/bin2c) |
| Versions | 0.0.1 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [bin2c/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/b/bin2c/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] bin2c
```

##### Integration in the project (xmake.lua)

```lua
add_requires("bin2c")
```


### binutils (cross)


| Description | *GNU binary tools for native development* |
| -- | -- |
| Homepage | [https://www.gnu.org/software/binutils/binutils.html](https://www.gnu.org/software/binutils/binutils.html) |
| License | GPL-2.0 |
| Versions | 2.34, 2.38 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [binutils/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/b/binutils/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] binutils
```

##### Integration in the project (xmake.lua)

```lua
add_requires("binutils")
```


### bison (cross)


| Description | *A general-purpose parser generator.* |
| -- | -- |
| Homepage | [https://www.gnu.org/software/bison/](https://www.gnu.org/software/bison/) |
| License | GPL-3.0 |
| Versions | 3.7.4, 3.7.6, 3.8.2 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [bison/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/b/bison/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] bison
```

##### Integration in the project (xmake.lua)

```lua
add_requires("bison")
```


### blake3 (cross)


| Description | *BLAKE3 is a cryptographic hash function that is much faster than MD5, SHA-1, SHA-2, SHA-3, and BLAKE2; secure, unlike MD5 and SHA-1 (and secure against length extension, unlike SHA-2); highly parallelizable across any number of threads and SIMD lanes, because it's a Merkle tree on the inside; capable of verified streaming and incremental updates (Merkle tree); a PRF, MAC, KDF, and XOF, as well as a regular hash; and is a single algorithm with no variants, fast on x86-64 and also on smaller architectures.* |
| -- | -- |
| Homepage | [https://blake3.io/](https://blake3.io/) |
| License | CC0-1.0 |
| Versions | 1.3.1, 1.3.3 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [blake3/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/b/blake3/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] blake3
```

##### Integration in the project (xmake.lua)

```lua
add_requires("blake3")
```


### boost (cross)


| Description | *Collection of portable C++ source libraries.* |
| -- | -- |
| Homepage | [https://www.boost.org/](https://www.boost.org/) |
| License | BSL-1.0 |
| Versions | 1.70.0, 1.72.0, 1.73.0, 1.74.0, 1.75.0, 1.76.0, 1.77.0, 1.78.0, 1.79.0, 1.80.0, 1.81.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [boost/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/b/boost/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] boost
```

##### Integration in the project (xmake.lua)

```lua
add_requires("boost")
```


### brotli (cross)


| Description | *Brotli compression format.* |
| -- | -- |
| Homepage | [https://github.com/google/brotli](https://github.com/google/brotli) |
| Versions | 1.0.9 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [brotli/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/b/brotli/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] brotli
```

##### Integration in the project (xmake.lua)

```lua
add_requires("brotli")
```


### brynet (cross)


| Description | *Header Only Cross platform high performance TCP network library using C++ 11* |
| -- | -- |
| Homepage | [https://github.com/IronsDu/brynet](https://github.com/IronsDu/brynet) |
| Versions | 1.0.9 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [brynet/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/b/brynet/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] brynet
```

##### Integration in the project (xmake.lua)

```lua
add_requires("brynet")
```


### bullet3 (cross)


| Description | *Bullet Physics SDK.* |
| -- | -- |
| Homepage | [http://bulletphysics.org](http://bulletphysics.org) |
| License | zlib |
| Versions | 2.88, 3.05, 3.09, 3.24 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [bullet3/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/b/bullet3/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] bullet3
```

##### Integration in the project (xmake.lua)

```lua
add_requires("bullet3")
```


### bzip2 (cross)


| Description | *Freely available, patent free, high-quality data compressor.* |
| -- | -- |
| Homepage | [https://sourceware.org/bzip2/](https://sourceware.org/bzip2/) |
| Versions | 1.0.8 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [bzip2/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/b/bzip2/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] bzip2
```

##### Integration in the project (xmake.lua)

```lua
add_requires("bzip2")
```



## c
### ca-certificates (cross)


| Description | *Mozilla’s carefully curated collection of Root Certificates for validating the trustworthiness of SSL certificates while verifying the identity of TLS hosts.* |
| -- | -- |
| Homepage | [https://mkcert.org/](https://mkcert.org/) |
| Versions | 20211118, 20220604 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [ca-certificates/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/ca-certificates/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] ca-certificates
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ca-certificates")
```


### cargs (cross)


| Description | *A lightweight cross-platform getopt alternative that works on Linux, Windows and macOS. Command line argument parser library for C/C++. Can be used to parse argv and argc parameters.* |
| -- | -- |
| Homepage | [https://likle.github.io/cargs/](https://likle.github.io/cargs/) |
| License | MIT |
| Versions | v1.0.3 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [cargs/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/cargs/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] cargs
```

##### Integration in the project (xmake.lua)

```lua
add_requires("cargs")
```


### catch2 (cross)


| Description | *Catch2 is a multi-paradigm test framework for C++. which also supports Objective-C (and maybe C). * |
| -- | -- |
| Homepage | [https://github.com/catchorg/Catch2](https://github.com/catchorg/Catch2) |
| License | BSL-1.0 |
| Versions | v2.13.10, v2.13.5, v2.13.6, v2.13.7, v2.13.8, v2.13.9, v2.9.2, v3.1.0, v3.1.1, v3.2.0, v3.2.1 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [catch2/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/catch2/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] catch2
```

##### Integration in the project (xmake.lua)

```lua
add_requires("catch2")
```


### cereal (cross)


| Description | *cereal is a header-only C++11 serialization library.* |
| -- | -- |
| Homepage | [https://uscilab.github.io/cereal/index.html](https://uscilab.github.io/cereal/index.html) |
| License | BSD-3-Clause |
| Versions | 1.3.0, 1.3.1 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [cereal/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/cereal/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] cereal
```

##### Integration in the project (xmake.lua)

```lua
add_requires("cereal")
```


### ceval (cross)


| Description | *A C/C++ library for parsing and evaluation of arithmetic expressions.* |
| -- | -- |
| Homepage | [https://github.com/erstan/ceval](https://github.com/erstan/ceval) |
| License | MIT |
| Versions | 1.0.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [ceval/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/ceval/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] ceval
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ceval")
```


### cgetopt (cross)


| Description | *A GNU getopt() implementation written in pure C.* |
| -- | -- |
| Homepage | [https://github.com/xq114/cgetopt/](https://github.com/xq114/cgetopt/) |
| Versions | 1.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [cgetopt/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/cgetopt/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] cgetopt
```

##### Integration in the project (xmake.lua)

```lua
add_requires("cgetopt")
```


### chromium_zlib (cross)


| Description | *zlib from chromium* |
| -- | -- |
| Homepage | [https://chromium.googlesource.com/chromium/src/third_party/zlib/](https://chromium.googlesource.com/chromium/src/third_party/zlib/) |
| License | zlib |
| Versions | 2022.02.22 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [chromium_zlib/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/chromium_zlib/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] chromium_zlib
```

##### Integration in the project (xmake.lua)

```lua
add_requires("chromium_zlib")
```


### civetweb (cross)


| Description | *Embedded C/C++ web server* |
| -- | -- |
| Homepage | [https://github.com/civetweb/civetweb](https://github.com/civetweb/civetweb) |
| License | MIT |
| Versions | v1.15 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [civetweb/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/civetweb/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] civetweb
```

##### Integration in the project (xmake.lua)

```lua
add_requires("civetweb")
```


### clara (cross)


| Description | *A simple to use, composable, command line parser for C++ 11 and beyond.* |
| -- | -- |
| Homepage | [https://github.com/catchorg/Clara](https://github.com/catchorg/Clara) |
| License | BSL-1.0 |
| Versions | 1.1.5 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [clara/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/clara/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] clara
```

##### Integration in the project (xmake.lua)

```lua
add_requires("clara")
```


### cli (cross)


| Description | *A library for interactive command line interfaces in modern C++* |
| -- | -- |
| Homepage | [https://github.com/daniele77/cli](https://github.com/daniele77/cli) |
| Versions | v2.0.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [cli/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/cli/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] cli
```

##### Integration in the project (xmake.lua)

```lua
add_requires("cli")
```


### clib (cross)


| Description | *Header-only library for C99 that implements the most important classes from GLib: GList, GHashTable and GString.* |
| -- | -- |
| Homepage | [https://github.com/aheck/clib](https://github.com/aheck/clib) |
| License | MIT |
| Versions | 2022.12.25 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [clib/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/clib/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] clib
```

##### Integration in the project (xmake.lua)

```lua
add_requires("clib")
```


### cmake (cross)


| Description | *A cross-platform family of tool designed to build, test and package software* |
| -- | -- |
| Homepage | [https://cmake.org](https://cmake.org) |
| Versions | 3.11.4, 3.15.4, 3.18.4, 3.21.0, 3.22.1, 3.24.1, 3.24.2 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [cmake/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/cmake/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] cmake
```

##### Integration in the project (xmake.lua)

```lua
add_requires("cmake")
```


### cnpy (cross)


| Description | *library to read/write .npy and .npz files in C/C++* |
| -- | -- |
| Homepage | [https://github.com/rogersce/cnpy](https://github.com/rogersce/cnpy) |
| License | MIT |
| Versions | 2018.06.01 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [cnpy/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/cnpy/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] cnpy
```

##### Integration in the project (xmake.lua)

```lua
add_requires("cnpy")
```


### concurrentqueue (cross)


| Description | *An industrial-strength lock-free queue for C++.* |
| -- | -- |
| Homepage | [https://github.com/cameron314/concurrentqueue](https://github.com/cameron314/concurrentqueue) |
| Versions |  |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [concurrentqueue/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/concurrentqueue/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] concurrentqueue
```

##### Integration in the project (xmake.lua)

```lua
add_requires("concurrentqueue")
```


### cpp-httplib (cross)


| Description | *A C++11 single-file header-only cross platform HTTP/HTTPS library.* |
| -- | -- |
| Homepage | [https://github.com/yhirose/cpp-httplib](https://github.com/yhirose/cpp-httplib) |
| Versions | 0.8.5, 0.9.2 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [cpp-httplib/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/cpp-httplib/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] cpp-httplib
```

##### Integration in the project (xmake.lua)

```lua
add_requires("cpp-httplib")
```


### crc32c (cross)


| Description | *CRC32C implementation with support for CPU-specific acceleration instructions* |
| -- | -- |
| Homepage | [https://github.com/google/crc32c](https://github.com/google/crc32c) |
| Versions | 1.1.2 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [crc32c/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/crc32c/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] crc32c
```

##### Integration in the project (xmake.lua)

```lua
add_requires("crc32c")
```


### csv2 (cross)


| Description | *A CSV parser library* |
| -- | -- |
| Homepage | [https://github.com/p-ranav/csv2](https://github.com/p-ranav/csv2) |
| License | MIT |
| Versions | v0.1 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [csv2/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/csv2/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] csv2
```

##### Integration in the project (xmake.lua)

```lua
add_requires("csv2")
```


### csvparser (cross)


| Description | *A modern C++ library for reading, writing, and analyzing CSV (and similar) files (by vincentlaucsb)* |
| -- | -- |
| Homepage | [https://github.com/vincentlaucsb/csv-parser](https://github.com/vincentlaucsb/csv-parser) |
| Versions | 2.1.1 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [csvparser/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/csvparser/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] csvparser
```

##### Integration in the project (xmake.lua)

```lua
add_requires("csvparser")
```


### ctre (cross)


| Description | *ctre is a Compile time PCRE (almost) compatible regular expression matcher.* |
| -- | -- |
| Homepage | [https://github.com/hanickadot/compile-time-regular-expressions/](https://github.com/hanickadot/compile-time-regular-expressions/) |
| Versions | 3.4.1 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [ctre/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/ctre/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] ctre
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ctre")
```


### cxxopts (cross)


| Description | *Lightweight C++ command line option parser* |
| -- | -- |
| Homepage | [https://github.com/jarro2783/cxxopts](https://github.com/jarro2783/cxxopts) |
| Versions | v2.2.0, v3.0.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [cxxopts/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/cxxopts/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] cxxopts
```

##### Integration in the project (xmake.lua)

```lua
add_requires("cxxopts")
```



## d
### date (cross)


| Description | *A date and time library for use with C++11 and C++14.* |
| -- | -- |
| Homepage | [https://github.com/HowardHinnant/date](https://github.com/HowardHinnant/date) |
| License | MIT |
| Versions | v3.0.1 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [date/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/date/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] date
```

##### Integration in the project (xmake.lua)

```lua
add_requires("date")
```


### dbg-macro (cross)


| Description | *A dbg(…) macro for C++* |
| -- | -- |
| Homepage | [https://github.com/sharkdp/dbg-macro](https://github.com/sharkdp/dbg-macro) |
| License | MIT |
| Versions | v0.4.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [dbg-macro/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/dbg-macro/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] dbg-macro
```

##### Integration in the project (xmake.lua)

```lua
add_requires("dbg-macro")
```


### dbus (cross)


| Description | *D-Bus is a message bus system, a simple way for applications to talk to one another.* |
| -- | -- |
| Homepage | [https://www.freedesktop.org/wiki/Software/dbus/](https://www.freedesktop.org/wiki/Software/dbus/) |
| License | MIT |
| Versions | 1.14.2 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [dbus/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/dbus/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] dbus
```

##### Integration in the project (xmake.lua)

```lua
add_requires("dbus")
```


### debugbreak (cross)


| Description | *break into the debugger programmatically* |
| -- | -- |
| Homepage | [https://github.com/scottt/debugbreak](https://github.com/scottt/debugbreak) |
| Versions | v1.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [debugbreak/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/debugbreak/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] debugbreak
```

##### Integration in the project (xmake.lua)

```lua
add_requires("debugbreak")
```


### decimal_for_cpp (cross)


| Description | *Decimal data type support, for COBOL-like fixed-point operations on currency/money values.* |
| -- | -- |
| Homepage | [https://github.com/vpiotr/decimal_for_cpp](https://github.com/vpiotr/decimal_for_cpp) |
| License | BSD-3-Clause |
| Versions | 1.19 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [decimal_for_cpp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/decimal_for_cpp/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] decimal_for_cpp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("decimal_for_cpp")
```


### demumble (cross)


| Description | *A better c++filt and a better undname.exe, in one binary.* |
| -- | -- |
| Homepage | [https://github.com/nico/demumble](https://github.com/nico/demumble) |
| License | Apache-2.0 |
| Versions | 2022.3.23 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [demumble/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/demumble/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] demumble
```

##### Integration in the project (xmake.lua)

```lua
add_requires("demumble")
```


### docopt (cross)


| Description | *Pythonic command line arguments parser (C++11 port)* |
| -- | -- |
| Homepage | [https://github.com/docopt/docopt.cpp](https://github.com/docopt/docopt.cpp) |
| License | BSL-1.0 |
| Versions | v0.6.3 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [docopt/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/docopt/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] docopt
```

##### Integration in the project (xmake.lua)

```lua
add_requires("docopt")
```


### doctest (cross)


| Description | *The fastest feature-rich C++11/14/17/20 single-header testing framework for unit tests and TDD* |
| -- | -- |
| Homepage | [http://bit.ly/doctest-docs](http://bit.ly/doctest-docs) |
| Versions | 2.3.1, 2.3.6, 2.4.8, 2.4.9 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [doctest/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/doctest/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] doctest
```

##### Integration in the project (xmake.lua)

```lua
add_requires("doctest")
```


### doxygen (cross)


| Description | *%s* |
| -- | -- |
| Homepage | [https://www.doxygen.nl/](https://www.doxygen.nl/) |
| License | GPL-2.0 |
| Versions | 1.9.1, 1.9.2, 1.9.3 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [doxygen/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/doxygen/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] doxygen
```

##### Integration in the project (xmake.lua)

```lua
add_requires("doxygen")
```


### dr_flac (cross)


| Description | *Single file audio decoding libraries for C/C++.* |
| -- | -- |
| Homepage | [https://github.com/mackron/dr_libs](https://github.com/mackron/dr_libs) |
| License | MIT |
| Versions | 0.12.29 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [dr_flac/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/dr_flac/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] dr_flac
```

##### Integration in the project (xmake.lua)

```lua
add_requires("dr_flac")
```


### dr_mp3 (cross)


| Description | *Single file audio decoding libraries for C/C++.* |
| -- | -- |
| Homepage | [https://github.com/mackron/dr_libs](https://github.com/mackron/dr_libs) |
| License | MIT |
| Versions | 0.6.27 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [dr_mp3/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/dr_mp3/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] dr_mp3
```

##### Integration in the project (xmake.lua)

```lua
add_requires("dr_mp3")
```


### dr_wav (cross)


| Description | *Single file audio decoding libraries for C/C++.* |
| -- | -- |
| Homepage | [https://github.com/mackron/dr_libs](https://github.com/mackron/dr_libs) |
| License | MIT |
| Versions | 0.12.19 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [dr_wav/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/dr_wav/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] dr_wav
```

##### Integration in the project (xmake.lua)

```lua
add_requires("dr_wav")
```



## e
### easyloggingpp (cross)


| Description | *Single header C++ logging library.* |
| -- | -- |
| Homepage | [https://github.com/amrayn/easyloggingpp](https://github.com/amrayn/easyloggingpp) |
| License | MIT |
| Versions | v9.97.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [easyloggingpp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/e/easyloggingpp/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] easyloggingpp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("easyloggingpp")
```


### elfio (cross)


| Description | *ELFIO - ELF (Executable and Linkable Format) reader and producer implemented as a header only C++ library* |
| -- | -- |
| Homepage | [http://serge1.github.io/ELFIO](http://serge1.github.io/ELFIO) |
| License | MIT |
| Versions | 3.11 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [elfio/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/e/elfio/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] elfio
```

##### Integration in the project (xmake.lua)

```lua
add_requires("elfio")
```


### enet (cross)


| Description | *Reliable UDP networking library.* |
| -- | -- |
| Homepage | [http://enet.bespin.org](http://enet.bespin.org) |
| License | MIT |
| Versions | v1.3.17 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [enet/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/e/enet/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] enet
```

##### Integration in the project (xmake.lua)

```lua
add_requires("enet")
```


### entt (cross)


| Description | *Gaming meets modern C++ - a fast and reliable entity component system (ECS) and much more.* |
| -- | -- |
| Homepage | [https://github.com/skypjack/entt](https://github.com/skypjack/entt) |
| License | MIT |
| Versions | v3.10.0, v3.10.1, v3.10.3, v3.11.0, v3.11.1, v3.6.0, v3.7.0, v3.7.1, v3.8.0, v3.8.1, v3.9.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [entt/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/e/entt/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] entt
```

##### Integration in the project (xmake.lua)

```lua
add_requires("entt")
```


### expat (cross)


| Description | *XML 1.0 parser* |
| -- | -- |
| Homepage | [https://libexpat.github.io](https://libexpat.github.io) |
| License | MIT |
| Versions | 2.2.10, 2.2.6, 2.3.0, 2.4.1, 2.4.5, 2.4.7, 2.4.8 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [expat/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/e/expat/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] expat
```

##### Integration in the project (xmake.lua)

```lua
add_requires("expat")
```


### expected (cross)


| Description | *C++11/14/17 std::expected with functional-style extensions* |
| -- | -- |
| Homepage | [https://github.com/TartanLlama/expected](https://github.com/TartanLlama/expected) |
| License | CC0 |
| Versions | v1.0.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [expected/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/e/expected/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] expected
```

##### Integration in the project (xmake.lua)

```lua
add_requires("expected")
```


### exprtk (cross)


| Description | *C++ Mathematical Expression Parsing And Evaluation Library* |
| -- | -- |
| Homepage | [https://www.partow.net/programming/exprtk/index.html](https://www.partow.net/programming/exprtk/index.html) |
| License | MIT |
| Versions | 2021.06.06 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [exprtk/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/e/exprtk/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] exprtk
```

##### Integration in the project (xmake.lua)

```lua
add_requires("exprtk")
```



## f
### fast_float (cross)


| Description | *Fast and exact implementation of the C++ from_chars functions for float and double types: 4x faster than strtod* |
| -- | -- |
| Homepage | [https://github.com/fastfloat/fast_float](https://github.com/fastfloat/fast_float) |
| License | Apache-2.0 |
| Versions | v3.4.0, v3.5.1 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [fast_float/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/fast_float/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] fast_float
```

##### Integration in the project (xmake.lua)

```lua
add_requires("fast_float")
```


### fastcppcsvparser (cross)


| Description | *This is a small, easy-to-use and fast header-only library for reading comma separated value (CSV) files (by ben-strasser)* |
| -- | -- |
| Homepage | [https://github.com/ben-strasser/fast-cpp-csv-parser](https://github.com/ben-strasser/fast-cpp-csv-parser) |
| Versions | 2021.01.03 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [fastcppcsvparser/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/fastcppcsvparser/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] fastcppcsvparser
```

##### Integration in the project (xmake.lua)

```lua
add_requires("fastcppcsvparser")
```


### flex (cross)


| Description | *%s* |
| -- | -- |
| Homepage | [https://github.com/westes/flex/](https://github.com/westes/flex/) |
| License | BSD-2-Clause |
| Versions | 2.6.4 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [flex/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/flex/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] flex
```

##### Integration in the project (xmake.lua)

```lua
add_requires("flex")
```


### fmt (cross)


| Description | *fmt is an open-source formatting library for C++. It can be used as a safe and fast alternative to (s)printf and iostreams.* |
| -- | -- |
| Homepage | [https://fmt.dev](https://fmt.dev) |
| Versions | 5.3.0, 6.0.0, 6.2.0, 7.1.3, 8.0.0, 8.0.1, 8.1.1, 9.0.0, 9.1.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [fmt/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/fmt/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] fmt
```

##### Integration in the project (xmake.lua)

```lua
add_requires("fmt")
```


### freetype (cross)


| Description | *A freely available software library to render fonts.* |
| -- | -- |
| Homepage | [https://www.freetype.org](https://www.freetype.org) |
| Versions | 2.10.4, 2.11.0, 2.11.1, 2.12.1, 2.9.1 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [freetype/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/freetype/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] freetype
```

##### Integration in the project (xmake.lua)

```lua
add_requires("freetype")
```


### frozen (cross)


| Description | *A header-only, constexpr alternative to gperf for C++14 users* |
| -- | -- |
| Homepage | [https://github.com/serge-sans-paille/frozen](https://github.com/serge-sans-paille/frozen) |
| License | Apache-2.0 |
| Versions | 1.1.1 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [frozen/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/frozen/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] frozen
```

##### Integration in the project (xmake.lua)

```lua
add_requires("frozen")
```


### functionalplus (cross)


| Description | *Functional Programming Library for C++. Write concise and readable C++ code.* |
| -- | -- |
| Homepage | [http://www.editgym.com/fplus-api-search/](http://www.editgym.com/fplus-api-search/) |
| Versions | v0.2.18-p0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [functionalplus/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/functionalplus/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] functionalplus
```

##### Integration in the project (xmake.lua)

```lua
add_requires("functionalplus")
```


### fx-gltf (cross)


| Description | *A C++14/C++17 header-only library for simple, efficient, and robust serialization/deserialization of glTF 2.0* |
| -- | -- |
| Homepage | [https://github.com/jessey-git/fx-gltf](https://github.com/jessey-git/fx-gltf) |
| License | MIT |
| Versions | v1.2.0, v2.0.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [fx-gltf/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/fx-gltf/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] fx-gltf
```

##### Integration in the project (xmake.lua)

```lua
add_requires("fx-gltf")
```



## g
### genie (cross)


| Description | *GENie - Project generator tool* |
| -- | -- |
| Homepage | [https://github.com/bkaradzic/GENie](https://github.com/bkaradzic/GENie) |
| Versions | 1160.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [genie/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/genie/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] genie
```

##### Integration in the project (xmake.lua)

```lua
add_requires("genie")
```


### gflags (cross)


| Description | *The gflags package contains a C++ library that implements commandline flags processing.* |
| -- | -- |
| Homepage | [https://github.com/gflags/gflags/](https://github.com/gflags/gflags/) |
| License | BSD-3-Clause |
| Versions | v2.2.2 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [gflags/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/gflags/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] gflags
```

##### Integration in the project (xmake.lua)

```lua
add_requires("gflags")
```


### ghc_filesystem (cross)


| Description | *An implementation of C++17 std::filesystem for C++11 /C++14/C++17/C++20 on Windows, macOS, Linux and FreeBSD.* |
| -- | -- |
| Homepage | [https://github.com/gulrak/filesystem](https://github.com/gulrak/filesystem) |
| License | MIT |
| Versions | v1.5.10 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [ghc_filesystem/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/ghc_filesystem/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] ghc_filesystem
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ghc_filesystem")
```


### gli (cross)


| Description | *OpenGL Image (GLI)* |
| -- | -- |
| Homepage | [https://gli.g-truc.net/](https://gli.g-truc.net/) |
| Versions | 0.8.2.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [gli/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/gli/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] gli
```

##### Integration in the project (xmake.lua)

```lua
add_requires("gli")
```


### glib (cross)


| Description | *Core application library for C.* |
| -- | -- |
| Homepage | [https://developer.gnome.org/glib/](https://developer.gnome.org/glib/) |
| Versions | 2.71.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [glib/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/glib/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] glib
```

##### Integration in the project (xmake.lua)

```lua
add_requires("glib")
```


### glm (cross)


| Description | *OpenGL Mathematics (GLM)* |
| -- | -- |
| Homepage | [https://glm.g-truc.net/](https://glm.g-truc.net/) |
| Versions | 0.9.9+8 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [glm/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/glm/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] glm
```

##### Integration in the project (xmake.lua)

```lua
add_requires("glm")
```


### glog (cross)


| Description | *C++ implementation of the Google logging module* |
| -- | -- |
| Homepage | [https://github.com/google/glog/](https://github.com/google/glog/) |
| License | BSD-3-Clause |
| Versions | v0.4.0, v0.5.0, v0.6.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [glog/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/glog/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] glog
```

##### Integration in the project (xmake.lua)

```lua
add_requires("glog")
```


### gn (cross)


| Description | *GN is a meta-build system that generates build files for Ninja.* |
| -- | -- |
| Homepage | [https://gn.googlesource.com/gn](https://gn.googlesource.com/gn) |
| Versions | 20211117 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [gn/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/gn/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] gn
```

##### Integration in the project (xmake.lua)

```lua
add_requires("gn")
```


### gnu-rm (cross)


| Description | *GNU Arm Embedded Toolchain* |
| -- | -- |
| Homepage | [https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm) |
| Versions | 2020.10, 2021.10 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [gnu-rm/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/gnu-rm/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] gnu-rm
```

##### Integration in the project (xmake.lua)

```lua
add_requires("gnu-rm")
```


### gsl (cross)


| Description | *Guidelines Support Library* |
| -- | -- |
| Homepage | [https://github.com/microsoft/GSL](https://github.com/microsoft/GSL) |
| License | MIT |
| Versions | v3.1.0, v4.0.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [gsl/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/gsl/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] gsl
```

##### Integration in the project (xmake.lua)

```lua
add_requires("gsl")
```


### gtest (cross)


| Description | *Google Testing and Mocking Framework.* |
| -- | -- |
| Homepage | [https://github.com/google/googletest](https://github.com/google/googletest) |
| Versions | 1.10.0, 1.11.0, 1.12.0, 1.12.1, 1.8.1 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [gtest/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/gtest/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] gtest
```

##### Integration in the project (xmake.lua)

```lua
add_requires("gtest")
```


### guetzli (cross)


| Description | *Perceptual JPEG encoder* |
| -- | -- |
| Homepage | [https://github.com/google/guetzli](https://github.com/google/guetzli) |
| Versions | v1.0.1 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [guetzli/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/guetzli/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] guetzli
```

##### Integration in the project (xmake.lua)

```lua
add_requires("guetzli")
```


### gyp-next (cross)


| Description | *A fork of the GYP build system for use in the Node.js projects* |
| -- | -- |
| Homepage | [https://github.com/nodejs/gyp-next](https://github.com/nodejs/gyp-next) |
| License | BSD-3-Clause |
| Versions | v0.11.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [gyp-next/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/gyp-next/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] gyp-next
```

##### Integration in the project (xmake.lua)

```lua
add_requires("gyp-next")
```



## h
### happly (cross)


| Description | *A C++ header-only parser for the PLY file format.* |
| -- | -- |
| Homepage | [https://github.com/nmwsharp/happly](https://github.com/nmwsharp/happly) |
| License | MIT |
| Versions | 2022.01.07 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [happly/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/h/happly/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] happly
```

##### Integration in the project (xmake.lua)

```lua
add_requires("happly")
```


### hash-library (cross)


| Description | *Portable C++ hashing library* |
| -- | -- |
| Homepage | [https://create.stephan-brumme.com/hash-library/](https://create.stephan-brumme.com/hash-library/) |
| License | zlib |
| Versions | 2021.09.29 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [hash-library/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/h/hash-library/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] hash-library
```

##### Integration in the project (xmake.lua)

```lua
add_requires("hash-library")
```


### hiredis (cross)


| Description | *Minimalistic C client for Redis >= 1.2* |
| -- | -- |
| Homepage | [https://github.com/redis/hiredis](https://github.com/redis/hiredis) |
| License | BSD-3-Clause |
| Versions | v1.0.2 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [hiredis/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/h/hiredis/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] hiredis
```

##### Integration in the project (xmake.lua)

```lua
add_requires("hiredis")
```


### hopscotch-map (cross)


| Description | *A C++ implementation of a fast hash map and hash set using hopscotch hashing* |
| -- | -- |
| Homepage | [https://github.com/Tessil/hopscotch-map](https://github.com/Tessil/hopscotch-map) |
| Versions | v2.3.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [hopscotch-map/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/h/hopscotch-map/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] hopscotch-map
```

##### Integration in the project (xmake.lua)

```lua
add_requires("hopscotch-map")
```


### http_parser (cross)


| Description | *Parser for HTTP messages written in C.* |
| -- | -- |
| Homepage | [https://github.com/nodejs/http-parser](https://github.com/nodejs/http-parser) |
| Versions | v2.9.4 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [http_parser/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/h/http_parser/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] http_parser
```

##### Integration in the project (xmake.lua)

```lua
add_requires("http_parser")
```



## i
### ifort (cross)


| Description | *The Fortran Compiler provided by Intel®* |
| -- | -- |
| Homepage | [https://www.intel.com/content/www/us/en/developer/tools/oneapi/fortran-compiler.html](https://www.intel.com/content/www/us/en/developer/tools/oneapi/fortran-compiler.html) |
| Versions | 2021.4.0+3224 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [ifort/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/i/ifort/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] ifort
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ifort")
```


### indicators (cross)


| Description | *Activity Indicators for Modern C++* |
| -- | -- |
| Homepage | [https://github.com/p-ranav/indicators](https://github.com/p-ranav/indicators) |
| License | MIT |
| Versions | 2.2 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [indicators/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/i/indicators/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] indicators
```

##### Integration in the project (xmake.lua)

```lua
add_requires("indicators")
```


### inja (cross)


| Description | *A Template Engine for Modern C++* |
| -- | -- |
| Homepage | [https://pantor.github.io/inja/](https://pantor.github.io/inja/) |
| Versions | v2.1.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [inja/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/i/inja/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] inja
```

##### Integration in the project (xmake.lua)

```lua
add_requires("inja")
```


### ip2region (cross)


| Description | *IP address region search library.* |
| -- | -- |
| Homepage | [https://github.com/lionsoul2014/ip2region](https://github.com/lionsoul2014/ip2region) |
| License | Apache-2.0 |
| Versions | v2020.10.31 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [ip2region/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/i/ip2region/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] ip2region
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ip2region")
```


### irrxml (cross)


| Description | *High speed and easy-to-use XML Parser for C++* |
| -- | -- |
| Homepage | [https://sourceforge.net/projects/irrlicht/](https://sourceforge.net/projects/irrlicht/) |
| Versions | 1.2 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [irrxml/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/i/irrxml/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] irrxml
```

##### Integration in the project (xmake.lua)

```lua
add_requires("irrxml")
```


### ispc (cross)


| Description | *Intel® Implicit SPMD Program Compiler* |
| -- | -- |
| Homepage | [https://ispc.github.io/](https://ispc.github.io/) |
| License | BSD-3-Clause |
| Versions | 1.17.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [ispc/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/i/ispc/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] ispc
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ispc")
```



## j
### jsmn (cross)


| Description | *Jsmn is a world fastest JSON parser/tokenizer* |
| -- | -- |
| Homepage | [https://github.com/zserge/jsmn](https://github.com/zserge/jsmn) |
| Versions | v1.1.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [jsmn/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/j/jsmn/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] jsmn
```

##### Integration in the project (xmake.lua)

```lua
add_requires("jsmn")
```


### json-schema-validator (cross)


| Description | *JSON schema validator for JSON for Modern C++* |
| -- | -- |
| Homepage | [https://github.com/pboettch/json-schema-validator](https://github.com/pboettch/json-schema-validator) |
| Versions | 2.1.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [json-schema-validator/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/j/json-schema-validator/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] json-schema-validator
```

##### Integration in the project (xmake.lua)

```lua
add_requires("json-schema-validator")
```


### json.h (cross)


| Description | *single header json parser for C and C++* |
| -- | -- |
| Homepage | [https://github.com/sheredom/json.h](https://github.com/sheredom/json.h) |
| Versions | 2022.11.27 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [json.h/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/j/json.h/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] json.h
```

##### Integration in the project (xmake.lua)

```lua
add_requires("json.h")
```


### jsoncons (cross)


| Description | *A C++, header-only library for constructing JSON and JSON-like data formats, with JSON Pointer, JSON Patch, JSONPath, JMESPath, CSV, MessagePack, CBOR, BSON, UBJSON* |
| -- | -- |
| Homepage | [https://danielaparker.github.io/jsoncons/](https://danielaparker.github.io/jsoncons/) |
| Versions | v0.158.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [jsoncons/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/j/jsoncons/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] jsoncons
```

##### Integration in the project (xmake.lua)

```lua
add_requires("jsoncons")
```


### jsoncpp (cross)


| Description | *A C++ library for interacting with JSON.* |
| -- | -- |
| Homepage | [https://github.com/open-source-parsers/jsoncpp/wiki](https://github.com/open-source-parsers/jsoncpp/wiki) |
| Versions | 1.9.4, 1.9.5 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [jsoncpp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/j/jsoncpp/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] jsoncpp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("jsoncpp")
```



## k
### kcp (cross)


| Description | *A Fast and Reliable ARQ Protocol.* |
| -- | -- |
| Homepage | [https://github.com/skywind3000/kcp](https://github.com/skywind3000/kcp) |
| Versions | 1.7 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [kcp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/k/kcp/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] kcp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("kcp")
```


### kiwisolver (cross)


| Description | *Efficient C++ implementation of the Cassowary constraint solving algorithm* |
| -- | -- |
| Homepage | [https://kiwisolver.readthedocs.io/en/latest/](https://kiwisolver.readthedocs.io/en/latest/) |
| Versions | 1.3.1, 1.3.2, 1.4.4 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [kiwisolver/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/k/kiwisolver/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] kiwisolver
```

##### Integration in the project (xmake.lua)

```lua
add_requires("kiwisolver")
```



## l
### lexy (cross)


| Description | *C++ parsing DSL* |
| -- | -- |
| Homepage | [https://lexy.foonathan.net](https://lexy.foonathan.net) |
| Versions | 2022.03.21 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [lexy/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/lexy/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] lexy
```

##### Integration in the project (xmake.lua)

```lua
add_requires("lexy")
```


### libcurl (cross)


| Description | *The multiprotocol file transfer library.* |
| -- | -- |
| Homepage | [https://curl.haxx.se/](https://curl.haxx.se/) |
| License | MIT |
| Versions | 7.31.0, 7.32.0, 7.33.0, 7.34.0, 7.35.0, 7.36.0, 7.37.1, 7.38.0, 7.39.0, 7.40.0, 7.41.0, 7.42.1, 7.43.0, 7.44.0, 7.45.0, 7.46.0, 7.47.1, 7.48.0, 7.49.1, 7.50.3, 7.51.0, 7.52.1, 7.53.1, 7.54.1, 7.55.1, 7.56.1, 7.57.0, 7.58.0, 7.59.0, 7.60.0, 7.61.0, 7.61.1, 7.62.0, 7.63.0, 7.64.0, 7.64.1, 7.65.3, 7.66.0, 7.67.0, 7.68.0, 7.69.1, 7.70.0, 7.71.1, 7.72.0, 7.73.0, 7.74.0, 7.75.0, 7.76.1, 7.77.0, 7.78.0, 7.80.0, 7.81.0, 7.82.0, 7.84.0, 7.85.0, 7.86.0, 7.87.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [libcurl/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libcurl/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] libcurl
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libcurl")
```


### libdivide (cross)


| Description | *Official git repository for libdivide: optimized integer division* |
| -- | -- |
| Homepage | [http://libdivide.com](http://libdivide.com) |
| Versions | 5.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [libdivide/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libdivide/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] libdivide
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libdivide")
```


### libdivsufsort (cross)


| Description | *A lightweight suffix array sorting library* |
| -- | -- |
| Homepage | [https://android.googlesource.com/platform/external/libdivsufsort/](https://android.googlesource.com/platform/external/libdivsufsort/) |
| Versions | 2021.2.18 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [libdivsufsort/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libdivsufsort/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] libdivsufsort
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libdivsufsort")
```


### libffi (cross)


| Description | *Portable Foreign Function Interface library.* |
| -- | -- |
| Homepage | [https://sourceware.org/libffi/](https://sourceware.org/libffi/) |
| Versions | 3.2.1, 3.3, 3.4.2 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [libffi/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libffi/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] libffi
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libffi")
```


### libiconv (cross)


| Description | *Character set conversion library.* |
| -- | -- |
| Homepage | [https://www.gnu.org/software/libiconv](https://www.gnu.org/software/libiconv) |
| Versions | 1.15, 1.16, 1.17 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [libiconv/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libiconv/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] libiconv
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libiconv")
```


### libjpeg (cross)


| Description | *A widely used C library for reading and writing JPEG image files.* |
| -- | -- |
| Homepage | [http://ijg.org/](http://ijg.org/) |
| Versions | v9b, v9c, v9d, v9e |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [libjpeg/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libjpeg/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] libjpeg
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libjpeg")
```


### libogg (cross)


| Description | *Ogg Bitstream Library* |
| -- | -- |
| Homepage | [https://www.xiph.org/ogg/](https://www.xiph.org/ogg/) |
| Versions | v1.3.4 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [libogg/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libogg/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] libogg
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libogg")
```


### libomp (cross)


| Description | *LLVM's OpenMP runtime library.* |
| -- | -- |
| Homepage | [https://openmp.llvm.org/](https://openmp.llvm.org/) |
| Versions | 10.0.1, 11.1.0, 12.0.1 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [libomp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libomp/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] libomp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libomp")
```


### libplist (cross)


| Description | *Library for Apple Binary- and XML-Property Lists* |
| -- | -- |
| Homepage | [https://www.libimobiledevice.org/](https://www.libimobiledevice.org/) |
| License | LGPL-2.1 |
| Versions | 2.2.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [libplist/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libplist/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] libplist
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libplist")
```


### libpng (cross)


| Description | *The official PNG reference library* |
| -- | -- |
| Homepage | [http://www.libpng.org/pub/png/libpng.html](http://www.libpng.org/pub/png/libpng.html) |
| License | libpng-2.0 |
| Versions | v1.6.34, v1.6.35, v1.6.36, v1.6.37 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [libpng/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libpng/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] libpng
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libpng")
```


### librdkafka (cross)


| Description | *The Apache Kafka C/C++ library* |
| -- | -- |
| Homepage | [https://github.com/edenhill/librdkafka](https://github.com/edenhill/librdkafka) |
| Versions | v1.6.2, v1.8.2-POST2 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [librdkafka/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/librdkafka/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] librdkafka
```

##### Integration in the project (xmake.lua)

```lua
add_requires("librdkafka")
```


### libsais (cross)


| Description | *libsais is a library for linear time suffix array, longest common prefix array and burrows wheeler transform construction based on induced sorting algorithm.* |
| -- | -- |
| Homepage | [https://github.com/IlyaGrebnov/libsais](https://github.com/IlyaGrebnov/libsais) |
| License | Apache-2.0 |
| Versions | v2.7.1 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [libsais/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libsais/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] libsais
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libsais")
```


### libsdl (cross)


| Description | *Simple DirectMedia Layer* |
| -- | -- |
| Homepage | [https://www.libsdl.org/](https://www.libsdl.org/) |
| License | zlib |
| Versions | 2.0.12, 2.0.14, 2.0.16, 2.0.18, 2.0.20, 2.0.22, 2.0.8, 2.24.0, 2.24.2, 2.26.0, 2.26.1, 2.26.2 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [libsdl/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libsdl/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] libsdl
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libsdl")
```


### libsoundio (cross)


| Description | *C library for cross-platform real-time audio input and output.* |
| -- | -- |
| Homepage | [http://libsound.io/](http://libsound.io/) |
| License | MIT |
| Versions | 2.0.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [libsoundio/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libsoundio/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] libsoundio
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libsoundio")
```


### libspng (cross)


| Description | *Simple, modern libpng alternative* |
| -- | -- |
| Homepage | [https://libspng.org](https://libspng.org) |
| Versions | v0.7.1 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [libspng/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libspng/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] libspng
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libspng")
```


### libsv (cross)


| Description | *libsv - Public domain cross-platform semantic versioning in c99* |
| -- | -- |
| Homepage | [https://github.com/uael/sv](https://github.com/uael/sv) |
| Versions | 2021.11.27 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [libsv/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libsv/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] libsv
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libsv")
```


### libsvm (cross)


| Description | *A simple, easy-to-use, and efficient software for SVM classification and regression* |
| -- | -- |
| Homepage | [https://github.com/cjlin1/libsvm](https://github.com/cjlin1/libsvm) |
| Versions | v325 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [libsvm/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libsvm/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] libsvm
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libsvm")
```


### libtool (cross)


| Description | *A generic library support script.* |
| -- | -- |
| Homepage | [https://www.gnu.org/software/libtool/](https://www.gnu.org/software/libtool/) |
| Versions | 2.4.5, 2.4.6 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [libtool/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libtool/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] libtool
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libtool")
```


### libunwind (cross)


| Description | *A portable and efficient C programming interface (API) to determine the call-chain of a program.* |
| -- | -- |
| Homepage | [https://www.nongnu.org/libunwind/](https://www.nongnu.org/libunwind/) |
| Versions | v1.5, v1.6.2 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [libunwind/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libunwind/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] libunwind
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libunwind")
```


### linux-headers (cross)


| Description | *Header files of the Linux kernel* |
| -- | -- |
| Homepage | [https://kernel.org/](https://kernel.org/) |
| License | GPL-2.0-only |
| Versions | 4.20.9, 5.0.8, 5.10.46, 5.15, 5.16.9, 5.9.16 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [linux-headers/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/linux-headers/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] linux-headers
```

##### Integration in the project (xmake.lua)

```lua
add_requires("linux-headers")
```


### littlefs (cross)


| Description | *A little fail-safe filesystem designed for microcontrollers* |
| -- | -- |
| Homepage | [https://github.com/littlefs-project/littlefs](https://github.com/littlefs-project/littlefs) |
| Versions | v2.5.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [littlefs/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/littlefs/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] littlefs
```

##### Integration in the project (xmake.lua)

```lua
add_requires("littlefs")
```


### llhttp (cross)


| Description | *Port of http_parser to llparse* |
| -- | -- |
| Homepage | [https://github.com/nodejs/llhttp](https://github.com/nodejs/llhttp) |
| License | MIT |
| Versions | v3.0.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [llhttp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/llhttp/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] llhttp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("llhttp")
```


### llvm (cross)


| Description | *The LLVM Compiler Infrastructure* |
| -- | -- |
| Homepage | [https://llvm.org/](https://llvm.org/) |
| Versions | 11.0.0, 14.0.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [llvm/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/llvm/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] llvm
```

##### Integration in the project (xmake.lua)

```lua
add_requires("llvm")
```


### llvm-mingw (cross)


| Description | *An LLVM/Clang/LLD based mingw-w64 toolchain* |
| -- | -- |
| Homepage | [https://github.com/mstorsjo/llvm-mingw](https://github.com/mstorsjo/llvm-mingw) |
| Versions | 20211002, 20220323 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [llvm-mingw/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/llvm-mingw/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] llvm-mingw
```

##### Integration in the project (xmake.lua)

```lua
add_requires("llvm-mingw")
```


### lodepng (cross)


| Description | *PNG encoder and decoder in C and C++.* |
| -- | -- |
| Homepage | [https://lodev.org/lodepng/](https://lodev.org/lodepng/) |
| License | zlib |
| Versions |  |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [lodepng/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/lodepng/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] lodepng
```

##### Integration in the project (xmake.lua)

```lua
add_requires("lodepng")
```


### loguru (cross)


| Description | *A lightweight C++ logging library* |
| -- | -- |
| Homepage | [https://github.com/emilk/loguru](https://github.com/emilk/loguru) |
| Versions | v2.1.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [loguru/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/loguru/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] loguru
```

##### Integration in the project (xmake.lua)

```lua
add_requires("loguru")
```


### lua (cross)


| Description | *A powerful, efficient, lightweight, embeddable scripting language.* |
| -- | -- |
| Homepage | [http://lua.org](http://lua.org) |
| Versions | v5.1.1, v5.1.5, v5.2.3, v5.3.6, v5.4.1, v5.4.2, v5.4.3, v5.4.4 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [lua/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/lua/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] lua
```

##### Integration in the project (xmake.lua)

```lua
add_requires("lua")
```


### lua-format (cross)


| Description | *Code formatter for Lua* |
| -- | -- |
| Homepage | [https://github.com/Koihik/LuaFormatter](https://github.com/Koihik/LuaFormatter) |
| Versions | 1.3.5 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [lua-format/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/lua-format/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] lua-format
```

##### Integration in the project (xmake.lua)

```lua
add_requires("lua-format")
```


### lvgl (cross)


| Description | *Light and Versatile Graphics Library* |
| -- | -- |
| Homepage | [https://lvgl.io](https://lvgl.io) |
| License | MIT |
| Versions | v8.0.2, v8.2.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [lvgl/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/lvgl/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] lvgl
```

##### Integration in the project (xmake.lua)

```lua
add_requires("lvgl")
```


### lyra (cross)


| Description | *A simple to use, composable, command line parser for C++ 11 and beyond* |
| -- | -- |
| Homepage | [https://www.bfgroup.xyz/Lyra/](https://www.bfgroup.xyz/Lyra/) |
| License | BSL-1.0 |
| Versions | 1.5.1, 1.6 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [lyra/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/lyra/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] lyra
```

##### Integration in the project (xmake.lua)

```lua
add_requires("lyra")
```


### lz4 (cross)


| Description | *LZ4 - Extremely fast compression* |
| -- | -- |
| Homepage | [https://www.lz4.org/](https://www.lz4.org/) |
| Versions | v1.9.3 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [lz4/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/lz4/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] lz4
```

##### Integration in the project (xmake.lua)

```lua
add_requires("lz4")
```


### lzo (cross)


| Description | *LZO is a portable lossless data compression library written in ANSI C.* |
| -- | -- |
| Homepage | [http://www.oberhumer.com/opensource/lzo](http://www.oberhumer.com/opensource/lzo) |
| License | GPL-2.0 |
| Versions | 2.10 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [lzo/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/lzo/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] lzo
```

##### Integration in the project (xmake.lua)

```lua
add_requires("lzo")
```



## m
### m4 (cross)


| Description | *Macro processing language* |
| -- | -- |
| Homepage | [https://www.gnu.org/software/m4](https://www.gnu.org/software/m4) |
| Versions | 1.4.18, 1.4.19 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [m4/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/m4/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] m4
```

##### Integration in the project (xmake.lua)

```lua
add_requires("m4")
```


### magic_enum (cross)


| Description | *Static reflection for enums (to string, from string, iteration) for modern C++, work with any enum type without any macro or boilerplate code* |
| -- | -- |
| Homepage | [https://github.com/Neargye/magic_enum](https://github.com/Neargye/magic_enum) |
| License | MIT |
| Versions | v0.7.3, v0.8.0, v0.8.1 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [magic_enum/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/magic_enum/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] magic_enum
```

##### Integration in the project (xmake.lua)

```lua
add_requires("magic_enum")
```


### make (cross)


| Description | *GNU make tool.* |
| -- | -- |
| Homepage | [https://www.gnu.org/software/make/](https://www.gnu.org/software/make/) |
| Versions | 4.2.1, 4.3 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [make/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/make/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] make
```

##### Integration in the project (xmake.lua)

```lua
add_requires("make")
```


### mapbox_earcut (cross)


| Description | *A C++ port of earcut.js, a fast, header-only polygon triangulation library.* |
| -- | -- |
| Homepage | [https://github.com/mapbox/earcut.hpp](https://github.com/mapbox/earcut.hpp) |
| License | ISC |
| Versions | 2.2.3 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [mapbox_earcut/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/mapbox_earcut/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] mapbox_earcut
```

##### Integration in the project (xmake.lua)

```lua
add_requires("mapbox_earcut")
```


### mapbox_eternal (cross)


| Description | *A C++14 compile-time/constexpr map and hash map with minimal binary footprint* |
| -- | -- |
| Homepage | [https://github.com/mapbox/eternal](https://github.com/mapbox/eternal) |
| License | ISC |
| Versions | v1.0.1 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [mapbox_eternal/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/mapbox_eternal/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] mapbox_eternal
```

##### Integration in the project (xmake.lua)

```lua
add_requires("mapbox_eternal")
```


### mapbox_geometry (cross)


| Description | *Provides header-only, generic C++ interfaces for geometry types, geometry collections, and features.* |
| -- | -- |
| Homepage | [https://github.com/mapbox/geometry.hpp](https://github.com/mapbox/geometry.hpp) |
| License | ISC |
| Versions | 1.1.0, 2.0.3 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [mapbox_geometry/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/mapbox_geometry/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] mapbox_geometry
```

##### Integration in the project (xmake.lua)

```lua
add_requires("mapbox_geometry")
```


### mapbox_variant (cross)


| Description | *C++11/C++14 Variant* |
| -- | -- |
| Homepage | [https://github.com/mapbox/variant](https://github.com/mapbox/variant) |
| License | BSD |
| Versions | v1.2.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [mapbox_variant/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/mapbox_variant/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] mapbox_variant
```

##### Integration in the project (xmake.lua)

```lua
add_requires("mapbox_variant")
```


### mathfu (cross)


| Description | *C++ math library developed primarily for games focused on simplicity and efficiency.* |
| -- | -- |
| Homepage | [http://google.github.io/mathfu](http://google.github.io/mathfu) |
| License | Apache-2.0 |
| Versions | 2022.5.10 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [mathfu/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/mathfu/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] mathfu
```

##### Integration in the project (xmake.lua)

```lua
add_requires("mathfu")
```


### mbedtls (cross)


| Description | *An SSL library* |
| -- | -- |
| Homepage | [https://tls.mbed.org](https://tls.mbed.org) |
| Versions | 2.13.0, 2.25.0, 2.7.6 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [mbedtls/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/mbedtls/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] mbedtls
```

##### Integration in the project (xmake.lua)

```lua
add_requires("mbedtls")
```


### meson (cross)


| Description | *Fast and user friendly build system.* |
| -- | -- |
| Homepage | [https://mesonbuild.com/](https://mesonbuild.com/) |
| License | Apache-2.0 |
| Versions | 0.50.1, 0.56.0, 0.58.0, 0.58.1, 0.59.1, 0.59.2, 0.60.1, 0.61.2, 0.62.1 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [meson/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/meson/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] meson
```

##### Integration in the project (xmake.lua)

```lua
add_requires("meson")
```


### microsoft-gsl (cross)


| Description | *Guidelines Support Library* |
| -- | -- |
| Homepage | [https://github.com/microsoft/GSL](https://github.com/microsoft/GSL) |
| License | MIT |
| Versions | v3.1.0, v4.0.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [microsoft-gsl/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/microsoft-gsl/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] microsoft-gsl
```

##### Integration in the project (xmake.lua)

```lua
add_requires("microsoft-gsl")
```


### mikktspace (cross)


| Description | *A common standard for tangent space used in baking tools to produce normal maps.* |
| -- | -- |
| Homepage | [http://www.mikktspace.com/](http://www.mikktspace.com/) |
| Versions | 2020.03.26 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [mikktspace/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/mikktspace/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] mikktspace
```

##### Integration in the project (xmake.lua)

```lua
add_requires("mikktspace")
```


### miniaudio (cross)


| Description | *Single file audio playback and capture library written in C.* |
| -- | -- |
| Homepage | [https://miniaud.io](https://miniaud.io) |
| Versions | 2021.12.31 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [miniaudio/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/miniaudio/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] miniaudio
```

##### Integration in the project (xmake.lua)

```lua
add_requires("miniaudio")
```


### minilzo (cross)


| Description | *A very lightweight subset of the LZO library intended for easy inclusion with your application* |
| -- | -- |
| Homepage | [http://www.oberhumer.com/opensource/lzo/#minilzo](http://www.oberhumer.com/opensource/lzo/#minilzo) |
| Versions | 2.10 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [minilzo/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/minilzo/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] minilzo
```

##### Integration in the project (xmake.lua)

```lua
add_requires("minilzo")
```


### minimp3 (cross)


| Description | *Minimalistic MP3 decoder single header library* |
| -- | -- |
| Homepage | [https://github.com/lieff/minimp3](https://github.com/lieff/minimp3) |
| License | CC0 |
| Versions | 2021.05.29 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [minimp3/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/minimp3/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] minimp3
```

##### Integration in the project (xmake.lua)

```lua
add_requires("minimp3")
```


### miniz (cross)


| Description | *miniz: Single C source file zlib-replacement library* |
| -- | -- |
| Homepage | [https://github.com/richgel999/miniz/](https://github.com/richgel999/miniz/) |
| License | MIT |
| Versions | 2.1.0, 2.2.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [miniz/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/miniz/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] miniz
```

##### Integration in the project (xmake.lua)

```lua
add_requires("miniz")
```


### minizip (cross)


| Description | *Mini zip and unzip based on zlib* |
| -- | -- |
| Homepage | [https://www.zlib.net/](https://www.zlib.net/) |
| License | zlib |
| Versions | v1.2.10, v1.2.11, v1.2.12 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [minizip/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/minizip/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] minizip
```

##### Integration in the project (xmake.lua)

```lua
add_requires("minizip")
```


### mjson (cross)


| Description | *C/C++ JSON parser, emitter, JSON-RPC engine for embedded systems* |
| -- | -- |
| Homepage | [https://github.com/cesanta/mjson](https://github.com/cesanta/mjson) |
| License | MIT |
| Versions | 1.2.6 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [mjson/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/mjson/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] mjson
```

##### Integration in the project (xmake.lua)

```lua
add_requires("mjson")
```


### mma (cross)


| Description | *A self-contained C++ implementation of MMA and GCMMA.* |
| -- | -- |
| Homepage | [https://github.com/jdumas/mma](https://github.com/jdumas/mma) |
| License | MIT |
| Versions | 2018.08.01 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [mma/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/mma/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] mma
```

##### Integration in the project (xmake.lua)

```lua
add_requires("mma")
```


### mpmcqueue (cross)


| Description | *A bounded multi-producer multi-consumer concurrent queue written in C++11* |
| -- | -- |
| Homepage | [https://github.com/rigtorp/MPMCQueue](https://github.com/rigtorp/MPMCQueue) |
| Versions | v1.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [mpmcqueue/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/mpmcqueue/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] mpmcqueue
```

##### Integration in the project (xmake.lua)

```lua
add_requires("mpmcqueue")
```


### muslcc (cross)


| Description | *static cross- and native- musl-based toolchains.* |
| -- | -- |
| Homepage | [https://musl.cc/](https://musl.cc/) |
| Versions | 20210202 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [muslcc/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/muslcc/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] muslcc
```

##### Integration in the project (xmake.lua)

```lua
add_requires("muslcc")
```



## n
### named_type (cross)


| Description | *Implementation of strong types in C++.* |
| -- | -- |
| Homepage | [https://github.com/joboccara/NamedType](https://github.com/joboccara/NamedType) |
| License | MIT |
| Versions | v1.1.0.20210209 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [named_type/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/named_type/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] named_type
```

##### Integration in the project (xmake.lua)

```lua
add_requires("named_type")
```


### nanoflann (cross)


| Description | *nanoflann: a C++11 header-only library for Nearest Neighbor (NN) search with KD-trees* |
| -- | -- |
| Homepage | [https://github.com/jlblancoc/nanoflann/](https://github.com/jlblancoc/nanoflann/) |
| License | BSD-2-Clause |
| Versions | v1.3.2, v1.4.2 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [nanoflann/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/nanoflann/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] nanoflann
```

##### Integration in the project (xmake.lua)

```lua
add_requires("nanoflann")
```


### nanosvg (cross)


| Description | *Simple stupid SVG parser* |
| -- | -- |
| Homepage | [https://github.com/memononen/nanosvg](https://github.com/memononen/nanosvg) |
| License | zlib |
| Versions | 2022.07.09 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [nanosvg/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/nanosvg/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] nanosvg
```

##### Integration in the project (xmake.lua)

```lua
add_requires("nanosvg")
```


### nasm (cross)


| Description | *Netwide Assembler (NASM) is an 80x86 assembler.* |
| -- | -- |
| Homepage | [https://www.nasm.us/](https://www.nasm.us/) |
| License | BSD-2-Clause |
| Versions | 2.13.03, 2.15.05 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [nasm/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/nasm/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] nasm
```

##### Integration in the project (xmake.lua)

```lua
add_requires("nasm")
```


### ndk (cross)


| Description | *Android NDK toolchain.* |
| -- | -- |
| Homepage | [https://developer.android.com/ndk](https://developer.android.com/ndk) |
| Versions | 21.0, 22.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [ndk/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/ndk/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] ndk
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ndk")
```


### niftiheader (cross)


| Description | *Header structure descriptions for the nifti1 and nifti2 file formats.* |
| -- | -- |
| Homepage | [https://nifti.nimh.nih.gov/](https://nifti.nimh.nih.gov/) |
| License | Public Domain |
| Versions | 0.0.1 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [niftiheader/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/niftiheader/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] niftiheader
```

##### Integration in the project (xmake.lua)

```lua
add_requires("niftiheader")
```


### ninja (cross)


| Description | *Small build system for use with gyp or CMake.* |
| -- | -- |
| Homepage | [https://ninja-build.org/](https://ninja-build.org/) |
| Versions | 1.10.1, 1.10.2, 1.11.0, 1.11.1, 1.9.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [ninja/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/ninja/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] ninja
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ninja")
```


### nlohmann_json (cross)


| Description | *JSON for Modern C++* |
| -- | -- |
| Homepage | [https://nlohmann.github.io/json/](https://nlohmann.github.io/json/) |
| License | MIT |
| Versions | v3.10.0, v3.10.5, v3.11.2, v3.9.1 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [nlohmann_json/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/nlohmann_json/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] nlohmann_json
```

##### Integration in the project (xmake.lua)

```lua
add_requires("nlohmann_json")
```


### nod (cross)


| Description | *Small, header only signals and slots C++11 library.* |
| -- | -- |
| Homepage | [https://github.com/fr00b0/nod](https://github.com/fr00b0/nod) |
| License | MIT |
| Versions | v0.5.4 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [nod/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/nod/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] nod
```

##### Integration in the project (xmake.lua)

```lua
add_requires("nod")
```


### nodesoup (cross)


| Description | *Force-directed graph layout with Fruchterman-Reingold* |
| -- | -- |
| Homepage | [https://github.com/olvb/nodesoup](https://github.com/olvb/nodesoup) |
| Versions | 2020.09.05 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [nodesoup/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/nodesoup/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] nodesoup
```

##### Integration in the project (xmake.lua)

```lua
add_requires("nodesoup")
```


### ntkernel-error-category (cross)


| Description | *A C++ 11 std::error_category for the NT kernel's NTSTATUS error codes * |
| -- | -- |
| Homepage | [https://github.com/ned14/ntkernel-error-category](https://github.com/ned14/ntkernel-error-category) |
| License | Apache-2.0 |
| Versions | v1.0.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [ntkernel-error-category/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/ntkernel-error-category/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] ntkernel-error-category
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ntkernel-error-category")
```



## o
### olive.c (cross)


| Description | *Simple 2D Graphics Library for C* |
| -- | -- |
| Homepage | [https://tsoding.github.io/olive.c/](https://tsoding.github.io/olive.c/) |
| License | MIT |
| Versions | 2022.12.14 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [olive.c/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/olive.c/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] olive.c
```

##### Integration in the project (xmake.lua)

```lua
add_requires("olive.c")
```


### openal-soft (cross)


| Description | *OpenAL Soft is a software implementation of the OpenAL 3D audio API.* |
| -- | -- |
| Homepage | [https://openal-soft.org](https://openal-soft.org) |
| License | LGPL-2.0 |
| Versions | 1.21.1, 1.22.0, 1.22.2 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [openal-soft/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/openal-soft/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] openal-soft
```

##### Integration in the project (xmake.lua)

```lua
add_requires("openal-soft")
```


### opencl-clhpp (cross)


| Description | *OpenCL API C++ bindings* |
| -- | -- |
| Homepage | [https://github.com/KhronosGroup/OpenCL-CLHPP/](https://github.com/KhronosGroup/OpenCL-CLHPP/) |
| License | Apache-2.0 |
| Versions | 1.2.8, 2.0.15 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [opencl-clhpp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/opencl-clhpp/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] opencl-clhpp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("opencl-clhpp")
```


### opencl-headers (cross)


| Description | *Khronos OpenCL-Headers* |
| -- | -- |
| Homepage | [https://github.com/KhronosGroup/OpenCL-Headers/](https://github.com/KhronosGroup/OpenCL-Headers/) |
| License | Apache-2.0 |
| Versions | v2021.06.30 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [opencl-headers/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/opencl-headers/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] opencl-headers
```

##### Integration in the project (xmake.lua)

```lua
add_requires("opencl-headers")
```


### openssl (cross)


| Description | *A robust, commercial-grade, and full-featured toolkit for TLS and SSL.* |
| -- | -- |
| Homepage | [https://www.openssl.org/](https://www.openssl.org/) |
| Versions | 1.0.0, 1.0.2-u, 1.1.0-l, 1.1.1-h, 1.1.1-k, 1.1.1-l, 1.1.1-m, 1.1.1-n, 1.1.1-o, 1.1.1-p, 1.1.1-q, 1.1.1-r, 1.1.1-s |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [openssl/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/openssl/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] openssl
```

##### Integration in the project (xmake.lua)

```lua
add_requires("openssl")
```


### openssl3 (cross)


| Description | *A robust, commercial-grade, and full-featured toolkit for TLS and SSL.* |
| -- | -- |
| Homepage | [https://www.openssl.org/](https://www.openssl.org/) |
| Versions | 3.0.0, 3.0.1, 3.0.2, 3.0.3, 3.0.4, 3.0.5, 3.0.6, 3.0.7 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [openssl3/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/openssl3/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] openssl3
```

##### Integration in the project (xmake.lua)

```lua
add_requires("openssl3")
```


### ordered_map (cross)


| Description | *C++ hash map and hash set which preserve the order of insertion* |
| -- | -- |
| Homepage | [https://github.com/Tessil/ordered-map](https://github.com/Tessil/ordered-map) |
| License | MIT |
| Versions | v1.0.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [ordered_map/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/ordered_map/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] ordered_map
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ordered_map")
```


### out_ptr (cross)


| Description | *Repository for a C++11 implementation of std::out_ptr (p1132), as a standalone library!* |
| -- | -- |
| Homepage | [https://github.com/soasis/out_ptr](https://github.com/soasis/out_ptr) |
| License | Apache-2.0 |
| Versions | 2022.10.07 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [out_ptr/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/out_ptr/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] out_ptr
```

##### Integration in the project (xmake.lua)

```lua
add_requires("out_ptr")
```


### outcome (cross)


| Description | *Provides very lightweight outcome<T> and result<T> (non-Boost edition)* |
| -- | -- |
| Homepage | [https://github.com/ned14/outcome](https://github.com/ned14/outcome) |
| License | Apache-2.0 |
| Versions | v2.2.4 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [outcome/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/outcome/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] outcome
```

##### Integration in the project (xmake.lua)

```lua
add_requires("outcome")
```



## p
### parallel-hashmap (cross)


| Description | *A family of header-only, very fast and memory-friendly hashmap and btree containers.* |
| -- | -- |
| Homepage | [https://greg7mdp.github.io/parallel-hashmap/](https://greg7mdp.github.io/parallel-hashmap/) |
| License | Apache-2.0 |
| Versions | 1.33, 1.34, 1.35 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [parallel-hashmap/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/parallel-hashmap/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] parallel-hashmap
```

##### Integration in the project (xmake.lua)

```lua
add_requires("parallel-hashmap")
```


### patch (cross)


| Description | *GNU patch, which applies diff files to original files.* |
| -- | -- |
| Homepage | [http://www.gnu.org/software/patch/patch.html](http://www.gnu.org/software/patch/patch.html) |
| Versions | 2.7.6 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [patch/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/patch/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] patch
```

##### Integration in the project (xmake.lua)

```lua
add_requires("patch")
```


### pcre (cross)


| Description | *A Perl Compatible Regular Expressions Library* |
| -- | -- |
| Homepage | [https://www.pcre.org/](https://www.pcre.org/) |
| Versions | 8.45 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [pcre/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/pcre/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] pcre
```

##### Integration in the project (xmake.lua)

```lua
add_requires("pcre")
```


### pcre2 (cross)


| Description | *A Perl Compatible Regular Expressions Library* |
| -- | -- |
| Homepage | [https://www.pcre.org/](https://www.pcre.org/) |
| Versions | 10.39, 10.40 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [pcre2/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/pcre2/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] pcre2
```

##### Integration in the project (xmake.lua)

```lua
add_requires("pcre2")
```


### picojson (cross)


| Description | *A header-file-only, JSON parser serializer in C++* |
| -- | -- |
| Homepage | [https://pocoproject.org/](https://pocoproject.org/) |
| License | BSD-2-Clause |
| Versions | v1.3.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [picojson/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/picojson/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] picojson
```

##### Integration in the project (xmake.lua)

```lua
add_requires("picojson")
```


### pkg-config (cross)


| Description | *A helper tool used when compiling applications and libraries.* |
| -- | -- |
| Homepage | [https://freedesktop.org/wiki/Software/pkg-config/](https://freedesktop.org/wiki/Software/pkg-config/) |
| Versions | 0.29.2 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [pkg-config/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/pkg-config/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] pkg-config
```

##### Integration in the project (xmake.lua)

```lua
add_requires("pkg-config")
```


### pkgconf (cross)


| Description | *A program which helps to configure compiler and linker flags for development frameworks.* |
| -- | -- |
| Homepage | [http://pkgconf.org](http://pkgconf.org) |
| Versions | 1.7.4, 1.8.0, 1.9.3 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [pkgconf/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/pkgconf/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] pkgconf
```

##### Integration in the project (xmake.lua)

```lua
add_requires("pkgconf")
```


### pprint (cross)


| Description | *Pretty Printer for Modern C++* |
| -- | -- |
| Homepage | [https://github.com/p-ranav/pprint](https://github.com/p-ranav/pprint) |
| Versions | 2020.2.20 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [pprint/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/pprint/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] pprint
```

##### Integration in the project (xmake.lua)

```lua
add_requires("pprint")
```


### pqp (cross)


| Description | *A Proximity Query Package* |
| -- | -- |
| Homepage | [http://gamma.cs.unc.edu/SSV/](http://gamma.cs.unc.edu/SSV/) |
| Versions | 1.3 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [pqp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/pqp/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] pqp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("pqp")
```


### premake5 (cross)


| Description | *Premake - Powerfully simple build configuration* |
| -- | -- |
| Homepage | [https://premake.github.io/](https://premake.github.io/) |
| Versions | 2022.11.17 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [premake5/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/premake5/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] premake5
```

##### Integration in the project (xmake.lua)

```lua
add_requires("premake5")
```


### protoc (cross)


| Description | *Google's data interchange format compiler* |
| -- | -- |
| Homepage | [https://developers.google.com/protocol-buffers/](https://developers.google.com/protocol-buffers/) |
| Versions | 3.8.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [protoc/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/protoc/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] protoc
```

##### Integration in the project (xmake.lua)

```lua
add_requires("protoc")
```


### prvhash (cross)


| Description | *PRVHASH - Pseudo-Random-Value Hash* |
| -- | -- |
| Homepage | [https://github.com/avaneev/prvhash](https://github.com/avaneev/prvhash) |
| License | MIT |
| Versions | 4.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [prvhash/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/prvhash/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] prvhash
```

##### Integration in the project (xmake.lua)

```lua
add_requires("prvhash")
```


### pystring (cross)


| Description | *Pystring is a collection of C++ functions which match the interface and behavior of python's string class methods using std::string.* |
| -- | -- |
| Homepage | [https://github.com/imageworks/pystring](https://github.com/imageworks/pystring) |
| Versions | 2020.02.04 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [pystring/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/pystring/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] pystring
```

##### Integration in the project (xmake.lua)

```lua
add_requires("pystring")
```


### python (cross)


| Description | *The python programming language.* |
| -- | -- |
| Homepage | [https://www.python.org/](https://www.python.org/) |
| Versions | 2.7.18, 3.10.6, 3.7.9, 3.8.10, 3.9.10, 3.9.13, 3.9.5, 3.9.6 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [python/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/python/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] python
```

##### Integration in the project (xmake.lua)

```lua
add_requires("python")
```


### python2 (cross)


| Description | *The python programming language.* |
| -- | -- |
| Homepage | [https://www.python.org/](https://www.python.org/) |
| Versions | 2.7.15, 2.7.18 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [python2/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/python2/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] python2
```

##### Integration in the project (xmake.lua)

```lua
add_requires("python2")
```



## q
### qoi (cross)


| Description | *The Quite OK Image Format for fast, lossless image compression* |
| -- | -- |
| Homepage | [https://qoiformat.org/](https://qoiformat.org/) |
| License | MIT |
| Versions | 2021.12.22, 2022.11.17 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [qoi/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/q/qoi/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] qoi
```

##### Integration in the project (xmake.lua)

```lua
add_requires("qoi")
```


### quickcpplib (cross)


| Description | *Eliminate all the tedious hassle when making state-of-the-art C++ 14 - 23 libraries!* |
| -- | -- |
| Homepage | [https://github.com/ned14/quickcpplib](https://github.com/ned14/quickcpplib) |
| License | Apache-2.0 |
| Versions | 20221116 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [quickcpplib/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/q/quickcpplib/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] quickcpplib
```

##### Integration in the project (xmake.lua)

```lua
add_requires("quickcpplib")
```


### quickjs (cross)


| Description | *QuickJS is a small and embeddable Javascript engine* |
| -- | -- |
| Homepage | [https://bellard.org/quickjs/](https://bellard.org/quickjs/) |
| Versions | 2021.03.27 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [quickjs/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/q/quickjs/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] quickjs
```

##### Integration in the project (xmake.lua)

```lua
add_requires("quickjs")
```



## r
### range-v3 (cross)


| Description | *Range library for C++14/17/20, basis for C++20's std::ranges* |
| -- | -- |
| Homepage | [https://github.com/ericniebler/range-v3/](https://github.com/ericniebler/range-v3/) |
| License | BSL-1.0 |
| Versions | 0.11.0, 0.12.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [range-v3/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/range-v3/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] range-v3
```

##### Integration in the project (xmake.lua)

```lua
add_requires("range-v3")
```


### rapidcsv (cross)


| Description | *C++ header-only library for CSV parsing (by d99kris)* |
| -- | -- |
| Homepage | [https://github.com/d99kris/rapidcsv](https://github.com/d99kris/rapidcsv) |
| Versions | 8.50 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [rapidcsv/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/rapidcsv/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] rapidcsv
```

##### Integration in the project (xmake.lua)

```lua
add_requires("rapidcsv")
```


### rapidjson (cross)


| Description | *RapidJSON is a JSON parser and generator for C++.* |
| -- | -- |
| Homepage | [https://github.com/Tencent/rapidjson](https://github.com/Tencent/rapidjson) |
| Versions | 2022.7.20, v1.1.0, v1.1.0-arrow |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [rapidjson/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/rapidjson/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] rapidjson
```

##### Integration in the project (xmake.lua)

```lua
add_requires("rapidjson")
```


### re2 (cross)


| Description | *RE2 is a fast, safe, thread-friendly alternative to backtracking regular expression engines like those used in PCRE, Perl, and Python. It is a C++ library.* |
| -- | -- |
| Homepage | [https://github.com/google/re2](https://github.com/google/re2) |
| License | BSD-3-Clause |
| Versions | 2020.11.01, 2021.06.01, 2021.08.01, 2021.11.01, 2022.02.01 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [re2/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/re2/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] re2
```

##### Integration in the project (xmake.lua)

```lua
add_requires("re2")
```


### readerwriterqueue (cross)


| Description | *A fast single-producer, single-consumer lock-free queue for C++* |
| -- | -- |
| Homepage | [https://github.com/cameron314/readerwriterqueue](https://github.com/cameron314/readerwriterqueue) |
| License | BSD-3-Clause |
| Versions | v1.0.6 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [readerwriterqueue/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/readerwriterqueue/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] readerwriterqueue
```

##### Integration in the project (xmake.lua)

```lua
add_requires("readerwriterqueue")
```


### recastnavigation (cross)


| Description | *Navigation-mesh Toolset for Games* |
| -- | -- |
| Homepage | [https://github.com/recastnavigation/recastnavigation](https://github.com/recastnavigation/recastnavigation) |
| License | zlib |
| Versions | 1.5.1 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [recastnavigation/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/recastnavigation/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] recastnavigation
```

##### Integration in the project (xmake.lua)

```lua
add_requires("recastnavigation")
```


### reproc (cross)


| Description | *a cross-platform C/C++ library that simplifies starting, stopping and communicating with external programs.* |
| -- | -- |
| Homepage | [https://github.com/DaanDeMeyer/reproc](https://github.com/DaanDeMeyer/reproc) |
| License | MIT |
| Versions | v14.2.4 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [reproc/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/reproc/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] reproc
```

##### Integration in the project (xmake.lua)

```lua
add_requires("reproc")
```


### robin-hood-hashing (cross)


| Description | *Fast & memory efficient hashtable based on robin hood hashing for C++11/14/17/20* |
| -- | -- |
| Homepage | [https://github.com/martinus/robin-hood-hashing](https://github.com/martinus/robin-hood-hashing) |
| License | MIT |
| Versions | 3.11.3, 3.11.5 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [robin-hood-hashing/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/robin-hood-hashing/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] robin-hood-hashing
```

##### Integration in the project (xmake.lua)

```lua
add_requires("robin-hood-hashing")
```


### robin-map (cross)


| Description | *A C++ implementation of a fast hash map and hash set using robin hood hashing* |
| -- | -- |
| Homepage | [https://github.com/Tessil/robin-map](https://github.com/Tessil/robin-map) |
| License | MIT |
| Versions | v0.6.3 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [robin-map/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/robin-map/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] robin-map
```

##### Integration in the project (xmake.lua)

```lua
add_requires("robin-map")
```


### rply (cross)


| Description | *RPly is a library that lets applications read and write PLY files.* |
| -- | -- |
| Homepage | [http://w3.impa.br/~diego/software/rply/](http://w3.impa.br/~diego/software/rply/) |
| License | MIT |
| Versions | 1.1.4 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [rply/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/rply/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] rply
```

##### Integration in the project (xmake.lua)

```lua
add_requires("rply")
```


### rpmalloc (cross)


| Description | *Public domain cross platform lock free thread caching 16-byte aligned memory allocator implemented in C* |
| -- | -- |
| Homepage | [https://github.com/mjansson/rpmalloc](https://github.com/mjansson/rpmalloc) |
| Versions | 1.4.4 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [rpmalloc/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/rpmalloc/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] rpmalloc
```

##### Integration in the project (xmake.lua)

```lua
add_requires("rpmalloc")
```


### rttr (cross)


| Description | *rttr: An open source library, which adds reflection to C++.* |
| -- | -- |
| Homepage | [https://www.rttr.org](https://www.rttr.org) |
| License | MIT |
| Versions | 0.9.5, 0.9.6 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [rttr/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/rttr/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] rttr
```

##### Integration in the project (xmake.lua)

```lua
add_requires("rttr")
```



## s
### scnlib (cross)


| Description | *scnlib is a modern C++ library for replacing scanf and std::istream* |
| -- | -- |
| Homepage | [https://scnlib.readthedocs.io/](https://scnlib.readthedocs.io/) |
| Versions | 0.4, 1.1.2 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [scnlib/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/scnlib/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] scnlib
```

##### Integration in the project (xmake.lua)

```lua
add_requires("scnlib")
```


### scons (cross)


| Description | *A software construction tool* |
| -- | -- |
| Homepage | [https://scons.org](https://scons.org) |
| Versions | 4.1.0, 4.3.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [scons/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/scons/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] scons
```

##### Integration in the project (xmake.lua)

```lua
add_requires("scons")
```


### simde (cross)


| Description | *Implementations of SIMD instruction sets for systems which don't natively support them.* |
| -- | -- |
| Homepage | [simd-everywhere.github.io/blog/](simd-everywhere.github.io/blog/) |
| Versions | 0.7.2 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [simde/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/simde/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] simde
```

##### Integration in the project (xmake.lua)

```lua
add_requires("simde")
```


### simplethreadpool (cross)


| Description | *Simple thread pooling library in C++* |
| -- | -- |
| Homepage | [https://github.com/romch007/simplethreadpool](https://github.com/romch007/simplethreadpool) |
| License | MIT |
| Versions | 2022.11.18 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [simplethreadpool/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/simplethreadpool/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] simplethreadpool
```

##### Integration in the project (xmake.lua)

```lua
add_requires("simplethreadpool")
```


### sokol (cross)


| Description | *Simple STB-style cross-platform libraries for C and C++, written in C.* |
| -- | -- |
| Homepage | [https://github.com/floooh/sokol](https://github.com/floooh/sokol) |
| License | zlib |
| Versions | 2022.02.10, 2023.01.27 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [sokol/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/sokol/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] sokol
```

##### Integration in the project (xmake.lua)

```lua
add_requires("sokol")
```


### sol2 (cross)


| Description | *A C++ library binding to Lua.* |
| -- | -- |
| Homepage | [https://github.com/ThePhD/sol2](https://github.com/ThePhD/sol2) |
| Versions | v3.2.1, v3.2.2, v3.2.3, v3.3.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [sol2/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/sol2/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] sol2
```

##### Integration in the project (xmake.lua)

```lua
add_requires("sol2")
```


### sparsepp (cross)


| Description | *A fast, memory efficient hash map for C++* |
| -- | -- |
| Homepage | [https://github.com/greg7mdp/sparsepp](https://github.com/greg7mdp/sparsepp) |
| Versions | 1.22 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [sparsepp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/sparsepp/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] sparsepp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("sparsepp")
```


### spdlog (cross)


| Description | *Fast C++ logging library.* |
| -- | -- |
| Homepage | [https://github.com/gabime/spdlog](https://github.com/gabime/spdlog) |
| Versions | v1.10.0, v1.11.0, v1.3.1, v1.4.2, v1.5.0, v1.8.0, v1.8.1, v1.8.2, v1.8.5, v1.9.0, v1.9.1, v1.9.2 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [spdlog/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/spdlog/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] spdlog
```

##### Integration in the project (xmake.lua)

```lua
add_requires("spdlog")
```


### spirv-headers (cross)


| Description | *SPIR-V Headers* |
| -- | -- |
| Homepage | [https://github.com/KhronosGroup/SPIRV-Headers/](https://github.com/KhronosGroup/SPIRV-Headers/) |
| License | MIT |
| Versions | 1.2.198+0, 1.3.211+0, 1.3.231+1 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [spirv-headers/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/spirv-headers/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] spirv-headers
```

##### Integration in the project (xmake.lua)

```lua
add_requires("spirv-headers")
```


### sqlite3 (cross)


| Description | *The most used database engine in the world* |
| -- | -- |
| Homepage | [https://sqlite.org/](https://sqlite.org/) |
| Versions | 3.23.0+0, 3.24.0+0, 3.34.0+100, 3.35.0+300, 3.35.0+400, 3.36.0+0, 3.37.0+200, 3.39.0+200 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [sqlite3/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/sqlite3/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] sqlite3
```

##### Integration in the project (xmake.lua)

```lua
add_requires("sqlite3")
```


### stb (cross)


| Description | *single-file public domain (or MIT licensed) libraries for C/C++* |
| -- | -- |
| Homepage | [https://github.com/nothings/stb](https://github.com/nothings/stb) |
| Versions | 2021.07.13, 2021.09.10, 2023.01.30 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [stb/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/stb/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] stb
```

##### Integration in the project (xmake.lua)

```lua
add_requires("stb")
```


### string-view-lite (cross)


| Description | *string_view lite - A C++17-like string_view for C++98, C++11 and later in a single-file header-only library* |
| -- | -- |
| Homepage | [https://github.com/martinmoene/string-view-lite](https://github.com/martinmoene/string-view-lite) |
| License | BSL-1.0 |
| Versions | v1.7.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [string-view-lite/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/string-view-lite/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] string-view-lite
```

##### Integration in the project (xmake.lua)

```lua
add_requires("string-view-lite")
```


### strtk (cross)


| Description | *C++ String Toolkit Library* |
| -- | -- |
| Homepage | [https://www.partow.net/programming/strtk/index.html](https://www.partow.net/programming/strtk/index.html) |
| License | MIT |
| Versions | 2020.01.01 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [strtk/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/strtk/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] strtk
```

##### Integration in the project (xmake.lua)

```lua
add_requires("strtk")
```


### swig (cross)


| Description | *SWIG is a software development tool that connects programs written in C and C++ with a variety of high-level programming languages.* |
| -- | -- |
| Homepage | [http://swig.org/](http://swig.org/) |
| License | GPL-3.0 |
| Versions | 4.0.2 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [swig/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/swig/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] swig
```

##### Integration in the project (xmake.lua)

```lua
add_requires("swig")
```



## t
### tabulate (cross)


| Description | *Header-only library for printing aligned, formatted and colorized tables in Modern C++* |
| -- | -- |
| Homepage | [https://github.com/p-ranav/tabulate](https://github.com/p-ranav/tabulate) |
| License | MIT |
| Versions | 1.4 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [tabulate/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tabulate/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] tabulate
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tabulate")
```


### taskflow (cross)


| Description | *A fast C++ header-only library to help you quickly write parallel programs with complex task dependencies* |
| -- | -- |
| Homepage | [https://taskflow.github.io/](https://taskflow.github.io/) |
| License | MIT |
| Versions | v3.0.0, v3.1.0, v3.2.0, v3.3.0, v3.4.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [taskflow/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/taskflow/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] taskflow
```

##### Integration in the project (xmake.lua)

```lua
add_requires("taskflow")
```


### taywee_args (cross)


| Description | *A simple header-only C++ argument parser library.* |
| -- | -- |
| Homepage | [https://taywee.github.io/args/](https://taywee.github.io/args/) |
| License | MIT |
| Versions | 6.3.0, 6.4.6 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [taywee_args/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/taywee_args/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] taywee_args
```

##### Integration in the project (xmake.lua)

```lua
add_requires("taywee_args")
```


### tbox (cross)


| Description | *A glib-like multi-platform c library* |
| -- | -- |
| Homepage | [https://tboox.org](https://tboox.org) |
| Versions | v1.6.2, v1.6.3, v1.6.4, v1.6.5, v1.6.6, v1.6.7, v1.6.9, v1.7.1 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [tbox/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tbox/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] tbox
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tbox")
```


### tclap (cross)


| Description | *This is a simple templatized C++ library for parsing command line arguments.* |
| -- | -- |
| Homepage | [https://sourceforge.net/projects/tclap/](https://sourceforge.net/projects/tclap/) |
| License | MIT |
| Versions | 1.4.0-rc1 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [tclap/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tclap/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] tclap
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tclap")
```


### termcolor (cross)


| Description | *Termcolor is a header-only C++ library for printing colored messages to the terminal. Written just for fun with a help of the Force.* |
| -- | -- |
| Homepage | [https://github.com/ikalnytskyi/termcolor](https://github.com/ikalnytskyi/termcolor) |
| License | BSD-3-Clause |
| Versions | 2.1.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [termcolor/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/termcolor/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] termcolor
```

##### Integration in the project (xmake.lua)

```lua
add_requires("termcolor")
```


### thread-pool (cross)


| Description | *BS::thread_pool: a fast, lightweight, and easy-to-use C++17 thread pool library* |
| -- | -- |
| Homepage | [https://github.com/bshoshany/thread-pool](https://github.com/bshoshany/thread-pool) |
| License | MIT |
| Versions | v3.3.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [thread-pool/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/thread-pool/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] thread-pool
```

##### Integration in the project (xmake.lua)

```lua
add_requires("thread-pool")
```


### thrift (cross)


| Description | *Thrift is a lightweight, language-independent software stack for point-to-point RPC implementation.* |
| -- | -- |
| Homepage | [https://thrift.apache.org/](https://thrift.apache.org/) |
| License | Apache-2.0 |
| Versions | v0.16.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [thrift/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/thrift/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] thrift
```

##### Integration in the project (xmake.lua)

```lua
add_requires("thrift")
```


### thrust (cross)


| Description | *The C++ parallel algorithms library.* |
| -- | -- |
| Homepage | [https://github.com/NVIDIA/thrust](https://github.com/NVIDIA/thrust) |
| License | Apache-2.0 |
| Versions | 1.17.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [thrust/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/thrust/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] thrust
```

##### Integration in the project (xmake.lua)

```lua
add_requires("thrust")
```


### tiny-process-library (cross)


| Description | *A small platform independent library making it simple to create and stop new processes in C++, as well as writing to stdin and reading from stdout and stderr of a new process* |
| -- | -- |
| Homepage | [https://gitlab.com/eidheim/tiny-process-library](https://gitlab.com/eidheim/tiny-process-library) |
| License | MIT |
| Versions | v2.0.4 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [tiny-process-library/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tiny-process-library/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] tiny-process-library
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tiny-process-library")
```


### tinycbor (cross)


| Description | *Concise Binary Object Representation (CBOR) Library* |
| -- | -- |
| Homepage | [https://github.com/intel/tinycbor](https://github.com/intel/tinycbor) |
| License | MIT |
| Versions | v0.6.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [tinycbor/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tinycbor/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] tinycbor
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tinycbor")
```


### tinycrypt (cross)


| Description | *TinyCrypt Cryptographic Library* |
| -- | -- |
| Homepage | [https://github.com/intel/tinycrypt](https://github.com/intel/tinycrypt) |
| Versions | 2019.9.18 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [tinycrypt/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tinycrypt/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] tinycrypt
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tinycrypt")
```


### tinyexr (cross)


| Description | *Tiny OpenEXR image loader/saver library* |
| -- | -- |
| Homepage | [https://github.com/syoyo/tinyexr/](https://github.com/syoyo/tinyexr/) |
| License | BSD-3-Clause |
| Versions | v1.0.1 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [tinyexr/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tinyexr/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] tinyexr
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tinyexr")
```


### tinyformat (cross)


| Description | *Minimal, type safe printf replacement library for C++* |
| -- | -- |
| Homepage | [https://github.com/c42f/tinyformat/](https://github.com/c42f/tinyformat/) |
| Versions | 2.3.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [tinyformat/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tinyformat/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] tinyformat
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tinyformat")
```


### tinygltf (cross)


| Description | *Header only C++11 tiny glTF 2.0 library* |
| -- | -- |
| Homepage | [https://github.com/syoyo/tinygltf/](https://github.com/syoyo/tinygltf/) |
| License | MIT |
| Versions | v2.5.0, v2.6.3 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [tinygltf/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tinygltf/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] tinygltf
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tinygltf")
```


### tinyxml (cross)


| Description | *TinyXML is a simple, small, minimal, C++ XML parser that can be easily integrating into other programs.* |
| -- | -- |
| Homepage | [https://sourceforge.net/projects/tinyxml/](https://sourceforge.net/projects/tinyxml/) |
| License | zlib |
| Versions | 2.6.2 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [tinyxml/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tinyxml/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] tinyxml
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tinyxml")
```


### tl_expected (cross)


| Description | *C++11/14/17 std::expected with functional-style extensions* |
| -- | -- |
| Homepage | [https://github.com/TartanLlama/expected](https://github.com/TartanLlama/expected) |
| License | CC0 |
| Versions | v1.0.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [tl_expected/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tl_expected/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] tl_expected
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tl_expected")
```


### tl_function_ref (cross)


| Description | *A lightweight, non-owning reference to a callable.* |
| -- | -- |
| Homepage | [https://github.com/TartanLlama/function_ref](https://github.com/TartanLlama/function_ref) |
| License | CC0 |
| Versions | v1.0.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [tl_function_ref/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tl_function_ref/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] tl_function_ref
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tl_function_ref")
```


### toml++ (cross)


| Description | *toml++ is a header-only TOML config file parser and serializer for C++17 (and later!).* |
| -- | -- |
| Homepage | [https://marzer.github.io/tomlplusplus/](https://marzer.github.io/tomlplusplus/) |
| Versions | v2.5.0, v3.0.0, v3.1.0, v3.2.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [toml++/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/toml++/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] toml++
```

##### Integration in the project (xmake.lua)

```lua
add_requires("toml++")
```


### toml11 (cross)


| Description | *TOML for Modern C++* |
| -- | -- |
| Homepage | [https://github.com/ToruNiina/toml11](https://github.com/ToruNiina/toml11) |
| License | MIT |
| Versions | v3.7.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [toml11/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/toml11/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] toml11
```

##### Integration in the project (xmake.lua)

```lua
add_requires("toml11")
```



## u
### uchardet (cross)


| Description | *uchardet is an encoding detector library, which takes a sequence of bytes in an unknown character encoding without any additional information, and attempts to determine the encoding of the text. * |
| -- | -- |
| Homepage | [https://www.freedesktop.org/wiki/Software/uchardet/](https://www.freedesktop.org/wiki/Software/uchardet/) |
| Versions | 0.0.7 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [uchardet/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/u/uchardet/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] uchardet
```

##### Integration in the project (xmake.lua)

```lua
add_requires("uchardet")
```


### unordered_dense (cross)


| Description | *A fast & densely stored hashmap and hashset based on robin-hood backward shift deletion.* |
| -- | -- |
| Homepage | [https://github.com/martinus/unordered_dense](https://github.com/martinus/unordered_dense) |
| License | MIT |
| Versions | v1.1.0, v1.4.0, v2.0.2, v3.0.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [unordered_dense/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/u/unordered_dense/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] unordered_dense
```

##### Integration in the project (xmake.lua)

```lua
add_requires("unordered_dense")
```


### unzip (cross)


| Description | *UnZip is an extraction utility for archives compressed in .zip format.* |
| -- | -- |
| Homepage | [http://infozip.sourceforge.net/UnZip.html](http://infozip.sourceforge.net/UnZip.html) |
| Versions | 6.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [unzip/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/u/unzip/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] unzip
```

##### Integration in the project (xmake.lua)

```lua
add_requires("unzip")
```


### urdfdom-headers (cross)


| Description | *Headers for URDF parsers* |
| -- | -- |
| Homepage | [http://ros.org/wiki/urdf](http://ros.org/wiki/urdf) |
| License | BSD-3-Clause |
| Versions | 1.0.5 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [urdfdom-headers/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/u/urdfdom-headers/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] urdfdom-headers
```

##### Integration in the project (xmake.lua)

```lua
add_requires("urdfdom-headers")
```


### utest.h (cross)


| Description | *single header unit testing framework for C and C++* |
| -- | -- |
| Homepage | [https://www.duskborn.com/utest_h/](https://www.duskborn.com/utest_h/) |
| Versions | 2022.09.01 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [utest.h/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/u/utest.h/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] utest.h
```

##### Integration in the project (xmake.lua)

```lua
add_requires("utest.h")
```


### utf8.h (cross)


| Description | *single header utf8 string functions for C and C++* |
| -- | -- |
| Homepage | [https://github.com/sheredom/utf8.h](https://github.com/sheredom/utf8.h) |
| Versions | 2022.07.04 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [utf8.h/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/u/utf8.h/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] utf8.h
```

##### Integration in the project (xmake.lua)

```lua
add_requires("utf8.h")
```


### utfcpp (cross)


| Description | *UTF8-CPP: UTF-8 with C++ in a Portable Way* |
| -- | -- |
| Homepage | [https://github.com/nemtrif/utfcpp](https://github.com/nemtrif/utfcpp) |
| License | BSL-1.0 |
| Versions | v3.2.1 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [utfcpp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/u/utfcpp/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] utfcpp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("utfcpp")
```



## v
### vectorial (cross)


| Description | *Vector math library with NEON/SSE support* |
| -- | -- |
| Homepage | [https://github.com/scoopr/vectorial](https://github.com/scoopr/vectorial) |
| Versions | 2019.06.28 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [vectorial/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/v/vectorial/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] vectorial
```

##### Integration in the project (xmake.lua)

```lua
add_requires("vectorial")
```


### vulkan-headers (cross)


| Description | *Vulkan Header files and API registry* |
| -- | -- |
| Homepage | [https://github.com/KhronosGroup/Vulkan-Headers/](https://github.com/KhronosGroup/Vulkan-Headers/) |
| License | Apache-2.0 |
| Versions | 1.2.154+0, 1.2.162+0, 1.2.182+0, 1.2.189+1, 1.2.198+0, 1.3.211+0, 1.3.231+1 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [vulkan-headers/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/v/vulkan-headers/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] vulkan-headers
```

##### Integration in the project (xmake.lua)

```lua
add_requires("vulkan-headers")
```



## w
### wolfssl (cross)


| Description | *The wolfSSL library is a small, fast, portable implementation of TLS/SSL for embedded devices to the cloud.  wolfSSL supports up to TLS 1.3!* |
| -- | -- |
| Homepage | [https://www.wolfssl.com](https://www.wolfssl.com) |
| License | GPL-2.0 |
| Versions | v5.3.0-stable |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [wolfssl/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/w/wolfssl/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] wolfssl
```

##### Integration in the project (xmake.lua)

```lua
add_requires("wolfssl")
```



## x
### xbyak (cross)


| Description | *A JIT assembler for x86(IA-32)/x64(AMD64, x86-64) MMX/SSE/SSE2/SSE3/SSSE3/SSE4/FPU/AVX/AVX2/AVX-512 by C++ header* |
| -- | -- |
| Homepage | [https://github.com/herumi/xbyak](https://github.com/herumi/xbyak) |
| Versions | v6.02, v6.03 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [xbyak/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/x/xbyak/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] xbyak
```

##### Integration in the project (xmake.lua)

```lua
add_requires("xbyak")
```


### xxhash (cross)


| Description | *xxHash is an extremely fast non-cryptographic hash algorithm, working at RAM speed limit.* |
| -- | -- |
| Homepage | [http://cyan4973.github.io/xxHash/](http://cyan4973.github.io/xxHash/) |
| License | BSD-2-Clause |
| Versions | v0.8.0, v0.8.1 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [xxhash/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/x/xxhash/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] xxhash
```

##### Integration in the project (xmake.lua)

```lua
add_requires("xxhash")
```



## y
### yasm (cross)


| Description | *Modular BSD reimplementation of NASM.* |
| -- | -- |
| Homepage | [https://yasm.tortall.net/](https://yasm.tortall.net/) |
| Versions | 1.3.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [yasm/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/y/yasm/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] yasm
```

##### Integration in the project (xmake.lua)

```lua
add_requires("yasm")
```


### yyjson (cross)


| Description | *The fastest JSON library in C.* |
| -- | -- |
| Homepage | [https://github.com/ibireme/yyjson](https://github.com/ibireme/yyjson) |
| Versions | 0.2.0, 0.3.0, 0.4.0, 0.5.0, 0.5.1 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [yyjson/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/y/yyjson/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] yyjson
```

##### Integration in the project (xmake.lua)

```lua
add_requires("yyjson")
```



## z
### zig (cross)


| Description | *Zig is a general-purpose programming language and toolchain for maintaining robust, optimal, and reusable software.* |
| -- | -- |
| Homepage | [https://www.ziglang.org/](https://www.ziglang.org/) |
| Versions | 0.10.0, 0.7.1, 0.9.1 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [zig/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/z/zig/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] zig
```

##### Integration in the project (xmake.lua)

```lua
add_requires("zig")
```


### zlib (cross)


| Description | *A Massively Spiffy Yet Delicately Unobtrusive Compression Library* |
| -- | -- |
| Homepage | [http://www.zlib.net](http://www.zlib.net) |
| Versions | v1.2.10, v1.2.11, v1.2.12, v1.2.13 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [zlib/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/z/zlib/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] zlib
```

##### Integration in the project (xmake.lua)

```lua
add_requires("zlib")
```


### zlibcomplete (cross)


| Description | *C++ interface to the ZLib library supporting compression with FLUSH, decompression, and std::string. RAII* |
| -- | -- |
| Homepage | [https://github.com/rudi-cilibrasi/zlibcomplete](https://github.com/rudi-cilibrasi/zlibcomplete) |
| License | MIT |
| Versions | 1.0.5 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [zlibcomplete/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/z/zlibcomplete/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] zlibcomplete
```

##### Integration in the project (xmake.lua)

```lua
add_requires("zlibcomplete")
```


### zstd (cross)


| Description | *Zstandard - Fast real-time compression algorithm* |
| -- | -- |
| Homepage | [https://www.zstd.net/](https://www.zstd.net/) |
| License | BSD-3-Clause |
| Versions | v1.4.5, v1.5.0, v1.5.2 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [zstd/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/z/zstd/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] zstd
```

##### Integration in the project (xmake.lua)

```lua
add_requires("zstd")
```


### zycore-c (cross)


| Description | *Internal library providing platform independent types, macros and a fallback for environments without LibC.* |
| -- | -- |
| Homepage | [https://github.com/zyantific/zycore-c](https://github.com/zyantific/zycore-c) |
| License | MIT |
| Versions | v1.0.0, v1.1.0 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [zycore-c/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/z/zycore-c/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] zycore-c
```

##### Integration in the project (xmake.lua)

```lua
add_requires("zycore-c")
```


### zydis (cross)


| Description | *Fast and lightweight x86/x86-64 disassembler and code generation library* |
| -- | -- |
| Homepage | [https://zydis.re](https://zydis.re) |
| License | MIT |
| Versions | v3.2.1 |
| Architectures | arm, arm64, i386, mips, mips64, ppc, ppc64, riscv, riscv64, s390x, sh4, x86_64 |
| Definition | [zydis/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/z/zydis/xmake.lua) |

##### Install command

```console
xrepo install -p cross [--sdk=/xxx] zydis
```

##### Integration in the project (xmake.lua)

```lua
add_requires("zydis")
```



