## a
### aqt (iphoneos)


| Description | *aqt: Another (unofficial) Qt CLI Installer on multi-platforms* |
| -- | -- |
| Homepage | [https://github.com/miurahr/aqtinstall](https://github.com/miurahr/aqtinstall) |
| License | MIT |
| Versions |  |
| Architectures | arm64, x86_64 |
| Definition | [aqt/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/a/aqt/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos aqt
```

##### Integration in the project (xmake.lua)

```lua
add_requires("aqt")
```


### argh (iphoneos)


| Description | *Argh! A minimalist argument handler.* |
| -- | -- |
| Homepage | [https://github.com/adishavit/argh](https://github.com/adishavit/argh) |
| License | BSD-3-Clause |
| Versions | v1.3.2 |
| Architectures | arm64, x86_64 |
| Definition | [argh/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/a/argh/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos argh
```

##### Integration in the project (xmake.lua)

```lua
add_requires("argh")
```


### argparse (iphoneos)


| Description | *A single header argument parser for C++17* |
| -- | -- |
| Homepage | [https://github.com/p-ranav/argparse](https://github.com/p-ranav/argparse) |
| License | MIT |
| Versions | 2.6, 2.7, 2.8, 2.9 |
| Architectures | arm64, x86_64 |
| Definition | [argparse/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/a/argparse/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos argparse
```

##### Integration in the project (xmake.lua)

```lua
add_requires("argparse")
```


### asio (iphoneos)


| Description | *Asio is a cross-platform C++ library for network and low-level I/O programming that provides developers with a consistent asynchronous model using a modern C++ approach.* |
| -- | -- |
| Homepage | [http://think-async.com/Asio/](http://think-async.com/Asio/) |
| License | BSL-1.0 |
| Versions | 1.20.0, 1.21.0, 1.24.0 |
| Architectures | arm64, x86_64 |
| Definition | [asio/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/a/asio/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos asio
```

##### Integration in the project (xmake.lua)

```lua
add_requires("asio")
```


### autoconf (iphoneos)


| Description | *An extensible package of M4 macros that produce shell scripts to automatically configure software source code packages.* |
| -- | -- |
| Homepage | [https://www.gnu.org/software/autoconf/autoconf.html](https://www.gnu.org/software/autoconf/autoconf.html) |
| Versions | 2.68, 2.69, 2.71 |
| Architectures | arm64, x86_64 |
| Definition | [autoconf/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/a/autoconf/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos autoconf
```

##### Integration in the project (xmake.lua)

```lua
add_requires("autoconf")
```


### automake (iphoneos)


| Description | *A tool for automatically generating Makefile.in files compliant with the GNU Coding Standards.* |
| -- | -- |
| Homepage | [https://www.gnu.org/software/automake/](https://www.gnu.org/software/automake/) |
| Versions | 1.15.1, 1.16.1, 1.16.4, 1.9.5, 1.9.6 |
| Architectures | arm64, x86_64 |
| Definition | [automake/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/a/automake/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos automake
```

##### Integration in the project (xmake.lua)

```lua
add_requires("automake")
```



## b
### bazel (iphoneos)


| Description | *A fast, scalable, multi-language and extensible build system* |
| -- | -- |
| Homepage | [https://bazel.build/](https://bazel.build/) |
| Versions | 5.0.0 |
| Architectures | arm64, x86_64 |
| Definition | [bazel/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/b/bazel/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos bazel
```

##### Integration in the project (xmake.lua)

```lua
add_requires("bazel")
```


### better-enums (iphoneos)


| Description | *C++ compile-time enum to string, iteration, in a single header file* |
| -- | -- |
| Homepage | [http://aantron.github.io/better-enums](http://aantron.github.io/better-enums) |
| License | BSD-2-Clause |
| Versions | 0.11.3 |
| Architectures | arm64, x86_64 |
| Definition | [better-enums/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/b/better-enums/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos better-enums
```

##### Integration in the project (xmake.lua)

```lua
add_requires("better-enums")
```


### bin2c (iphoneos)


| Description | *A simple utility for converting a binary file to a c application* |
| -- | -- |
| Homepage | [https://github.com/gwilymk/bin2c](https://github.com/gwilymk/bin2c) |
| Versions | 0.0.1 |
| Architectures | arm64, x86_64 |
| Definition | [bin2c/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/b/bin2c/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos bin2c
```

##### Integration in the project (xmake.lua)

```lua
add_requires("bin2c")
```


### binutils (iphoneos)


| Description | *GNU binary tools for native development* |
| -- | -- |
| Homepage | [https://www.gnu.org/software/binutils/binutils.html](https://www.gnu.org/software/binutils/binutils.html) |
| License | GPL-2.0 |
| Versions | 2.34, 2.38 |
| Architectures | arm64, x86_64 |
| Definition | [binutils/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/b/binutils/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos binutils
```

##### Integration in the project (xmake.lua)

```lua
add_requires("binutils")
```


### bison (iphoneos)


| Description | *A general-purpose parser generator.* |
| -- | -- |
| Homepage | [https://www.gnu.org/software/bison/](https://www.gnu.org/software/bison/) |
| License | GPL-3.0 |
| Versions | 3.7.4, 3.7.6, 3.8.2 |
| Architectures | arm64, x86_64 |
| Definition | [bison/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/b/bison/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos bison
```

##### Integration in the project (xmake.lua)

```lua
add_requires("bison")
```


### blake3 (iphoneos)


| Description | *BLAKE3 is a cryptographic hash function that is much faster than MD5, SHA-1, SHA-2, SHA-3, and BLAKE2; secure, unlike MD5 and SHA-1 (and secure against length extension, unlike SHA-2); highly parallelizable across any number of threads and SIMD lanes, because it's a Merkle tree on the inside; capable of verified streaming and incremental updates (Merkle tree); a PRF, MAC, KDF, and XOF, as well as a regular hash; and is a single algorithm with no variants, fast on x86-64 and also on smaller architectures.* |
| -- | -- |
| Homepage | [https://blake3.io/](https://blake3.io/) |
| License | CC0-1.0 |
| Versions | 1.3.1, 1.3.3 |
| Architectures | arm64, x86_64 |
| Definition | [blake3/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/b/blake3/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos blake3
```

##### Integration in the project (xmake.lua)

```lua
add_requires("blake3")
```


### brotli (iphoneos)


| Description | *Brotli compression format.* |
| -- | -- |
| Homepage | [https://github.com/google/brotli](https://github.com/google/brotli) |
| Versions | 1.0.9 |
| Architectures | arm64, x86_64 |
| Definition | [brotli/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/b/brotli/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos brotli
```

##### Integration in the project (xmake.lua)

```lua
add_requires("brotli")
```


### bullet3 (iphoneos)


| Description | *Bullet Physics SDK.* |
| -- | -- |
| Homepage | [http://bulletphysics.org](http://bulletphysics.org) |
| License | zlib |
| Versions | 2.88, 3.05, 3.09, 3.24 |
| Architectures | arm64, x86_64 |
| Definition | [bullet3/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/b/bullet3/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos bullet3
```

##### Integration in the project (xmake.lua)

```lua
add_requires("bullet3")
```


### bzip2 (iphoneos)


| Description | *Freely available, patent free, high-quality data compressor.* |
| -- | -- |
| Homepage | [https://sourceware.org/bzip2/](https://sourceware.org/bzip2/) |
| Versions | 1.0.8 |
| Architectures | arm64, x86_64 |
| Definition | [bzip2/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/b/bzip2/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos bzip2
```

##### Integration in the project (xmake.lua)

```lua
add_requires("bzip2")
```



## c
### ca-certificates (iphoneos)


| Description | *Mozilla’s carefully curated collection of Root Certificates for validating the trustworthiness of SSL certificates while verifying the identity of TLS hosts.* |
| -- | -- |
| Homepage | [https://mkcert.org/](https://mkcert.org/) |
| Versions | 20211118, 20220604 |
| Architectures | arm64, x86_64 |
| Definition | [ca-certificates/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/ca-certificates/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos ca-certificates
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ca-certificates")
```


### capstone (iphoneos)


| Description | *Disassembly framework with the target of becoming the ultimate disasm engine for binary analysis and reversing in the security community.* |
| -- | -- |
| Homepage | [http://www.capstone-engine.org](http://www.capstone-engine.org) |
| Versions | 4.0.2 |
| Architectures | arm64, x86_64 |
| Definition | [capstone/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/capstone/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos capstone
```

##### Integration in the project (xmake.lua)

```lua
add_requires("capstone")
```


### cargs (iphoneos)


| Description | *A lightweight cross-platform getopt alternative that works on Linux, Windows and macOS. Command line argument parser library for C/C++. Can be used to parse argv and argc parameters.* |
| -- | -- |
| Homepage | [https://likle.github.io/cargs/](https://likle.github.io/cargs/) |
| License | MIT |
| Versions | v1.0.3 |
| Architectures | arm64, x86_64 |
| Definition | [cargs/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/cargs/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos cargs
```

##### Integration in the project (xmake.lua)

```lua
add_requires("cargs")
```


### catch2 (iphoneos)


| Description | *Catch2 is a multi-paradigm test framework for C++. which also supports Objective-C (and maybe C). * |
| -- | -- |
| Homepage | [https://github.com/catchorg/Catch2](https://github.com/catchorg/Catch2) |
| License | BSL-1.0 |
| Versions | v2.13.10, v2.13.5, v2.13.6, v2.13.7, v2.13.8, v2.13.9, v2.9.2, v3.1.0, v3.1.1, v3.2.0, v3.2.1 |
| Architectures | arm64, x86_64 |
| Definition | [catch2/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/catch2/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos catch2
```

##### Integration in the project (xmake.lua)

```lua
add_requires("catch2")
```


### cereal (iphoneos)


| Description | *cereal is a header-only C++11 serialization library.* |
| -- | -- |
| Homepage | [https://uscilab.github.io/cereal/index.html](https://uscilab.github.io/cereal/index.html) |
| License | BSD-3-Clause |
| Versions | 1.3.0, 1.3.1 |
| Architectures | arm64, x86_64 |
| Definition | [cereal/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/cereal/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos cereal
```

##### Integration in the project (xmake.lua)

```lua
add_requires("cereal")
```


### ceval (iphoneos)


| Description | *A C/C++ library for parsing and evaluation of arithmetic expressions.* |
| -- | -- |
| Homepage | [https://github.com/erstan/ceval](https://github.com/erstan/ceval) |
| License | MIT |
| Versions | 1.0.0 |
| Architectures | arm64, x86_64 |
| Definition | [ceval/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/ceval/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos ceval
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ceval")
```


### cgetopt (iphoneos)


| Description | *A GNU getopt() implementation written in pure C.* |
| -- | -- |
| Homepage | [https://github.com/xq114/cgetopt/](https://github.com/xq114/cgetopt/) |
| Versions | 1.0 |
| Architectures | arm64, x86_64 |
| Definition | [cgetopt/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/cgetopt/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos cgetopt
```

##### Integration in the project (xmake.lua)

```lua
add_requires("cgetopt")
```


### chaiscript (iphoneos)


| Description | *Header-only C++ embedded scripting language loosely based on ECMA script.* |
| -- | -- |
| Homepage | [http://chaiscript.com](http://chaiscript.com) |
| License | BSD-3-Clause |
| Versions | v6.1.0 |
| Architectures | arm64, x86_64 |
| Definition | [chaiscript/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/chaiscript/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos chaiscript
```

##### Integration in the project (xmake.lua)

```lua
add_requires("chaiscript")
```


### chipmunk2d (iphoneos)


| Description | *A fast and lightweight 2D game physics library.* |
| -- | -- |
| Homepage | [https://chipmunk-physics.net/](https://chipmunk-physics.net/) |
| License | MIT |
| Versions | 7.0.3 |
| Architectures | arm64, x86_64 |
| Definition | [chipmunk2d/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/chipmunk2d/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos chipmunk2d
```

##### Integration in the project (xmake.lua)

```lua
add_requires("chipmunk2d")
```


### chromium_zlib (iphoneos)


| Description | *zlib from chromium* |
| -- | -- |
| Homepage | [https://chromium.googlesource.com/chromium/src/third_party/zlib/](https://chromium.googlesource.com/chromium/src/third_party/zlib/) |
| License | zlib |
| Versions | 2022.02.22 |
| Architectures | arm64, x86_64 |
| Definition | [chromium_zlib/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/chromium_zlib/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos chromium_zlib
```

##### Integration in the project (xmake.lua)

```lua
add_requires("chromium_zlib")
```


### civetweb (iphoneos)


| Description | *Embedded C/C++ web server* |
| -- | -- |
| Homepage | [https://github.com/civetweb/civetweb](https://github.com/civetweb/civetweb) |
| License | MIT |
| Versions | v1.15 |
| Architectures | arm64, x86_64 |
| Definition | [civetweb/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/civetweb/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos civetweb
```

##### Integration in the project (xmake.lua)

```lua
add_requires("civetweb")
```


### cjson (iphoneos)


| Description | *Ultralightweight JSON parser in ANSI C.* |
| -- | -- |
| Homepage | [https://github.com/DaveGamble/cJSON](https://github.com/DaveGamble/cJSON) |
| License | MIT |
| Versions | 1.7.10, 1.7.14, 1.7.15 |
| Architectures | arm64, x86_64 |
| Definition | [cjson/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/cjson/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos cjson
```

##### Integration in the project (xmake.lua)

```lua
add_requires("cjson")
```


### clara (iphoneos)


| Description | *A simple to use, composable, command line parser for C++ 11 and beyond.* |
| -- | -- |
| Homepage | [https://github.com/catchorg/Clara](https://github.com/catchorg/Clara) |
| License | BSL-1.0 |
| Versions | 1.1.5 |
| Architectures | arm64, x86_64 |
| Definition | [clara/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/clara/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos clara
```

##### Integration in the project (xmake.lua)

```lua
add_requires("clara")
```


### cli (iphoneos)


| Description | *A library for interactive command line interfaces in modern C++* |
| -- | -- |
| Homepage | [https://github.com/daniele77/cli](https://github.com/daniele77/cli) |
| Versions | v2.0.0 |
| Architectures | arm64, x86_64 |
| Definition | [cli/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/cli/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos cli
```

##### Integration in the project (xmake.lua)

```lua
add_requires("cli")
```


### clib (iphoneos)


| Description | *Header-only library for C99 that implements the most important classes from GLib: GList, GHashTable and GString.* |
| -- | -- |
| Homepage | [https://github.com/aheck/clib](https://github.com/aheck/clib) |
| License | MIT |
| Versions | 2022.12.25 |
| Architectures | arm64, x86_64 |
| Definition | [clib/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/clib/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos clib
```

##### Integration in the project (xmake.lua)

```lua
add_requires("clib")
```


### cmake (iphoneos)


| Description | *A cross-platform family of tool designed to build, test and package software* |
| -- | -- |
| Homepage | [https://cmake.org](https://cmake.org) |
| Versions | 3.11.4, 3.15.4, 3.18.4, 3.21.0, 3.22.1, 3.24.1, 3.24.2 |
| Architectures | arm64, x86_64 |
| Definition | [cmake/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/cmake/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos cmake
```

##### Integration in the project (xmake.lua)

```lua
add_requires("cmake")
```


### cnpy (iphoneos)


| Description | *library to read/write .npy and .npz files in C/C++* |
| -- | -- |
| Homepage | [https://github.com/rogersce/cnpy](https://github.com/rogersce/cnpy) |
| License | MIT |
| Versions | 2018.06.01 |
| Architectures | arm64, x86_64 |
| Definition | [cnpy/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/cnpy/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos cnpy
```

##### Integration in the project (xmake.lua)

```lua
add_requires("cnpy")
```


### concurrentqueue (iphoneos)


| Description | *An industrial-strength lock-free queue for C++.* |
| -- | -- |
| Homepage | [https://github.com/cameron314/concurrentqueue](https://github.com/cameron314/concurrentqueue) |
| Versions |  |
| Architectures | arm64, x86_64 |
| Definition | [concurrentqueue/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/concurrentqueue/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos concurrentqueue
```

##### Integration in the project (xmake.lua)

```lua
add_requires("concurrentqueue")
```


### cpp-httplib (iphoneos)


| Description | *A C++11 single-file header-only cross platform HTTP/HTTPS library.* |
| -- | -- |
| Homepage | [https://github.com/yhirose/cpp-httplib](https://github.com/yhirose/cpp-httplib) |
| Versions | 0.8.5, 0.9.2 |
| Architectures | arm64, x86_64 |
| Definition | [cpp-httplib/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/cpp-httplib/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos cpp-httplib
```

##### Integration in the project (xmake.lua)

```lua
add_requires("cpp-httplib")
```


### crc32c (iphoneos)


| Description | *CRC32C implementation with support for CPU-specific acceleration instructions* |
| -- | -- |
| Homepage | [https://github.com/google/crc32c](https://github.com/google/crc32c) |
| Versions | 1.1.2 |
| Architectures | arm64, x86_64 |
| Definition | [crc32c/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/crc32c/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos crc32c
```

##### Integration in the project (xmake.lua)

```lua
add_requires("crc32c")
```


### crossguid (iphoneos)


| Description | *Lightweight cross platform C++ GUID/UUID library* |
| -- | -- |
| Homepage | [https://github.com/graeme-hill/crossguid](https://github.com/graeme-hill/crossguid) |
| License | MIT |
| Versions | 2019.3.29 |
| Architectures | arm64, x86_64 |
| Definition | [crossguid/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/crossguid/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos crossguid
```

##### Integration in the project (xmake.lua)

```lua
add_requires("crossguid")
```


### cryptopp (iphoneos)


| Description | *free C++ class library of cryptographic schemes* |
| -- | -- |
| Homepage | [https://cryptopp.com/](https://cryptopp.com/) |
| Versions | 8.4.0, 8.5.0, 8.6.0, 8.7.0 |
| Architectures | arm64, x86_64 |
| Definition | [cryptopp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/cryptopp/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos cryptopp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("cryptopp")
```


### csv2 (iphoneos)


| Description | *A CSV parser library* |
| -- | -- |
| Homepage | [https://github.com/p-ranav/csv2](https://github.com/p-ranav/csv2) |
| License | MIT |
| Versions | v0.1 |
| Architectures | arm64, x86_64 |
| Definition | [csv2/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/csv2/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos csv2
```

##### Integration in the project (xmake.lua)

```lua
add_requires("csv2")
```


### csvparser (iphoneos)


| Description | *A modern C++ library for reading, writing, and analyzing CSV (and similar) files (by vincentlaucsb)* |
| -- | -- |
| Homepage | [https://github.com/vincentlaucsb/csv-parser](https://github.com/vincentlaucsb/csv-parser) |
| Versions | 2.1.1 |
| Architectures | arm64, x86_64 |
| Definition | [csvparser/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/csvparser/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos csvparser
```

##### Integration in the project (xmake.lua)

```lua
add_requires("csvparser")
```


### ctre (iphoneos)


| Description | *ctre is a Compile time PCRE (almost) compatible regular expression matcher.* |
| -- | -- |
| Homepage | [https://github.com/hanickadot/compile-time-regular-expressions/](https://github.com/hanickadot/compile-time-regular-expressions/) |
| Versions | 3.4.1 |
| Architectures | arm64, x86_64 |
| Definition | [ctre/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/ctre/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos ctre
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ctre")
```


### cxxopts (iphoneos)


| Description | *Lightweight C++ command line option parser* |
| -- | -- |
| Homepage | [https://github.com/jarro2783/cxxopts](https://github.com/jarro2783/cxxopts) |
| Versions | v2.2.0, v3.0.0 |
| Architectures | arm64, x86_64 |
| Definition | [cxxopts/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/cxxopts/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos cxxopts
```

##### Integration in the project (xmake.lua)

```lua
add_requires("cxxopts")
```



## d
### date (iphoneos)


| Description | *A date and time library for use with C++11 and C++14.* |
| -- | -- |
| Homepage | [https://github.com/HowardHinnant/date](https://github.com/HowardHinnant/date) |
| License | MIT |
| Versions | v3.0.1 |
| Architectures | arm64, x86_64 |
| Definition | [date/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/date/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos date
```

##### Integration in the project (xmake.lua)

```lua
add_requires("date")
```


### dbg-macro (iphoneos)


| Description | *A dbg(…) macro for C++* |
| -- | -- |
| Homepage | [https://github.com/sharkdp/dbg-macro](https://github.com/sharkdp/dbg-macro) |
| License | MIT |
| Versions | v0.4.0 |
| Architectures | arm64, x86_64 |
| Definition | [dbg-macro/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/dbg-macro/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos dbg-macro
```

##### Integration in the project (xmake.lua)

```lua
add_requires("dbg-macro")
```


### debugbreak (iphoneos)


| Description | *break into the debugger programmatically* |
| -- | -- |
| Homepage | [https://github.com/scottt/debugbreak](https://github.com/scottt/debugbreak) |
| Versions | v1.0 |
| Architectures | arm64, x86_64 |
| Definition | [debugbreak/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/debugbreak/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos debugbreak
```

##### Integration in the project (xmake.lua)

```lua
add_requires("debugbreak")
```


### decimal_for_cpp (iphoneos)


| Description | *Decimal data type support, for COBOL-like fixed-point operations on currency/money values.* |
| -- | -- |
| Homepage | [https://github.com/vpiotr/decimal_for_cpp](https://github.com/vpiotr/decimal_for_cpp) |
| License | BSD-3-Clause |
| Versions | 1.19 |
| Architectures | arm64, x86_64 |
| Definition | [decimal_for_cpp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/decimal_for_cpp/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos decimal_for_cpp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("decimal_for_cpp")
```


### demumble (iphoneos)


| Description | *A better c++filt and a better undname.exe, in one binary.* |
| -- | -- |
| Homepage | [https://github.com/nico/demumble](https://github.com/nico/demumble) |
| License | Apache-2.0 |
| Versions | 2022.3.23 |
| Architectures | arm64, x86_64 |
| Definition | [demumble/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/demumble/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos demumble
```

##### Integration in the project (xmake.lua)

```lua
add_requires("demumble")
```


### docopt (iphoneos)


| Description | *Pythonic command line arguments parser (C++11 port)* |
| -- | -- |
| Homepage | [https://github.com/docopt/docopt.cpp](https://github.com/docopt/docopt.cpp) |
| License | BSL-1.0 |
| Versions | v0.6.3 |
| Architectures | arm64, x86_64 |
| Definition | [docopt/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/docopt/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos docopt
```

##### Integration in the project (xmake.lua)

```lua
add_requires("docopt")
```


### doctest (iphoneos)


| Description | *The fastest feature-rich C++11/14/17/20 single-header testing framework for unit tests and TDD* |
| -- | -- |
| Homepage | [http://bit.ly/doctest-docs](http://bit.ly/doctest-docs) |
| Versions | 2.3.1, 2.3.6, 2.4.8, 2.4.9 |
| Architectures | arm64, x86_64 |
| Definition | [doctest/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/doctest/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos doctest
```

##### Integration in the project (xmake.lua)

```lua
add_requires("doctest")
```


### doxygen (iphoneos)


| Description | *%s* |
| -- | -- |
| Homepage | [https://www.doxygen.nl/](https://www.doxygen.nl/) |
| License | GPL-2.0 |
| Versions | 1.9.1, 1.9.2, 1.9.3 |
| Architectures | arm64, x86_64 |
| Definition | [doxygen/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/doxygen/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos doxygen
```

##### Integration in the project (xmake.lua)

```lua
add_requires("doxygen")
```


### dr_flac (iphoneos)


| Description | *Single file audio decoding libraries for C/C++.* |
| -- | -- |
| Homepage | [https://github.com/mackron/dr_libs](https://github.com/mackron/dr_libs) |
| License | MIT |
| Versions | 0.12.29 |
| Architectures | arm64, x86_64 |
| Definition | [dr_flac/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/dr_flac/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos dr_flac
```

##### Integration in the project (xmake.lua)

```lua
add_requires("dr_flac")
```


### dr_mp3 (iphoneos)


| Description | *Single file audio decoding libraries for C/C++.* |
| -- | -- |
| Homepage | [https://github.com/mackron/dr_libs](https://github.com/mackron/dr_libs) |
| License | MIT |
| Versions | 0.6.27 |
| Architectures | arm64, x86_64 |
| Definition | [dr_mp3/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/dr_mp3/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos dr_mp3
```

##### Integration in the project (xmake.lua)

```lua
add_requires("dr_mp3")
```


### dr_wav (iphoneos)


| Description | *Single file audio decoding libraries for C/C++.* |
| -- | -- |
| Homepage | [https://github.com/mackron/dr_libs](https://github.com/mackron/dr_libs) |
| License | MIT |
| Versions | 0.12.19 |
| Architectures | arm64, x86_64 |
| Definition | [dr_wav/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/dr_wav/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos dr_wav
```

##### Integration in the project (xmake.lua)

```lua
add_requires("dr_wav")
```



## e
### easyloggingpp (iphoneos)


| Description | *Single header C++ logging library.* |
| -- | -- |
| Homepage | [https://github.com/amrayn/easyloggingpp](https://github.com/amrayn/easyloggingpp) |
| License | MIT |
| Versions | v9.97.0 |
| Architectures | arm64, x86_64 |
| Definition | [easyloggingpp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/e/easyloggingpp/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos easyloggingpp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("easyloggingpp")
```


### elfio (iphoneos)


| Description | *ELFIO - ELF (Executable and Linkable Format) reader and producer implemented as a header only C++ library* |
| -- | -- |
| Homepage | [http://serge1.github.io/ELFIO](http://serge1.github.io/ELFIO) |
| License | MIT |
| Versions | 3.11 |
| Architectures | arm64, x86_64 |
| Definition | [elfio/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/e/elfio/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos elfio
```

##### Integration in the project (xmake.lua)

```lua
add_requires("elfio")
```


### enet (iphoneos)


| Description | *Reliable UDP networking library.* |
| -- | -- |
| Homepage | [http://enet.bespin.org](http://enet.bespin.org) |
| License | MIT |
| Versions | v1.3.17 |
| Architectures | arm64, x86_64 |
| Definition | [enet/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/e/enet/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos enet
```

##### Integration in the project (xmake.lua)

```lua
add_requires("enet")
```


### entt (iphoneos)


| Description | *Gaming meets modern C++ - a fast and reliable entity component system (ECS) and much more.* |
| -- | -- |
| Homepage | [https://github.com/skypjack/entt](https://github.com/skypjack/entt) |
| License | MIT |
| Versions | v3.10.0, v3.10.1, v3.10.3, v3.11.0, v3.11.1, v3.6.0, v3.7.0, v3.7.1, v3.8.0, v3.8.1, v3.9.0 |
| Architectures | arm64, x86_64 |
| Definition | [entt/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/e/entt/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos entt
```

##### Integration in the project (xmake.lua)

```lua
add_requires("entt")
```


### expected (iphoneos)


| Description | *C++11/14/17 std::expected with functional-style extensions* |
| -- | -- |
| Homepage | [https://github.com/TartanLlama/expected](https://github.com/TartanLlama/expected) |
| License | CC0 |
| Versions | v1.0.0 |
| Architectures | arm64, x86_64 |
| Definition | [expected/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/e/expected/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos expected
```

##### Integration in the project (xmake.lua)

```lua
add_requires("expected")
```


### exprtk (iphoneos)


| Description | *C++ Mathematical Expression Parsing And Evaluation Library* |
| -- | -- |
| Homepage | [https://www.partow.net/programming/exprtk/index.html](https://www.partow.net/programming/exprtk/index.html) |
| License | MIT |
| Versions | 2021.06.06 |
| Architectures | arm64, x86_64 |
| Definition | [exprtk/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/e/exprtk/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos exprtk
```

##### Integration in the project (xmake.lua)

```lua
add_requires("exprtk")
```



## f
### fast_float (iphoneos)


| Description | *Fast and exact implementation of the C++ from_chars functions for float and double types: 4x faster than strtod* |
| -- | -- |
| Homepage | [https://github.com/fastfloat/fast_float](https://github.com/fastfloat/fast_float) |
| License | Apache-2.0 |
| Versions | v3.4.0, v3.5.1 |
| Architectures | arm64, x86_64 |
| Definition | [fast_float/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/fast_float/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos fast_float
```

##### Integration in the project (xmake.lua)

```lua
add_requires("fast_float")
```


### fastcppcsvparser (iphoneos)


| Description | *This is a small, easy-to-use and fast header-only library for reading comma separated value (CSV) files (by ben-strasser)* |
| -- | -- |
| Homepage | [https://github.com/ben-strasser/fast-cpp-csv-parser](https://github.com/ben-strasser/fast-cpp-csv-parser) |
| Versions | 2021.01.03 |
| Architectures | arm64, x86_64 |
| Definition | [fastcppcsvparser/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/fastcppcsvparser/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos fastcppcsvparser
```

##### Integration in the project (xmake.lua)

```lua
add_requires("fastcppcsvparser")
```


### flatbuffers (iphoneos)


| Description | *FlatBuffers is a cross platform serialization library architected for maximum memory efficiency.* |
| -- | -- |
| Homepage | [http://google.github.io/flatbuffers/](http://google.github.io/flatbuffers/) |
| Versions | 1.12.0, 2.0.0 |
| Architectures | arm64, x86_64 |
| Definition | [flatbuffers/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/flatbuffers/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos flatbuffers
```

##### Integration in the project (xmake.lua)

```lua
add_requires("flatbuffers")
```


### flex (iphoneos)


| Description | *%s* |
| -- | -- |
| Homepage | [https://github.com/westes/flex/](https://github.com/westes/flex/) |
| License | BSD-2-Clause |
| Versions | 2.6.4 |
| Architectures | arm64, x86_64 |
| Definition | [flex/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/flex/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos flex
```

##### Integration in the project (xmake.lua)

```lua
add_requires("flex")
```


### fmt (iphoneos)


| Description | *fmt is an open-source formatting library for C++. It can be used as a safe and fast alternative to (s)printf and iostreams.* |
| -- | -- |
| Homepage | [https://fmt.dev](https://fmt.dev) |
| Versions | 5.3.0, 6.0.0, 6.2.0, 7.1.3, 8.0.0, 8.0.1, 8.1.1, 9.0.0, 9.1.0 |
| Architectures | arm64, x86_64 |
| Definition | [fmt/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/fmt/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos fmt
```

##### Integration in the project (xmake.lua)

```lua
add_requires("fmt")
```


### freetype (iphoneos)


| Description | *A freely available software library to render fonts.* |
| -- | -- |
| Homepage | [https://www.freetype.org](https://www.freetype.org) |
| Versions | 2.10.4, 2.11.0, 2.11.1, 2.12.1, 2.9.1 |
| Architectures | arm64, x86_64 |
| Definition | [freetype/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/freetype/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos freetype
```

##### Integration in the project (xmake.lua)

```lua
add_requires("freetype")
```


### frozen (iphoneos)


| Description | *A header-only, constexpr alternative to gperf for C++14 users* |
| -- | -- |
| Homepage | [https://github.com/serge-sans-paille/frozen](https://github.com/serge-sans-paille/frozen) |
| License | Apache-2.0 |
| Versions | 1.1.1 |
| Architectures | arm64, x86_64 |
| Definition | [frozen/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/frozen/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos frozen
```

##### Integration in the project (xmake.lua)

```lua
add_requires("frozen")
```


### functionalplus (iphoneos)


| Description | *Functional Programming Library for C++. Write concise and readable C++ code.* |
| -- | -- |
| Homepage | [http://www.editgym.com/fplus-api-search/](http://www.editgym.com/fplus-api-search/) |
| Versions | v0.2.18-p0 |
| Architectures | arm64, x86_64 |
| Definition | [functionalplus/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/functionalplus/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos functionalplus
```

##### Integration in the project (xmake.lua)

```lua
add_requires("functionalplus")
```


### fx-gltf (iphoneos)


| Description | *A C++14/C++17 header-only library for simple, efficient, and robust serialization/deserialization of glTF 2.0* |
| -- | -- |
| Homepage | [https://github.com/jessey-git/fx-gltf](https://github.com/jessey-git/fx-gltf) |
| License | MIT |
| Versions | v1.2.0, v2.0.0 |
| Architectures | arm64, x86_64 |
| Definition | [fx-gltf/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/fx-gltf/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos fx-gltf
```

##### Integration in the project (xmake.lua)

```lua
add_requires("fx-gltf")
```



## g
### genie (iphoneos)


| Description | *GENie - Project generator tool* |
| -- | -- |
| Homepage | [https://github.com/bkaradzic/GENie](https://github.com/bkaradzic/GENie) |
| Versions | 1160.0 |
| Architectures | arm64, x86_64 |
| Definition | [genie/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/genie/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos genie
```

##### Integration in the project (xmake.lua)

```lua
add_requires("genie")
```


### gflags (iphoneos)


| Description | *The gflags package contains a C++ library that implements commandline flags processing.* |
| -- | -- |
| Homepage | [https://github.com/gflags/gflags/](https://github.com/gflags/gflags/) |
| License | BSD-3-Clause |
| Versions | v2.2.2 |
| Architectures | arm64, x86_64 |
| Definition | [gflags/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/gflags/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos gflags
```

##### Integration in the project (xmake.lua)

```lua
add_requires("gflags")
```


### ghc_filesystem (iphoneos)


| Description | *An implementation of C++17 std::filesystem for C++11 /C++14/C++17/C++20 on Windows, macOS, Linux and FreeBSD.* |
| -- | -- |
| Homepage | [https://github.com/gulrak/filesystem](https://github.com/gulrak/filesystem) |
| License | MIT |
| Versions | v1.5.10 |
| Architectures | arm64, x86_64 |
| Definition | [ghc_filesystem/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/ghc_filesystem/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos ghc_filesystem
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ghc_filesystem")
```


### giflib (iphoneos)


| Description | *A library for reading and writing gif images.* |
| -- | -- |
| Homepage | [https://sourceforge.net/projects/giflib/](https://sourceforge.net/projects/giflib/) |
| License | MIT |
| Versions | 5.2.1 |
| Architectures | arm64, x86_64 |
| Definition | [giflib/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/giflib/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos giflib
```

##### Integration in the project (xmake.lua)

```lua
add_requires("giflib")
```


### gli (iphoneos)


| Description | *OpenGL Image (GLI)* |
| -- | -- |
| Homepage | [https://gli.g-truc.net/](https://gli.g-truc.net/) |
| Versions | 0.8.2.0 |
| Architectures | arm64, x86_64 |
| Definition | [gli/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/gli/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos gli
```

##### Integration in the project (xmake.lua)

```lua
add_requires("gli")
```


### glm (iphoneos)


| Description | *OpenGL Mathematics (GLM)* |
| -- | -- |
| Homepage | [https://glm.g-truc.net/](https://glm.g-truc.net/) |
| Versions | 0.9.9+8 |
| Architectures | arm64, x86_64 |
| Definition | [glm/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/glm/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos glm
```

##### Integration in the project (xmake.lua)

```lua
add_requires("glm")
```


### glog (iphoneos)


| Description | *C++ implementation of the Google logging module* |
| -- | -- |
| Homepage | [https://github.com/google/glog/](https://github.com/google/glog/) |
| License | BSD-3-Clause |
| Versions | v0.4.0, v0.5.0, v0.6.0 |
| Architectures | arm64, x86_64 |
| Definition | [glog/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/glog/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos glog
```

##### Integration in the project (xmake.lua)

```lua
add_requires("glog")
```


### gn (iphoneos)


| Description | *GN is a meta-build system that generates build files for Ninja.* |
| -- | -- |
| Homepage | [https://gn.googlesource.com/gn](https://gn.googlesource.com/gn) |
| Versions | 20211117 |
| Architectures | arm64, x86_64 |
| Definition | [gn/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/gn/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos gn
```

##### Integration in the project (xmake.lua)

```lua
add_requires("gn")
```


### gnu-rm (iphoneos)


| Description | *GNU Arm Embedded Toolchain* |
| -- | -- |
| Homepage | [https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm) |
| Versions | 2020.10, 2021.10 |
| Architectures | arm64, x86_64 |
| Definition | [gnu-rm/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/gnu-rm/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos gnu-rm
```

##### Integration in the project (xmake.lua)

```lua
add_requires("gnu-rm")
```


### godotcpp (iphoneos)


| Description | *C++ bindings for the Godot script API* |
| -- | -- |
| Homepage | [https://godotengine.org/](https://godotengine.org/) |
| Versions | 3.2, 3.3, 3.4.0, 3.4.3, 3.4.4 |
| Architectures | arm64, x86_64 |
| Definition | [godotcpp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/godotcpp/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos godotcpp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("godotcpp")
```


### gsl (iphoneos)


| Description | *Guidelines Support Library* |
| -- | -- |
| Homepage | [https://github.com/microsoft/GSL](https://github.com/microsoft/GSL) |
| License | MIT |
| Versions | v3.1.0, v4.0.0 |
| Architectures | arm64, x86_64 |
| Definition | [gsl/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/gsl/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos gsl
```

##### Integration in the project (xmake.lua)

```lua
add_requires("gsl")
```


### gtest (iphoneos)


| Description | *Google Testing and Mocking Framework.* |
| -- | -- |
| Homepage | [https://github.com/google/googletest](https://github.com/google/googletest) |
| Versions | 1.10.0, 1.11.0, 1.12.0, 1.12.1, 1.8.1 |
| Architectures | arm64, x86_64 |
| Definition | [gtest/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/gtest/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos gtest
```

##### Integration in the project (xmake.lua)

```lua
add_requires("gtest")
```


### guetzli (iphoneos)


| Description | *Perceptual JPEG encoder* |
| -- | -- |
| Homepage | [https://github.com/google/guetzli](https://github.com/google/guetzli) |
| Versions | v1.0.1 |
| Architectures | arm64, x86_64 |
| Definition | [guetzli/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/guetzli/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos guetzli
```

##### Integration in the project (xmake.lua)

```lua
add_requires("guetzli")
```


### guilite (iphoneos)


| Description | *The smallest header-only GUI library (4 KLOC) for all platforms.* |
| -- | -- |
| Homepage | [https://github.com/idea4good/GuiLite](https://github.com/idea4good/GuiLite) |
| License | Apache-2.0 |
| Versions | v2.1 |
| Architectures | arm64, x86_64 |
| Definition | [guilite/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/guilite/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos guilite
```

##### Integration in the project (xmake.lua)

```lua
add_requires("guilite")
```


### gyp-next (iphoneos)


| Description | *A fork of the GYP build system for use in the Node.js projects* |
| -- | -- |
| Homepage | [https://github.com/nodejs/gyp-next](https://github.com/nodejs/gyp-next) |
| License | BSD-3-Clause |
| Versions | v0.11.0 |
| Architectures | arm64, x86_64 |
| Definition | [gyp-next/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/gyp-next/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos gyp-next
```

##### Integration in the project (xmake.lua)

```lua
add_requires("gyp-next")
```



## h
### handy (iphoneos)


| Description | *A simple C++11 network server framework* |
| -- | -- |
| Homepage | [https://github.com/yedf2/handy](https://github.com/yedf2/handy) |
| Versions | 0.2.0 |
| Architectures | arm64, x86_64 |
| Definition | [handy/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/h/handy/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos handy
```

##### Integration in the project (xmake.lua)

```lua
add_requires("handy")
```


### happly (iphoneos)


| Description | *A C++ header-only parser for the PLY file format.* |
| -- | -- |
| Homepage | [https://github.com/nmwsharp/happly](https://github.com/nmwsharp/happly) |
| License | MIT |
| Versions | 2022.01.07 |
| Architectures | arm64, x86_64 |
| Definition | [happly/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/h/happly/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos happly
```

##### Integration in the project (xmake.lua)

```lua
add_requires("happly")
```


### hash-library (iphoneos)


| Description | *Portable C++ hashing library* |
| -- | -- |
| Homepage | [https://create.stephan-brumme.com/hash-library/](https://create.stephan-brumme.com/hash-library/) |
| License | zlib |
| Versions | 2021.09.29 |
| Architectures | arm64, x86_64 |
| Definition | [hash-library/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/h/hash-library/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos hash-library
```

##### Integration in the project (xmake.lua)

```lua
add_requires("hash-library")
```


### hiredis (iphoneos)


| Description | *Minimalistic C client for Redis >= 1.2* |
| -- | -- |
| Homepage | [https://github.com/redis/hiredis](https://github.com/redis/hiredis) |
| License | BSD-3-Clause |
| Versions | v1.0.2 |
| Architectures | arm64, x86_64 |
| Definition | [hiredis/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/h/hiredis/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos hiredis
```

##### Integration in the project (xmake.lua)

```lua
add_requires("hiredis")
```


### hopscotch-map (iphoneos)


| Description | *A C++ implementation of a fast hash map and hash set using hopscotch hashing* |
| -- | -- |
| Homepage | [https://github.com/Tessil/hopscotch-map](https://github.com/Tessil/hopscotch-map) |
| Versions | v2.3.0 |
| Architectures | arm64, x86_64 |
| Definition | [hopscotch-map/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/h/hopscotch-map/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos hopscotch-map
```

##### Integration in the project (xmake.lua)

```lua
add_requires("hopscotch-map")
```


### http_parser (iphoneos)


| Description | *Parser for HTTP messages written in C.* |
| -- | -- |
| Homepage | [https://github.com/nodejs/http-parser](https://github.com/nodejs/http-parser) |
| Versions | v2.9.4 |
| Architectures | arm64, x86_64 |
| Definition | [http_parser/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/h/http_parser/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos http_parser
```

##### Integration in the project (xmake.lua)

```lua
add_requires("http_parser")
```



## i
### ifort (iphoneos)


| Description | *The Fortran Compiler provided by Intel®* |
| -- | -- |
| Homepage | [https://www.intel.com/content/www/us/en/developer/tools/oneapi/fortran-compiler.html](https://www.intel.com/content/www/us/en/developer/tools/oneapi/fortran-compiler.html) |
| Versions | 2021.4.0+3224 |
| Architectures | arm64, x86_64 |
| Definition | [ifort/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/i/ifort/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos ifort
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ifort")
```


### imgui (iphoneos)


| Description | *Bloat-free Immediate Mode Graphical User interface for C++ with minimal dependencies* |
| -- | -- |
| Homepage | [https://github.com/ocornut/imgui](https://github.com/ocornut/imgui) |
| License | MIT |
| Versions | v1.75, v1.79, v1.80, v1.81, v1.82, v1.83, v1.83-docking, v1.84.1, v1.84.2, v1.85, v1.85-docking, v1.86, v1.87, v1.87-docking, v1.88, v1.88-docking, v1.89, v1.89-docking |
| Architectures | arm64, x86_64 |
| Definition | [imgui/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/i/imgui/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos imgui
```

##### Integration in the project (xmake.lua)

```lua
add_requires("imgui")
```


### imguizmo (iphoneos)


| Description | *Immediate mode 3D gizmo for scene editing and other controls based on Dear Imgui* |
| -- | -- |
| Homepage | [https://github.com/CedricGuillemet/ImGuizmo](https://github.com/CedricGuillemet/ImGuizmo) |
| Versions | 1.83, 1.89+WIP |
| Architectures | arm64, x86_64 |
| Definition | [imguizmo/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/i/imguizmo/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos imguizmo
```

##### Integration in the project (xmake.lua)

```lua
add_requires("imguizmo")
```


### indicators (iphoneos)


| Description | *Activity Indicators for Modern C++* |
| -- | -- |
| Homepage | [https://github.com/p-ranav/indicators](https://github.com/p-ranav/indicators) |
| License | MIT |
| Versions | 2.2 |
| Architectures | arm64, x86_64 |
| Definition | [indicators/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/i/indicators/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos indicators
```

##### Integration in the project (xmake.lua)

```lua
add_requires("indicators")
```


### inja (iphoneos)


| Description | *A Template Engine for Modern C++* |
| -- | -- |
| Homepage | [https://pantor.github.io/inja/](https://pantor.github.io/inja/) |
| Versions | v2.1.0 |
| Architectures | arm64, x86_64 |
| Definition | [inja/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/i/inja/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos inja
```

##### Integration in the project (xmake.lua)

```lua
add_requires("inja")
```


### ip2region (iphoneos)


| Description | *IP address region search library.* |
| -- | -- |
| Homepage | [https://github.com/lionsoul2014/ip2region](https://github.com/lionsoul2014/ip2region) |
| License | Apache-2.0 |
| Versions | v2020.10.31 |
| Architectures | arm64, x86_64 |
| Definition | [ip2region/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/i/ip2region/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos ip2region
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ip2region")
```


### irrxml (iphoneos)


| Description | *High speed and easy-to-use XML Parser for C++* |
| -- | -- |
| Homepage | [https://sourceforge.net/projects/irrlicht/](https://sourceforge.net/projects/irrlicht/) |
| Versions | 1.2 |
| Architectures | arm64, x86_64 |
| Definition | [irrxml/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/i/irrxml/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos irrxml
```

##### Integration in the project (xmake.lua)

```lua
add_requires("irrxml")
```


### ispc (iphoneos)


| Description | *Intel® Implicit SPMD Program Compiler* |
| -- | -- |
| Homepage | [https://ispc.github.io/](https://ispc.github.io/) |
| License | BSD-3-Clause |
| Versions | 1.17.0 |
| Architectures | arm64, x86_64 |
| Definition | [ispc/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/i/ispc/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos ispc
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ispc")
```



## j
### jsmn (iphoneos)


| Description | *Jsmn is a world fastest JSON parser/tokenizer* |
| -- | -- |
| Homepage | [https://github.com/zserge/jsmn](https://github.com/zserge/jsmn) |
| Versions | v1.1.0 |
| Architectures | arm64, x86_64 |
| Definition | [jsmn/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/j/jsmn/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos jsmn
```

##### Integration in the project (xmake.lua)

```lua
add_requires("jsmn")
```


### json-c (iphoneos)


| Description | *JSON parser for C* |
| -- | -- |
| Homepage | [https://github.com/json-c/json-c/wiki](https://github.com/json-c/json-c/wiki) |
| Versions | 0.13.1-20180305 |
| Architectures | arm64, x86_64 |
| Definition | [json-c/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/j/json-c/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos json-c
```

##### Integration in the project (xmake.lua)

```lua
add_requires("json-c")
```


### json-schema-validator (iphoneos)


| Description | *JSON schema validator for JSON for Modern C++* |
| -- | -- |
| Homepage | [https://github.com/pboettch/json-schema-validator](https://github.com/pboettch/json-schema-validator) |
| Versions | 2.1.0 |
| Architectures | arm64, x86_64 |
| Definition | [json-schema-validator/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/j/json-schema-validator/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos json-schema-validator
```

##### Integration in the project (xmake.lua)

```lua
add_requires("json-schema-validator")
```


### json.h (iphoneos)


| Description | *single header json parser for C and C++* |
| -- | -- |
| Homepage | [https://github.com/sheredom/json.h](https://github.com/sheredom/json.h) |
| Versions | 2022.11.27 |
| Architectures | arm64, x86_64 |
| Definition | [json.h/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/j/json.h/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos json.h
```

##### Integration in the project (xmake.lua)

```lua
add_requires("json.h")
```


### jsoncons (iphoneos)


| Description | *A C++, header-only library for constructing JSON and JSON-like data formats, with JSON Pointer, JSON Patch, JSONPath, JMESPath, CSV, MessagePack, CBOR, BSON, UBJSON* |
| -- | -- |
| Homepage | [https://danielaparker.github.io/jsoncons/](https://danielaparker.github.io/jsoncons/) |
| Versions | v0.158.0 |
| Architectures | arm64, x86_64 |
| Definition | [jsoncons/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/j/jsoncons/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos jsoncons
```

##### Integration in the project (xmake.lua)

```lua
add_requires("jsoncons")
```


### jsoncpp (iphoneos)


| Description | *A C++ library for interacting with JSON.* |
| -- | -- |
| Homepage | [https://github.com/open-source-parsers/jsoncpp/wiki](https://github.com/open-source-parsers/jsoncpp/wiki) |
| Versions | 1.9.4, 1.9.5 |
| Architectures | arm64, x86_64 |
| Definition | [jsoncpp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/j/jsoncpp/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos jsoncpp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("jsoncpp")
```



## k
### kcp (iphoneos)


| Description | *A Fast and Reliable ARQ Protocol.* |
| -- | -- |
| Homepage | [https://github.com/skywind3000/kcp](https://github.com/skywind3000/kcp) |
| Versions | 1.7 |
| Architectures | arm64, x86_64 |
| Definition | [kcp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/k/kcp/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos kcp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("kcp")
```


### kiwisolver (iphoneos)


| Description | *Efficient C++ implementation of the Cassowary constraint solving algorithm* |
| -- | -- |
| Homepage | [https://kiwisolver.readthedocs.io/en/latest/](https://kiwisolver.readthedocs.io/en/latest/) |
| Versions | 1.3.1, 1.3.2, 1.4.4 |
| Architectures | arm64, x86_64 |
| Definition | [kiwisolver/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/k/kiwisolver/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos kiwisolver
```

##### Integration in the project (xmake.lua)

```lua
add_requires("kiwisolver")
```



## l
### lexy (iphoneos)


| Description | *C++ parsing DSL* |
| -- | -- |
| Homepage | [https://lexy.foonathan.net](https://lexy.foonathan.net) |
| Versions | 2022.03.21 |
| Architectures | arm64, x86_64 |
| Definition | [lexy/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/lexy/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos lexy
```

##### Integration in the project (xmake.lua)

```lua
add_requires("lexy")
```


### libcurl (iphoneos)


| Description | *The multiprotocol file transfer library.* |
| -- | -- |
| Homepage | [https://curl.haxx.se/](https://curl.haxx.se/) |
| License | MIT |
| Versions | 7.31.0, 7.32.0, 7.33.0, 7.34.0, 7.35.0, 7.36.0, 7.37.1, 7.38.0, 7.39.0, 7.40.0, 7.41.0, 7.42.1, 7.43.0, 7.44.0, 7.45.0, 7.46.0, 7.47.1, 7.48.0, 7.49.1, 7.50.3, 7.51.0, 7.52.1, 7.53.1, 7.54.1, 7.55.1, 7.56.1, 7.57.0, 7.58.0, 7.59.0, 7.60.0, 7.61.0, 7.61.1, 7.62.0, 7.63.0, 7.64.0, 7.64.1, 7.65.3, 7.66.0, 7.67.0, 7.68.0, 7.69.1, 7.70.0, 7.71.1, 7.72.0, 7.73.0, 7.74.0, 7.75.0, 7.76.1, 7.77.0, 7.78.0, 7.80.0, 7.81.0, 7.82.0, 7.84.0, 7.85.0, 7.86.0, 7.87.0 |
| Architectures | arm64, x86_64 |
| Definition | [libcurl/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libcurl/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos libcurl
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libcurl")
```


### libdivide (iphoneos)


| Description | *Official git repository for libdivide: optimized integer division* |
| -- | -- |
| Homepage | [http://libdivide.com](http://libdivide.com) |
| Versions | 5.0 |
| Architectures | arm64, x86_64 |
| Definition | [libdivide/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libdivide/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos libdivide
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libdivide")
```


### libdivsufsort (iphoneos)


| Description | *A lightweight suffix array sorting library* |
| -- | -- |
| Homepage | [https://android.googlesource.com/platform/external/libdivsufsort/](https://android.googlesource.com/platform/external/libdivsufsort/) |
| Versions | 2021.2.18 |
| Architectures | arm64, x86_64 |
| Definition | [libdivsufsort/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libdivsufsort/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos libdivsufsort
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libdivsufsort")
```


### libev (iphoneos)


| Description | *Full-featured high-performance event loop loosely modelled after libevent.* |
| -- | -- |
| Homepage | [http://software.schmorp.de/pkg/libev](http://software.schmorp.de/pkg/libev) |
| Versions | 4.33 |
| Architectures | arm64, x86_64 |
| Definition | [libev/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libev/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos libev
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libev")
```


### libffi (iphoneos)


| Description | *Portable Foreign Function Interface library.* |
| -- | -- |
| Homepage | [https://sourceware.org/libffi/](https://sourceware.org/libffi/) |
| Versions | 3.2.1, 3.3, 3.4.2 |
| Architectures | arm64, x86_64 |
| Definition | [libffi/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libffi/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos libffi
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libffi")
```


### libflac (iphoneos)


| Description | *Free Lossless Audio Codec* |
| -- | -- |
| Homepage | [https://xiph.org/flac](https://xiph.org/flac) |
| License | BSD |
| Versions | 1.3.3 |
| Architectures | arm64, x86_64 |
| Definition | [libflac/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libflac/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos libflac
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libflac")
```


### libgit2 (iphoneos)


| Description | *A cross-platform, linkable library implementation of Git that you can use in your application.* |
| -- | -- |
| Homepage | [https://libgit2.org/](https://libgit2.org/) |
| License | GPL-2.0-only |
| Versions | v1.3.0 |
| Architectures | arm64, x86_64 |
| Definition | [libgit2/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libgit2/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos libgit2
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libgit2")
```


### libhv (iphoneos)


| Description | *Like libevent, libev, and libuv, libhv provides event-loop with non-blocking IO and timer, but simpler api and richer protocols.* |
| -- | -- |
| Homepage | [https://github.com/ithewei/libhv](https://github.com/ithewei/libhv) |
| Versions | 1.0.0, 1.1.0, 1.1.1, 1.2.1, 1.2.2, 1.2.3, 1.2.4, 1.2.6 |
| Architectures | arm64, x86_64 |
| Definition | [libhv/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libhv/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos libhv
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libhv")
```


### libjpeg (iphoneos)


| Description | *A widely used C library for reading and writing JPEG image files.* |
| -- | -- |
| Homepage | [http://ijg.org/](http://ijg.org/) |
| Versions | v9b, v9c, v9d, v9e |
| Architectures | arm64, x86_64 |
| Definition | [libjpeg/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libjpeg/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos libjpeg
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libjpeg")
```


### libogg (iphoneos)


| Description | *Ogg Bitstream Library* |
| -- | -- |
| Homepage | [https://www.xiph.org/ogg/](https://www.xiph.org/ogg/) |
| Versions | v1.3.4 |
| Architectures | arm64, x86_64 |
| Definition | [libogg/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libogg/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos libogg
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libogg")
```


### libopus (iphoneos)


| Description | *Modern audio compression for the internet.* |
| -- | -- |
| Homepage | [https://opus-codec.org](https://opus-codec.org) |
| Versions | 1.3.1 |
| Architectures | arm64, x86_64 |
| Definition | [libopus/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libopus/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos libopus
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libopus")
```


### libplist (iphoneos)


| Description | *Library for Apple Binary- and XML-Property Lists* |
| -- | -- |
| Homepage | [https://www.libimobiledevice.org/](https://www.libimobiledevice.org/) |
| License | LGPL-2.1 |
| Versions | 2.2.0 |
| Architectures | arm64, x86_64 |
| Definition | [libplist/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libplist/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos libplist
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libplist")
```


### libpng (iphoneos)


| Description | *The official PNG reference library* |
| -- | -- |
| Homepage | [http://www.libpng.org/pub/png/libpng.html](http://www.libpng.org/pub/png/libpng.html) |
| License | libpng-2.0 |
| Versions | v1.6.34, v1.6.35, v1.6.36, v1.6.37 |
| Architectures | arm64, x86_64 |
| Definition | [libpng/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libpng/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos libpng
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libpng")
```


### libraw (iphoneos)


| Description | *LibRaw is a library for reading RAW files from digital cameras.* |
| -- | -- |
| Homepage | [http://www.libraw.org](http://www.libraw.org) |
| License | LGPL-2.1 |
| Versions | 0.19.5, 0.20.2 |
| Architectures | arm64, x86_64 |
| Definition | [libraw/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libraw/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos libraw
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libraw")
```


### librdkafka (iphoneos)


| Description | *The Apache Kafka C/C++ library* |
| -- | -- |
| Homepage | [https://github.com/edenhill/librdkafka](https://github.com/edenhill/librdkafka) |
| Versions | v1.6.2, v1.8.2-POST2 |
| Architectures | arm64, x86_64 |
| Definition | [librdkafka/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/librdkafka/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos librdkafka
```

##### Integration in the project (xmake.lua)

```lua
add_requires("librdkafka")
```


### libsais (iphoneos)


| Description | *libsais is a library for linear time suffix array, longest common prefix array and burrows wheeler transform construction based on induced sorting algorithm.* |
| -- | -- |
| Homepage | [https://github.com/IlyaGrebnov/libsais](https://github.com/IlyaGrebnov/libsais) |
| License | Apache-2.0 |
| Versions | v2.7.1 |
| Architectures | arm64, x86_64 |
| Definition | [libsais/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libsais/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos libsais
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libsais")
```


### libsdl (iphoneos)


| Description | *Simple DirectMedia Layer* |
| -- | -- |
| Homepage | [https://www.libsdl.org/](https://www.libsdl.org/) |
| License | zlib |
| Versions | 2.0.12, 2.0.14, 2.0.16, 2.0.18, 2.0.20, 2.0.22, 2.0.8, 2.24.0, 2.24.2, 2.26.0, 2.26.1, 2.26.2 |
| Architectures | arm64, x86_64 |
| Definition | [libsdl/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libsdl/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos libsdl
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libsdl")
```


### libsndfile (iphoneos)


| Description | *A C library for reading and writing sound files containing sampled audio data.* |
| -- | -- |
| Homepage | [https://libsndfile.github.io/libsndfile/](https://libsndfile.github.io/libsndfile/) |
| License | LGPL-2.1 |
| Versions | 1.0.31, v1.0.30 |
| Architectures | arm64, x86_64 |
| Definition | [libsndfile/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libsndfile/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos libsndfile
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libsndfile")
```


### libsoundio (iphoneos)


| Description | *C library for cross-platform real-time audio input and output.* |
| -- | -- |
| Homepage | [http://libsound.io/](http://libsound.io/) |
| License | MIT |
| Versions | 2.0.0 |
| Architectures | arm64, x86_64 |
| Definition | [libsoundio/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libsoundio/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos libsoundio
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libsoundio")
```


### libspng (iphoneos)


| Description | *Simple, modern libpng alternative* |
| -- | -- |
| Homepage | [https://libspng.org](https://libspng.org) |
| Versions | v0.7.1 |
| Architectures | arm64, x86_64 |
| Definition | [libspng/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libspng/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos libspng
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libspng")
```


### libsv (iphoneos)


| Description | *libsv - Public domain cross-platform semantic versioning in c99* |
| -- | -- |
| Homepage | [https://github.com/uael/sv](https://github.com/uael/sv) |
| Versions | 2021.11.27 |
| Architectures | arm64, x86_64 |
| Definition | [libsv/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libsv/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos libsv
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libsv")
```


### libsvm (iphoneos)


| Description | *A simple, easy-to-use, and efficient software for SVM classification and regression* |
| -- | -- |
| Homepage | [https://github.com/cjlin1/libsvm](https://github.com/cjlin1/libsvm) |
| Versions | v325 |
| Architectures | arm64, x86_64 |
| Definition | [libsvm/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libsvm/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos libsvm
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libsvm")
```


### libtool (iphoneos)


| Description | *A generic library support script.* |
| -- | -- |
| Homepage | [https://www.gnu.org/software/libtool/](https://www.gnu.org/software/libtool/) |
| Versions | 2.4.5, 2.4.6 |
| Architectures | arm64, x86_64 |
| Definition | [libtool/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libtool/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos libtool
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libtool")
```


### libvorbis (iphoneos)


| Description | *Reference implementation of the Ogg Vorbis audio format.* |
| -- | -- |
| Homepage | [https://xiph.org/vorbis](https://xiph.org/vorbis) |
| License | BSD-3 |
| Versions | 1.3.7 |
| Architectures | arm64, x86_64 |
| Definition | [libvorbis/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libvorbis/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos libvorbis
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libvorbis")
```


### libxml2 (iphoneos)


| Description | *The XML C parser and toolkit of Gnome.* |
| -- | -- |
| Homepage | [http://xmlsoft.org/](http://xmlsoft.org/) |
| License | MIT |
| Versions | 2.9.10, 2.9.12, 2.9.9 |
| Architectures | arm64, x86_64 |
| Definition | [libxml2/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libxml2/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos libxml2
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libxml2")
```


### littlefs (iphoneos)


| Description | *A little fail-safe filesystem designed for microcontrollers* |
| -- | -- |
| Homepage | [https://github.com/littlefs-project/littlefs](https://github.com/littlefs-project/littlefs) |
| Versions | v2.5.0 |
| Architectures | arm64, x86_64 |
| Definition | [littlefs/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/littlefs/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos littlefs
```

##### Integration in the project (xmake.lua)

```lua
add_requires("littlefs")
```


### llfio (iphoneos)


| Description | *UTF8-CPP: UTF-8 with C++ in a Portable Way* |
| -- | -- |
| Homepage | [https://github.com/ned14/llfio](https://github.com/ned14/llfio) |
| License | Apache-2.0 |
| Versions | 2022.9.7 |
| Architectures | arm64, x86_64 |
| Definition | [llfio/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/llfio/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos llfio
```

##### Integration in the project (xmake.lua)

```lua
add_requires("llfio")
```


### llhttp (iphoneos)


| Description | *Port of http_parser to llparse* |
| -- | -- |
| Homepage | [https://github.com/nodejs/llhttp](https://github.com/nodejs/llhttp) |
| License | MIT |
| Versions | v3.0.0 |
| Architectures | arm64, x86_64 |
| Definition | [llhttp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/llhttp/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos llhttp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("llhttp")
```


### llvm (iphoneos)


| Description | *The LLVM Compiler Infrastructure* |
| -- | -- |
| Homepage | [https://llvm.org/](https://llvm.org/) |
| Versions | 11.0.0, 14.0.0 |
| Architectures | arm64, x86_64 |
| Definition | [llvm/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/llvm/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos llvm
```

##### Integration in the project (xmake.lua)

```lua
add_requires("llvm")
```


### llvm-mingw (iphoneos)


| Description | *An LLVM/Clang/LLD based mingw-w64 toolchain* |
| -- | -- |
| Homepage | [https://github.com/mstorsjo/llvm-mingw](https://github.com/mstorsjo/llvm-mingw) |
| Versions | 20211002, 20220323 |
| Architectures | arm64, x86_64 |
| Definition | [llvm-mingw/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/llvm-mingw/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos llvm-mingw
```

##### Integration in the project (xmake.lua)

```lua
add_requires("llvm-mingw")
```


### lodepng (iphoneos)


| Description | *PNG encoder and decoder in C and C++.* |
| -- | -- |
| Homepage | [https://lodev.org/lodepng/](https://lodev.org/lodepng/) |
| License | zlib |
| Versions |  |
| Architectures | arm64, x86_64 |
| Definition | [lodepng/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/lodepng/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos lodepng
```

##### Integration in the project (xmake.lua)

```lua
add_requires("lodepng")
```


### loguru (iphoneos)


| Description | *A lightweight C++ logging library* |
| -- | -- |
| Homepage | [https://github.com/emilk/loguru](https://github.com/emilk/loguru) |
| Versions | v2.1.0 |
| Architectures | arm64, x86_64 |
| Definition | [loguru/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/loguru/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos loguru
```

##### Integration in the project (xmake.lua)

```lua
add_requires("loguru")
```


### lua (iphoneos)


| Description | *A powerful, efficient, lightweight, embeddable scripting language.* |
| -- | -- |
| Homepage | [http://lua.org](http://lua.org) |
| Versions | v5.1.1, v5.1.5, v5.2.3, v5.3.6, v5.4.1, v5.4.2, v5.4.3, v5.4.4 |
| Architectures | arm64, x86_64 |
| Definition | [lua/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/lua/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos lua
```

##### Integration in the project (xmake.lua)

```lua
add_requires("lua")
```


### lua-format (iphoneos)


| Description | *Code formatter for Lua* |
| -- | -- |
| Homepage | [https://github.com/Koihik/LuaFormatter](https://github.com/Koihik/LuaFormatter) |
| Versions | 1.3.5 |
| Architectures | arm64, x86_64 |
| Definition | [lua-format/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/lua-format/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos lua-format
```

##### Integration in the project (xmake.lua)

```lua
add_requires("lua-format")
```


### luajit (iphoneos)


| Description | *A Just-In-Time Compiler (JIT) for the Lua programming language.* |
| -- | -- |
| Homepage | [http://luajit.org](http://luajit.org) |
| Versions | 2.1.0-beta3 |
| Architectures | arm64, x86_64 |
| Definition | [luajit/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/luajit/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos luajit
```

##### Integration in the project (xmake.lua)

```lua
add_requires("luajit")
```


### lvgl (iphoneos)


| Description | *Light and Versatile Graphics Library* |
| -- | -- |
| Homepage | [https://lvgl.io](https://lvgl.io) |
| License | MIT |
| Versions | v8.0.2, v8.2.0 |
| Architectures | arm64, x86_64 |
| Definition | [lvgl/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/lvgl/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos lvgl
```

##### Integration in the project (xmake.lua)

```lua
add_requires("lvgl")
```


### lyra (iphoneos)


| Description | *A simple to use, composable, command line parser for C++ 11 and beyond* |
| -- | -- |
| Homepage | [https://www.bfgroup.xyz/Lyra/](https://www.bfgroup.xyz/Lyra/) |
| License | BSL-1.0 |
| Versions | 1.5.1, 1.6 |
| Architectures | arm64, x86_64 |
| Definition | [lyra/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/lyra/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos lyra
```

##### Integration in the project (xmake.lua)

```lua
add_requires("lyra")
```


### lz4 (iphoneos)


| Description | *LZ4 - Extremely fast compression* |
| -- | -- |
| Homepage | [https://www.lz4.org/](https://www.lz4.org/) |
| Versions | v1.9.3 |
| Architectures | arm64, x86_64 |
| Definition | [lz4/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/lz4/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos lz4
```

##### Integration in the project (xmake.lua)

```lua
add_requires("lz4")
```


### lzo (iphoneos)


| Description | *LZO is a portable lossless data compression library written in ANSI C.* |
| -- | -- |
| Homepage | [http://www.oberhumer.com/opensource/lzo](http://www.oberhumer.com/opensource/lzo) |
| License | GPL-2.0 |
| Versions | 2.10 |
| Architectures | arm64, x86_64 |
| Definition | [lzo/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/lzo/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos lzo
```

##### Integration in the project (xmake.lua)

```lua
add_requires("lzo")
```



## m
### m4 (iphoneos)


| Description | *Macro processing language* |
| -- | -- |
| Homepage | [https://www.gnu.org/software/m4](https://www.gnu.org/software/m4) |
| Versions | 1.4.18, 1.4.19 |
| Architectures | arm64, x86_64 |
| Definition | [m4/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/m4/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos m4
```

##### Integration in the project (xmake.lua)

```lua
add_requires("m4")
```


### magic_enum (iphoneos)


| Description | *Static reflection for enums (to string, from string, iteration) for modern C++, work with any enum type without any macro or boilerplate code* |
| -- | -- |
| Homepage | [https://github.com/Neargye/magic_enum](https://github.com/Neargye/magic_enum) |
| License | MIT |
| Versions | v0.7.3, v0.8.0, v0.8.1 |
| Architectures | arm64, x86_64 |
| Definition | [magic_enum/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/magic_enum/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos magic_enum
```

##### Integration in the project (xmake.lua)

```lua
add_requires("magic_enum")
```


### make (iphoneos)


| Description | *GNU make tool.* |
| -- | -- |
| Homepage | [https://www.gnu.org/software/make/](https://www.gnu.org/software/make/) |
| Versions | 4.2.1, 4.3 |
| Architectures | arm64, x86_64 |
| Definition | [make/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/make/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos make
```

##### Integration in the project (xmake.lua)

```lua
add_requires("make")
```


### mapbox_earcut (iphoneos)


| Description | *A C++ port of earcut.js, a fast, header-only polygon triangulation library.* |
| -- | -- |
| Homepage | [https://github.com/mapbox/earcut.hpp](https://github.com/mapbox/earcut.hpp) |
| License | ISC |
| Versions | 2.2.3 |
| Architectures | arm64, x86_64 |
| Definition | [mapbox_earcut/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/mapbox_earcut/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos mapbox_earcut
```

##### Integration in the project (xmake.lua)

```lua
add_requires("mapbox_earcut")
```


### mapbox_eternal (iphoneos)


| Description | *A C++14 compile-time/constexpr map and hash map with minimal binary footprint* |
| -- | -- |
| Homepage | [https://github.com/mapbox/eternal](https://github.com/mapbox/eternal) |
| License | ISC |
| Versions | v1.0.1 |
| Architectures | arm64, x86_64 |
| Definition | [mapbox_eternal/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/mapbox_eternal/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos mapbox_eternal
```

##### Integration in the project (xmake.lua)

```lua
add_requires("mapbox_eternal")
```


### mapbox_geometry (iphoneos)


| Description | *Provides header-only, generic C++ interfaces for geometry types, geometry collections, and features.* |
| -- | -- |
| Homepage | [https://github.com/mapbox/geometry.hpp](https://github.com/mapbox/geometry.hpp) |
| License | ISC |
| Versions | 1.1.0, 2.0.3 |
| Architectures | arm64, x86_64 |
| Definition | [mapbox_geometry/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/mapbox_geometry/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos mapbox_geometry
```

##### Integration in the project (xmake.lua)

```lua
add_requires("mapbox_geometry")
```


### mapbox_variant (iphoneos)


| Description | *C++11/C++14 Variant* |
| -- | -- |
| Homepage | [https://github.com/mapbox/variant](https://github.com/mapbox/variant) |
| License | BSD |
| Versions | v1.2.0 |
| Architectures | arm64, x86_64 |
| Definition | [mapbox_variant/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/mapbox_variant/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos mapbox_variant
```

##### Integration in the project (xmake.lua)

```lua
add_requires("mapbox_variant")
```


### mathfu (iphoneos)


| Description | *C++ math library developed primarily for games focused on simplicity and efficiency.* |
| -- | -- |
| Homepage | [http://google.github.io/mathfu](http://google.github.io/mathfu) |
| License | Apache-2.0 |
| Versions | 2022.5.10 |
| Architectures | arm64, x86_64 |
| Definition | [mathfu/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/mathfu/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos mathfu
```

##### Integration in the project (xmake.lua)

```lua
add_requires("mathfu")
```


### mbedtls (iphoneos)


| Description | *An SSL library* |
| -- | -- |
| Homepage | [https://tls.mbed.org](https://tls.mbed.org) |
| Versions | 2.13.0, 2.25.0, 2.7.6 |
| Architectures | arm64, x86_64 |
| Definition | [mbedtls/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/mbedtls/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos mbedtls
```

##### Integration in the project (xmake.lua)

```lua
add_requires("mbedtls")
```


### memorymapping (iphoneos)


| Description | *fmemopen port library* |
| -- | -- |
| Homepage | [https://github.com/NimbusKit/memorymapping](https://github.com/NimbusKit/memorymapping) |
| Versions | 2014.12.21 |
| Architectures | arm64, x86_64 |
| Definition | [memorymapping/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/memorymapping/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos memorymapping
```

##### Integration in the project (xmake.lua)

```lua
add_requires("memorymapping")
```


### meson (iphoneos)


| Description | *Fast and user friendly build system.* |
| -- | -- |
| Homepage | [https://mesonbuild.com/](https://mesonbuild.com/) |
| License | Apache-2.0 |
| Versions | 0.50.1, 0.56.0, 0.58.0, 0.58.1, 0.59.1, 0.59.2, 0.60.1, 0.61.2, 0.62.1 |
| Architectures | arm64, x86_64 |
| Definition | [meson/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/meson/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos meson
```

##### Integration in the project (xmake.lua)

```lua
add_requires("meson")
```


### microsoft-gsl (iphoneos)


| Description | *Guidelines Support Library* |
| -- | -- |
| Homepage | [https://github.com/microsoft/GSL](https://github.com/microsoft/GSL) |
| License | MIT |
| Versions | v3.1.0, v4.0.0 |
| Architectures | arm64, x86_64 |
| Definition | [microsoft-gsl/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/microsoft-gsl/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos microsoft-gsl
```

##### Integration in the project (xmake.lua)

```lua
add_requires("microsoft-gsl")
```


### mikktspace (iphoneos)


| Description | *A common standard for tangent space used in baking tools to produce normal maps.* |
| -- | -- |
| Homepage | [http://www.mikktspace.com/](http://www.mikktspace.com/) |
| Versions | 2020.03.26 |
| Architectures | arm64, x86_64 |
| Definition | [mikktspace/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/mikktspace/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos mikktspace
```

##### Integration in the project (xmake.lua)

```lua
add_requires("mikktspace")
```


### miniaudio (iphoneos)


| Description | *Single file audio playback and capture library written in C.* |
| -- | -- |
| Homepage | [https://miniaud.io](https://miniaud.io) |
| Versions | 2021.12.31 |
| Architectures | arm64, x86_64 |
| Definition | [miniaudio/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/miniaudio/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos miniaudio
```

##### Integration in the project (xmake.lua)

```lua
add_requires("miniaudio")
```


### minilzo (iphoneos)


| Description | *A very lightweight subset of the LZO library intended for easy inclusion with your application* |
| -- | -- |
| Homepage | [http://www.oberhumer.com/opensource/lzo/#minilzo](http://www.oberhumer.com/opensource/lzo/#minilzo) |
| Versions | 2.10 |
| Architectures | arm64, x86_64 |
| Definition | [minilzo/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/minilzo/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos minilzo
```

##### Integration in the project (xmake.lua)

```lua
add_requires("minilzo")
```


### minimp3 (iphoneos)


| Description | *Minimalistic MP3 decoder single header library* |
| -- | -- |
| Homepage | [https://github.com/lieff/minimp3](https://github.com/lieff/minimp3) |
| License | CC0 |
| Versions | 2021.05.29 |
| Architectures | arm64, x86_64 |
| Definition | [minimp3/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/minimp3/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos minimp3
```

##### Integration in the project (xmake.lua)

```lua
add_requires("minimp3")
```


### miniz (iphoneos)


| Description | *miniz: Single C source file zlib-replacement library* |
| -- | -- |
| Homepage | [https://github.com/richgel999/miniz/](https://github.com/richgel999/miniz/) |
| License | MIT |
| Versions | 2.1.0, 2.2.0 |
| Architectures | arm64, x86_64 |
| Definition | [miniz/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/miniz/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos miniz
```

##### Integration in the project (xmake.lua)

```lua
add_requires("miniz")
```


### minizip (iphoneos)


| Description | *Mini zip and unzip based on zlib* |
| -- | -- |
| Homepage | [https://www.zlib.net/](https://www.zlib.net/) |
| License | zlib |
| Versions | v1.2.10, v1.2.11, v1.2.12 |
| Architectures | arm64, x86_64 |
| Definition | [minizip/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/minizip/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos minizip
```

##### Integration in the project (xmake.lua)

```lua
add_requires("minizip")
```


### mio (iphoneos)


| Description | *Cross-platform C++11 header-only library for memory mapped file IO* |
| -- | -- |
| Homepage | [https://github.com/mandreyel/mio](https://github.com/mandreyel/mio) |
| License | MIT |
| Versions | 2021.9.21 |
| Architectures | arm64, x86_64 |
| Definition | [mio/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/mio/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos mio
```

##### Integration in the project (xmake.lua)

```lua
add_requires("mio")
```


### mjson (iphoneos)


| Description | *C/C++ JSON parser, emitter, JSON-RPC engine for embedded systems* |
| -- | -- |
| Homepage | [https://github.com/cesanta/mjson](https://github.com/cesanta/mjson) |
| License | MIT |
| Versions | 1.2.6 |
| Architectures | arm64, x86_64 |
| Definition | [mjson/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/mjson/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos mjson
```

##### Integration in the project (xmake.lua)

```lua
add_requires("mjson")
```


### mma (iphoneos)


| Description | *A self-contained C++ implementation of MMA and GCMMA.* |
| -- | -- |
| Homepage | [https://github.com/jdumas/mma](https://github.com/jdumas/mma) |
| License | MIT |
| Versions | 2018.08.01 |
| Architectures | arm64, x86_64 |
| Definition | [mma/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/mma/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos mma
```

##### Integration in the project (xmake.lua)

```lua
add_requires("mma")
```


### moonjit (iphoneos)


| Description | *A Just-In-Time Compiler (JIT) for the Lua programming language.* |
| -- | -- |
| Homepage | [https://github.com/moonjit/moonjit](https://github.com/moonjit/moonjit) |
| Versions | 2.2.0 |
| Architectures | arm64, x86_64 |
| Definition | [moonjit/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/moonjit/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos moonjit
```

##### Integration in the project (xmake.lua)

```lua
add_requires("moonjit")
```


### mpmcqueue (iphoneos)


| Description | *A bounded multi-producer multi-consumer concurrent queue written in C++11* |
| -- | -- |
| Homepage | [https://github.com/rigtorp/MPMCQueue](https://github.com/rigtorp/MPMCQueue) |
| Versions | v1.0 |
| Architectures | arm64, x86_64 |
| Definition | [mpmcqueue/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/mpmcqueue/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos mpmcqueue
```

##### Integration in the project (xmake.lua)

```lua
add_requires("mpmcqueue")
```


### muslcc (iphoneos)


| Description | *static cross- and native- musl-based toolchains.* |
| -- | -- |
| Homepage | [https://musl.cc/](https://musl.cc/) |
| Versions | 20210202 |
| Architectures | arm64, x86_64 |
| Definition | [muslcc/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/muslcc/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos muslcc
```

##### Integration in the project (xmake.lua)

```lua
add_requires("muslcc")
```



## n
### named_type (iphoneos)


| Description | *Implementation of strong types in C++.* |
| -- | -- |
| Homepage | [https://github.com/joboccara/NamedType](https://github.com/joboccara/NamedType) |
| License | MIT |
| Versions | v1.1.0.20210209 |
| Architectures | arm64, x86_64 |
| Definition | [named_type/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/named_type/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos named_type
```

##### Integration in the project (xmake.lua)

```lua
add_requires("named_type")
```


### nanoflann (iphoneos)


| Description | *nanoflann: a C++11 header-only library for Nearest Neighbor (NN) search with KD-trees* |
| -- | -- |
| Homepage | [https://github.com/jlblancoc/nanoflann/](https://github.com/jlblancoc/nanoflann/) |
| License | BSD-2-Clause |
| Versions | v1.3.2, v1.4.2 |
| Architectures | arm64, x86_64 |
| Definition | [nanoflann/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/nanoflann/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos nanoflann
```

##### Integration in the project (xmake.lua)

```lua
add_requires("nanoflann")
```


### nanosvg (iphoneos)


| Description | *Simple stupid SVG parser* |
| -- | -- |
| Homepage | [https://github.com/memononen/nanosvg](https://github.com/memononen/nanosvg) |
| License | zlib |
| Versions | 2022.07.09 |
| Architectures | arm64, x86_64 |
| Definition | [nanosvg/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/nanosvg/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos nanosvg
```

##### Integration in the project (xmake.lua)

```lua
add_requires("nanosvg")
```


### nasm (iphoneos)


| Description | *Netwide Assembler (NASM) is an 80x86 assembler.* |
| -- | -- |
| Homepage | [https://www.nasm.us/](https://www.nasm.us/) |
| License | BSD-2-Clause |
| Versions | 2.13.03, 2.15.05 |
| Architectures | arm64, x86_64 |
| Definition | [nasm/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/nasm/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos nasm
```

##### Integration in the project (xmake.lua)

```lua
add_requires("nasm")
```


### ndk (iphoneos)


| Description | *Android NDK toolchain.* |
| -- | -- |
| Homepage | [https://developer.android.com/ndk](https://developer.android.com/ndk) |
| Versions | 21.0, 22.0 |
| Architectures | arm64, x86_64 |
| Definition | [ndk/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/ndk/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos ndk
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ndk")
```


### niftiheader (iphoneos)


| Description | *Header structure descriptions for the nifti1 and nifti2 file formats.* |
| -- | -- |
| Homepage | [https://nifti.nimh.nih.gov/](https://nifti.nimh.nih.gov/) |
| License | Public Domain |
| Versions | 0.0.1 |
| Architectures | arm64, x86_64 |
| Definition | [niftiheader/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/niftiheader/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos niftiheader
```

##### Integration in the project (xmake.lua)

```lua
add_requires("niftiheader")
```


### ninja (iphoneos)


| Description | *Small build system for use with gyp or CMake.* |
| -- | -- |
| Homepage | [https://ninja-build.org/](https://ninja-build.org/) |
| Versions | 1.10.1, 1.10.2, 1.11.0, 1.11.1, 1.9.0 |
| Architectures | arm64, x86_64 |
| Definition | [ninja/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/ninja/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos ninja
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ninja")
```


### nlohmann_json (iphoneos)


| Description | *JSON for Modern C++* |
| -- | -- |
| Homepage | [https://nlohmann.github.io/json/](https://nlohmann.github.io/json/) |
| License | MIT |
| Versions | v3.10.0, v3.10.5, v3.11.2, v3.9.1 |
| Architectures | arm64, x86_64 |
| Definition | [nlohmann_json/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/nlohmann_json/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos nlohmann_json
```

##### Integration in the project (xmake.lua)

```lua
add_requires("nlohmann_json")
```


### nng (iphoneos)


| Description | *NNG, like its predecessors nanomsg (and to some extent ZeroMQ), is a lightweight, broker-less library, offering a simple API to solve common recurring messaging problems.* |
| -- | -- |
| Homepage | [https://github.com/nanomsg/nng](https://github.com/nanomsg/nng) |
| Versions | 1.3.2, 1.4.0, 1.5.2 |
| Architectures | arm64, x86_64 |
| Definition | [nng/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/nng/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos nng
```

##### Integration in the project (xmake.lua)

```lua
add_requires("nng")
```


### nngpp (iphoneos)


| Description | *C++ wrapper around the nanomsg NNG API.* |
| -- | -- |
| Homepage | [https://github.com/cwzx/nngpp](https://github.com/cwzx/nngpp) |
| Versions | v2020.10.30 |
| Architectures | arm64, x86_64 |
| Definition | [nngpp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/nngpp/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos nngpp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("nngpp")
```


### nod (iphoneos)


| Description | *Small, header only signals and slots C++11 library.* |
| -- | -- |
| Homepage | [https://github.com/fr00b0/nod](https://github.com/fr00b0/nod) |
| License | MIT |
| Versions | v0.5.4 |
| Architectures | arm64, x86_64 |
| Definition | [nod/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/nod/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos nod
```

##### Integration in the project (xmake.lua)

```lua
add_requires("nod")
```


### nodesoup (iphoneos)


| Description | *Force-directed graph layout with Fruchterman-Reingold* |
| -- | -- |
| Homepage | [https://github.com/olvb/nodesoup](https://github.com/olvb/nodesoup) |
| Versions | 2020.09.05 |
| Architectures | arm64, x86_64 |
| Definition | [nodesoup/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/nodesoup/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos nodesoup
```

##### Integration in the project (xmake.lua)

```lua
add_requires("nodesoup")
```


### ntkernel-error-category (iphoneos)


| Description | *A C++ 11 std::error_category for the NT kernel's NTSTATUS error codes * |
| -- | -- |
| Homepage | [https://github.com/ned14/ntkernel-error-category](https://github.com/ned14/ntkernel-error-category) |
| License | Apache-2.0 |
| Versions | v1.0.0 |
| Architectures | arm64, x86_64 |
| Definition | [ntkernel-error-category/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/ntkernel-error-category/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos ntkernel-error-category
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ntkernel-error-category")
```



## o
### olive.c (iphoneos)


| Description | *Simple 2D Graphics Library for C* |
| -- | -- |
| Homepage | [https://tsoding.github.io/olive.c/](https://tsoding.github.io/olive.c/) |
| License | MIT |
| Versions | 2022.12.14 |
| Architectures | arm64, x86_64 |
| Definition | [olive.c/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/olive.c/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos olive.c
```

##### Integration in the project (xmake.lua)

```lua
add_requires("olive.c")
```


### openal-soft (iphoneos)


| Description | *OpenAL Soft is a software implementation of the OpenAL 3D audio API.* |
| -- | -- |
| Homepage | [https://openal-soft.org](https://openal-soft.org) |
| License | LGPL-2.0 |
| Versions | 1.21.1, 1.22.0, 1.22.2 |
| Architectures | arm64, x86_64 |
| Definition | [openal-soft/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/openal-soft/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos openal-soft
```

##### Integration in the project (xmake.lua)

```lua
add_requires("openal-soft")
```


### opencl-clhpp (iphoneos)


| Description | *OpenCL API C++ bindings* |
| -- | -- |
| Homepage | [https://github.com/KhronosGroup/OpenCL-CLHPP/](https://github.com/KhronosGroup/OpenCL-CLHPP/) |
| License | Apache-2.0 |
| Versions | 1.2.8, 2.0.15 |
| Architectures | arm64, x86_64 |
| Definition | [opencl-clhpp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/opencl-clhpp/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos opencl-clhpp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("opencl-clhpp")
```


### opencl-headers (iphoneos)


| Description | *Khronos OpenCL-Headers* |
| -- | -- |
| Homepage | [https://github.com/KhronosGroup/OpenCL-Headers/](https://github.com/KhronosGroup/OpenCL-Headers/) |
| License | Apache-2.0 |
| Versions | v2021.06.30 |
| Architectures | arm64, x86_64 |
| Definition | [opencl-headers/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/opencl-headers/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos opencl-headers
```

##### Integration in the project (xmake.lua)

```lua
add_requires("opencl-headers")
```


### openrestry-luajit (iphoneos)


| Description | *OpenResty's Branch of LuaJIT 2* |
| -- | -- |
| Homepage | [https://github.com/openresty/luajit2](https://github.com/openresty/luajit2) |
| Versions | v2.1-20220310 |
| Architectures | arm64, x86_64 |
| Definition | [openrestry-luajit/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/openrestry-luajit/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos openrestry-luajit
```

##### Integration in the project (xmake.lua)

```lua
add_requires("openrestry-luajit")
```


### ordered_map (iphoneos)


| Description | *C++ hash map and hash set which preserve the order of insertion* |
| -- | -- |
| Homepage | [https://github.com/Tessil/ordered-map](https://github.com/Tessil/ordered-map) |
| License | MIT |
| Versions | v1.0.0 |
| Architectures | arm64, x86_64 |
| Definition | [ordered_map/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/ordered_map/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos ordered_map
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ordered_map")
```


### out_ptr (iphoneos)


| Description | *Repository for a C++11 implementation of std::out_ptr (p1132), as a standalone library!* |
| -- | -- |
| Homepage | [https://github.com/soasis/out_ptr](https://github.com/soasis/out_ptr) |
| License | Apache-2.0 |
| Versions | 2022.10.07 |
| Architectures | arm64, x86_64 |
| Definition | [out_ptr/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/out_ptr/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos out_ptr
```

##### Integration in the project (xmake.lua)

```lua
add_requires("out_ptr")
```


### outcome (iphoneos)


| Description | *Provides very lightweight outcome<T> and result<T> (non-Boost edition)* |
| -- | -- |
| Homepage | [https://github.com/ned14/outcome](https://github.com/ned14/outcome) |
| License | Apache-2.0 |
| Versions | v2.2.4 |
| Architectures | arm64, x86_64 |
| Definition | [outcome/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/outcome/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos outcome
```

##### Integration in the project (xmake.lua)

```lua
add_requires("outcome")
```



## p
### parallel-hashmap (iphoneos)


| Description | *A family of header-only, very fast and memory-friendly hashmap and btree containers.* |
| -- | -- |
| Homepage | [https://greg7mdp.github.io/parallel-hashmap/](https://greg7mdp.github.io/parallel-hashmap/) |
| License | Apache-2.0 |
| Versions | 1.33, 1.34, 1.35 |
| Architectures | arm64, x86_64 |
| Definition | [parallel-hashmap/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/parallel-hashmap/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos parallel-hashmap
```

##### Integration in the project (xmake.lua)

```lua
add_requires("parallel-hashmap")
```


### patch (iphoneos)


| Description | *GNU patch, which applies diff files to original files.* |
| -- | -- |
| Homepage | [http://www.gnu.org/software/patch/patch.html](http://www.gnu.org/software/patch/patch.html) |
| Versions | 2.7.6 |
| Architectures | arm64, x86_64 |
| Definition | [patch/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/patch/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos patch
```

##### Integration in the project (xmake.lua)

```lua
add_requires("patch")
```


### pcre2 (iphoneos)


| Description | *A Perl Compatible Regular Expressions Library* |
| -- | -- |
| Homepage | [https://www.pcre.org/](https://www.pcre.org/) |
| Versions | 10.39, 10.40 |
| Architectures | arm64, x86_64 |
| Definition | [pcre2/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/pcre2/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos pcre2
```

##### Integration in the project (xmake.lua)

```lua
add_requires("pcre2")
```


### picojson (iphoneos)


| Description | *A header-file-only, JSON parser serializer in C++* |
| -- | -- |
| Homepage | [https://pocoproject.org/](https://pocoproject.org/) |
| License | BSD-2-Clause |
| Versions | v1.3.0 |
| Architectures | arm64, x86_64 |
| Definition | [picojson/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/picojson/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos picojson
```

##### Integration in the project (xmake.lua)

```lua
add_requires("picojson")
```


### piex (iphoneos)


| Description | *Preview Image Extractor (PIEX)* |
| -- | -- |
| Homepage | [https://github.com/google/piex](https://github.com/google/piex) |
| License | Apache-2.0 |
| Versions | 20190530 |
| Architectures | arm64, x86_64 |
| Definition | [piex/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/piex/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos piex
```

##### Integration in the project (xmake.lua)

```lua
add_requires("piex")
```


### pkg-config (iphoneos)


| Description | *A helper tool used when compiling applications and libraries.* |
| -- | -- |
| Homepage | [https://freedesktop.org/wiki/Software/pkg-config/](https://freedesktop.org/wiki/Software/pkg-config/) |
| Versions | 0.29.2 |
| Architectures | arm64, x86_64 |
| Definition | [pkg-config/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/pkg-config/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos pkg-config
```

##### Integration in the project (xmake.lua)

```lua
add_requires("pkg-config")
```


### pkgconf (iphoneos)


| Description | *A program which helps to configure compiler and linker flags for development frameworks.* |
| -- | -- |
| Homepage | [http://pkgconf.org](http://pkgconf.org) |
| Versions | 1.7.4, 1.8.0, 1.9.3 |
| Architectures | arm64, x86_64 |
| Definition | [pkgconf/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/pkgconf/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos pkgconf
```

##### Integration in the project (xmake.lua)

```lua
add_requires("pkgconf")
```


### pprint (iphoneos)


| Description | *Pretty Printer for Modern C++* |
| -- | -- |
| Homepage | [https://github.com/p-ranav/pprint](https://github.com/p-ranav/pprint) |
| Versions | 2020.2.20 |
| Architectures | arm64, x86_64 |
| Definition | [pprint/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/pprint/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos pprint
```

##### Integration in the project (xmake.lua)

```lua
add_requires("pprint")
```


### pqp (iphoneos)


| Description | *A Proximity Query Package* |
| -- | -- |
| Homepage | [http://gamma.cs.unc.edu/SSV/](http://gamma.cs.unc.edu/SSV/) |
| Versions | 1.3 |
| Architectures | arm64, x86_64 |
| Definition | [pqp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/pqp/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos pqp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("pqp")
```


### premake5 (iphoneos)


| Description | *Premake - Powerfully simple build configuration* |
| -- | -- |
| Homepage | [https://premake.github.io/](https://premake.github.io/) |
| Versions | 2022.11.17 |
| Architectures | arm64, x86_64 |
| Definition | [premake5/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/premake5/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos premake5
```

##### Integration in the project (xmake.lua)

```lua
add_requires("premake5")
```


### protoc (iphoneos)


| Description | *Google's data interchange format compiler* |
| -- | -- |
| Homepage | [https://developers.google.com/protocol-buffers/](https://developers.google.com/protocol-buffers/) |
| Versions | 3.8.0 |
| Architectures | arm64, x86_64 |
| Definition | [protoc/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/protoc/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos protoc
```

##### Integration in the project (xmake.lua)

```lua
add_requires("protoc")
```


### prvhash (iphoneos)


| Description | *PRVHASH - Pseudo-Random-Value Hash* |
| -- | -- |
| Homepage | [https://github.com/avaneev/prvhash](https://github.com/avaneev/prvhash) |
| License | MIT |
| Versions | 4.0 |
| Architectures | arm64, x86_64 |
| Definition | [prvhash/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/prvhash/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos prvhash
```

##### Integration in the project (xmake.lua)

```lua
add_requires("prvhash")
```


### pystring (iphoneos)


| Description | *Pystring is a collection of C++ functions which match the interface and behavior of python's string class methods using std::string.* |
| -- | -- |
| Homepage | [https://github.com/imageworks/pystring](https://github.com/imageworks/pystring) |
| Versions | 2020.02.04 |
| Architectures | arm64, x86_64 |
| Definition | [pystring/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/pystring/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos pystring
```

##### Integration in the project (xmake.lua)

```lua
add_requires("pystring")
```


### python (iphoneos)


| Description | *The python programming language.* |
| -- | -- |
| Homepage | [https://www.python.org/](https://www.python.org/) |
| Versions | 2.7.18, 3.10.6, 3.7.9, 3.8.10, 3.9.10, 3.9.13, 3.9.5, 3.9.6 |
| Architectures | arm64, x86_64 |
| Definition | [python/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/python/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos python
```

##### Integration in the project (xmake.lua)

```lua
add_requires("python")
```


### python2 (iphoneos)


| Description | *The python programming language.* |
| -- | -- |
| Homepage | [https://www.python.org/](https://www.python.org/) |
| Versions | 2.7.15, 2.7.18 |
| Architectures | arm64, x86_64 |
| Definition | [python2/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/python2/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos python2
```

##### Integration in the project (xmake.lua)

```lua
add_requires("python2")
```



## q
### qoi (iphoneos)


| Description | *The Quite OK Image Format for fast, lossless image compression* |
| -- | -- |
| Homepage | [https://qoiformat.org/](https://qoiformat.org/) |
| License | MIT |
| Versions | 2021.12.22, 2022.11.17 |
| Architectures | arm64, x86_64 |
| Definition | [qoi/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/q/qoi/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos qoi
```

##### Integration in the project (xmake.lua)

```lua
add_requires("qoi")
```


### qt5base (iphoneos)


| Description | *Qt is the faster, smarter way to create innovative devices, modern UIs & applications for multiple screens. Cross-platform software development at its best.* |
| -- | -- |
| Homepage | [https://www.qt.io](https://www.qt.io) |
| License | LGPL-3 |
| Versions | 5.12.5, 5.15.2 |
| Architectures | arm64, x86_64 |
| Definition | [qt5base/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/q/qt5base/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos qt5base
```

##### Integration in the project (xmake.lua)

```lua
add_requires("qt5base")
```


### qt5core (iphoneos)


| Description | *Qt is the faster, smarter way to create innovative devices, modern UIs & applications for multiple screens. Cross-platform software development at its best.* |
| -- | -- |
| Homepage | [https://www.qt.io](https://www.qt.io) |
| License | LGPL-3 |
| Versions | 5.12.5, 5.15.2 |
| Architectures | arm64, x86_64 |
| Definition | [qt5core/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/q/qt5core/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos qt5core
```

##### Integration in the project (xmake.lua)

```lua
add_requires("qt5core")
```


### qt5gui (iphoneos)


| Description | *Qt is the faster, smarter way to create innovative devices, modern UIs & applications for multiple screens. Cross-platform software development at its best.* |
| -- | -- |
| Homepage | [https://www.qt.io](https://www.qt.io) |
| License | LGPL-3 |
| Versions | 5.12.5, 5.15.2 |
| Architectures | arm64, x86_64 |
| Definition | [qt5gui/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/q/qt5gui/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos qt5gui
```

##### Integration in the project (xmake.lua)

```lua
add_requires("qt5gui")
```


### qt5lib (iphoneos)


| Description | *Qt is the faster, smarter way to create innovative devices, modern UIs & applications for multiple screens. Cross-platform software development at its best.* |
| -- | -- |
| Homepage | [https://www.qt.io](https://www.qt.io) |
| License | LGPL-3 |
| Versions | 5.12.5, 5.15.2 |
| Architectures | arm64, x86_64 |
| Definition | [qt5lib/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/q/qt5lib/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos qt5lib
```

##### Integration in the project (xmake.lua)

```lua
add_requires("qt5lib")
```


### qt5network (iphoneos)


| Description | *Qt is the faster, smarter way to create innovative devices, modern UIs & applications for multiple screens. Cross-platform software development at its best.* |
| -- | -- |
| Homepage | [https://www.qt.io](https://www.qt.io) |
| License | LGPL-3 |
| Versions | 5.12.5, 5.15.2 |
| Architectures | arm64, x86_64 |
| Definition | [qt5network/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/q/qt5network/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos qt5network
```

##### Integration in the project (xmake.lua)

```lua
add_requires("qt5network")
```


### qt5widgets (iphoneos)


| Description | *Qt is the faster, smarter way to create innovative devices, modern UIs & applications for multiple screens. Cross-platform software development at its best.* |
| -- | -- |
| Homepage | [https://www.qt.io](https://www.qt.io) |
| License | LGPL-3 |
| Versions | 5.12.5, 5.15.2 |
| Architectures | arm64, x86_64 |
| Definition | [qt5widgets/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/q/qt5widgets/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos qt5widgets
```

##### Integration in the project (xmake.lua)

```lua
add_requires("qt5widgets")
```


### quickcpplib (iphoneos)


| Description | *Eliminate all the tedious hassle when making state-of-the-art C++ 14 - 23 libraries!* |
| -- | -- |
| Homepage | [https://github.com/ned14/quickcpplib](https://github.com/ned14/quickcpplib) |
| License | Apache-2.0 |
| Versions | 20221116 |
| Architectures | arm64, x86_64 |
| Definition | [quickcpplib/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/q/quickcpplib/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos quickcpplib
```

##### Integration in the project (xmake.lua)

```lua
add_requires("quickcpplib")
```


### quickjs (iphoneos)


| Description | *QuickJS is a small and embeddable Javascript engine* |
| -- | -- |
| Homepage | [https://bellard.org/quickjs/](https://bellard.org/quickjs/) |
| Versions | 2021.03.27 |
| Architectures | arm64, x86_64 |
| Definition | [quickjs/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/q/quickjs/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos quickjs
```

##### Integration in the project (xmake.lua)

```lua
add_requires("quickjs")
```



## r
### range-v3 (iphoneos)


| Description | *Range library for C++14/17/20, basis for C++20's std::ranges* |
| -- | -- |
| Homepage | [https://github.com/ericniebler/range-v3/](https://github.com/ericniebler/range-v3/) |
| License | BSL-1.0 |
| Versions | 0.11.0, 0.12.0 |
| Architectures | arm64, x86_64 |
| Definition | [range-v3/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/range-v3/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos range-v3
```

##### Integration in the project (xmake.lua)

```lua
add_requires("range-v3")
```


### rapidcsv (iphoneos)


| Description | *C++ header-only library for CSV parsing (by d99kris)* |
| -- | -- |
| Homepage | [https://github.com/d99kris/rapidcsv](https://github.com/d99kris/rapidcsv) |
| Versions | 8.50 |
| Architectures | arm64, x86_64 |
| Definition | [rapidcsv/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/rapidcsv/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos rapidcsv
```

##### Integration in the project (xmake.lua)

```lua
add_requires("rapidcsv")
```


### rapidjson (iphoneos)


| Description | *RapidJSON is a JSON parser and generator for C++.* |
| -- | -- |
| Homepage | [https://github.com/Tencent/rapidjson](https://github.com/Tencent/rapidjson) |
| Versions | 2022.7.20, v1.1.0, v1.1.0-arrow |
| Architectures | arm64, x86_64 |
| Definition | [rapidjson/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/rapidjson/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos rapidjson
```

##### Integration in the project (xmake.lua)

```lua
add_requires("rapidjson")
```


### re2 (iphoneos)


| Description | *RE2 is a fast, safe, thread-friendly alternative to backtracking regular expression engines like those used in PCRE, Perl, and Python. It is a C++ library.* |
| -- | -- |
| Homepage | [https://github.com/google/re2](https://github.com/google/re2) |
| License | BSD-3-Clause |
| Versions | 2020.11.01, 2021.06.01, 2021.08.01, 2021.11.01, 2022.02.01 |
| Architectures | arm64, x86_64 |
| Definition | [re2/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/re2/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos re2
```

##### Integration in the project (xmake.lua)

```lua
add_requires("re2")
```


### readerwriterqueue (iphoneos)


| Description | *A fast single-producer, single-consumer lock-free queue for C++* |
| -- | -- |
| Homepage | [https://github.com/cameron314/readerwriterqueue](https://github.com/cameron314/readerwriterqueue) |
| License | BSD-3-Clause |
| Versions | v1.0.6 |
| Architectures | arm64, x86_64 |
| Definition | [readerwriterqueue/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/readerwriterqueue/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos readerwriterqueue
```

##### Integration in the project (xmake.lua)

```lua
add_requires("readerwriterqueue")
```


### recastnavigation (iphoneos)


| Description | *Navigation-mesh Toolset for Games* |
| -- | -- |
| Homepage | [https://github.com/recastnavigation/recastnavigation](https://github.com/recastnavigation/recastnavigation) |
| License | zlib |
| Versions | 1.5.1 |
| Architectures | arm64, x86_64 |
| Definition | [recastnavigation/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/recastnavigation/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos recastnavigation
```

##### Integration in the project (xmake.lua)

```lua
add_requires("recastnavigation")
```


### reproc (iphoneos)


| Description | *a cross-platform C/C++ library that simplifies starting, stopping and communicating with external programs.* |
| -- | -- |
| Homepage | [https://github.com/DaanDeMeyer/reproc](https://github.com/DaanDeMeyer/reproc) |
| License | MIT |
| Versions | v14.2.4 |
| Architectures | arm64, x86_64 |
| Definition | [reproc/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/reproc/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos reproc
```

##### Integration in the project (xmake.lua)

```lua
add_requires("reproc")
```


### robin-hood-hashing (iphoneos)


| Description | *Fast & memory efficient hashtable based on robin hood hashing for C++11/14/17/20* |
| -- | -- |
| Homepage | [https://github.com/martinus/robin-hood-hashing](https://github.com/martinus/robin-hood-hashing) |
| License | MIT |
| Versions | 3.11.3, 3.11.5 |
| Architectures | arm64, x86_64 |
| Definition | [robin-hood-hashing/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/robin-hood-hashing/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos robin-hood-hashing
```

##### Integration in the project (xmake.lua)

```lua
add_requires("robin-hood-hashing")
```


### robin-map (iphoneos)


| Description | *A C++ implementation of a fast hash map and hash set using robin hood hashing* |
| -- | -- |
| Homepage | [https://github.com/Tessil/robin-map](https://github.com/Tessil/robin-map) |
| License | MIT |
| Versions | v0.6.3 |
| Architectures | arm64, x86_64 |
| Definition | [robin-map/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/robin-map/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos robin-map
```

##### Integration in the project (xmake.lua)

```lua
add_requires("robin-map")
```


### rpclib (iphoneos)


| Description | *rpclib is a modern C++ msgpack-RPC server and client library* |
| -- | -- |
| Homepage | [http://rpclib.net](http://rpclib.net) |
| Versions | v2.3.0 |
| Architectures | arm64, x86_64 |
| Definition | [rpclib/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/rpclib/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos rpclib
```

##### Integration in the project (xmake.lua)

```lua
add_requires("rpclib")
```


### rply (iphoneos)


| Description | *RPly is a library that lets applications read and write PLY files.* |
| -- | -- |
| Homepage | [http://w3.impa.br/~diego/software/rply/](http://w3.impa.br/~diego/software/rply/) |
| License | MIT |
| Versions | 1.1.4 |
| Architectures | arm64, x86_64 |
| Definition | [rply/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/rply/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos rply
```

##### Integration in the project (xmake.lua)

```lua
add_requires("rply")
```


### rpmalloc (iphoneos)


| Description | *Public domain cross platform lock free thread caching 16-byte aligned memory allocator implemented in C* |
| -- | -- |
| Homepage | [https://github.com/mjansson/rpmalloc](https://github.com/mjansson/rpmalloc) |
| Versions | 1.4.4 |
| Architectures | arm64, x86_64 |
| Definition | [rpmalloc/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/rpmalloc/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos rpmalloc
```

##### Integration in the project (xmake.lua)

```lua
add_requires("rpmalloc")
```


### rttr (iphoneos)


| Description | *rttr: An open source library, which adds reflection to C++.* |
| -- | -- |
| Homepage | [https://www.rttr.org](https://www.rttr.org) |
| License | MIT |
| Versions | 0.9.5, 0.9.6 |
| Architectures | arm64, x86_64 |
| Definition | [rttr/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/rttr/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos rttr
```

##### Integration in the project (xmake.lua)

```lua
add_requires("rttr")
```



## s
### scnlib (iphoneos)


| Description | *scnlib is a modern C++ library for replacing scanf and std::istream* |
| -- | -- |
| Homepage | [https://scnlib.readthedocs.io/](https://scnlib.readthedocs.io/) |
| Versions | 0.4, 1.1.2 |
| Architectures | arm64, x86_64 |
| Definition | [scnlib/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/scnlib/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos scnlib
```

##### Integration in the project (xmake.lua)

```lua
add_requires("scnlib")
```


### scons (iphoneos)


| Description | *A software construction tool* |
| -- | -- |
| Homepage | [https://scons.org](https://scons.org) |
| Versions | 4.1.0, 4.3.0 |
| Architectures | arm64, x86_64 |
| Definition | [scons/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/scons/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos scons
```

##### Integration in the project (xmake.lua)

```lua
add_requires("scons")
```


### simde (iphoneos)


| Description | *Implementations of SIMD instruction sets for systems which don't natively support them.* |
| -- | -- |
| Homepage | [simd-everywhere.github.io/blog/](simd-everywhere.github.io/blog/) |
| Versions | 0.7.2 |
| Architectures | arm64, x86_64 |
| Definition | [simde/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/simde/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos simde
```

##### Integration in the project (xmake.lua)

```lua
add_requires("simde")
```


### simdjson (iphoneos)


| Description | *Ridiculously fast JSON parsing, UTF-8 validation and JSON minifying for popular 64 bit systems.* |
| -- | -- |
| Homepage | [https://simdjson.org](https://simdjson.org) |
| License | Apache-2.0 |
| Versions | v0.9.5, v0.9.7, v1.0.0, v1.1.0, v3.0.0 |
| Architectures | arm64 |
| Definition | [simdjson/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/simdjson/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos simdjson
```

##### Integration in the project (xmake.lua)

```lua
add_requires("simdjson")
```


### simplethreadpool (iphoneos)


| Description | *Simple thread pooling library in C++* |
| -- | -- |
| Homepage | [https://github.com/romch007/simplethreadpool](https://github.com/romch007/simplethreadpool) |
| License | MIT |
| Versions | 2022.11.18 |
| Architectures | arm64, x86_64 |
| Definition | [simplethreadpool/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/simplethreadpool/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos simplethreadpool
```

##### Integration in the project (xmake.lua)

```lua
add_requires("simplethreadpool")
```


### sokol (iphoneos)


| Description | *Simple STB-style cross-platform libraries for C and C++, written in C.* |
| -- | -- |
| Homepage | [https://github.com/floooh/sokol](https://github.com/floooh/sokol) |
| License | zlib |
| Versions | 2022.02.10, 2023.01.27 |
| Architectures | arm64, x86_64 |
| Definition | [sokol/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/sokol/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos sokol
```

##### Integration in the project (xmake.lua)

```lua
add_requires("sokol")
```


### sol2 (iphoneos)


| Description | *A C++ library binding to Lua.* |
| -- | -- |
| Homepage | [https://github.com/ThePhD/sol2](https://github.com/ThePhD/sol2) |
| Versions | v3.2.1, v3.2.2, v3.2.3, v3.3.0 |
| Architectures | arm64, x86_64 |
| Definition | [sol2/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/sol2/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos sol2
```

##### Integration in the project (xmake.lua)

```lua
add_requires("sol2")
```


### sparsepp (iphoneos)


| Description | *A fast, memory efficient hash map for C++* |
| -- | -- |
| Homepage | [https://github.com/greg7mdp/sparsepp](https://github.com/greg7mdp/sparsepp) |
| Versions | 1.22 |
| Architectures | arm64, x86_64 |
| Definition | [sparsepp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/sparsepp/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos sparsepp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("sparsepp")
```


### spdlog (iphoneos)


| Description | *Fast C++ logging library.* |
| -- | -- |
| Homepage | [https://github.com/gabime/spdlog](https://github.com/gabime/spdlog) |
| Versions | v1.10.0, v1.11.0, v1.3.1, v1.4.2, v1.5.0, v1.8.0, v1.8.1, v1.8.2, v1.8.5, v1.9.0, v1.9.1, v1.9.2 |
| Architectures | arm64, x86_64 |
| Definition | [spdlog/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/spdlog/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos spdlog
```

##### Integration in the project (xmake.lua)

```lua
add_requires("spdlog")
```


### spirv-headers (iphoneos)


| Description | *SPIR-V Headers* |
| -- | -- |
| Homepage | [https://github.com/KhronosGroup/SPIRV-Headers/](https://github.com/KhronosGroup/SPIRV-Headers/) |
| License | MIT |
| Versions | 1.2.198+0, 1.3.211+0, 1.3.231+1 |
| Architectures | arm64, x86_64 |
| Definition | [spirv-headers/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/spirv-headers/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos spirv-headers
```

##### Integration in the project (xmake.lua)

```lua
add_requires("spirv-headers")
```


### sqlite3 (iphoneos)


| Description | *The most used database engine in the world* |
| -- | -- |
| Homepage | [https://sqlite.org/](https://sqlite.org/) |
| Versions | 3.23.0+0, 3.24.0+0, 3.34.0+100, 3.35.0+300, 3.35.0+400, 3.36.0+0, 3.37.0+200, 3.39.0+200 |
| Architectures | arm64, x86_64 |
| Definition | [sqlite3/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/sqlite3/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos sqlite3
```

##### Integration in the project (xmake.lua)

```lua
add_requires("sqlite3")
```


### stb (iphoneos)


| Description | *single-file public domain (or MIT licensed) libraries for C/C++* |
| -- | -- |
| Homepage | [https://github.com/nothings/stb](https://github.com/nothings/stb) |
| Versions | 2021.07.13, 2021.09.10, 2023.01.30 |
| Architectures | arm64, x86_64 |
| Definition | [stb/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/stb/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos stb
```

##### Integration in the project (xmake.lua)

```lua
add_requires("stb")
```


### string-view-lite (iphoneos)


| Description | *string_view lite - A C++17-like string_view for C++98, C++11 and later in a single-file header-only library* |
| -- | -- |
| Homepage | [https://github.com/martinmoene/string-view-lite](https://github.com/martinmoene/string-view-lite) |
| License | BSL-1.0 |
| Versions | v1.7.0 |
| Architectures | arm64, x86_64 |
| Definition | [string-view-lite/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/string-view-lite/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos string-view-lite
```

##### Integration in the project (xmake.lua)

```lua
add_requires("string-view-lite")
```


### strtk (iphoneos)


| Description | *C++ String Toolkit Library* |
| -- | -- |
| Homepage | [https://www.partow.net/programming/strtk/index.html](https://www.partow.net/programming/strtk/index.html) |
| License | MIT |
| Versions | 2020.01.01 |
| Architectures | arm64, x86_64 |
| Definition | [strtk/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/strtk/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos strtk
```

##### Integration in the project (xmake.lua)

```lua
add_requires("strtk")
```


### swig (iphoneos)


| Description | *SWIG is a software development tool that connects programs written in C and C++ with a variety of high-level programming languages.* |
| -- | -- |
| Homepage | [http://swig.org/](http://swig.org/) |
| License | GPL-3.0 |
| Versions | 4.0.2 |
| Architectures | arm64, x86_64 |
| Definition | [swig/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/swig/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos swig
```

##### Integration in the project (xmake.lua)

```lua
add_requires("swig")
```



## t
### tabulate (iphoneos)


| Description | *Header-only library for printing aligned, formatted and colorized tables in Modern C++* |
| -- | -- |
| Homepage | [https://github.com/p-ranav/tabulate](https://github.com/p-ranav/tabulate) |
| License | MIT |
| Versions | 1.4 |
| Architectures | arm64, x86_64 |
| Definition | [tabulate/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tabulate/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos tabulate
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tabulate")
```


### taskflow (iphoneos)


| Description | *A fast C++ header-only library to help you quickly write parallel programs with complex task dependencies* |
| -- | -- |
| Homepage | [https://taskflow.github.io/](https://taskflow.github.io/) |
| License | MIT |
| Versions | v3.0.0, v3.1.0, v3.2.0, v3.3.0, v3.4.0 |
| Architectures | arm64, x86_64 |
| Definition | [taskflow/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/taskflow/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos taskflow
```

##### Integration in the project (xmake.lua)

```lua
add_requires("taskflow")
```


### taywee_args (iphoneos)


| Description | *A simple header-only C++ argument parser library.* |
| -- | -- |
| Homepage | [https://taywee.github.io/args/](https://taywee.github.io/args/) |
| License | MIT |
| Versions | 6.3.0, 6.4.6 |
| Architectures | arm64, x86_64 |
| Definition | [taywee_args/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/taywee_args/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos taywee_args
```

##### Integration in the project (xmake.lua)

```lua
add_requires("taywee_args")
```


### tbox (iphoneos)


| Description | *A glib-like multi-platform c library* |
| -- | -- |
| Homepage | [https://tboox.org](https://tboox.org) |
| Versions | v1.6.2, v1.6.3, v1.6.4, v1.6.5, v1.6.6, v1.6.7, v1.6.9, v1.7.1 |
| Architectures | arm64, x86_64 |
| Definition | [tbox/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tbox/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos tbox
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tbox")
```


### tclap (iphoneos)


| Description | *This is a simple templatized C++ library for parsing command line arguments.* |
| -- | -- |
| Homepage | [https://sourceforge.net/projects/tclap/](https://sourceforge.net/projects/tclap/) |
| License | MIT |
| Versions | 1.4.0-rc1 |
| Architectures | arm64, x86_64 |
| Definition | [tclap/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tclap/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos tclap
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tclap")
```


### termcolor (iphoneos)


| Description | *Termcolor is a header-only C++ library for printing colored messages to the terminal. Written just for fun with a help of the Force.* |
| -- | -- |
| Homepage | [https://github.com/ikalnytskyi/termcolor](https://github.com/ikalnytskyi/termcolor) |
| License | BSD-3-Clause |
| Versions | 2.1.0 |
| Architectures | arm64, x86_64 |
| Definition | [termcolor/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/termcolor/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos termcolor
```

##### Integration in the project (xmake.lua)

```lua
add_requires("termcolor")
```


### thread-pool (iphoneos)


| Description | *BS::thread_pool: a fast, lightweight, and easy-to-use C++17 thread pool library* |
| -- | -- |
| Homepage | [https://github.com/bshoshany/thread-pool](https://github.com/bshoshany/thread-pool) |
| License | MIT |
| Versions | v3.3.0 |
| Architectures | arm64, x86_64 |
| Definition | [thread-pool/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/thread-pool/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos thread-pool
```

##### Integration in the project (xmake.lua)

```lua
add_requires("thread-pool")
```


### thrust (iphoneos)


| Description | *The C++ parallel algorithms library.* |
| -- | -- |
| Homepage | [https://github.com/NVIDIA/thrust](https://github.com/NVIDIA/thrust) |
| License | Apache-2.0 |
| Versions | 1.17.0 |
| Architectures | arm64, x86_64 |
| Definition | [thrust/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/thrust/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos thrust
```

##### Integration in the project (xmake.lua)

```lua
add_requires("thrust")
```


### tiny-process-library (iphoneos)


| Description | *A small platform independent library making it simple to create and stop new processes in C++, as well as writing to stdin and reading from stdout and stderr of a new process* |
| -- | -- |
| Homepage | [https://gitlab.com/eidheim/tiny-process-library](https://gitlab.com/eidheim/tiny-process-library) |
| License | MIT |
| Versions | v2.0.4 |
| Architectures | arm64, x86_64 |
| Definition | [tiny-process-library/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tiny-process-library/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos tiny-process-library
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tiny-process-library")
```


### tinycbor (iphoneos)


| Description | *Concise Binary Object Representation (CBOR) Library* |
| -- | -- |
| Homepage | [https://github.com/intel/tinycbor](https://github.com/intel/tinycbor) |
| License | MIT |
| Versions | v0.6.0 |
| Architectures | arm64, x86_64 |
| Definition | [tinycbor/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tinycbor/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos tinycbor
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tinycbor")
```


### tinycrypt (iphoneos)


| Description | *TinyCrypt Cryptographic Library* |
| -- | -- |
| Homepage | [https://github.com/intel/tinycrypt](https://github.com/intel/tinycrypt) |
| Versions | 2019.9.18 |
| Architectures | arm64, x86_64 |
| Definition | [tinycrypt/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tinycrypt/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos tinycrypt
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tinycrypt")
```


### tinyexr (iphoneos)


| Description | *Tiny OpenEXR image loader/saver library* |
| -- | -- |
| Homepage | [https://github.com/syoyo/tinyexr/](https://github.com/syoyo/tinyexr/) |
| License | BSD-3-Clause |
| Versions | v1.0.1 |
| Architectures | arm64, x86_64 |
| Definition | [tinyexr/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tinyexr/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos tinyexr
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tinyexr")
```


### tinyformat (iphoneos)


| Description | *Minimal, type safe printf replacement library for C++* |
| -- | -- |
| Homepage | [https://github.com/c42f/tinyformat/](https://github.com/c42f/tinyformat/) |
| Versions | 2.3.0 |
| Architectures | arm64, x86_64 |
| Definition | [tinyformat/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tinyformat/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos tinyformat
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tinyformat")
```


### tinygltf (iphoneos)


| Description | *Header only C++11 tiny glTF 2.0 library* |
| -- | -- |
| Homepage | [https://github.com/syoyo/tinygltf/](https://github.com/syoyo/tinygltf/) |
| License | MIT |
| Versions | v2.5.0, v2.6.3 |
| Architectures | arm64, x86_64 |
| Definition | [tinygltf/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tinygltf/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos tinygltf
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tinygltf")
```


### tinyobjloader (iphoneos)


| Description | *Tiny but powerful single file wavefront obj loader* |
| -- | -- |
| Homepage | [https://github.com/tinyobjloader/tinyobjloader](https://github.com/tinyobjloader/tinyobjloader) |
| License | MIT |
| Versions | v1.0.7, v2.0.0rc10 |
| Architectures | arm64, x86_64 |
| Definition | [tinyobjloader/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tinyobjloader/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos tinyobjloader
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tinyobjloader")
```


### tinyxml (iphoneos)


| Description | *TinyXML is a simple, small, minimal, C++ XML parser that can be easily integrating into other programs.* |
| -- | -- |
| Homepage | [https://sourceforge.net/projects/tinyxml/](https://sourceforge.net/projects/tinyxml/) |
| License | zlib |
| Versions | 2.6.2 |
| Architectures | arm64, x86_64 |
| Definition | [tinyxml/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tinyxml/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos tinyxml
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tinyxml")
```


### tinyxml2 (iphoneos)


| Description | *simple, small, efficient, C++ XML parser that can be easily integrating into other programs.* |
| -- | -- |
| Homepage | [http://www.grinninglizard.com/tinyxml2/](http://www.grinninglizard.com/tinyxml2/) |
| Versions | 8.0.0, 9.0.0 |
| Architectures | arm64, x86_64 |
| Definition | [tinyxml2/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tinyxml2/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos tinyxml2
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tinyxml2")
```


### tl_expected (iphoneos)


| Description | *C++11/14/17 std::expected with functional-style extensions* |
| -- | -- |
| Homepage | [https://github.com/TartanLlama/expected](https://github.com/TartanLlama/expected) |
| License | CC0 |
| Versions | v1.0.0 |
| Architectures | arm64, x86_64 |
| Definition | [tl_expected/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tl_expected/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos tl_expected
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tl_expected")
```


### tl_function_ref (iphoneos)


| Description | *A lightweight, non-owning reference to a callable.* |
| -- | -- |
| Homepage | [https://github.com/TartanLlama/function_ref](https://github.com/TartanLlama/function_ref) |
| License | CC0 |
| Versions | v1.0.0 |
| Architectures | arm64, x86_64 |
| Definition | [tl_function_ref/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tl_function_ref/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos tl_function_ref
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tl_function_ref")
```


### toml++ (iphoneos)


| Description | *toml++ is a header-only TOML config file parser and serializer for C++17 (and later!).* |
| -- | -- |
| Homepage | [https://marzer.github.io/tomlplusplus/](https://marzer.github.io/tomlplusplus/) |
| Versions | v2.5.0, v3.0.0, v3.1.0, v3.2.0 |
| Architectures | arm64, x86_64 |
| Definition | [toml++/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/toml++/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos toml++
```

##### Integration in the project (xmake.lua)

```lua
add_requires("toml++")
```


### toml11 (iphoneos)


| Description | *TOML for Modern C++* |
| -- | -- |
| Homepage | [https://github.com/ToruNiina/toml11](https://github.com/ToruNiina/toml11) |
| License | MIT |
| Versions | v3.7.0 |
| Architectures | arm64, x86_64 |
| Definition | [toml11/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/toml11/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos toml11
```

##### Integration in the project (xmake.lua)

```lua
add_requires("toml11")
```



## u
### uchardet (iphoneos)


| Description | *uchardet is an encoding detector library, which takes a sequence of bytes in an unknown character encoding without any additional information, and attempts to determine the encoding of the text. * |
| -- | -- |
| Homepage | [https://www.freedesktop.org/wiki/Software/uchardet/](https://www.freedesktop.org/wiki/Software/uchardet/) |
| Versions | 0.0.7 |
| Architectures | arm64, x86_64 |
| Definition | [uchardet/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/u/uchardet/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos uchardet
```

##### Integration in the project (xmake.lua)

```lua
add_requires("uchardet")
```


### unordered_dense (iphoneos)


| Description | *A fast & densely stored hashmap and hashset based on robin-hood backward shift deletion.* |
| -- | -- |
| Homepage | [https://github.com/martinus/unordered_dense](https://github.com/martinus/unordered_dense) |
| License | MIT |
| Versions | v1.1.0, v1.4.0, v2.0.2, v3.0.0 |
| Architectures | arm64, x86_64 |
| Definition | [unordered_dense/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/u/unordered_dense/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos unordered_dense
```

##### Integration in the project (xmake.lua)

```lua
add_requires("unordered_dense")
```


### unzip (iphoneos)


| Description | *UnZip is an extraction utility for archives compressed in .zip format.* |
| -- | -- |
| Homepage | [http://infozip.sourceforge.net/UnZip.html](http://infozip.sourceforge.net/UnZip.html) |
| Versions | 6.0 |
| Architectures | arm64, x86_64 |
| Definition | [unzip/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/u/unzip/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos unzip
```

##### Integration in the project (xmake.lua)

```lua
add_requires("unzip")
```


### urdfdom-headers (iphoneos)


| Description | *Headers for URDF parsers* |
| -- | -- |
| Homepage | [http://ros.org/wiki/urdf](http://ros.org/wiki/urdf) |
| License | BSD-3-Clause |
| Versions | 1.0.5 |
| Architectures | arm64, x86_64 |
| Definition | [urdfdom-headers/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/u/urdfdom-headers/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos urdfdom-headers
```

##### Integration in the project (xmake.lua)

```lua
add_requires("urdfdom-headers")
```


### utest.h (iphoneos)


| Description | *single header unit testing framework for C and C++* |
| -- | -- |
| Homepage | [https://www.duskborn.com/utest_h/](https://www.duskborn.com/utest_h/) |
| Versions | 2022.09.01 |
| Architectures | arm64, x86_64 |
| Definition | [utest.h/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/u/utest.h/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos utest.h
```

##### Integration in the project (xmake.lua)

```lua
add_requires("utest.h")
```


### utf8.h (iphoneos)


| Description | *single header utf8 string functions for C and C++* |
| -- | -- |
| Homepage | [https://github.com/sheredom/utf8.h](https://github.com/sheredom/utf8.h) |
| Versions | 2022.07.04 |
| Architectures | arm64, x86_64 |
| Definition | [utf8.h/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/u/utf8.h/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos utf8.h
```

##### Integration in the project (xmake.lua)

```lua
add_requires("utf8.h")
```


### utfcpp (iphoneos)


| Description | *UTF8-CPP: UTF-8 with C++ in a Portable Way* |
| -- | -- |
| Homepage | [https://github.com/nemtrif/utfcpp](https://github.com/nemtrif/utfcpp) |
| License | BSL-1.0 |
| Versions | v3.2.1 |
| Architectures | arm64, x86_64 |
| Definition | [utfcpp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/u/utfcpp/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos utfcpp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("utfcpp")
```


### uvw (iphoneos)


| Description | *Header-only, event based, tiny and easy to use libuv wrapper in modern C++* |
| -- | -- |
| Homepage | [https://github.com/skypjack/uvw](https://github.com/skypjack/uvw) |
| Versions | 2.10.0 |
| Architectures | arm64, x86_64 |
| Definition | [uvw/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/u/uvw/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos uvw
```

##### Integration in the project (xmake.lua)

```lua
add_requires("uvw")
```



## v
### vectorial (iphoneos)


| Description | *Vector math library with NEON/SSE support* |
| -- | -- |
| Homepage | [https://github.com/scoopr/vectorial](https://github.com/scoopr/vectorial) |
| Versions | 2019.06.28 |
| Architectures | arm64, x86_64 |
| Definition | [vectorial/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/v/vectorial/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos vectorial
```

##### Integration in the project (xmake.lua)

```lua
add_requires("vectorial")
```


### volk (iphoneos)


| Description | *volk is a meta-loader for Vulkan* |
| -- | -- |
| Homepage | [https://github.com/zeux/volk](https://github.com/zeux/volk) |
| License | MIT |
| Versions | 1.2.162, 1.2.190, 1.3.204, 1.3.231+1 |
| Architectures | arm64, x86_64 |
| Definition | [volk/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/v/volk/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos volk
```

##### Integration in the project (xmake.lua)

```lua
add_requires("volk")
```


### vulkan-headers (iphoneos)


| Description | *Vulkan Header files and API registry* |
| -- | -- |
| Homepage | [https://github.com/KhronosGroup/Vulkan-Headers/](https://github.com/KhronosGroup/Vulkan-Headers/) |
| License | Apache-2.0 |
| Versions | 1.2.154+0, 1.2.162+0, 1.2.182+0, 1.2.189+1, 1.2.198+0, 1.3.211+0, 1.3.231+1 |
| Architectures | arm64, x86_64 |
| Definition | [vulkan-headers/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/v/vulkan-headers/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos vulkan-headers
```

##### Integration in the project (xmake.lua)

```lua
add_requires("vulkan-headers")
```


### vulkan-memory-allocator (iphoneos)


| Description | *Easy to integrate Vulkan memory allocation library.* |
| -- | -- |
| Homepage | [https://gpuopen-librariesandsdks.github.io/VulkanMemoryAllocator/html/](https://gpuopen-librariesandsdks.github.io/VulkanMemoryAllocator/html/) |
| License | MIT |
| Versions | v3.0.0, v3.0.1 |
| Architectures | arm64, x86_64 |
| Definition | [vulkan-memory-allocator/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/v/vulkan-memory-allocator/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos vulkan-memory-allocator
```

##### Integration in the project (xmake.lua)

```lua
add_requires("vulkan-memory-allocator")
```



## w
### wolfssl (iphoneos)


| Description | *The wolfSSL library is a small, fast, portable implementation of TLS/SSL for embedded devices to the cloud.  wolfSSL supports up to TLS 1.3!* |
| -- | -- |
| Homepage | [https://www.wolfssl.com](https://www.wolfssl.com) |
| License | GPL-2.0 |
| Versions | v5.3.0-stable |
| Architectures | arm64, x86_64 |
| Definition | [wolfssl/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/w/wolfssl/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos wolfssl
```

##### Integration in the project (xmake.lua)

```lua
add_requires("wolfssl")
```



## x
### xbyak (iphoneos)


| Description | *A JIT assembler for x86(IA-32)/x64(AMD64, x86-64) MMX/SSE/SSE2/SSE3/SSSE3/SSE4/FPU/AVX/AVX2/AVX-512 by C++ header* |
| -- | -- |
| Homepage | [https://github.com/herumi/xbyak](https://github.com/herumi/xbyak) |
| Versions | v6.02, v6.03 |
| Architectures | arm64, x86_64 |
| Definition | [xbyak/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/x/xbyak/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos xbyak
```

##### Integration in the project (xmake.lua)

```lua
add_requires("xbyak")
```


### xxhash (iphoneos)


| Description | *xxHash is an extremely fast non-cryptographic hash algorithm, working at RAM speed limit.* |
| -- | -- |
| Homepage | [http://cyan4973.github.io/xxHash/](http://cyan4973.github.io/xxHash/) |
| License | BSD-2-Clause |
| Versions | v0.8.0, v0.8.1 |
| Architectures | arm64, x86_64 |
| Definition | [xxhash/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/x/xxhash/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos xxhash
```

##### Integration in the project (xmake.lua)

```lua
add_requires("xxhash")
```



## y
### yasm (iphoneos)


| Description | *Modular BSD reimplementation of NASM.* |
| -- | -- |
| Homepage | [https://yasm.tortall.net/](https://yasm.tortall.net/) |
| Versions | 1.3.0 |
| Architectures | arm64, x86_64 |
| Definition | [yasm/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/y/yasm/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos yasm
```

##### Integration in the project (xmake.lua)

```lua
add_requires("yasm")
```


### yyjson (iphoneos)


| Description | *The fastest JSON library in C.* |
| -- | -- |
| Homepage | [https://github.com/ibireme/yyjson](https://github.com/ibireme/yyjson) |
| Versions | 0.2.0, 0.3.0, 0.4.0, 0.5.0, 0.5.1 |
| Architectures | arm64, x86_64 |
| Definition | [yyjson/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/y/yyjson/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos yyjson
```

##### Integration in the project (xmake.lua)

```lua
add_requires("yyjson")
```



## z
### zig (iphoneos)


| Description | *Zig is a general-purpose programming language and toolchain for maintaining robust, optimal, and reusable software.* |
| -- | -- |
| Homepage | [https://www.ziglang.org/](https://www.ziglang.org/) |
| Versions | 0.10.0, 0.7.1, 0.9.1 |
| Architectures | arm64, x86_64 |
| Definition | [zig/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/z/zig/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos zig
```

##### Integration in the project (xmake.lua)

```lua
add_requires("zig")
```


### zlib (iphoneos)


| Description | *A Massively Spiffy Yet Delicately Unobtrusive Compression Library* |
| -- | -- |
| Homepage | [http://www.zlib.net](http://www.zlib.net) |
| Versions | v1.2.10, v1.2.11, v1.2.12, v1.2.13 |
| Architectures | arm64, x86_64 |
| Definition | [zlib/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/z/zlib/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos zlib
```

##### Integration in the project (xmake.lua)

```lua
add_requires("zlib")
```


### zlibcomplete (iphoneos)


| Description | *C++ interface to the ZLib library supporting compression with FLUSH, decompression, and std::string. RAII* |
| -- | -- |
| Homepage | [https://github.com/rudi-cilibrasi/zlibcomplete](https://github.com/rudi-cilibrasi/zlibcomplete) |
| License | MIT |
| Versions | 1.0.5 |
| Architectures | arm64, x86_64 |
| Definition | [zlibcomplete/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/z/zlibcomplete/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos zlibcomplete
```

##### Integration in the project (xmake.lua)

```lua
add_requires("zlibcomplete")
```


### zstd (iphoneos)


| Description | *Zstandard - Fast real-time compression algorithm* |
| -- | -- |
| Homepage | [https://www.zstd.net/](https://www.zstd.net/) |
| License | BSD-3-Clause |
| Versions | v1.4.5, v1.5.0, v1.5.2 |
| Architectures | arm64, x86_64 |
| Definition | [zstd/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/z/zstd/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos zstd
```

##### Integration in the project (xmake.lua)

```lua
add_requires("zstd")
```


### zycore-c (iphoneos)


| Description | *Internal library providing platform independent types, macros and a fallback for environments without LibC.* |
| -- | -- |
| Homepage | [https://github.com/zyantific/zycore-c](https://github.com/zyantific/zycore-c) |
| License | MIT |
| Versions | v1.0.0, v1.1.0 |
| Architectures | arm64, x86_64 |
| Definition | [zycore-c/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/z/zycore-c/xmake.lua) |

##### Install command

```console
xrepo install -p iphoneos zycore-c
```

##### Integration in the project (xmake.lua)

```lua
add_requires("zycore-c")
```



