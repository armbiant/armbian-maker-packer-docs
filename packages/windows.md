## 7
### 7z (windows)


| Description | *A file archiver with a high compression ratio.* |
| -- | -- |
| Homepage | [https://www.7-zip.org/](https://www.7-zip.org/) |
| Versions | 21.02 |
| Architectures | arm64, x64, x86 |
| Definition | [7z/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/7/7z/xmake.lua) |

##### Install command

```console
xrepo install 7z
```

##### Integration in the project (xmake.lua)

```lua
add_requires("7z")
```



## a
### abseil (windows)


| Description | *C++ Common Libraries* |
| -- | -- |
| Homepage | [https://abseil.io](https://abseil.io) |
| License | Apache-2.0 |
| Versions | 20200225.1, 20210324.1, 20210324.2, 20211102.0, 20220623.0 |
| Architectures | arm64, x64, x86 |
| Definition | [abseil/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/a/abseil/xmake.lua) |

##### Install command

```console
xrepo install abseil
```

##### Integration in the project (xmake.lua)

```lua
add_requires("abseil")
```


### amgcl (windows)


| Description | *C++ library for solving large sparse linear systems with algebraic multigrid method* |
| -- | -- |
| Homepage | [https://github.com/ddemidov/amgcl/](https://github.com/ddemidov/amgcl/) |
| License | MIT |
| Versions | 1.4.0, 1.4.2, 1.4.3 |
| Architectures | arm64, x64, x86 |
| Definition | [amgcl/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/a/amgcl/xmake.lua) |

##### Install command

```console
xrepo install amgcl
```

##### Integration in the project (xmake.lua)

```lua
add_requires("amgcl")
```


### angelscript (windows)


| Description | *Extremely flexible cross-platform scripting library designed to allow applications to extend their functionality through external scripts* |
| -- | -- |
| Homepage | [http://angelcode.com/angelscript/](http://angelcode.com/angelscript/) |
| License | zlib |
| Versions | 2.34.0, 2.35.0, 2.35.1 |
| Architectures | arm64, x64, x86 |
| Definition | [angelscript/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/a/angelscript/xmake.lua) |

##### Install command

```console
xrepo install angelscript
```

##### Integration in the project (xmake.lua)

```lua
add_requires("angelscript")
```


### apr (windows)


| Description | *Mirror of Apache Portable Runtime* |
| -- | -- |
| Homepage | [https://github.com/apache/apr](https://github.com/apache/apr) |
| License | Apache-2.0 |
| Versions | 1.7.0 |
| Architectures | arm64, x64, x86 |
| Definition | [apr/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/a/apr/xmake.lua) |

##### Install command

```console
xrepo install apr
```

##### Integration in the project (xmake.lua)

```lua
add_requires("apr")
```


### aqt (windows)


| Description | *aqt: Another (unofficial) Qt CLI Installer on multi-platforms* |
| -- | -- |
| Homepage | [https://github.com/miurahr/aqtinstall](https://github.com/miurahr/aqtinstall) |
| License | MIT |
| Versions |  |
| Architectures | arm64, x64, x86 |
| Definition | [aqt/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/a/aqt/xmake.lua) |

##### Install command

```console
xrepo install aqt
```

##### Integration in the project (xmake.lua)

```lua
add_requires("aqt")
```


### argh (windows)


| Description | *Argh! A minimalist argument handler.* |
| -- | -- |
| Homepage | [https://github.com/adishavit/argh](https://github.com/adishavit/argh) |
| License | BSD-3-Clause |
| Versions | v1.3.2 |
| Architectures | arm64, x64, x86 |
| Definition | [argh/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/a/argh/xmake.lua) |

##### Install command

```console
xrepo install argh
```

##### Integration in the project (xmake.lua)

```lua
add_requires("argh")
```


### argparse (windows)


| Description | *A single header argument parser for C++17* |
| -- | -- |
| Homepage | [https://github.com/p-ranav/argparse](https://github.com/p-ranav/argparse) |
| License | MIT |
| Versions | 2.6, 2.7, 2.8, 2.9 |
| Architectures | arm64, x64, x86 |
| Definition | [argparse/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/a/argparse/xmake.lua) |

##### Install command

```console
xrepo install argparse
```

##### Integration in the project (xmake.lua)

```lua
add_requires("argparse")
```


### armadillo (windows)


| Description | *C++ library for linear algebra & scientific computing* |
| -- | -- |
| Homepage | [http://arma.sourceforge.net/](http://arma.sourceforge.net/) |
| License | Apache-2.0 |
| Versions | 10.7.0, 10.7.3, 10.8.1, 11.2.3 |
| Architectures | arm64, x64, x86 |
| Definition | [armadillo/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/a/armadillo/xmake.lua) |

##### Install command

```console
xrepo install armadillo
```

##### Integration in the project (xmake.lua)

```lua
add_requires("armadillo")
```


### asio (windows)


| Description | *Asio is a cross-platform C++ library for network and low-level I/O programming that provides developers with a consistent asynchronous model using a modern C++ approach.* |
| -- | -- |
| Homepage | [http://think-async.com/Asio/](http://think-async.com/Asio/) |
| License | BSL-1.0 |
| Versions | 1.20.0, 1.21.0, 1.24.0 |
| Architectures | arm64, x64, x86 |
| Definition | [asio/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/a/asio/xmake.lua) |

##### Install command

```console
xrepo install asio
```

##### Integration in the project (xmake.lua)

```lua
add_requires("asio")
```


### asmjit (windows)


| Description | *AsmJit is a lightweight library for machine code generation written in C++ language.* |
| -- | -- |
| Homepage | [https://asmjit.com/](https://asmjit.com/) |
| License | zlib |
| Versions | 2021.06.27, 2022.01.18 |
| Architectures | arm64, x64, x86 |
| Definition | [asmjit/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/a/asmjit/xmake.lua) |

##### Install command

```console
xrepo install asmjit
```

##### Integration in the project (xmake.lua)

```lua
add_requires("asmjit")
```


### assimp (windows)


| Description | *Portable Open-Source library to import various well-known 3D model formats in a uniform manner* |
| -- | -- |
| Homepage | [https://assimp.org](https://assimp.org) |
| License | BSD-3-Clause |
| Versions | v5.0.1, v5.1.4, v5.2.1, v5.2.2, v5.2.3, v5.2.4, v5.2.5 |
| Architectures | arm64, x64, x86 |
| Definition | [assimp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/a/assimp/xmake.lua) |

##### Install command

```console
xrepo install assimp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("assimp")
```


### autoconf (windows)


| Description | *An extensible package of M4 macros that produce shell scripts to automatically configure software source code packages.* |
| -- | -- |
| Homepage | [https://www.gnu.org/software/autoconf/autoconf.html](https://www.gnu.org/software/autoconf/autoconf.html) |
| Versions | 2.68, 2.69, 2.71 |
| Architectures | arm64, x64, x86 |
| Definition | [autoconf/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/a/autoconf/xmake.lua) |

##### Install command

```console
xrepo install autoconf
```

##### Integration in the project (xmake.lua)

```lua
add_requires("autoconf")
```


### automake (windows)


| Description | *A tool for automatically generating Makefile.in files compliant with the GNU Coding Standards.* |
| -- | -- |
| Homepage | [https://www.gnu.org/software/automake/](https://www.gnu.org/software/automake/) |
| Versions | 1.15.1, 1.16.1, 1.16.4, 1.9.5, 1.9.6 |
| Architectures | arm64, x64, x86 |
| Definition | [automake/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/a/automake/xmake.lua) |

##### Install command

```console
xrepo install automake
```

##### Integration in the project (xmake.lua)

```lua
add_requires("automake")
```



## b
### backward-cpp (windows)


| Description | *Backward is a beautiful stack trace pretty printer for C++.* |
| -- | -- |
| Homepage | [https://github.com/bombela/backward-cpp](https://github.com/bombela/backward-cpp) |
| License | MIT |
| Versions | v1.6 |
| Architectures | arm64, x64, x86 |
| Definition | [backward-cpp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/b/backward-cpp/xmake.lua) |

##### Install command

```console
xrepo install backward-cpp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("backward-cpp")
```


### bazel (windows)


| Description | *A fast, scalable, multi-language and extensible build system* |
| -- | -- |
| Homepage | [https://bazel.build/](https://bazel.build/) |
| Versions | 5.0.0 |
| Architectures | arm64, x64, x86 |
| Definition | [bazel/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/b/bazel/xmake.lua) |

##### Install command

```console
xrepo install bazel
```

##### Integration in the project (xmake.lua)

```lua
add_requires("bazel")
```


### benchmark (windows)


| Description | *A microbenchmark support library* |
| -- | -- |
| Homepage | [https://github.com/google/benchmark](https://github.com/google/benchmark) |
| License | Apache-2.0 |
| Versions | 1.5.2, 1.5.3, 1.5.4, 1.5.5, 1.5.6, 1.6.0, 1.6.1, 1.7.0 |
| Architectures | arm64, x64, x86 |
| Definition | [benchmark/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/b/benchmark/xmake.lua) |

##### Install command

```console
xrepo install benchmark
```

##### Integration in the project (xmake.lua)

```lua
add_requires("benchmark")
```


### better-enums (windows)


| Description | *C++ compile-time enum to string, iteration, in a single header file* |
| -- | -- |
| Homepage | [http://aantron.github.io/better-enums](http://aantron.github.io/better-enums) |
| License | BSD-2-Clause |
| Versions | 0.11.3 |
| Architectures | arm64, x64, x86 |
| Definition | [better-enums/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/b/better-enums/xmake.lua) |

##### Install command

```console
xrepo install better-enums
```

##### Integration in the project (xmake.lua)

```lua
add_requires("better-enums")
```


### bgfx (windows)


| Description | *Cross-platform, graphics API agnostic, “Bring Your Own Engine/Framework” style rendering library* |
| -- | -- |
| Homepage | [https://bkaradzic.github.io/bgfx/](https://bkaradzic.github.io/bgfx/) |
| License | BSD-2-Clause |
| Versions | 7816, 8203 |
| Architectures | arm64, x64, x86 |
| Definition | [bgfx/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/b/bgfx/xmake.lua) |

##### Install command

```console
xrepo install bgfx
```

##### Integration in the project (xmake.lua)

```lua
add_requires("bgfx")
```


### bin2c (windows)


| Description | *A simple utility for converting a binary file to a c application* |
| -- | -- |
| Homepage | [https://github.com/gwilymk/bin2c](https://github.com/gwilymk/bin2c) |
| Versions | 0.0.1 |
| Architectures | arm64, x64, x86 |
| Definition | [bin2c/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/b/bin2c/xmake.lua) |

##### Install command

```console
xrepo install bin2c
```

##### Integration in the project (xmake.lua)

```lua
add_requires("bin2c")
```


### binutils (windows)


| Description | *GNU binary tools for native development* |
| -- | -- |
| Homepage | [https://www.gnu.org/software/binutils/binutils.html](https://www.gnu.org/software/binutils/binutils.html) |
| License | GPL-2.0 |
| Versions | 2.34, 2.38 |
| Architectures | arm64, x64, x86 |
| Definition | [binutils/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/b/binutils/xmake.lua) |

##### Install command

```console
xrepo install binutils
```

##### Integration in the project (xmake.lua)

```lua
add_requires("binutils")
```


### bison (windows)


| Description | *A general-purpose parser generator.* |
| -- | -- |
| Homepage | [https://www.gnu.org/software/bison/](https://www.gnu.org/software/bison/) |
| License | GPL-3.0 |
| Versions | 3.7.4, 3.7.6, 3.8.2 |
| Architectures | arm64, x64, x86 |
| Definition | [bison/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/b/bison/xmake.lua) |

##### Install command

```console
xrepo install bison
```

##### Integration in the project (xmake.lua)

```lua
add_requires("bison")
```


### blah (windows)


| Description | *A small 2d c++ game framework* |
| -- | -- |
| Homepage | [https://github.com/NoelFB/blah](https://github.com/NoelFB/blah) |
| License | MIT |
| Versions | 2023.01.03 |
| Architectures | arm64, x64, x86 |
| Definition | [blah/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/b/blah/xmake.lua) |

##### Install command

```console
xrepo install blah
```

##### Integration in the project (xmake.lua)

```lua
add_requires("blah")
```


### blake3 (windows)


| Description | *BLAKE3 is a cryptographic hash function that is much faster than MD5, SHA-1, SHA-2, SHA-3, and BLAKE2; secure, unlike MD5 and SHA-1 (and secure against length extension, unlike SHA-2); highly parallelizable across any number of threads and SIMD lanes, because it's a Merkle tree on the inside; capable of verified streaming and incremental updates (Merkle tree); a PRF, MAC, KDF, and XOF, as well as a regular hash; and is a single algorithm with no variants, fast on x86-64 and also on smaller architectures.* |
| -- | -- |
| Homepage | [https://blake3.io/](https://blake3.io/) |
| License | CC0-1.0 |
| Versions | 1.3.1, 1.3.3 |
| Architectures | arm64, x64, x86 |
| Definition | [blake3/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/b/blake3/xmake.lua) |

##### Install command

```console
xrepo install blake3
```

##### Integration in the project (xmake.lua)

```lua
add_requires("blake3")
```


### blaze (windows)


| Description | *A high performance C++ math library.* |
| -- | -- |
| Homepage | [https://bitbucket.org/blaze-lib/blaze/](https://bitbucket.org/blaze-lib/blaze/) |
| License | BSD-3-Clause |
| Versions | 3.8, 3.8.1 |
| Architectures | x64 |
| Definition | [blaze/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/b/blaze/xmake.lua) |

##### Install command

```console
xrepo install blaze
```

##### Integration in the project (xmake.lua)

```lua
add_requires("blaze")
```


### blend2d (windows)


| Description | *2D Vector Graphics Engine Powered by a JIT Compiler* |
| -- | -- |
| Homepage | [https://blend2d.com](https://blend2d.com) |
| License | zlib |
| Versions | 2022.05.12 |
| Architectures | arm64, x64, x86 |
| Definition | [blend2d/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/b/blend2d/xmake.lua) |

##### Install command

```console
xrepo install blend2d
```

##### Integration in the project (xmake.lua)

```lua
add_requires("blend2d")
```


### blitz (windows)


| Description | *Blitz++ Multi-Dimensional Array Library for C++* |
| -- | -- |
| Homepage | [https://github.com/blitzpp/blitz](https://github.com/blitzpp/blitz) |
| License | LGPL-3.0 |
| Versions | 1.0.2 |
| Architectures | arm64, x64, x86 |
| Definition | [blitz/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/b/blitz/xmake.lua) |

##### Install command

```console
xrepo install blitz
```

##### Integration in the project (xmake.lua)

```lua
add_requires("blitz")
```


### blosc (windows)


| Description | *A blocking, shuffling and loss-less compression library* |
| -- | -- |
| Homepage | [https://www.blosc.org/](https://www.blosc.org/) |
| License | BSD-3-Clause |
| Versions | 1.20.1, 1.21.1, 1.5.0 |
| Architectures | arm64, x64, x86 |
| Definition | [blosc/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/b/blosc/xmake.lua) |

##### Install command

```console
xrepo install blosc
```

##### Integration in the project (xmake.lua)

```lua
add_requires("blosc")
```


### boost (windows)


| Description | *Collection of portable C++ source libraries.* |
| -- | -- |
| Homepage | [https://www.boost.org/](https://www.boost.org/) |
| License | BSL-1.0 |
| Versions | 1.70.0, 1.72.0, 1.73.0, 1.74.0, 1.75.0, 1.76.0, 1.77.0, 1.78.0, 1.79.0, 1.80.0, 1.81.0 |
| Architectures | arm64, x64, x86 |
| Definition | [boost/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/b/boost/xmake.lua) |

##### Install command

```console
xrepo install boost
```

##### Integration in the project (xmake.lua)

```lua
add_requires("boost")
```


### boringssl (windows)


| Description | *A fork of OpenSSL that is designed to meet Google's needs.* |
| -- | -- |
| Homepage | [https://boringssl.googlesource.com/boringssl](https://boringssl.googlesource.com/boringssl) |
| Versions | 2021.12.29 |
| Architectures | arm64, x64, x86 |
| Definition | [boringssl/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/b/boringssl/xmake.lua) |

##### Install command

```console
xrepo install boringssl
```

##### Integration in the project (xmake.lua)

```lua
add_requires("boringssl")
```


### box2d (windows)


| Description | *A 2D Physics Engine for Games* |
| -- | -- |
| Homepage | [https://box2d.org](https://box2d.org) |
| Versions | 2.4.0, 2.4.1 |
| Architectures | arm64, x64, x86 |
| Definition | [box2d/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/b/box2d/xmake.lua) |

##### Install command

```console
xrepo install box2d
```

##### Integration in the project (xmake.lua)

```lua
add_requires("box2d")
```


### brotli (windows)


| Description | *Brotli compression format.* |
| -- | -- |
| Homepage | [https://github.com/google/brotli](https://github.com/google/brotli) |
| Versions | 1.0.9 |
| Architectures | arm64, x64, x86 |
| Definition | [brotli/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/b/brotli/xmake.lua) |

##### Install command

```console
xrepo install brotli
```

##### Integration in the project (xmake.lua)

```lua
add_requires("brotli")
```


### brynet (windows)


| Description | *Header Only Cross platform high performance TCP network library using C++ 11* |
| -- | -- |
| Homepage | [https://github.com/IronsDu/brynet](https://github.com/IronsDu/brynet) |
| Versions | 1.0.9 |
| Architectures | arm64, x64, x86 |
| Definition | [brynet/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/b/brynet/xmake.lua) |

##### Install command

```console
xrepo install brynet
```

##### Integration in the project (xmake.lua)

```lua
add_requires("brynet")
```


### bullet3 (windows)


| Description | *Bullet Physics SDK.* |
| -- | -- |
| Homepage | [http://bulletphysics.org](http://bulletphysics.org) |
| License | zlib |
| Versions | 2.88, 3.05, 3.09, 3.24 |
| Architectures | arm64, x64, x86 |
| Definition | [bullet3/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/b/bullet3/xmake.lua) |

##### Install command

```console
xrepo install bullet3
```

##### Integration in the project (xmake.lua)

```lua
add_requires("bullet3")
```


### bzip2 (windows)


| Description | *Freely available, patent free, high-quality data compressor.* |
| -- | -- |
| Homepage | [https://sourceware.org/bzip2/](https://sourceware.org/bzip2/) |
| Versions | 1.0.8 |
| Architectures | arm64, x64, x86 |
| Definition | [bzip2/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/b/bzip2/xmake.lua) |

##### Install command

```console
xrepo install bzip2
```

##### Integration in the project (xmake.lua)

```lua
add_requires("bzip2")
```



## c
### c-ares (windows)


| Description | *A C library for asynchronous DNS requests* |
| -- | -- |
| Homepage | [https://c-ares.org/](https://c-ares.org/) |
| Versions | 1.16.1, 1.17.0, 1.17.1, 1.17.2, 1.18.0, 1.18.1 |
| Architectures | arm64, x64, x86 |
| Definition | [c-ares/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/c-ares/xmake.lua) |

##### Install command

```console
xrepo install c-ares
```

##### Integration in the project (xmake.lua)

```lua
add_requires("c-ares")
```


### ca-certificates (windows)


| Description | *Mozilla’s carefully curated collection of Root Certificates for validating the trustworthiness of SSL certificates while verifying the identity of TLS hosts.* |
| -- | -- |
| Homepage | [https://mkcert.org/](https://mkcert.org/) |
| Versions | 20211118, 20220604 |
| Architectures | arm64, x64, x86 |
| Definition | [ca-certificates/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/ca-certificates/xmake.lua) |

##### Install command

```console
xrepo install ca-certificates
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ca-certificates")
```


### cairo (windows)


| Description | *Vector graphics library with cross-device output support.* |
| -- | -- |
| Homepage | [https://cairographics.org/](https://cairographics.org/) |
| Versions | 1.17.6 |
| Architectures | arm64, x64, x86 |
| Definition | [cairo/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/cairo/xmake.lua) |

##### Install command

```console
xrepo install cairo
```

##### Integration in the project (xmake.lua)

```lua
add_requires("cairo")
```


### capnproto (windows)


| Description | *Cap'n Proto serialization/RPC system - core tools and C++ library.* |
| -- | -- |
| Homepage | [https://github.com/capnproto/capnproto](https://github.com/capnproto/capnproto) |
| License | MIT |
| Versions | 0.7.0, 0.8.0, 0.9.0 |
| Architectures | arm64, x64, x86 |
| Definition | [capnproto/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/capnproto/xmake.lua) |

##### Install command

```console
xrepo install capnproto
```

##### Integration in the project (xmake.lua)

```lua
add_requires("capnproto")
```


### capstone (windows)


| Description | *Disassembly framework with the target of becoming the ultimate disasm engine for binary analysis and reversing in the security community.* |
| -- | -- |
| Homepage | [http://www.capstone-engine.org](http://www.capstone-engine.org) |
| Versions | 4.0.2 |
| Architectures | arm64, x64, x86 |
| Definition | [capstone/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/capstone/xmake.lua) |

##### Install command

```console
xrepo install capstone
```

##### Integration in the project (xmake.lua)

```lua
add_requires("capstone")
```


### cargs (windows)


| Description | *A lightweight cross-platform getopt alternative that works on Linux, Windows and macOS. Command line argument parser library for C/C++. Can be used to parse argv and argc parameters.* |
| -- | -- |
| Homepage | [https://likle.github.io/cargs/](https://likle.github.io/cargs/) |
| License | MIT |
| Versions | v1.0.3 |
| Architectures | arm64, x64, x86 |
| Definition | [cargs/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/cargs/xmake.lua) |

##### Install command

```console
xrepo install cargs
```

##### Integration in the project (xmake.lua)

```lua
add_requires("cargs")
```


### catch2 (windows)


| Description | *Catch2 is a multi-paradigm test framework for C++. which also supports Objective-C (and maybe C). * |
| -- | -- |
| Homepage | [https://github.com/catchorg/Catch2](https://github.com/catchorg/Catch2) |
| License | BSL-1.0 |
| Versions | v2.13.10, v2.13.5, v2.13.6, v2.13.7, v2.13.8, v2.13.9, v2.9.2, v3.1.0, v3.1.1, v3.2.0, v3.2.1 |
| Architectures | arm64, x64, x86 |
| Definition | [catch2/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/catch2/xmake.lua) |

##### Install command

```console
xrepo install catch2
```

##### Integration in the project (xmake.lua)

```lua
add_requires("catch2")
```


### caudio (windows)


| Description | *3D Audio Engine Based on Openal* |
| -- | -- |
| Homepage | [https://github.com/R4stl1n/cAudio](https://github.com/R4stl1n/cAudio) |
| Versions | 2.3.1 |
| Architectures | arm64, x64, x86 |
| Definition | [caudio/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/caudio/xmake.lua) |

##### Install command

```console
xrepo install caudio
```

##### Integration in the project (xmake.lua)

```lua
add_requires("caudio")
```


### cef (windows)


| Description | *Chromium Embedded Framework (CEF). A simple framework for embedding Chromium-based browsers in other applications.* |
| -- | -- |
| Homepage | [https://bitbucket.org/chromiumembedded](https://bitbucket.org/chromiumembedded) |
| License | BSD-3-Clause |
| Versions |  |
| Architectures | arm64, x64, x86 |
| Definition | [cef/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/cef/xmake.lua) |

##### Install command

```console
xrepo install cef
```

##### Integration in the project (xmake.lua)

```lua
add_requires("cef")
```


### celero (windows)


| Description | *C++ Benchmarking Library* |
| -- | -- |
| Homepage | [https://github.com/DigitalInBlue/Celero](https://github.com/DigitalInBlue/Celero) |
| License | Apache-2.0 |
| Versions | v2.8.2 |
| Architectures | arm64, x64, x86 |
| Definition | [celero/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/celero/xmake.lua) |

##### Install command

```console
xrepo install celero
```

##### Integration in the project (xmake.lua)

```lua
add_requires("celero")
```


### cereal (windows)


| Description | *cereal is a header-only C++11 serialization library.* |
| -- | -- |
| Homepage | [https://uscilab.github.io/cereal/index.html](https://uscilab.github.io/cereal/index.html) |
| License | BSD-3-Clause |
| Versions | 1.3.0, 1.3.1 |
| Architectures | arm64, x64, x86 |
| Definition | [cereal/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/cereal/xmake.lua) |

##### Install command

```console
xrepo install cereal
```

##### Integration in the project (xmake.lua)

```lua
add_requires("cereal")
```


### ceres-solver (windows)


| Description | *Ceres Solver is an open source C++ library for modeling and solving large, complicated optimization problems.* |
| -- | -- |
| Homepage | [http://ceres-solver.org/](http://ceres-solver.org/) |
| Versions | 2.0.0, 2.1.0 |
| Architectures | arm64, x64, x86 |
| Definition | [ceres-solver/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/ceres-solver/xmake.lua) |

##### Install command

```console
xrepo install ceres-solver
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ceres-solver")
```


### ceval (windows)


| Description | *A C/C++ library for parsing and evaluation of arithmetic expressions.* |
| -- | -- |
| Homepage | [https://github.com/erstan/ceval](https://github.com/erstan/ceval) |
| License | MIT |
| Versions | 1.0.0 |
| Architectures | arm64, x64, x86 |
| Definition | [ceval/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/ceval/xmake.lua) |

##### Install command

```console
xrepo install ceval
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ceval")
```


### cfitsio (windows)


| Description | *CFITSIO is a library of C and Fortran subroutines for reading and writing data files in FITS (Flexible Image Transport System) data format.* |
| -- | -- |
| Homepage | [https://heasarc.gsfc.nasa.gov/fitsio/](https://heasarc.gsfc.nasa.gov/fitsio/) |
| Versions | 4.0.0 |
| Architectures | arm64, x64, x86 |
| Definition | [cfitsio/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/cfitsio/xmake.lua) |

##### Install command

```console
xrepo install cfitsio
```

##### Integration in the project (xmake.lua)

```lua
add_requires("cfitsio")
```


### cgal (windows)


| Description | *CGAL is a software project that provides easy access to efficient and reliable geometric algorithms in the form of a C++ library.* |
| -- | -- |
| Homepage | [https://www.cgal.org/](https://www.cgal.org/) |
| License | LGPL-3.0 |
| Versions | 5.1.1, 5.2.1, 5.3, 5.4 |
| Architectures | arm64, x64, x86 |
| Definition | [cgal/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/cgal/xmake.lua) |

##### Install command

```console
xrepo install cgal
```

##### Integration in the project (xmake.lua)

```lua
add_requires("cgal")
```


### cgetopt (windows)


| Description | *A GNU getopt() implementation written in pure C.* |
| -- | -- |
| Homepage | [https://github.com/xq114/cgetopt/](https://github.com/xq114/cgetopt/) |
| Versions | 1.0 |
| Architectures | arm64, x64, x86 |
| Definition | [cgetopt/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/cgetopt/xmake.lua) |

##### Install command

```console
xrepo install cgetopt
```

##### Integration in the project (xmake.lua)

```lua
add_requires("cgetopt")
```


### cgns (windows)


| Description | *CFD General Notation System* |
| -- | -- |
| Homepage | [http://cgns.github.io/](http://cgns.github.io/) |
| Versions | v4.2.0 |
| Architectures | arm64, x64, x86 |
| Definition | [cgns/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/cgns/xmake.lua) |

##### Install command

```console
xrepo install cgns
```

##### Integration in the project (xmake.lua)

```lua
add_requires("cgns")
```


### chaiscript (windows)


| Description | *Header-only C++ embedded scripting language loosely based on ECMA script.* |
| -- | -- |
| Homepage | [http://chaiscript.com](http://chaiscript.com) |
| License | BSD-3-Clause |
| Versions | v6.1.0 |
| Architectures | arm64, x64, x86 |
| Definition | [chaiscript/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/chaiscript/xmake.lua) |

##### Install command

```console
xrepo install chaiscript
```

##### Integration in the project (xmake.lua)

```lua
add_requires("chaiscript")
```


### chipmunk2d (windows)


| Description | *A fast and lightweight 2D game physics library.* |
| -- | -- |
| Homepage | [https://chipmunk-physics.net/](https://chipmunk-physics.net/) |
| License | MIT |
| Versions | 7.0.3 |
| Architectures | arm64, x64, x86 |
| Definition | [chipmunk2d/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/chipmunk2d/xmake.lua) |

##### Install command

```console
xrepo install chipmunk2d
```

##### Integration in the project (xmake.lua)

```lua
add_requires("chipmunk2d")
```


### chromium_zlib (windows)


| Description | *zlib from chromium* |
| -- | -- |
| Homepage | [https://chromium.googlesource.com/chromium/src/third_party/zlib/](https://chromium.googlesource.com/chromium/src/third_party/zlib/) |
| License | zlib |
| Versions | 2022.02.22 |
| Architectures | arm64, x64, x86 |
| Definition | [chromium_zlib/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/chromium_zlib/xmake.lua) |

##### Install command

```console
xrepo install chromium_zlib
```

##### Integration in the project (xmake.lua)

```lua
add_requires("chromium_zlib")
```


### civetweb (windows)


| Description | *Embedded C/C++ web server* |
| -- | -- |
| Homepage | [https://github.com/civetweb/civetweb](https://github.com/civetweb/civetweb) |
| License | MIT |
| Versions | v1.15 |
| Architectures | arm64, x64, x86 |
| Definition | [civetweb/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/civetweb/xmake.lua) |

##### Install command

```console
xrepo install civetweb
```

##### Integration in the project (xmake.lua)

```lua
add_requires("civetweb")
```


### cjson (windows)


| Description | *Ultralightweight JSON parser in ANSI C.* |
| -- | -- |
| Homepage | [https://github.com/DaveGamble/cJSON](https://github.com/DaveGamble/cJSON) |
| License | MIT |
| Versions | 1.7.10, 1.7.14, 1.7.15 |
| Architectures | arm64, x64, x86 |
| Definition | [cjson/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/cjson/xmake.lua) |

##### Install command

```console
xrepo install cjson
```

##### Integration in the project (xmake.lua)

```lua
add_requires("cjson")
```


### clara (windows)


| Description | *A simple to use, composable, command line parser for C++ 11 and beyond.* |
| -- | -- |
| Homepage | [https://github.com/catchorg/Clara](https://github.com/catchorg/Clara) |
| License | BSL-1.0 |
| Versions | 1.1.5 |
| Architectures | arm64, x64, x86 |
| Definition | [clara/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/clara/xmake.lua) |

##### Install command

```console
xrepo install clara
```

##### Integration in the project (xmake.lua)

```lua
add_requires("clara")
```


### clhep (windows)


| Description | *CLHEP - A Class Library for High Energy Physics* |
| -- | -- |
| Homepage | [https://proj-clhep.web.cern.ch/proj-clhep/](https://proj-clhep.web.cern.ch/proj-clhep/) |
| License | LGPL-3.0 |
| Versions | 2.4.5+1 |
| Architectures | arm64, x64, x86 |
| Definition | [clhep/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/clhep/xmake.lua) |

##### Install command

```console
xrepo install clhep
```

##### Integration in the project (xmake.lua)

```lua
add_requires("clhep")
```


### cli (windows)


| Description | *A library for interactive command line interfaces in modern C++* |
| -- | -- |
| Homepage | [https://github.com/daniele77/cli](https://github.com/daniele77/cli) |
| Versions | v2.0.0 |
| Architectures | arm64, x64, x86 |
| Definition | [cli/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/cli/xmake.lua) |

##### Install command

```console
xrepo install cli
```

##### Integration in the project (xmake.lua)

```lua
add_requires("cli")
```


### cli11 (windows)


| Description | *CLI11 is a command line parser for C++11 and beyond that provides a rich feature set with a simple and intuitive interface.* |
| -- | -- |
| Homepage | [https://github.com/CLIUtils/CLI11](https://github.com/CLIUtils/CLI11) |
| License | BSD |
| Versions | v2.2.0 |
| Architectures | arm64, x64, x86 |
| Definition | [cli11/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/cli11/xmake.lua) |

##### Install command

```console
xrepo install cli11
```

##### Integration in the project (xmake.lua)

```lua
add_requires("cli11")
```


### clib (windows)


| Description | *Header-only library for C99 that implements the most important classes from GLib: GList, GHashTable and GString.* |
| -- | -- |
| Homepage | [https://github.com/aheck/clib](https://github.com/aheck/clib) |
| License | MIT |
| Versions | 2022.12.25 |
| Architectures | arm64, x64, x86 |
| Definition | [clib/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/clib/xmake.lua) |

##### Install command

```console
xrepo install clib
```

##### Integration in the project (xmake.lua)

```lua
add_requires("clib")
```


### clip (windows)


| Description | *Library to copy/retrieve content to/from the clipboard/pasteboard.* |
| -- | -- |
| Homepage | [https://github.com/dacap/clip](https://github.com/dacap/clip) |
| License | MIT |
| Versions | 1.5 |
| Architectures | arm64, x64, x86 |
| Definition | [clip/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/clip/xmake.lua) |

##### Install command

```console
xrepo install clip
```

##### Integration in the project (xmake.lua)

```lua
add_requires("clip")
```


### cmake (windows)


| Description | *A cross-platform family of tool designed to build, test and package software* |
| -- | -- |
| Homepage | [https://cmake.org](https://cmake.org) |
| Versions | 3.11.4, 3.15.4, 3.18.4, 3.21.0, 3.22.1, 3.24.1, 3.24.2 |
| Architectures | arm64, x64, x86 |
| Definition | [cmake/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/cmake/xmake.lua) |

##### Install command

```console
xrepo install cmake
```

##### Integration in the project (xmake.lua)

```lua
add_requires("cmake")
```


### cmocka (windows)


| Description | *cmocka is an elegant unit testing framework for C with support for mock objects.* |
| -- | -- |
| Homepage | [https://cmocka.org/](https://cmocka.org/) |
| License | Apache-2.0 |
| Versions | 1.1.5 |
| Architectures | arm64, x64, x86 |
| Definition | [cmocka/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/cmocka/xmake.lua) |

##### Install command

```console
xrepo install cmocka
```

##### Integration in the project (xmake.lua)

```lua
add_requires("cmocka")
```


### cnpy (windows)


| Description | *library to read/write .npy and .npz files in C/C++* |
| -- | -- |
| Homepage | [https://github.com/rogersce/cnpy](https://github.com/rogersce/cnpy) |
| License | MIT |
| Versions | 2018.06.01 |
| Architectures | arm64, x64, x86 |
| Definition | [cnpy/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/cnpy/xmake.lua) |

##### Install command

```console
xrepo install cnpy
```

##### Integration in the project (xmake.lua)

```lua
add_requires("cnpy")
```


### collada-dom (windows)


| Description | *COLLADA Document Object Model (DOM) C++ Library* |
| -- | -- |
| Homepage | [https://github.com/rdiankov/collada-dom/](https://github.com/rdiankov/collada-dom/) |
| Versions | v2.5.0 |
| Architectures | arm64, x64, x86 |
| Definition | [collada-dom/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/collada-dom/xmake.lua) |

##### Install command

```console
xrepo install collada-dom
```

##### Integration in the project (xmake.lua)

```lua
add_requires("collada-dom")
```


### commonlibsse-ng (windows)


| Description | *A reverse engineered library for Skyrim Special Edition.* |
| -- | -- |
| Homepage | [https://github.com/CharmedBaryon/CommonLibSSE-NG](https://github.com/CharmedBaryon/CommonLibSSE-NG) |
| License | MIT |
| Versions | v3.5.5, v3.5.6, v3.6.0 |
| Architectures | x64 |
| Definition | [commonlibsse-ng/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/commonlibsse-ng/xmake.lua) |

##### Install command

```console
xrepo install commonlibsse-ng
```

##### Integration in the project (xmake.lua)

```lua
add_requires("commonlibsse-ng")
```


### concurrencpp (windows)


| Description | *Modern concurrency for C++. Tasks, executors, timers and C++20 coroutines to rule them all* |
| -- | -- |
| Homepage | [https://github.com/David-Haim/concurrencpp](https://github.com/David-Haim/concurrencpp) |
| License | MIT |
| Versions | 0.1.5 |
| Architectures | arm64, x64, x86 |
| Definition | [concurrencpp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/concurrencpp/xmake.lua) |

##### Install command

```console
xrepo install concurrencpp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("concurrencpp")
```


### concurrentqueue (windows)


| Description | *An industrial-strength lock-free queue for C++.* |
| -- | -- |
| Homepage | [https://github.com/cameron314/concurrentqueue](https://github.com/cameron314/concurrentqueue) |
| Versions |  |
| Architectures | arm64, x64, x86 |
| Definition | [concurrentqueue/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/concurrentqueue/xmake.lua) |

##### Install command

```console
xrepo install concurrentqueue
```

##### Integration in the project (xmake.lua)

```lua
add_requires("concurrentqueue")
```


### console-bridge (windows)


| Description | *A ROS-independent package for logging that seamlessly pipes into rosconsole/rosout for ROS-dependent packages.* |
| -- | -- |
| Homepage | [https://github.com/ros/console_bridge](https://github.com/ros/console_bridge) |
| License | BSD-3-Clause |
| Versions | 1.0.1 |
| Architectures | arm64, x64, x86 |
| Definition | [console-bridge/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/console-bridge/xmake.lua) |

##### Install command

```console
xrepo install console-bridge
```

##### Integration in the project (xmake.lua)

```lua
add_requires("console-bridge")
```


### coost (windows)


| Description | *A tiny boost library in C++11.* |
| -- | -- |
| Homepage | [https://github.com/idealvin/coost](https://github.com/idealvin/coost) |
| Versions | v3.0.0 |
| Architectures | arm64, x64, x86 |
| Definition | [coost/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/coost/xmake.lua) |

##### Install command

```console
xrepo install coost
```

##### Integration in the project (xmake.lua)

```lua
add_requires("coost")
```


### corrade (windows)


| Description | *Corrade is a multiplatform utility library written in C++11/C++14.* |
| -- | -- |
| Homepage | [https://magnum.graphics/corrade/](https://magnum.graphics/corrade/) |
| License | MIT |
| Versions | v2020.06 |
| Architectures | arm64, x64, x86 |
| Definition | [corrade/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/corrade/xmake.lua) |

##### Install command

```console
xrepo install corrade
```

##### Integration in the project (xmake.lua)

```lua
add_requires("corrade")
```


### cpp-httplib (windows)


| Description | *A C++11 single-file header-only cross platform HTTP/HTTPS library.* |
| -- | -- |
| Homepage | [https://github.com/yhirose/cpp-httplib](https://github.com/yhirose/cpp-httplib) |
| Versions | 0.8.5, 0.9.2 |
| Architectures | arm64, x64, x86 |
| Definition | [cpp-httplib/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/cpp-httplib/xmake.lua) |

##### Install command

```console
xrepo install cpp-httplib
```

##### Integration in the project (xmake.lua)

```lua
add_requires("cpp-httplib")
```


### cppcoro (windows)


| Description | *A library of C++ coroutine abstractions for the coroutines TS* |
| -- | -- |
| Homepage | [https://github.com/lewissbaker/cppcoro](https://github.com/lewissbaker/cppcoro) |
| Versions | 2020.10.13 |
| Architectures | arm64, x64, x86 |
| Definition | [cppcoro/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/cppcoro/xmake.lua) |

##### Install command

```console
xrepo install cppcoro
```

##### Integration in the project (xmake.lua)

```lua
add_requires("cppcoro")
```


### cppfront (windows)


| Description | *A personal experimental C++ Syntax 2 -> Syntax 1 compiler* |
| -- | -- |
| Homepage | [https://github.com/hsutter/cppfront](https://github.com/hsutter/cppfront) |
| Versions | 2022.09.23 |
| Architectures | arm64, x64, x86 |
| Definition | [cppfront/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/cppfront/xmake.lua) |

##### Install command

```console
xrepo install cppfront
```

##### Integration in the project (xmake.lua)

```lua
add_requires("cppfront")
```


### cppzmq (windows)


| Description | *Header-only C++ binding for libzmq* |
| -- | -- |
| Homepage | [http://www.zeromq.org/](http://www.zeromq.org/) |
| License | MIT |
| Versions | v4.8.1 |
| Architectures | arm64, x64, x86 |
| Definition | [cppzmq/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/cppzmq/xmake.lua) |

##### Install command

```console
xrepo install cppzmq
```

##### Integration in the project (xmake.lua)

```lua
add_requires("cppzmq")
```


### cpr (windows)


| Description | *C++ Requests is a simple wrapper around libcurl inspired by the excellent Python Requests project.* |
| -- | -- |
| Homepage | [https://docs.libcpr.org/](https://docs.libcpr.org/) |
| License | MIT |
| Versions | 1.6.2, 1.7.2, 1.8.3 |
| Architectures | arm64, x64, x86 |
| Definition | [cpr/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/cpr/xmake.lua) |

##### Install command

```console
xrepo install cpr
```

##### Integration in the project (xmake.lua)

```lua
add_requires("cpr")
```


### cpu-features (windows)


| Description | *A cross platform C99 library to get cpu features at runtime.* |
| -- | -- |
| Homepage | [https://github.com/google/cpu_features](https://github.com/google/cpu_features) |
| License | Apache-2.0 |
| Versions | v0.6.0 |
| Architectures | arm64, x64, x86 |
| Definition | [cpu-features/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/cpu-features/xmake.lua) |

##### Install command

```console
xrepo install cpu-features
```

##### Integration in the project (xmake.lua)

```lua
add_requires("cpu-features")
```


### cpuinfo (windows)


| Description | *CPU INFOrmation library (x86/x86-64/ARM/ARM64, Linux/Windows/Android/macOS/iOS)* |
| -- | -- |
| Homepage | [https://github.com/pytorch/cpuinfo](https://github.com/pytorch/cpuinfo) |
| License | BSD 2-Clause |
| Versions | 2022.09.15 |
| Architectures | arm64, x64, x86 |
| Definition | [cpuinfo/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/cpuinfo/xmake.lua) |

##### Install command

```console
xrepo install cpuinfo
```

##### Integration in the project (xmake.lua)

```lua
add_requires("cpuinfo")
```


### crashpad (windows)


| Description | *Crashpad is a crash-reporting system.* |
| -- | -- |
| Homepage | [https://chromium.googlesource.com/crashpad/crashpad/+/refs/heads/main/README.md](https://chromium.googlesource.com/crashpad/crashpad/+/refs/heads/main/README.md) |
| Versions |  |
| Architectures | arm64, x64, x86 |
| Definition | [crashpad/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/crashpad/xmake.lua) |

##### Install command

```console
xrepo install crashpad
```

##### Integration in the project (xmake.lua)

```lua
add_requires("crashpad")
```


### crc32c (windows)


| Description | *CRC32C implementation with support for CPU-specific acceleration instructions* |
| -- | -- |
| Homepage | [https://github.com/google/crc32c](https://github.com/google/crc32c) |
| Versions | 1.1.2 |
| Architectures | arm64, x64, x86 |
| Definition | [crc32c/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/crc32c/xmake.lua) |

##### Install command

```console
xrepo install crc32c
```

##### Integration in the project (xmake.lua)

```lua
add_requires("crc32c")
```


### crossguid (windows)


| Description | *Lightweight cross platform C++ GUID/UUID library* |
| -- | -- |
| Homepage | [https://github.com/graeme-hill/crossguid](https://github.com/graeme-hill/crossguid) |
| License | MIT |
| Versions | 2019.3.29 |
| Architectures | arm64, x64, x86 |
| Definition | [crossguid/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/crossguid/xmake.lua) |

##### Install command

```console
xrepo install crossguid
```

##### Integration in the project (xmake.lua)

```lua
add_requires("crossguid")
```


### cryptopp (windows)


| Description | *free C++ class library of cryptographic schemes* |
| -- | -- |
| Homepage | [https://cryptopp.com/](https://cryptopp.com/) |
| Versions | 8.4.0, 8.5.0, 8.6.0, 8.7.0 |
| Architectures | arm64, x64, x86 |
| Definition | [cryptopp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/cryptopp/xmake.lua) |

##### Install command

```console
xrepo install cryptopp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("cryptopp")
```


### csv2 (windows)


| Description | *A CSV parser library* |
| -- | -- |
| Homepage | [https://github.com/p-ranav/csv2](https://github.com/p-ranav/csv2) |
| License | MIT |
| Versions | v0.1 |
| Architectures | arm64, x64, x86 |
| Definition | [csv2/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/csv2/xmake.lua) |

##### Install command

```console
xrepo install csv2
```

##### Integration in the project (xmake.lua)

```lua
add_requires("csv2")
```


### csvparser (windows)


| Description | *A modern C++ library for reading, writing, and analyzing CSV (and similar) files (by vincentlaucsb)* |
| -- | -- |
| Homepage | [https://github.com/vincentlaucsb/csv-parser](https://github.com/vincentlaucsb/csv-parser) |
| Versions | 2.1.1 |
| Architectures | arm64, x64, x86 |
| Definition | [csvparser/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/csvparser/xmake.lua) |

##### Install command

```console
xrepo install csvparser
```

##### Integration in the project (xmake.lua)

```lua
add_requires("csvparser")
```


### ctre (windows)


| Description | *ctre is a Compile time PCRE (almost) compatible regular expression matcher.* |
| -- | -- |
| Homepage | [https://github.com/hanickadot/compile-time-regular-expressions/](https://github.com/hanickadot/compile-time-regular-expressions/) |
| Versions | 3.4.1 |
| Architectures | arm64, x64, x86 |
| Definition | [ctre/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/ctre/xmake.lua) |

##### Install command

```console
xrepo install ctre
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ctre")
```


### cumem (windows)


| Description | *CUDA Memory Management Wrapper with Type Safety* |
| -- | -- |
| Homepage | [https://github.com/BinhaoQin/cuMem](https://github.com/BinhaoQin/cuMem) |
| Versions | 1.0.0 |
| Architectures | arm64, x64, x86 |
| Definition | [cumem/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/cumem/xmake.lua) |

##### Install command

```console
xrepo install cumem
```

##### Integration in the project (xmake.lua)

```lua
add_requires("cumem")
```


### cxxopts (windows)


| Description | *Lightweight C++ command line option parser* |
| -- | -- |
| Homepage | [https://github.com/jarro2783/cxxopts](https://github.com/jarro2783/cxxopts) |
| Versions | v2.2.0, v3.0.0 |
| Architectures | arm64, x64, x86 |
| Definition | [cxxopts/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/c/cxxopts/xmake.lua) |

##### Install command

```console
xrepo install cxxopts
```

##### Integration in the project (xmake.lua)

```lua
add_requires("cxxopts")
```



## d
### d3d12-memory-allocator (windows)


| Description | *Easy to integrate memory allocation library for Direct3D 12* |
| -- | -- |
| Homepage | [https://github.com/GPUOpen-LibrariesAndSDKs/D3D12MemoryAllocator](https://github.com/GPUOpen-LibrariesAndSDKs/D3D12MemoryAllocator) |
| License | MIT |
| Versions | v2.0.1 |
| Architectures | x64 |
| Definition | [d3d12-memory-allocator/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/d3d12-memory-allocator/xmake.lua) |

##### Install command

```console
xrepo install d3d12-memory-allocator
```

##### Integration in the project (xmake.lua)

```lua
add_requires("d3d12-memory-allocator")
```


### dataframe (windows)


| Description | *This is a C++ analytical library that provides interface and functionality similar to packages/libraries in Python and R.* |
| -- | -- |
| Homepage | [https://github.com/hosseinmoein/DataFrame](https://github.com/hosseinmoein/DataFrame) |
| License | MIT |
| Versions | 1.21.0, 1.22.0 |
| Architectures | arm64, x64, x86 |
| Definition | [dataframe/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/dataframe/xmake.lua) |

##### Install command

```console
xrepo install dataframe
```

##### Integration in the project (xmake.lua)

```lua
add_requires("dataframe")
```


### date (windows)


| Description | *A date and time library for use with C++11 and C++14.* |
| -- | -- |
| Homepage | [https://github.com/HowardHinnant/date](https://github.com/HowardHinnant/date) |
| License | MIT |
| Versions | v3.0.1 |
| Architectures | arm64, x64, x86 |
| Definition | [date/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/date/xmake.lua) |

##### Install command

```console
xrepo install date
```

##### Integration in the project (xmake.lua)

```lua
add_requires("date")
```


### dav1d (windows)


| Description | *dav1d is a new AV1 cross-platform decoder, open-source, and focused on speed, size and correctness.* |
| -- | -- |
| Homepage | [https://www.videolan.org/projects/dav1d.html](https://www.videolan.org/projects/dav1d.html) |
| License | BSD-2-Clause |
| Versions | 0.9.0 |
| Architectures | arm64, x64, x86 |
| Definition | [dav1d/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/dav1d/xmake.lua) |

##### Install command

```console
xrepo install dav1d
```

##### Integration in the project (xmake.lua)

```lua
add_requires("dav1d")
```


### dbg-macro (windows)


| Description | *A dbg(…) macro for C++* |
| -- | -- |
| Homepage | [https://github.com/sharkdp/dbg-macro](https://github.com/sharkdp/dbg-macro) |
| License | MIT |
| Versions | v0.4.0 |
| Architectures | arm64, x64, x86 |
| Definition | [dbg-macro/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/dbg-macro/xmake.lua) |

##### Install command

```console
xrepo install dbg-macro
```

##### Integration in the project (xmake.lua)

```lua
add_requires("dbg-macro")
```


### dbus (windows)


| Description | *D-Bus is a message bus system, a simple way for applications to talk to one another.* |
| -- | -- |
| Homepage | [https://www.freedesktop.org/wiki/Software/dbus/](https://www.freedesktop.org/wiki/Software/dbus/) |
| License | MIT |
| Versions | 1.14.2 |
| Architectures | arm64, x64, x86 |
| Definition | [dbus/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/dbus/xmake.lua) |

##### Install command

```console
xrepo install dbus
```

##### Integration in the project (xmake.lua)

```lua
add_requires("dbus")
```


### dcmtk (windows)


| Description | *DCMTK - DICOM Toolkit* |
| -- | -- |
| Homepage | [https://dcmtk.org/dcmtk.php.en](https://dcmtk.org/dcmtk.php.en) |
| License | BSD-3-Clause |
| Versions | 3.6.6 |
| Architectures | arm64, x64, x86 |
| Definition | [dcmtk/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/dcmtk/xmake.lua) |

##### Install command

```console
xrepo install dcmtk
```

##### Integration in the project (xmake.lua)

```lua
add_requires("dcmtk")
```


### debugbreak (windows)


| Description | *break into the debugger programmatically* |
| -- | -- |
| Homepage | [https://github.com/scottt/debugbreak](https://github.com/scottt/debugbreak) |
| Versions | v1.0 |
| Architectures | arm64, x64, x86 |
| Definition | [debugbreak/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/debugbreak/xmake.lua) |

##### Install command

```console
xrepo install debugbreak
```

##### Integration in the project (xmake.lua)

```lua
add_requires("debugbreak")
```


### decimal_for_cpp (windows)


| Description | *Decimal data type support, for COBOL-like fixed-point operations on currency/money values.* |
| -- | -- |
| Homepage | [https://github.com/vpiotr/decimal_for_cpp](https://github.com/vpiotr/decimal_for_cpp) |
| License | BSD-3-Clause |
| Versions | 1.19 |
| Architectures | arm64, x64, x86 |
| Definition | [decimal_for_cpp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/decimal_for_cpp/xmake.lua) |

##### Install command

```console
xrepo install decimal_for_cpp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("decimal_for_cpp")
```


### demumble (windows)


| Description | *A better c++filt and a better undname.exe, in one binary.* |
| -- | -- |
| Homepage | [https://github.com/nico/demumble](https://github.com/nico/demumble) |
| License | Apache-2.0 |
| Versions | 2022.3.23 |
| Architectures | arm64, x64, x86 |
| Definition | [demumble/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/demumble/xmake.lua) |

##### Install command

```console
xrepo install demumble
```

##### Integration in the project (xmake.lua)

```lua
add_requires("demumble")
```


### depot_tools (windows)


| Description | *Tools for working with Chromium development* |
| -- | -- |
| Homepage | [https://chromium.googlesource.com/chromium/tools/depot_tools](https://chromium.googlesource.com/chromium/tools/depot_tools) |
| Versions | 2022.2.1 |
| Architectures | arm64, x64, x86 |
| Definition | [depot_tools/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/depot_tools/xmake.lua) |

##### Install command

```console
xrepo install depot_tools
```

##### Integration in the project (xmake.lua)

```lua
add_requires("depot_tools")
```


### devil (windows)


| Description | *Developer's Image Library (DevIL) is a cross-platform image library utilizing a simple syntax to load, save, convert, manipulate, filter and display a variety of images with ease.* |
| -- | -- |
| Homepage | [https://sourceforge.net/projects/openil/](https://sourceforge.net/projects/openil/) |
| License | LGPL-2.1 |
| Versions | 1.8.0 |
| Architectures | arm64, x64, x86 |
| Definition | [devil/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/devil/xmake.lua) |

##### Install command

```console
xrepo install devil
```

##### Integration in the project (xmake.lua)

```lua
add_requires("devil")
```


### directxshadercompiler (windows)


| Description | *DirectX Shader Compiler* |
| -- | -- |
| Homepage | [https://github.com/microsoft/DirectXShaderCompiler/](https://github.com/microsoft/DirectXShaderCompiler/) |
| License | LLVM |
| Versions | 1.5.2010, 1.6.2104, 1.6.2106 |
| Architectures | x64 |
| Definition | [directxshadercompiler/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/directxshadercompiler/xmake.lua) |

##### Install command

```console
xrepo install directxshadercompiler
```

##### Integration in the project (xmake.lua)

```lua
add_requires("directxshadercompiler")
```


### directxtk (windows)


| Description | *This package contains the "DirectX Tool Kit", a collection of helper classes for writing Direct3D 11 C++ code for Universal Windows Platform (UWP) apps for Windows 10, Xbox One, and Win32 desktop applications for Windows 7 Service Pack 1 or later.* |
| -- | -- |
| Homepage | [https://github.com/microsoft/DirectXTK](https://github.com/microsoft/DirectXTK) |
| Versions | 20.9.0, 21.11.0, 21.4.0 |
| Architectures | arm64, x64, x86 |
| Definition | [directxtk/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/directxtk/xmake.lua) |

##### Install command

```console
xrepo install directxtk
```

##### Integration in the project (xmake.lua)

```lua
add_requires("directxtk")
```


### discord (windows)


| Description | *Whether you’re part of a school club, gaming group, worldwide art community, or just a handful of friends that want to spend time together, Discord makes it easy to talk every day and hang out more often.* |
| -- | -- |
| Homepage | [https://discord.com/developers/docs/game-sdk/](https://discord.com/developers/docs/game-sdk/) |
| Versions | 2.5.6, 3.2.1 |
| Architectures | x64, x86 |
| Definition | [discord/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/discord/xmake.lua) |

##### Install command

```console
xrepo install discord
```

##### Integration in the project (xmake.lua)

```lua
add_requires("discord")
```


### dlib (windows)


| Description | *A toolkit for making real world machine learning and data analysis applications in C++* |
| -- | -- |
| Homepage | [https://dlib.net](https://dlib.net) |
| License | Boost |
| Versions | v19.22 |
| Architectures | arm64, x64, x86 |
| Definition | [dlib/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/dlib/xmake.lua) |

##### Install command

```console
xrepo install dlib
```

##### Integration in the project (xmake.lua)

```lua
add_requires("dlib")
```


### docopt (windows)


| Description | *Pythonic command line arguments parser (C++11 port)* |
| -- | -- |
| Homepage | [https://github.com/docopt/docopt.cpp](https://github.com/docopt/docopt.cpp) |
| License | BSL-1.0 |
| Versions | v0.6.3 |
| Architectures | arm64, x64, x86 |
| Definition | [docopt/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/docopt/xmake.lua) |

##### Install command

```console
xrepo install docopt
```

##### Integration in the project (xmake.lua)

```lua
add_requires("docopt")
```


### doctest (windows)


| Description | *The fastest feature-rich C++11/14/17/20 single-header testing framework for unit tests and TDD* |
| -- | -- |
| Homepage | [http://bit.ly/doctest-docs](http://bit.ly/doctest-docs) |
| Versions | 2.3.1, 2.3.6, 2.4.8, 2.4.9 |
| Architectures | arm64, x64, x86 |
| Definition | [doctest/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/doctest/xmake.lua) |

##### Install command

```console
xrepo install doctest
```

##### Integration in the project (xmake.lua)

```lua
add_requires("doctest")
```


### double-conversion (windows)


| Description | *Efficient binary-decimal and decimal-binary conversion routines for IEEE doubles.* |
| -- | -- |
| Homepage | [https://github.com/google/double-conversion](https://github.com/google/double-conversion) |
| License | BSD-3-Clause |
| Versions | v3.1.5 |
| Architectures | arm64, x64, x86 |
| Definition | [double-conversion/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/double-conversion/xmake.lua) |

##### Install command

```console
xrepo install double-conversion
```

##### Integration in the project (xmake.lua)

```lua
add_requires("double-conversion")
```


### doxygen (windows)


| Description | *%s* |
| -- | -- |
| Homepage | [https://www.doxygen.nl/](https://www.doxygen.nl/) |
| License | GPL-2.0 |
| Versions | 1.9.1, 1.9.2, 1.9.3 |
| Architectures | arm64, x64, x86 |
| Definition | [doxygen/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/doxygen/xmake.lua) |

##### Install command

```console
xrepo install doxygen
```

##### Integration in the project (xmake.lua)

```lua
add_requires("doxygen")
```


### dpp (windows)


| Description | *D++ Extremely Lightweight C++ Discord Library* |
| -- | -- |
| Homepage | [https://github.com/brainboxdotcc/DPP](https://github.com/brainboxdotcc/DPP) |
| License | Apache-2.0 |
| Versions | v10.0.10, v10.0.11, v10.0.12, v10.0.13, v10.0.14, v10.0.15, v10.0.16, v10.0.17, v10.0.18, v10.0.19, v10.0.20, v10.0.21, v10.0.22, v10.0.8 |
| Architectures | arm64, x64, x86 |
| Definition | [dpp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/dpp/xmake.lua) |

##### Install command

```console
xrepo install dpp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("dpp")
```


### dr_flac (windows)


| Description | *Single file audio decoding libraries for C/C++.* |
| -- | -- |
| Homepage | [https://github.com/mackron/dr_libs](https://github.com/mackron/dr_libs) |
| License | MIT |
| Versions | 0.12.29 |
| Architectures | arm64, x64, x86 |
| Definition | [dr_flac/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/dr_flac/xmake.lua) |

##### Install command

```console
xrepo install dr_flac
```

##### Integration in the project (xmake.lua)

```lua
add_requires("dr_flac")
```


### dr_mp3 (windows)


| Description | *Single file audio decoding libraries for C/C++.* |
| -- | -- |
| Homepage | [https://github.com/mackron/dr_libs](https://github.com/mackron/dr_libs) |
| License | MIT |
| Versions | 0.6.27 |
| Architectures | arm64, x64, x86 |
| Definition | [dr_mp3/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/dr_mp3/xmake.lua) |

##### Install command

```console
xrepo install dr_mp3
```

##### Integration in the project (xmake.lua)

```lua
add_requires("dr_mp3")
```


### dr_wav (windows)


| Description | *Single file audio decoding libraries for C/C++.* |
| -- | -- |
| Homepage | [https://github.com/mackron/dr_libs](https://github.com/mackron/dr_libs) |
| License | MIT |
| Versions | 0.12.19 |
| Architectures | arm64, x64, x86 |
| Definition | [dr_wav/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/dr_wav/xmake.lua) |

##### Install command

```console
xrepo install dr_wav
```

##### Integration in the project (xmake.lua)

```lua
add_requires("dr_wav")
```


### draco (windows)


| Description | *Draco is an open-source library for compressing and decompressing 3D geometric meshes and point clouds.* |
| -- | -- |
| Homepage | [https://google.github.io/draco/](https://google.github.io/draco/) |
| License | Apache-2.0 |
| Versions | 1.4.1, 1.5.0 |
| Architectures | arm64, x64, x86 |
| Definition | [draco/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/draco/xmake.lua) |

##### Install command

```console
xrepo install draco
```

##### Integration in the project (xmake.lua)

```lua
add_requires("draco")
```


### drogon (windows)


| Description | *Drogon: A C++14/17/20 based HTTP web application framework running on Linux/macOS/Unix/Windows* |
| -- | -- |
| Homepage | [https://github.com/an-tao/drogon/](https://github.com/an-tao/drogon/) |
| License | MIT |
| Versions | v1.4.1, v1.6.0, v1.7.1, v1.7.3, v1.7.5, v1.8.0, v1.8.1, v1.8.2 |
| Architectures | x64 |
| Definition | [drogon/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/drogon/xmake.lua) |

##### Install command

```console
xrepo install drogon
```

##### Integration in the project (xmake.lua)

```lua
add_requires("drogon")
```


### dynareadout (windows)


| Description | *Ansi C library for parsing binary output files of LS Dyna (d3plot, binout)* |
| -- | -- |
| Homepage | [https://github.com/PucklaJ/dynareadout](https://github.com/PucklaJ/dynareadout) |
| Versions | 22.12, 23.01 |
| Architectures | arm64, x64, x86 |
| Definition | [dynareadout/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/d/dynareadout/xmake.lua) |

##### Install command

```console
xrepo install dynareadout
```

##### Integration in the project (xmake.lua)

```lua
add_requires("dynareadout")
```



## e
### eabase (windows)


| Description | *EABase is a small set of header files that define platform-independent data types and platform feature macros.* |
| -- | -- |
| Homepage | [https://github.com/electronicarts/EABase](https://github.com/electronicarts/EABase) |
| License | BSD-3-Clause |
| Versions | 2.09.05 |
| Architectures | arm64, x64, x86 |
| Definition | [eabase/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/e/eabase/xmake.lua) |

##### Install command

```console
xrepo install eabase
```

##### Integration in the project (xmake.lua)

```lua
add_requires("eabase")
```


### eastl (windows)


| Description | *EASTL stands for Electronic Arts Standard Template Library.* |
| -- | -- |
| Homepage | [https://github.com/electronicarts/EASTL](https://github.com/electronicarts/EASTL) |
| License | BSD-3-Clause |
| Versions | 3.17.03, 3.17.06 |
| Architectures | arm64, x64, x86 |
| Definition | [eastl/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/e/eastl/xmake.lua) |

##### Install command

```console
xrepo install eastl
```

##### Integration in the project (xmake.lua)

```lua
add_requires("eastl")
```


### easyloggingpp (windows)


| Description | *Single header C++ logging library.* |
| -- | -- |
| Homepage | [https://github.com/amrayn/easyloggingpp](https://github.com/amrayn/easyloggingpp) |
| License | MIT |
| Versions | v9.97.0 |
| Architectures | arm64, x64, x86 |
| Definition | [easyloggingpp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/e/easyloggingpp/xmake.lua) |

##### Install command

```console
xrepo install easyloggingpp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("easyloggingpp")
```


### effcee (windows)


| Description | *Effcee is a C++ library for stateful pattern matching of strings.* |
| -- | -- |
| Homepage | [https://github.com/google/effcee](https://github.com/google/effcee) |
| License | Apache-2.0 |
| Versions | 2019.1 |
| Architectures | arm64, x64, x86 |
| Definition | [effcee/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/e/effcee/xmake.lua) |

##### Install command

```console
xrepo install effcee
```

##### Integration in the project (xmake.lua)

```lua
add_requires("effcee")
```


### efsw (windows)


| Description | *efsw is a C++ cross-platform file system watcher and notifier.* |
| -- | -- |
| Homepage | [https://github.com/SpartanJ/efsw](https://github.com/SpartanJ/efsw) |
| License | MIT |
| Versions | 1.1.0 |
| Architectures | arm64, x64, x86 |
| Definition | [efsw/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/e/efsw/xmake.lua) |

##### Install command

```console
xrepo install efsw
```

##### Integration in the project (xmake.lua)

```lua
add_requires("efsw")
```


### eigen (windows)


| Description | *C++ template library for linear algebra* |
| -- | -- |
| Homepage | [https://eigen.tuxfamily.org/](https://eigen.tuxfamily.org/) |
| License | MPL-2.0 |
| Versions | 3.3.7, 3.3.8, 3.3.9, 3.4.0 |
| Architectures | arm64, x64, x86 |
| Definition | [eigen/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/e/eigen/xmake.lua) |

##### Install command

```console
xrepo install eigen
```

##### Integration in the project (xmake.lua)

```lua
add_requires("eigen")
```


### elfio (windows)


| Description | *ELFIO - ELF (Executable and Linkable Format) reader and producer implemented as a header only C++ library* |
| -- | -- |
| Homepage | [http://serge1.github.io/ELFIO](http://serge1.github.io/ELFIO) |
| License | MIT |
| Versions | 3.11 |
| Architectures | arm64, x64, x86 |
| Definition | [elfio/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/e/elfio/xmake.lua) |

##### Install command

```console
xrepo install elfio
```

##### Integration in the project (xmake.lua)

```lua
add_requires("elfio")
```


### embree (windows)


| Description | *Intel® Embree is a collection of high-performance ray tracing kernels, developed at Intel.* |
| -- | -- |
| Homepage | [https://www.embree.org/](https://www.embree.org/) |
| License | Apache-2.0 |
| Versions | v3.12.1, v3.13.0, v3.13.3, v3.13.4, v3.13.5 |
| Architectures | x64, x86 |
| Definition | [embree/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/e/embree/xmake.lua) |

##### Install command

```console
xrepo install embree
```

##### Integration in the project (xmake.lua)

```lua
add_requires("embree")
```


### enet (windows)


| Description | *Reliable UDP networking library.* |
| -- | -- |
| Homepage | [http://enet.bespin.org](http://enet.bespin.org) |
| License | MIT |
| Versions | v1.3.17 |
| Architectures | arm64, x64, x86 |
| Definition | [enet/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/e/enet/xmake.lua) |

##### Install command

```console
xrepo install enet
```

##### Integration in the project (xmake.lua)

```lua
add_requires("enet")
```


### enkits (windows)


| Description | *A permissively licensed C and C++ Task Scheduler for creating parallel programs.* |
| -- | -- |
| Homepage | [https://github.com/dougbinks/enkiTS](https://github.com/dougbinks/enkiTS) |
| License | zlib |
| Versions | v1.10 |
| Architectures | arm64, x64, x86 |
| Definition | [enkits/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/e/enkits/xmake.lua) |

##### Install command

```console
xrepo install enkits
```

##### Integration in the project (xmake.lua)

```lua
add_requires("enkits")
```


### ensmallen (windows)


| Description | *flexible C++ library for efficient numerical optimization* |
| -- | -- |
| Homepage | [https://ensmallen.org/](https://ensmallen.org/) |
| License | BSD-3-Clause |
| Versions | 2.18.0 |
| Architectures | arm64, x64, x86 |
| Definition | [ensmallen/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/e/ensmallen/xmake.lua) |

##### Install command

```console
xrepo install ensmallen
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ensmallen")
```


### entt (windows)


| Description | *Gaming meets modern C++ - a fast and reliable entity component system (ECS) and much more.* |
| -- | -- |
| Homepage | [https://github.com/skypjack/entt](https://github.com/skypjack/entt) |
| License | MIT |
| Versions | v3.10.0, v3.10.1, v3.10.3, v3.11.0, v3.11.1, v3.6.0, v3.7.0, v3.7.1, v3.8.0, v3.8.1, v3.9.0 |
| Architectures | arm64, x64, x86 |
| Definition | [entt/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/e/entt/xmake.lua) |

##### Install command

```console
xrepo install entt
```

##### Integration in the project (xmake.lua)

```lua
add_requires("entt")
```


### expat (windows)


| Description | *XML 1.0 parser* |
| -- | -- |
| Homepage | [https://libexpat.github.io](https://libexpat.github.io) |
| License | MIT |
| Versions | 2.2.10, 2.2.6, 2.3.0, 2.4.1, 2.4.5, 2.4.7, 2.4.8 |
| Architectures | arm64, x64, x86 |
| Definition | [expat/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/e/expat/xmake.lua) |

##### Install command

```console
xrepo install expat
```

##### Integration in the project (xmake.lua)

```lua
add_requires("expat")
```


### expected (windows)


| Description | *C++11/14/17 std::expected with functional-style extensions* |
| -- | -- |
| Homepage | [https://github.com/TartanLlama/expected](https://github.com/TartanLlama/expected) |
| License | CC0 |
| Versions | v1.0.0 |
| Architectures | arm64, x64, x86 |
| Definition | [expected/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/e/expected/xmake.lua) |

##### Install command

```console
xrepo install expected
```

##### Integration in the project (xmake.lua)

```lua
add_requires("expected")
```


### exprtk (windows)


| Description | *C++ Mathematical Expression Parsing And Evaluation Library* |
| -- | -- |
| Homepage | [https://www.partow.net/programming/exprtk/index.html](https://www.partow.net/programming/exprtk/index.html) |
| License | MIT |
| Versions | 2021.06.06 |
| Architectures | arm64, x64, x86 |
| Definition | [exprtk/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/e/exprtk/xmake.lua) |

##### Install command

```console
xrepo install exprtk
```

##### Integration in the project (xmake.lua)

```lua
add_requires("exprtk")
```


### ezc3d (windows)


| Description | *Easy to use C3D reader/writer for C++, Python and Matlab* |
| -- | -- |
| Homepage | [https://github.com/pyomeca/ezc3d](https://github.com/pyomeca/ezc3d) |
| License | MIT |
| Versions | 1.4.5, 1.4.7 |
| Architectures | arm64, x64, x86 |
| Definition | [ezc3d/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/e/ezc3d/xmake.lua) |

##### Install command

```console
xrepo install ezc3d
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ezc3d")
```



## f
### faad2 (windows)


| Description | *FAAD2 is a HE, LC, MAIN and LTP profile, MPEG2 and MPEG-4 AAC decoder.* |
| -- | -- |
| Homepage | [https://sourceforge.net/projects/faac](https://sourceforge.net/projects/faac) |
| License | GPL-2.0 |
| Versions | 2.10.0 |
| Architectures | arm64, x64, x86 |
| Definition | [faad2/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/faad2/xmake.lua) |

##### Install command

```console
xrepo install faad2
```

##### Integration in the project (xmake.lua)

```lua
add_requires("faad2")
```


### faiss (windows)


| Description | *A library for efficient similarity search and clustering of dense vectors.* |
| -- | -- |
| Homepage | [https://github.com/facebookresearch/faiss/](https://github.com/facebookresearch/faiss/) |
| License | MIT |
| Versions | v1.7.0 |
| Architectures | x64 |
| Definition | [faiss/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/faiss/xmake.lua) |

##### Install command

```console
xrepo install faiss
```

##### Integration in the project (xmake.lua)

```lua
add_requires("faiss")
```


### fann (windows)


| Description | *Official github repository for Fast Artificial Neural Network Library (FANN)* |
| -- | -- |
| Homepage | [https://github.com/libfann/fann](https://github.com/libfann/fann) |
| License | LGPL-2.1 |
| Versions | 2021.03.14 |
| Architectures | arm64, x64, x86 |
| Definition | [fann/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/fann/xmake.lua) |

##### Install command

```console
xrepo install fann
```

##### Integration in the project (xmake.lua)

```lua
add_requires("fann")
```


### farmhash (windows)


| Description | *FarmHash, a family of hash functions.* |
| -- | -- |
| Homepage | [https://github.com/google/farmhash](https://github.com/google/farmhash) |
| License | MIT |
| Versions | 2019.05.14 |
| Architectures | arm64, x64, x86 |
| Definition | [farmhash/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/farmhash/xmake.lua) |

##### Install command

```console
xrepo install farmhash
```

##### Integration in the project (xmake.lua)

```lua
add_requires("farmhash")
```


### fast_double_parser (windows)


| Description | *Fast function to parse strings containing decimal numbers into double-precision (binary64) floating-point values.* |
| -- | -- |
| Homepage | [https://github.com/lemire/fast_double_parser](https://github.com/lemire/fast_double_parser) |
| License | Apache-2.0 |
| Versions | v0.5.0 |
| Architectures | x64 |
| Definition | [fast_double_parser/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/fast_double_parser/xmake.lua) |

##### Install command

```console
xrepo install fast_double_parser
```

##### Integration in the project (xmake.lua)

```lua
add_requires("fast_double_parser")
```


### fast_float (windows)


| Description | *Fast and exact implementation of the C++ from_chars functions for float and double types: 4x faster than strtod* |
| -- | -- |
| Homepage | [https://github.com/fastfloat/fast_float](https://github.com/fastfloat/fast_float) |
| License | Apache-2.0 |
| Versions | v3.4.0, v3.5.1 |
| Architectures | arm64, x64, x86 |
| Definition | [fast_float/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/fast_float/xmake.lua) |

##### Install command

```console
xrepo install fast_float
```

##### Integration in the project (xmake.lua)

```lua
add_requires("fast_float")
```


### fast_io (windows)


| Description | *Significantly faster input/output for C++20* |
| -- | -- |
| Homepage | [https://github.com/cppfastio/fast_io](https://github.com/cppfastio/fast_io) |
| License | MIT |
| Versions | 2023.1.28 |
| Architectures | arm64, x64, x86 |
| Definition | [fast_io/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/fast_io/xmake.lua) |

##### Install command

```console
xrepo install fast_io
```

##### Integration in the project (xmake.lua)

```lua
add_requires("fast_io")
```


### fastcppcsvparser (windows)


| Description | *This is a small, easy-to-use and fast header-only library for reading comma separated value (CSV) files (by ben-strasser)* |
| -- | -- |
| Homepage | [https://github.com/ben-strasser/fast-cpp-csv-parser](https://github.com/ben-strasser/fast-cpp-csv-parser) |
| Versions | 2021.01.03 |
| Architectures | arm64, x64, x86 |
| Definition | [fastcppcsvparser/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/fastcppcsvparser/xmake.lua) |

##### Install command

```console
xrepo install fastcppcsvparser
```

##### Integration in the project (xmake.lua)

```lua
add_requires("fastcppcsvparser")
```


### fastor (windows)


| Description | *A lightweight high performance tensor algebra framework for modern C++* |
| -- | -- |
| Homepage | [https://github.com/romeric/Fastor](https://github.com/romeric/Fastor) |
| License | MIT |
| Versions | 0.6.3 |
| Architectures | arm64, x64, x86 |
| Definition | [fastor/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/fastor/xmake.lua) |

##### Install command

```console
xrepo install fastor
```

##### Integration in the project (xmake.lua)

```lua
add_requires("fastor")
```


### fcl (windows)


| Description | *Flexible Collision Library* |
| -- | -- |
| Homepage | [https://github.com/flexible-collision-library/fcl](https://github.com/flexible-collision-library/fcl) |
| License | BSD-3-Clause |
| Versions | 0.7.0, v0.6.1 |
| Architectures | arm64, x64, x86 |
| Definition | [fcl/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/fcl/xmake.lua) |

##### Install command

```console
xrepo install fcl
```

##### Integration in the project (xmake.lua)

```lua
add_requires("fcl")
```


### ffmpeg (windows)


| Description | *A collection of libraries to process multimedia content such as audio, video, subtitles and related metadata.* |
| -- | -- |
| Homepage | [https://www.ffmpeg.org](https://www.ffmpeg.org) |
| License | GPL-3.0 |
| Versions | 4.0.2, 5.0.1, 5.1.1, 5.1.2 |
| Architectures | x64 |
| Definition | [ffmpeg/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/ffmpeg/xmake.lua) |

##### Install command

```console
xrepo install ffmpeg
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ffmpeg")
```


### fftw (windows)


| Description | *A C subroutine library for computing the discrete Fourier transform (DFT) in one or more dimensions.* |
| -- | -- |
| Homepage | [http://fftw.org/](http://fftw.org/) |
| License | GPL-2.0 |
| Versions | 3.3.10, 3.3.8, 3.3.9 |
| Architectures | arm64, x64, x86 |
| Definition | [fftw/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/fftw/xmake.lua) |

##### Install command

```console
xrepo install fftw
```

##### Integration in the project (xmake.lua)

```lua
add_requires("fftw")
```


### field3d (windows)


| Description | *Field3D is an open source library for storing voxel data.* |
| -- | -- |
| Homepage | [https://sites.google.com/site/field3d/](https://sites.google.com/site/field3d/) |
| License | BSD-3-Clause |
| Versions | v1.7.3 |
| Architectures | arm64, x64, x86 |
| Definition | [field3d/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/field3d/xmake.lua) |

##### Install command

```console
xrepo install field3d
```

##### Integration in the project (xmake.lua)

```lua
add_requires("field3d")
```


### filament (windows)


| Description | *Filament is a real-time physically-based renderer written in C++.* |
| -- | -- |
| Homepage | [https://google.github.io/filament/](https://google.github.io/filament/) |
| License | Apache-2.0 |
| Versions | 1.20.3, 1.9.23 |
| Architectures | x64 |
| Definition | [filament/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/filament/xmake.lua) |

##### Install command

```console
xrepo install filament
```

##### Integration in the project (xmake.lua)

```lua
add_requires("filament")
```


### flann (windows)


| Description | *Fast Library for Approximate Nearest Neighbors* |
| -- | -- |
| Homepage | [https://github.com/flann-lib/flann/](https://github.com/flann-lib/flann/) |
| License | BSD-3-Clause |
| Versions | 1.9.1 |
| Architectures | arm64, x64, x86 |
| Definition | [flann/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/flann/xmake.lua) |

##### Install command

```console
xrepo install flann
```

##### Integration in the project (xmake.lua)

```lua
add_requires("flann")
```


### flatbuffers (windows)


| Description | *FlatBuffers is a cross platform serialization library architected for maximum memory efficiency.* |
| -- | -- |
| Homepage | [http://google.github.io/flatbuffers/](http://google.github.io/flatbuffers/) |
| Versions | 1.12.0, 2.0.0 |
| Architectures | arm64, x64, x86 |
| Definition | [flatbuffers/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/flatbuffers/xmake.lua) |

##### Install command

```console
xrepo install flatbuffers
```

##### Integration in the project (xmake.lua)

```lua
add_requires("flatbuffers")
```


### flecs (windows)


| Description | *A fast entity component system (ECS) for C & C++* |
| -- | -- |
| Homepage | [https://github.com/SanderMertens/flecs](https://github.com/SanderMertens/flecs) |
| License | MIT |
| Versions | v2.4.8, v3.0.0 |
| Architectures | arm64, x64, x86 |
| Definition | [flecs/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/flecs/xmake.lua) |

##### Install command

```console
xrepo install flecs
```

##### Integration in the project (xmake.lua)

```lua
add_requires("flecs")
```


### flex (windows)


| Description | *%s* |
| -- | -- |
| Homepage | [https://github.com/westes/flex/](https://github.com/westes/flex/) |
| License | BSD-2-Clause |
| Versions | 2.6.4 |
| Architectures | arm64, x64, x86 |
| Definition | [flex/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/flex/xmake.lua) |

##### Install command

```console
xrepo install flex
```

##### Integration in the project (xmake.lua)

```lua
add_requires("flex")
```


### fltk (windows)


| Description | *Fast Light Toolkit* |
| -- | -- |
| Homepage | [https://www.fltk.org](https://www.fltk.org) |
| Versions | 1.4.0 |
| Architectures | arm64, x64, x86 |
| Definition | [fltk/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/fltk/xmake.lua) |

##### Install command

```console
xrepo install fltk
```

##### Integration in the project (xmake.lua)

```lua
add_requires("fltk")
```


### fmt (windows)


| Description | *fmt is an open-source formatting library for C++. It can be used as a safe and fast alternative to (s)printf and iostreams.* |
| -- | -- |
| Homepage | [https://fmt.dev](https://fmt.dev) |
| Versions | 5.3.0, 6.0.0, 6.2.0, 7.1.3, 8.0.0, 8.0.1, 8.1.1, 9.0.0, 9.1.0 |
| Architectures | arm64, x64, x86 |
| Definition | [fmt/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/fmt/xmake.lua) |

##### Install command

```console
xrepo install fmt
```

##### Integration in the project (xmake.lua)

```lua
add_requires("fmt")
```


### fmtlog (windows)


| Description | *fmtlog is a performant fmtlib-style logging library with latency in nanoseconds.* |
| -- | -- |
| Homepage | [https://github.com/MengRao/fmtlog](https://github.com/MengRao/fmtlog) |
| License | MIT |
| Versions | v2.1.2 |
| Architectures | arm64, x64, x86 |
| Definition | [fmtlog/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/fmtlog/xmake.lua) |

##### Install command

```console
xrepo install fmtlog
```

##### Integration in the project (xmake.lua)

```lua
add_requires("fmtlog")
```


### folly (windows)


| Description | *An open-source C++ library developed and used at Facebook.* |
| -- | -- |
| Homepage | [https://github.com/facebook/folly](https://github.com/facebook/folly) |
| License | Apache-2.0 |
| Versions | 2021.06.28, 2021.08.02, 2021.11.01, 2022.02.14, 2022.04.25, 2022.08.29 |
| Architectures | x64 |
| Definition | [folly/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/folly/xmake.lua) |

##### Install command

```console
xrepo install folly
```

##### Integration in the project (xmake.lua)

```lua
add_requires("folly")
```


### fpng (windows)


| Description | *Super fast C++ .PNG writer/reader* |
| -- | -- |
| Homepage | [https://github.com/richgel999/fpng](https://github.com/richgel999/fpng) |
| Versions | v1.0.1 |
| Architectures | arm64, x64, x86 |
| Definition | [fpng/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/fpng/xmake.lua) |

##### Install command

```console
xrepo install fpng
```

##### Integration in the project (xmake.lua)

```lua
add_requires("fpng")
```


### freeglut (windows)


| Description | *A free-software/open-source alternative to the OpenGL Utility Toolkit (GLUT) library.* |
| -- | -- |
| Homepage | [http://freeglut.sourceforge.net](http://freeglut.sourceforge.net) |
| License | MIT |
| Versions | 3.0.0, 3.2.1, 3.2.2 |
| Architectures | arm64, x64, x86 |
| Definition | [freeglut/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/freeglut/xmake.lua) |

##### Install command

```console
xrepo install freeglut
```

##### Integration in the project (xmake.lua)

```lua
add_requires("freeglut")
```


### freeimage (windows)


| Description | *FreeImage is a library project for developers who would like to support popular graphics image formats (PNG, JPEG, TIFF, BMP and others).* |
| -- | -- |
| Homepage | [https://sourceforge.net/projects/freeimage/](https://sourceforge.net/projects/freeimage/) |
| Versions | 3.18.0 |
| Architectures | arm64, x64, x86 |
| Definition | [freeimage/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/freeimage/xmake.lua) |

##### Install command

```console
xrepo install freeimage
```

##### Integration in the project (xmake.lua)

```lua
add_requires("freeimage")
```


### freetype (windows)


| Description | *A freely available software library to render fonts.* |
| -- | -- |
| Homepage | [https://www.freetype.org](https://www.freetype.org) |
| Versions | 2.10.4, 2.11.0, 2.11.1, 2.12.1, 2.9.1 |
| Architectures | arm64, x64, x86 |
| Definition | [freetype/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/freetype/xmake.lua) |

##### Install command

```console
xrepo install freetype
```

##### Integration in the project (xmake.lua)

```lua
add_requires("freetype")
```


### fribidi (windows)


| Description | *The Free Implementation of the Unicode Bidirectional Algorithm.* |
| -- | -- |
| Homepage | [https://github.com/fribidi/fribidi](https://github.com/fribidi/fribidi) |
| License | LGPL-2.1 |
| Versions | 1.0.10, 1.0.11, 1.0.12 |
| Architectures | arm64, x64, x86 |
| Definition | [fribidi/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/fribidi/xmake.lua) |

##### Install command

```console
xrepo install fribidi
```

##### Integration in the project (xmake.lua)

```lua
add_requires("fribidi")
```


### frozen (windows)


| Description | *A header-only, constexpr alternative to gperf for C++14 users* |
| -- | -- |
| Homepage | [https://github.com/serge-sans-paille/frozen](https://github.com/serge-sans-paille/frozen) |
| License | Apache-2.0 |
| Versions | 1.1.1 |
| Architectures | arm64, x64, x86 |
| Definition | [frozen/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/frozen/xmake.lua) |

##### Install command

```console
xrepo install frozen
```

##### Integration in the project (xmake.lua)

```lua
add_requires("frozen")
```


### frozenca-btree (windows)


| Description | *A general-purpose high-performance lightweight STL-like modern C++ B-Tree* |
| -- | -- |
| Homepage | [https://github.com/frozenca/BTree](https://github.com/frozenca/BTree) |
| License | Apache-2.0 |
| Versions | 2022.08.02 |
| Architectures | arm64, x64, x86 |
| Definition | [frozenca-btree/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/frozenca-btree/xmake.lua) |

##### Install command

```console
xrepo install frozenca-btree
```

##### Integration in the project (xmake.lua)

```lua
add_requires("frozenca-btree")
```


### ftgl (windows)


| Description | *FTGL is a free open source library to enable developers to use arbitrary fonts in their OpenGL applications.* |
| -- | -- |
| Homepage | [https://github.com/frankheckenbach/ftgl](https://github.com/frankheckenbach/ftgl) |
| License | MIT |
| Versions | v2.4.0 |
| Architectures | arm64, x64, x86 |
| Definition | [ftgl/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/ftgl/xmake.lua) |

##### Install command

```console
xrepo install ftgl
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ftgl")
```


### functionalplus (windows)


| Description | *Functional Programming Library for C++. Write concise and readable C++ code.* |
| -- | -- |
| Homepage | [http://www.editgym.com/fplus-api-search/](http://www.editgym.com/fplus-api-search/) |
| Versions | v0.2.18-p0 |
| Architectures | arm64, x64, x86 |
| Definition | [functionalplus/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/functionalplus/xmake.lua) |

##### Install command

```console
xrepo install functionalplus
```

##### Integration in the project (xmake.lua)

```lua
add_requires("functionalplus")
```


### fx-gltf (windows)


| Description | *A C++14/C++17 header-only library for simple, efficient, and robust serialization/deserialization of glTF 2.0* |
| -- | -- |
| Homepage | [https://github.com/jessey-git/fx-gltf](https://github.com/jessey-git/fx-gltf) |
| License | MIT |
| Versions | v1.2.0, v2.0.0 |
| Architectures | arm64, x64, x86 |
| Definition | [fx-gltf/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/fx-gltf/xmake.lua) |

##### Install command

```console
xrepo install fx-gltf
```

##### Integration in the project (xmake.lua)

```lua
add_requires("fx-gltf")
```


### fxdiv (windows)


| Description | *C99/C++ header-only library for division via fixed-point multiplication by inverse* |
| -- | -- |
| Homepage | [https://github.com/Maratyszcza/FXdiv](https://github.com/Maratyszcza/FXdiv) |
| License | MIT |
| Versions | 2020.12.09 |
| Architectures | arm64, x64, x86 |
| Definition | [fxdiv/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/f/fxdiv/xmake.lua) |

##### Install command

```console
xrepo install fxdiv
```

##### Integration in the project (xmake.lua)

```lua
add_requires("fxdiv")
```



## g
### g2o (windows)


| Description | *g2o: A General Framework for Graph Optimization* |
| -- | -- |
| Homepage | [http://openslam.org/g2o.html](http://openslam.org/g2o.html) |
| Versions | 2020.12.23 |
| Architectures | arm64, x64, x86 |
| Definition | [g2o/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/g2o/xmake.lua) |

##### Install command

```console
xrepo install g2o
```

##### Integration in the project (xmake.lua)

```lua
add_requires("g2o")
```


### gamenetworkingsockets (windows)


| Description | *Reliable & unreliable messages over UDP. Robust message fragmentation & reassembly. P2P networking / NAT traversal. Encryption. * |
| -- | -- |
| Homepage | [https://github.com/ValveSoftware/GameNetworkingSockets](https://github.com/ValveSoftware/GameNetworkingSockets) |
| License | BSD-3-Clause |
| Versions | v1.2.0, v1.3.0, v1.4.0, v1.4.1 |
| Architectures | arm64, x64, x86 |
| Definition | [gamenetworkingsockets/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/gamenetworkingsockets/xmake.lua) |

##### Install command

```console
xrepo install gamenetworkingsockets
```

##### Integration in the project (xmake.lua)

```lua
add_requires("gamenetworkingsockets")
```


### gcem (windows)


| Description | *A C++ compile-time math library using generalized constant expressions* |
| -- | -- |
| Homepage | [https://www.kthohr.com/gcem.html](https://www.kthohr.com/gcem.html) |
| License | Apache-2.0 |
| Versions | v1.13.1, v1.16.0 |
| Architectures | arm64, x64, x86 |
| Definition | [gcem/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/gcem/xmake.lua) |

##### Install command

```console
xrepo install gcem
```

##### Integration in the project (xmake.lua)

```lua
add_requires("gcem")
```


### gdal (windows)


| Description | *GDAL is a translator library for raster and vector geospatial data formats by the Open Source Geospatial Foundation* |
| -- | -- |
| Homepage | [https://gdal.org/](https://gdal.org/) |
| License | MIT |
| Versions | 3.5.1 |
| Architectures | x64, x86 |
| Definition | [gdal/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/gdal/xmake.lua) |

##### Install command

```console
xrepo install gdal
```

##### Integration in the project (xmake.lua)

```lua
add_requires("gdal")
```


### geant4 (windows)


| Description | *Geant4 is a toolkit for the simulation of the passage of particles through matter.* |
| -- | -- |
| Homepage | [https://geant4.web.cern.ch/](https://geant4.web.cern.ch/) |
| Versions | 10.7.2 |
| Architectures | arm64, x64, x86 |
| Definition | [geant4/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/geant4/xmake.lua) |

##### Install command

```console
xrepo install geant4
```

##### Integration in the project (xmake.lua)

```lua
add_requires("geant4")
```


### genie (windows)


| Description | *GENie - Project generator tool* |
| -- | -- |
| Homepage | [https://github.com/bkaradzic/GENie](https://github.com/bkaradzic/GENie) |
| Versions | 1160.0 |
| Architectures | arm64, x64, x86 |
| Definition | [genie/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/genie/xmake.lua) |

##### Install command

```console
xrepo install genie
```

##### Integration in the project (xmake.lua)

```lua
add_requires("genie")
```


### geographiclib (windows)


| Description | *GeographicLib is a small C++ library for geodesic and rhumb line calculations and conversions between geographic, UTM, UPS, MGRS, geocentric* |
| -- | -- |
| Homepage | [https://geographiclib.sourceforge.io/C++/doc/index.html](https://geographiclib.sourceforge.io/C++/doc/index.html) |
| License | MIT License |
| Versions | 2.1.1 |
| Architectures | arm64, x64, x86 |
| Definition | [geographiclib/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/geographiclib/xmake.lua) |

##### Install command

```console
xrepo install geographiclib
```

##### Integration in the project (xmake.lua)

```lua
add_requires("geographiclib")
```


### geos (windows)


| Description | *GEOS (Geometry Engine - Open Source) is a C++ port of the ​JTS Topology Suite (JTS).* |
| -- | -- |
| Homepage | [https://trac.osgeo.org/geos/](https://trac.osgeo.org/geos/) |
| License | LGPL-2.1 |
| Versions | 3.9.1 |
| Architectures | arm64, x64, x86 |
| Definition | [geos/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/geos/xmake.lua) |

##### Install command

```console
xrepo install geos
```

##### Integration in the project (xmake.lua)

```lua
add_requires("geos")
```


### gflags (windows)


| Description | *The gflags package contains a C++ library that implements commandline flags processing.* |
| -- | -- |
| Homepage | [https://github.com/gflags/gflags/](https://github.com/gflags/gflags/) |
| License | BSD-3-Clause |
| Versions | v2.2.2 |
| Architectures | arm64, x64, x86 |
| Definition | [gflags/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/gflags/xmake.lua) |

##### Install command

```console
xrepo install gflags
```

##### Integration in the project (xmake.lua)

```lua
add_requires("gflags")
```


### ghc_filesystem (windows)


| Description | *An implementation of C++17 std::filesystem for C++11 /C++14/C++17/C++20 on Windows, macOS, Linux and FreeBSD.* |
| -- | -- |
| Homepage | [https://github.com/gulrak/filesystem](https://github.com/gulrak/filesystem) |
| License | MIT |
| Versions | v1.5.10 |
| Architectures | arm64, x64, x86 |
| Definition | [ghc_filesystem/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/ghc_filesystem/xmake.lua) |

##### Install command

```console
xrepo install ghc_filesystem
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ghc_filesystem")
```


### ghostscript (windows)


| Description | *Ghostscript is an interpreter for the PostScript® language and PDF files.* |
| -- | -- |
| Homepage | [https://www.ghostscript.com/](https://www.ghostscript.com/) |
| License | AGPL-3.0 |
| Versions | 9.55.0 |
| Architectures | arm64, x64, x86 |
| Definition | [ghostscript/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/ghostscript/xmake.lua) |

##### Install command

```console
xrepo install ghostscript
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ghostscript")
```


### giflib (windows)


| Description | *A library for reading and writing gif images.* |
| -- | -- |
| Homepage | [https://sourceforge.net/projects/giflib/](https://sourceforge.net/projects/giflib/) |
| License | MIT |
| Versions | 5.2.1 |
| Architectures | arm64, x64, x86 |
| Definition | [giflib/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/giflib/xmake.lua) |

##### Install command

```console
xrepo install giflib
```

##### Integration in the project (xmake.lua)

```lua
add_requires("giflib")
```


### glad (windows)


| Description | *Multi-Language Vulkan/GL/GLES/EGL/GLX/WGL Loader-Generator based on the official specs.* |
| -- | -- |
| Homepage | [https://glad.dav1d.de/](https://glad.dav1d.de/) |
| License | MIT |
| Versions | v0.1.34, v0.1.36 |
| Architectures | arm64, x64, x86 |
| Definition | [glad/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/glad/xmake.lua) |

##### Install command

```console
xrepo install glad
```

##### Integration in the project (xmake.lua)

```lua
add_requires("glad")
```


### glew (windows)


| Description | *A cross-platform open-source C/C++ extension loading library.* |
| -- | -- |
| Homepage | [http://glew.sourceforge.net/](http://glew.sourceforge.net/) |
| Versions | 2.1.0, 2.2.0 |
| Architectures | arm64, x64, x86 |
| Definition | [glew/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/glew/xmake.lua) |

##### Install command

```console
xrepo install glew
```

##### Integration in the project (xmake.lua)

```lua
add_requires("glew")
```


### glfw (windows)


| Description | *GLFW is an Open Source, multi-platform library for OpenGL, OpenGL ES and Vulkan application development.* |
| -- | -- |
| Homepage | [https://www.glfw.org/](https://www.glfw.org/) |
| License | zlib |
| Versions | 3.3.2, 3.3.4, 3.3.5, 3.3.6, 3.3.7, 3.3.8 |
| Architectures | arm64, x64, x86 |
| Definition | [glfw/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/glfw/xmake.lua) |

##### Install command

```console
xrepo install glfw
```

##### Integration in the project (xmake.lua)

```lua
add_requires("glfw")
```


### gli (windows)


| Description | *OpenGL Image (GLI)* |
| -- | -- |
| Homepage | [https://gli.g-truc.net/](https://gli.g-truc.net/) |
| Versions | 0.8.2.0 |
| Architectures | arm64, x64, x86 |
| Definition | [gli/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/gli/xmake.lua) |

##### Install command

```console
xrepo install gli
```

##### Integration in the project (xmake.lua)

```lua
add_requires("gli")
```


### glib (windows)


| Description | *Core application library for C.* |
| -- | -- |
| Homepage | [https://developer.gnome.org/glib/](https://developer.gnome.org/glib/) |
| Versions | 2.71.0 |
| Architectures | arm64, x64, x86 |
| Definition | [glib/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/glib/xmake.lua) |

##### Install command

```console
xrepo install glib
```

##### Integration in the project (xmake.lua)

```lua
add_requires("glib")
```


### glm (windows)


| Description | *OpenGL Mathematics (GLM)* |
| -- | -- |
| Homepage | [https://glm.g-truc.net/](https://glm.g-truc.net/) |
| Versions | 0.9.9+8 |
| Architectures | arm64, x64, x86 |
| Definition | [glm/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/glm/xmake.lua) |

##### Install command

```console
xrepo install glm
```

##### Integration in the project (xmake.lua)

```lua
add_requires("glm")
```


### glog (windows)


| Description | *C++ implementation of the Google logging module* |
| -- | -- |
| Homepage | [https://github.com/google/glog/](https://github.com/google/glog/) |
| License | BSD-3-Clause |
| Versions | v0.4.0, v0.5.0, v0.6.0 |
| Architectures | arm64, x64, x86 |
| Definition | [glog/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/glog/xmake.lua) |

##### Install command

```console
xrepo install glog
```

##### Integration in the project (xmake.lua)

```lua
add_requires("glog")
```


### glslang (windows)


| Description | *Khronos-reference front end for GLSL/ESSL, partial front end for HLSL, and a SPIR-V generator.* |
| -- | -- |
| Homepage | [https://github.com/KhronosGroup/glslang/](https://github.com/KhronosGroup/glslang/) |
| License | Apache-2.0 |
| Versions | 1.2.154+1, 1.2.162+0, 1.2.189+1, 1.3.211+0, 1.3.231+1 |
| Architectures | arm64, x64, x86 |
| Definition | [glslang/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/glslang/xmake.lua) |

##### Install command

```console
xrepo install glslang
```

##### Integration in the project (xmake.lua)

```lua
add_requires("glslang")
```


### gmm (windows)


| Description | *Gmm++ provides some basic types of sparse and dense matrices and vectors.* |
| -- | -- |
| Homepage | [http://getfem.org/gmm/index.html](http://getfem.org/gmm/index.html) |
| Versions | 5.4 |
| Architectures | arm64, x64, x86 |
| Definition | [gmm/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/gmm/xmake.lua) |

##### Install command

```console
xrepo install gmm
```

##### Integration in the project (xmake.lua)

```lua
add_requires("gmm")
```


### gmsh (windows)


| Description | *Gmsh is an open source 3D finite element mesh generator with a built-in CAD engine and post-processor.* |
| -- | -- |
| Homepage | [http://gmsh.info/](http://gmsh.info/) |
| License | GPL-2.0 |
| Versions | 4.8.4 |
| Architectures | arm64, x64, x86 |
| Definition | [gmsh/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/gmsh/xmake.lua) |

##### Install command

```console
xrepo install gmsh
```

##### Integration in the project (xmake.lua)

```lua
add_requires("gmsh")
```


### gn (windows)


| Description | *GN is a meta-build system that generates build files for Ninja.* |
| -- | -- |
| Homepage | [https://gn.googlesource.com/gn](https://gn.googlesource.com/gn) |
| Versions | 20211117 |
| Architectures | arm64, x64, x86 |
| Definition | [gn/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/gn/xmake.lua) |

##### Install command

```console
xrepo install gn
```

##### Integration in the project (xmake.lua)

```lua
add_requires("gn")
```


### gnu-rm (windows)


| Description | *GNU Arm Embedded Toolchain* |
| -- | -- |
| Homepage | [https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm) |
| Versions | 2020.10, 2021.10 |
| Architectures | arm64, x64, x86 |
| Definition | [gnu-rm/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/gnu-rm/xmake.lua) |

##### Install command

```console
xrepo install gnu-rm
```

##### Integration in the project (xmake.lua)

```lua
add_requires("gnu-rm")
```


### go (windows)


| Description | *The Go Programming Language* |
| -- | -- |
| Homepage | [https://golang.org/](https://golang.org/) |
| Versions | 1.17.6 |
| Architectures | arm64, x64, x86 |
| Definition | [go/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/go/xmake.lua) |

##### Install command

```console
xrepo install go
```

##### Integration in the project (xmake.lua)

```lua
add_requires("go")
```


### godotcpp (windows)


| Description | *C++ bindings for the Godot script API* |
| -- | -- |
| Homepage | [https://godotengine.org/](https://godotengine.org/) |
| Versions | 3.2, 3.3, 3.4.0, 3.4.3, 3.4.4 |
| Architectures | arm64, x64, x86 |
| Definition | [godotcpp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/godotcpp/xmake.lua) |

##### Install command

```console
xrepo install godotcpp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("godotcpp")
```


### gperftools (windows)


| Description | *gperftools is a collection of a high-performance multi-threaded malloc() implementation, plus some pretty nifty performance analysis tools.* |
| -- | -- |
| Homepage | [https://github.com/gperftools/gperftools](https://github.com/gperftools/gperftools) |
| License | BSD-3-Clause |
| Versions | 2.10 |
| Architectures | arm64, x64, x86 |
| Definition | [gperftools/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/gperftools/xmake.lua) |

##### Install command

```console
xrepo install gperftools
```

##### Integration in the project (xmake.lua)

```lua
add_requires("gperftools")
```


### gr (windows)


| Description | *GR framework: a graphics library for visualisation applications* |
| -- | -- |
| Homepage | [https://gr-framework.org/](https://gr-framework.org/) |
| License | MIT |
| Versions | 0.57.0, 0.58.0, 0.62.0, 0.64.0 |
| Architectures | x64 |
| Definition | [gr/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/gr/xmake.lua) |

##### Install command

```console
xrepo install gr
```

##### Integration in the project (xmake.lua)

```lua
add_requires("gr")
```


### graphene (windows)


| Description | *A thin layer of graphic data types* |
| -- | -- |
| Homepage | [http://ebassi.github.io/graphene/](http://ebassi.github.io/graphene/) |
| License | MIT |
| Versions | 1.10.6 |
| Architectures | arm64, x64, x86 |
| Definition | [graphene/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/graphene/xmake.lua) |

##### Install command

```console
xrepo install graphene
```

##### Integration in the project (xmake.lua)

```lua
add_requires("graphene")
```


### grpc (windows)


| Description | *The C based gRPC (C++, Python, Ruby, Objective-C, PHP, C#)* |
| -- | -- |
| Homepage | [https://grpc.io](https://grpc.io) |
| License | Apache-2.0 |
| Versions | v1.46.3 |
| Architectures | arm64, x64, x86 |
| Definition | [grpc/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/grpc/xmake.lua) |

##### Install command

```console
xrepo install grpc
```

##### Integration in the project (xmake.lua)

```lua
add_requires("grpc")
```


### gsl (windows)


| Description | *Guidelines Support Library* |
| -- | -- |
| Homepage | [https://github.com/microsoft/GSL](https://github.com/microsoft/GSL) |
| License | MIT |
| Versions | v3.1.0, v4.0.0 |
| Architectures | arm64, x64, x86 |
| Definition | [gsl/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/gsl/xmake.lua) |

##### Install command

```console
xrepo install gsl
```

##### Integration in the project (xmake.lua)

```lua
add_requires("gsl")
```


### gtest (windows)


| Description | *Google Testing and Mocking Framework.* |
| -- | -- |
| Homepage | [https://github.com/google/googletest](https://github.com/google/googletest) |
| Versions | 1.10.0, 1.11.0, 1.12.0, 1.12.1, 1.8.1 |
| Architectures | arm64, x64, x86 |
| Definition | [gtest/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/gtest/xmake.lua) |

##### Install command

```console
xrepo install gtest
```

##### Integration in the project (xmake.lua)

```lua
add_requires("gtest")
```


### guetzli (windows)


| Description | *Perceptual JPEG encoder* |
| -- | -- |
| Homepage | [https://github.com/google/guetzli](https://github.com/google/guetzli) |
| Versions | v1.0.1 |
| Architectures | arm64, x64, x86 |
| Definition | [guetzli/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/guetzli/xmake.lua) |

##### Install command

```console
xrepo install guetzli
```

##### Integration in the project (xmake.lua)

```lua
add_requires("guetzli")
```


### guilite (windows)


| Description | *The smallest header-only GUI library (4 KLOC) for all platforms.* |
| -- | -- |
| Homepage | [https://github.com/idea4good/GuiLite](https://github.com/idea4good/GuiLite) |
| License | Apache-2.0 |
| Versions | v2.1 |
| Architectures | arm64, x64, x86 |
| Definition | [guilite/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/guilite/xmake.lua) |

##### Install command

```console
xrepo install guilite
```

##### Integration in the project (xmake.lua)

```lua
add_requires("guilite")
```


### gyp-next (windows)


| Description | *A fork of the GYP build system for use in the Node.js projects* |
| -- | -- |
| Homepage | [https://github.com/nodejs/gyp-next](https://github.com/nodejs/gyp-next) |
| License | BSD-3-Clause |
| Versions | v0.11.0 |
| Architectures | arm64, x64, x86 |
| Definition | [gyp-next/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/g/gyp-next/xmake.lua) |

##### Install command

```console
xrepo install gyp-next
```

##### Integration in the project (xmake.lua)

```lua
add_requires("gyp-next")
```



## h
### h5cpp (windows)


| Description | *C++ wrapper for the HDF5 C-library* |
| -- | -- |
| Homepage | [https://ess-dmsc.github.io/h5cpp/](https://ess-dmsc.github.io/h5cpp/) |
| License | LGPL-2.1 |
| Versions | v0.5.1 |
| Architectures | arm64, x64, x86 |
| Definition | [h5cpp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/h/h5cpp/xmake.lua) |

##### Install command

```console
xrepo install h5cpp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("h5cpp")
```


### happly (windows)


| Description | *A C++ header-only parser for the PLY file format.* |
| -- | -- |
| Homepage | [https://github.com/nmwsharp/happly](https://github.com/nmwsharp/happly) |
| License | MIT |
| Versions | 2022.01.07 |
| Architectures | arm64, x64, x86 |
| Definition | [happly/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/h/happly/xmake.lua) |

##### Install command

```console
xrepo install happly
```

##### Integration in the project (xmake.lua)

```lua
add_requires("happly")
```


### harfbuzz (windows)


| Description | *HarfBuzz is a text shaping library.* |
| -- | -- |
| Homepage | [https://harfbuzz.github.io/](https://harfbuzz.github.io/) |
| License | MIT |
| Versions | 2.8.1, 2.9.0, 3.0.0, 3.1.1 |
| Architectures | arm64, x64, x86 |
| Definition | [harfbuzz/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/h/harfbuzz/xmake.lua) |

##### Install command

```console
xrepo install harfbuzz
```

##### Integration in the project (xmake.lua)

```lua
add_requires("harfbuzz")
```


### hash-library (windows)


| Description | *Portable C++ hashing library* |
| -- | -- |
| Homepage | [https://create.stephan-brumme.com/hash-library/](https://create.stephan-brumme.com/hash-library/) |
| License | zlib |
| Versions | 2021.09.29 |
| Architectures | arm64, x64, x86 |
| Definition | [hash-library/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/h/hash-library/xmake.lua) |

##### Install command

```console
xrepo install hash-library
```

##### Integration in the project (xmake.lua)

```lua
add_requires("hash-library")
```


### hdf5 (windows)


| Description | *High-performance data management and storage suite* |
| -- | -- |
| Homepage | [https://www.hdfgroup.org/solutions/hdf5/](https://www.hdfgroup.org/solutions/hdf5/) |
| License | BSD-3-Clause |
| Versions | 1.10.7, 1.12.0, 1.12.1, 1.12.2, 1.13.2, 1.13.3 |
| Architectures | arm64, x64, x86 |
| Definition | [hdf5/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/h/hdf5/xmake.lua) |

##### Install command

```console
xrepo install hdf5
```

##### Integration in the project (xmake.lua)

```lua
add_requires("hdf5")
```


### hdrhistogram_c (windows)


| Description | *C port of High Dynamic Range (HDR) Histogram* |
| -- | -- |
| Homepage | [https://github.com/HdrHistogram/HdrHistogram_c](https://github.com/HdrHistogram/HdrHistogram_c) |
| Versions | 2021.1.25 |
| Architectures | arm64, x64, x86 |
| Definition | [hdrhistogram_c/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/h/hdrhistogram_c/xmake.lua) |

##### Install command

```console
xrepo install hdrhistogram_c
```

##### Integration in the project (xmake.lua)

```lua
add_requires("hdrhistogram_c")
```


### highfive (windows)


| Description | *HighFive - Header-only C++ HDF5 interface* |
| -- | -- |
| Homepage | [https://github.com/BlueBrain/HighFive](https://github.com/BlueBrain/HighFive) |
| Versions | v2.3.1, v2.6.1 |
| Architectures | arm64, x64, x86 |
| Definition | [highfive/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/h/highfive/xmake.lua) |

##### Install command

```console
xrepo install highfive
```

##### Integration in the project (xmake.lua)

```lua
add_requires("highfive")
```


### hiredis (windows)


| Description | *Minimalistic C client for Redis >= 1.2* |
| -- | -- |
| Homepage | [https://github.com/redis/hiredis](https://github.com/redis/hiredis) |
| License | BSD-3-Clause |
| Versions | v1.0.2 |
| Architectures | arm64, x64, x86 |
| Definition | [hiredis/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/h/hiredis/xmake.lua) |

##### Install command

```console
xrepo install hiredis
```

##### Integration in the project (xmake.lua)

```lua
add_requires("hiredis")
```


### hopscotch-map (windows)


| Description | *A C++ implementation of a fast hash map and hash set using hopscotch hashing* |
| -- | -- |
| Homepage | [https://github.com/Tessil/hopscotch-map](https://github.com/Tessil/hopscotch-map) |
| Versions | v2.3.0 |
| Architectures | arm64, x64, x86 |
| Definition | [hopscotch-map/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/h/hopscotch-map/xmake.lua) |

##### Install command

```console
xrepo install hopscotch-map
```

##### Integration in the project (xmake.lua)

```lua
add_requires("hopscotch-map")
```


### hpsocket (windows)


| Description | *High Performance Network Framework* |
| -- | -- |
| Homepage | [https://github.com/ldcsaa/HP-Socket](https://github.com/ldcsaa/HP-Socket) |
| License | Apache-2.0 |
| Versions | v5.7.3, v5.8.4 |
| Architectures | arm64, x64, x86 |
| Definition | [hpsocket/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/h/hpsocket/xmake.lua) |

##### Install command

```console
xrepo install hpsocket
```

##### Integration in the project (xmake.lua)

```lua
add_requires("hpsocket")
```


### http_parser (windows)


| Description | *Parser for HTTP messages written in C.* |
| -- | -- |
| Homepage | [https://github.com/nodejs/http-parser](https://github.com/nodejs/http-parser) |
| Versions | v2.9.4 |
| Architectures | arm64, x64, x86 |
| Definition | [http_parser/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/h/http_parser/xmake.lua) |

##### Install command

```console
xrepo install http_parser
```

##### Integration in the project (xmake.lua)

```lua
add_requires("http_parser")
```


### hwloc (windows)


| Description | *Portable Hardware Locality (hwloc)* |
| -- | -- |
| Homepage | [https://www.open-mpi.org/software/hwloc/](https://www.open-mpi.org/software/hwloc/) |
| License | BSD-3-Clause |
| Versions | 2.5.0, 2.7.1 |
| Architectures | arm64, x64, x86 |
| Definition | [hwloc/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/h/hwloc/xmake.lua) |

##### Install command

```console
xrepo install hwloc
```

##### Integration in the project (xmake.lua)

```lua
add_requires("hwloc")
```


### hypre (windows)


| Description | *Parallel solvers for sparse linear systems featuring multigrid methods.* |
| -- | -- |
| Homepage | [https://computing.llnl.gov/projects/hypre-scalable-linear-solvers-multigrid-methods](https://computing.llnl.gov/projects/hypre-scalable-linear-solvers-multigrid-methods) |
| License | Apache-2.0 |
| Versions | v2.20.0, v2.23.0 |
| Architectures | arm64, x64, x86 |
| Definition | [hypre/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/h/hypre/xmake.lua) |

##### Install command

```console
xrepo install hypre
```

##### Integration in the project (xmake.lua)

```lua
add_requires("hypre")
```



## i
### icbc (windows)


| Description | *A High Quality SIMD BC1 Encoder* |
| -- | -- |
| Homepage | [https://github.com/castano/icbc](https://github.com/castano/icbc) |
| Versions | 1.05 |
| Architectures | arm64, x64, x86 |
| Definition | [icbc/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/i/icbc/xmake.lua) |

##### Install command

```console
xrepo install icbc
```

##### Integration in the project (xmake.lua)

```lua
add_requires("icbc")
```


### icu4c (windows)


| Description | *C/C++ libraries for Unicode and globalization.* |
| -- | -- |
| Homepage | [http://site.icu-project.org/](http://site.icu-project.org/) |
| Versions | 64.2, 68.1, 68.2, 69.1, 70.1, 71.1, 72.1 |
| Architectures | arm64, x64, x86 |
| Definition | [icu4c/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/i/icu4c/xmake.lua) |

##### Install command

```console
xrepo install icu4c
```

##### Integration in the project (xmake.lua)

```lua
add_requires("icu4c")
```


### ifort (windows)


| Description | *The Fortran Compiler provided by Intel®* |
| -- | -- |
| Homepage | [https://www.intel.com/content/www/us/en/developer/tools/oneapi/fortran-compiler.html](https://www.intel.com/content/www/us/en/developer/tools/oneapi/fortran-compiler.html) |
| Versions | 2021.4.0+3224 |
| Architectures | arm64, x64, x86 |
| Definition | [ifort/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/i/ifort/xmake.lua) |

##### Install command

```console
xrepo install ifort
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ifort")
```


### imath (windows)


| Description | *Imath is a C++ and python library of 2D and 3D vector, matrix, and math operations for computer graphics* |
| -- | -- |
| Homepage | [https://github.com/AcademySoftwareFoundation/Imath/](https://github.com/AcademySoftwareFoundation/Imath/) |
| License | BSD-3-Clause |
| Versions | v3.1.0, v3.1.1, v3.1.2, v3.1.3, v3.1.4, v3.1.5 |
| Architectures | arm64, x64, x86 |
| Definition | [imath/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/i/imath/xmake.lua) |

##### Install command

```console
xrepo install imath
```

##### Integration in the project (xmake.lua)

```lua
add_requires("imath")
```


### imgui (windows)


| Description | *Bloat-free Immediate Mode Graphical User interface for C++ with minimal dependencies* |
| -- | -- |
| Homepage | [https://github.com/ocornut/imgui](https://github.com/ocornut/imgui) |
| License | MIT |
| Versions | v1.75, v1.79, v1.80, v1.81, v1.82, v1.83, v1.83-docking, v1.84.1, v1.84.2, v1.85, v1.85-docking, v1.86, v1.87, v1.87-docking, v1.88, v1.88-docking, v1.89, v1.89-docking |
| Architectures | arm64, x64, x86 |
| Definition | [imgui/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/i/imgui/xmake.lua) |

##### Install command

```console
xrepo install imgui
```

##### Integration in the project (xmake.lua)

```lua
add_requires("imgui")
```


### imgui-sfml (windows)


| Description | *Dear ImGui binding for use with SFML* |
| -- | -- |
| Homepage | [https://github.com/eliasdaler/imgui-sfml](https://github.com/eliasdaler/imgui-sfml) |
| Versions | v2.5 |
| Architectures | arm64, x64, x86 |
| Definition | [imgui-sfml/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/i/imgui-sfml/xmake.lua) |

##### Install command

```console
xrepo install imgui-sfml
```

##### Integration in the project (xmake.lua)

```lua
add_requires("imgui-sfml")
```


### imguizmo (windows)


| Description | *Immediate mode 3D gizmo for scene editing and other controls based on Dear Imgui* |
| -- | -- |
| Homepage | [https://github.com/CedricGuillemet/ImGuizmo](https://github.com/CedricGuillemet/ImGuizmo) |
| Versions | 1.83, 1.89+WIP |
| Architectures | arm64, x64, x86 |
| Definition | [imguizmo/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/i/imguizmo/xmake.lua) |

##### Install command

```console
xrepo install imguizmo
```

##### Integration in the project (xmake.lua)

```lua
add_requires("imguizmo")
```


### indicators (windows)


| Description | *Activity Indicators for Modern C++* |
| -- | -- |
| Homepage | [https://github.com/p-ranav/indicators](https://github.com/p-ranav/indicators) |
| License | MIT |
| Versions | 2.2 |
| Architectures | arm64, x64, x86 |
| Definition | [indicators/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/i/indicators/xmake.lua) |

##### Install command

```console
xrepo install indicators
```

##### Integration in the project (xmake.lua)

```lua
add_requires("indicators")
```


### inja (windows)


| Description | *A Template Engine for Modern C++* |
| -- | -- |
| Homepage | [https://pantor.github.io/inja/](https://pantor.github.io/inja/) |
| Versions | v2.1.0 |
| Architectures | arm64, x64, x86 |
| Definition | [inja/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/i/inja/xmake.lua) |

##### Install command

```console
xrepo install inja
```

##### Integration in the project (xmake.lua)

```lua
add_requires("inja")
```


### ip2region (windows)


| Description | *IP address region search library.* |
| -- | -- |
| Homepage | [https://github.com/lionsoul2014/ip2region](https://github.com/lionsoul2014/ip2region) |
| License | Apache-2.0 |
| Versions | v2020.10.31 |
| Architectures | arm64, x64, x86 |
| Definition | [ip2region/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/i/ip2region/xmake.lua) |

##### Install command

```console
xrepo install ip2region
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ip2region")
```


### irrlicht (windows)


| Description | *The Irrlicht Engine is an open source realtime 3D engine written in C++.* |
| -- | -- |
| Homepage | [https://irrlicht.sourceforge.io/](https://irrlicht.sourceforge.io/) |
| License | zlib |
| Versions | 1.8.5 |
| Architectures | arm64, x64, x86 |
| Definition | [irrlicht/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/i/irrlicht/xmake.lua) |

##### Install command

```console
xrepo install irrlicht
```

##### Integration in the project (xmake.lua)

```lua
add_requires("irrlicht")
```


### irrxml (windows)


| Description | *High speed and easy-to-use XML Parser for C++* |
| -- | -- |
| Homepage | [https://sourceforge.net/projects/irrlicht/](https://sourceforge.net/projects/irrlicht/) |
| Versions | 1.2 |
| Architectures | arm64, x64, x86 |
| Definition | [irrxml/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/i/irrxml/xmake.lua) |

##### Install command

```console
xrepo install irrxml
```

##### Integration in the project (xmake.lua)

```lua
add_requires("irrxml")
```


### isocline (windows)


| Description | *Isocline is a portable GNU readline alternative * |
| -- | -- |
| Homepage | [https://github.com/daanx/isocline](https://github.com/daanx/isocline) |
| License | MIT |
| Versions | 2022.01.16 |
| Architectures | arm64, x64, x86 |
| Definition | [isocline/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/i/isocline/xmake.lua) |

##### Install command

```console
xrepo install isocline
```

##### Integration in the project (xmake.lua)

```lua
add_requires("isocline")
```


### ispc (windows)


| Description | *Intel® Implicit SPMD Program Compiler* |
| -- | -- |
| Homepage | [https://ispc.github.io/](https://ispc.github.io/) |
| License | BSD-3-Clause |
| Versions | 1.17.0 |
| Architectures | arm64, x64, x86 |
| Definition | [ispc/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/i/ispc/xmake.lua) |

##### Install command

```console
xrepo install ispc
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ispc")
```


### itk (windows)


| Description | *ITK is an open-source, cross-platform library that provides developers with an extensive suite of software tools for image analysis.* |
| -- | -- |
| Homepage | [https://itk.org/](https://itk.org/) |
| License | Apache-2.0 |
| Versions | 5.2.0, 5.2.1 |
| Architectures | arm64, x64, x86 |
| Definition | [itk/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/i/itk/xmake.lua) |

##### Install command

```console
xrepo install itk
```

##### Integration in the project (xmake.lua)

```lua
add_requires("itk")
```


### iverilog (windows)


| Description | *Icarus Verilog* |
| -- | -- |
| Homepage | [https://steveicarus.github.io/iverilog/](https://steveicarus.github.io/iverilog/) |
| Versions | 2023.1.7 |
| Architectures | arm64, x64, x86 |
| Definition | [iverilog/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/i/iverilog/xmake.lua) |

##### Install command

```console
xrepo install iverilog
```

##### Integration in the project (xmake.lua)

```lua
add_requires("iverilog")
```



## j
### jansson (windows)


| Description | *C library for encoding, decoding and manipulating JSON data* |
| -- | -- |
| Homepage | [https://github.com/akheron/jansson](https://github.com/akheron/jansson) |
| License | MIT |
| Versions | 2.14 |
| Architectures | arm64, x64, x86 |
| Definition | [jansson/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/j/jansson/xmake.lua) |

##### Install command

```console
xrepo install jansson
```

##### Integration in the project (xmake.lua)

```lua
add_requires("jansson")
```


### jasper (windows)


| Description | *Official Repository for the JasPer Image Coding Toolkit* |
| -- | -- |
| Homepage | [https://www.ece.uvic.ca/~frodo/jasper/](https://www.ece.uvic.ca/~frodo/jasper/) |
| License | BSD-2-Clause |
| Versions | 2.0.28, 2.0.32, 2.0.33 |
| Architectures | arm64, x64, x86 |
| Definition | [jasper/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/j/jasper/xmake.lua) |

##### Install command

```console
xrepo install jasper
```

##### Integration in the project (xmake.lua)

```lua
add_requires("jasper")
```


### johnnyengine (windows)


| Description | *A 2D/3D Engine using OpenGL and SDL for input and the window* |
| -- | -- |
| Homepage | [https://github.com/PucklaJ/JohnnyEngine](https://github.com/PucklaJ/JohnnyEngine) |
| Versions | 1.0.1 |
| Architectures | arm64, x64, x86 |
| Definition | [johnnyengine/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/j/johnnyengine/xmake.lua) |

##### Install command

```console
xrepo install johnnyengine
```

##### Integration in the project (xmake.lua)

```lua
add_requires("johnnyengine")
```


### jsmn (windows)


| Description | *Jsmn is a world fastest JSON parser/tokenizer* |
| -- | -- |
| Homepage | [https://github.com/zserge/jsmn](https://github.com/zserge/jsmn) |
| Versions | v1.1.0 |
| Architectures | arm64, x64, x86 |
| Definition | [jsmn/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/j/jsmn/xmake.lua) |

##### Install command

```console
xrepo install jsmn
```

##### Integration in the project (xmake.lua)

```lua
add_requires("jsmn")
```


### json-schema-validator (windows)


| Description | *JSON schema validator for JSON for Modern C++* |
| -- | -- |
| Homepage | [https://github.com/pboettch/json-schema-validator](https://github.com/pboettch/json-schema-validator) |
| Versions | 2.1.0 |
| Architectures | arm64, x64, x86 |
| Definition | [json-schema-validator/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/j/json-schema-validator/xmake.lua) |

##### Install command

```console
xrepo install json-schema-validator
```

##### Integration in the project (xmake.lua)

```lua
add_requires("json-schema-validator")
```


### json.h (windows)


| Description | *single header json parser for C and C++* |
| -- | -- |
| Homepage | [https://github.com/sheredom/json.h](https://github.com/sheredom/json.h) |
| Versions | 2022.11.27 |
| Architectures | arm64, x64, x86 |
| Definition | [json.h/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/j/json.h/xmake.lua) |

##### Install command

```console
xrepo install json.h
```

##### Integration in the project (xmake.lua)

```lua
add_requires("json.h")
```


### jsoncons (windows)


| Description | *A C++, header-only library for constructing JSON and JSON-like data formats, with JSON Pointer, JSON Patch, JSONPath, JMESPath, CSV, MessagePack, CBOR, BSON, UBJSON* |
| -- | -- |
| Homepage | [https://danielaparker.github.io/jsoncons/](https://danielaparker.github.io/jsoncons/) |
| Versions | v0.158.0 |
| Architectures | arm64, x64, x86 |
| Definition | [jsoncons/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/j/jsoncons/xmake.lua) |

##### Install command

```console
xrepo install jsoncons
```

##### Integration in the project (xmake.lua)

```lua
add_requires("jsoncons")
```


### jsoncpp (windows)


| Description | *A C++ library for interacting with JSON.* |
| -- | -- |
| Homepage | [https://github.com/open-source-parsers/jsoncpp/wiki](https://github.com/open-source-parsers/jsoncpp/wiki) |
| Versions | 1.9.4, 1.9.5 |
| Architectures | arm64, x64, x86 |
| Definition | [jsoncpp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/j/jsoncpp/xmake.lua) |

##### Install command

```console
xrepo install jsoncpp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("jsoncpp")
```



## k
### kcp (windows)


| Description | *A Fast and Reliable ARQ Protocol.* |
| -- | -- |
| Homepage | [https://github.com/skywind3000/kcp](https://github.com/skywind3000/kcp) |
| Versions | 1.7 |
| Architectures | arm64, x64, x86 |
| Definition | [kcp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/k/kcp/xmake.lua) |

##### Install command

```console
xrepo install kcp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("kcp")
```


### kiwisolver (windows)


| Description | *Efficient C++ implementation of the Cassowary constraint solving algorithm* |
| -- | -- |
| Homepage | [https://kiwisolver.readthedocs.io/en/latest/](https://kiwisolver.readthedocs.io/en/latest/) |
| Versions | 1.3.1, 1.3.2, 1.4.4 |
| Architectures | arm64, x64, x86 |
| Definition | [kiwisolver/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/k/kiwisolver/xmake.lua) |

##### Install command

```console
xrepo install kiwisolver
```

##### Integration in the project (xmake.lua)

```lua
add_requires("kiwisolver")
```


### kompute (windows)


| Description | *General purpose GPU compute framework for cross vendor graphics cards* |
| -- | -- |
| Homepage | [https://github.com/KomputeProject/kompute](https://github.com/KomputeProject/kompute) |
| License | Apache-2.0 |
| Versions | v0.8.0 |
| Architectures | arm64, x64, x86 |
| Definition | [kompute/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/k/kompute/xmake.lua) |

##### Install command

```console
xrepo install kompute
```

##### Integration in the project (xmake.lua)

```lua
add_requires("kompute")
```


### kuba-zip (windows)


| Description | *A portable, simple zip library written in C* |
| -- | -- |
| Homepage | [https://github.com/kuba--/zip](https://github.com/kuba--/zip) |
| Versions | v0.2.2, v0.2.5 |
| Architectures | arm64, x64, x86 |
| Definition | [kuba-zip/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/k/kuba-zip/xmake.lua) |

##### Install command

```console
xrepo install kuba-zip
```

##### Integration in the project (xmake.lua)

```lua
add_requires("kuba-zip")
```



## l
### lbuild (windows)


| Description | *lbuild: a generic, modular code generator in Python 3* |
| -- | -- |
| Homepage | [https://pypi.org/project/lbuild](https://pypi.org/project/lbuild) |
| Versions | 2022.02.14 |
| Architectures | arm64, x64, x86 |
| Definition | [lbuild/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/lbuild/xmake.lua) |

##### Install command

```console
xrepo install lbuild
```

##### Integration in the project (xmake.lua)

```lua
add_requires("lbuild")
```


### lcms (windows)


| Description | *A free, open source, CMM engine.* |
| -- | -- |
| Homepage | [https://www.littlecms.com/](https://www.littlecms.com/) |
| License | MIT |
| Versions | 2.11, 2.12 |
| Architectures | arm64, x64, x86 |
| Definition | [lcms/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/lcms/xmake.lua) |

##### Install command

```console
xrepo install lcms
```

##### Integration in the project (xmake.lua)

```lua
add_requires("lcms")
```


### lemon (windows)


| Description | *Library for Efficient Modeling and Optimization in Networks.* |
| -- | -- |
| Homepage | [https://lemon.cs.elte.hu/trac/lemon](https://lemon.cs.elte.hu/trac/lemon) |
| License | BSL-1.0 |
| Versions | 1.3.1 |
| Architectures | arm64, x64, x86 |
| Definition | [lemon/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/lemon/xmake.lua) |

##### Install command

```console
xrepo install lemon
```

##### Integration in the project (xmake.lua)

```lua
add_requires("lemon")
```


### leptonica (windows)


| Description | *Leptonica is a pedagogically-oriented open source site containing software that is broadly useful for image processing and image analysis applications.* |
| -- | -- |
| Homepage | [http://www.leptonica.org/](http://www.leptonica.org/) |
| License | BSD-2-Clause |
| Versions | 1.80.0, 1.81.1, 1.82.0 |
| Architectures | arm64, x64, x86 |
| Definition | [leptonica/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/leptonica/xmake.lua) |

##### Install command

```console
xrepo install leptonica
```

##### Integration in the project (xmake.lua)

```lua
add_requires("leptonica")
```


### leveldb (windows)


| Description | *LevelDB is a fast key-value storage library written at Google that provides an ordered mapping from string keys to string values.* |
| -- | -- |
| Homepage | [https://github.com/google/leveldb](https://github.com/google/leveldb) |
| Versions | 1.22, 1.23 |
| Architectures | arm64, x64, x86 |
| Definition | [leveldb/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/leveldb/xmake.lua) |

##### Install command

```console
xrepo install leveldb
```

##### Integration in the project (xmake.lua)

```lua
add_requires("leveldb")
```


### lexy (windows)


| Description | *C++ parsing DSL* |
| -- | -- |
| Homepage | [https://lexy.foonathan.net](https://lexy.foonathan.net) |
| Versions | 2022.03.21 |
| Architectures | arm64, x64, x86 |
| Definition | [lexy/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/lexy/xmake.lua) |

##### Install command

```console
xrepo install lexy
```

##### Integration in the project (xmake.lua)

```lua
add_requires("lexy")
```


### libaesgm (windows)


| Description | *https://repology.org/project/libaesgm/packages* |
| -- | -- |
| Homepage | [https://github.com/xmake-mirror/libaesgm](https://github.com/xmake-mirror/libaesgm) |
| Versions | 2013.1.1 |
| Architectures | arm64, x64, x86 |
| Definition | [libaesgm/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libaesgm/xmake.lua) |

##### Install command

```console
xrepo install libaesgm
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libaesgm")
```


### libarchive (windows)


| Description | *Multi-format archive and compression library* |
| -- | -- |
| Homepage | [https://libarchive.org/](https://libarchive.org/) |
| License | BSD-2-Clause |
| Versions | 3.5.1, 3.5.2 |
| Architectures | arm64, x64, x86 |
| Definition | [libarchive/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libarchive/xmake.lua) |

##### Install command

```console
xrepo install libarchive
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libarchive")
```


### libargon2 (windows)


| Description | *The password hash Argon2, winner of PHC* |
| -- | -- |
| Homepage | [https://github.com/P-H-C/phc-winner-argon2](https://github.com/P-H-C/phc-winner-argon2) |
| Versions | 20190702 |
| Architectures | arm64, x64, x86 |
| Definition | [libargon2/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libargon2/xmake.lua) |

##### Install command

```console
xrepo install libargon2
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libargon2")
```


### libavif (windows)


| Description | *libavif - Library for encoding and decoding .avif files* |
| -- | -- |
| Homepage | [https://github.com/AOMediaCodec/libavif](https://github.com/AOMediaCodec/libavif) |
| License | BSD-2-Clause |
| Versions | v0.9.1 |
| Architectures | arm64, x64, x86 |
| Definition | [libavif/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libavif/xmake.lua) |

##### Install command

```console
xrepo install libavif
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libavif")
```


### libbpg (windows)


| Description | *Image format meant to improve on JPEG quality and file size* |
| -- | -- |
| Homepage | [https://bellard.org/bpg/](https://bellard.org/bpg/) |
| Versions | 0.9.8 |
| Architectures | arm64, x64, x86 |
| Definition | [libbpg/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libbpg/xmake.lua) |

##### Install command

```console
xrepo install libbpg
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libbpg")
```


### libccd (windows)


| Description | *libccd is library for a collision detection between two convex shapes.* |
| -- | -- |
| Homepage | [https://github.com/danfis/libccd/](https://github.com/danfis/libccd/) |
| License | BSD-3-Clause |
| Versions | v2.1 |
| Architectures | arm64, x64, x86 |
| Definition | [libccd/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libccd/xmake.lua) |

##### Install command

```console
xrepo install libccd
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libccd")
```


### libcpuid (windows)


| Description | *a small C library for x86 CPU detection and feature extraction* |
| -- | -- |
| Homepage | [https://github.com/anrieff/libcpuid](https://github.com/anrieff/libcpuid) |
| Versions | v0.5.1 |
| Architectures | arm64, x64, x86 |
| Definition | [libcpuid/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libcpuid/xmake.lua) |

##### Install command

```console
xrepo install libcpuid
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libcpuid")
```


### libcroco (windows)


| Description | *Libcroco is a standalone css2 parsing and manipulation library.* |
| -- | -- |
| Homepage | [https://gitlab.com/inkscape/libcroco](https://gitlab.com/inkscape/libcroco) |
| License | LGPL-2.0 |
| Versions | 0.6.13 |
| Architectures | arm64, x64, x86 |
| Definition | [libcroco/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libcroco/xmake.lua) |

##### Install command

```console
xrepo install libcroco
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libcroco")
```


### libcurl (windows)


| Description | *The multiprotocol file transfer library.* |
| -- | -- |
| Homepage | [https://curl.haxx.se/](https://curl.haxx.se/) |
| License | MIT |
| Versions | 7.31.0, 7.32.0, 7.33.0, 7.34.0, 7.35.0, 7.36.0, 7.37.1, 7.38.0, 7.39.0, 7.40.0, 7.41.0, 7.42.1, 7.43.0, 7.44.0, 7.45.0, 7.46.0, 7.47.1, 7.48.0, 7.49.1, 7.50.3, 7.51.0, 7.52.1, 7.53.1, 7.54.1, 7.55.1, 7.56.1, 7.57.0, 7.58.0, 7.59.0, 7.60.0, 7.61.0, 7.61.1, 7.62.0, 7.63.0, 7.64.0, 7.64.1, 7.65.3, 7.66.0, 7.67.0, 7.68.0, 7.69.1, 7.70.0, 7.71.1, 7.72.0, 7.73.0, 7.74.0, 7.75.0, 7.76.1, 7.77.0, 7.78.0, 7.80.0, 7.81.0, 7.82.0, 7.84.0, 7.85.0, 7.86.0, 7.87.0 |
| Architectures | arm64, x64, x86 |
| Definition | [libcurl/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libcurl/xmake.lua) |

##### Install command

```console
xrepo install libcurl
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libcurl")
```


### libde265 (windows)


| Description | *Open h.265 video codec implementation.* |
| -- | -- |
| Homepage | [https://www.libde265.org/](https://www.libde265.org/) |
| License | LGPL-3.0 |
| Versions | 1.0.8 |
| Architectures | arm64, x64, x86 |
| Definition | [libde265/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libde265/xmake.lua) |

##### Install command

```console
xrepo install libde265
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libde265")
```


### libdeflate (windows)


| Description | *libdeflate is a library for fast, whole-buffer DEFLATE-based compression and decompression.* |
| -- | -- |
| Homepage | [https://github.com/ebiggers/libdeflate](https://github.com/ebiggers/libdeflate) |
| License | MIT |
| Versions | v1.10, v1.13, v1.15, v1.8 |
| Architectures | arm64, x64, x86 |
| Definition | [libdeflate/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libdeflate/xmake.lua) |

##### Install command

```console
xrepo install libdeflate
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libdeflate")
```


### libdivide (windows)


| Description | *Official git repository for libdivide: optimized integer division* |
| -- | -- |
| Homepage | [http://libdivide.com](http://libdivide.com) |
| Versions | 5.0 |
| Architectures | arm64, x64, x86 |
| Definition | [libdivide/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libdivide/xmake.lua) |

##### Install command

```console
xrepo install libdivide
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libdivide")
```


### libdivsufsort (windows)


| Description | *A lightweight suffix array sorting library* |
| -- | -- |
| Homepage | [https://android.googlesource.com/platform/external/libdivsufsort/](https://android.googlesource.com/platform/external/libdivsufsort/) |
| Versions | 2021.2.18 |
| Architectures | arm64, x64, x86 |
| Definition | [libdivsufsort/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libdivsufsort/xmake.lua) |

##### Install command

```console
xrepo install libdivsufsort
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libdivsufsort")
```


### libepoxy (windows)


| Description | *Epoxy is a library for handling OpenGL function pointer management for you.* |
| -- | -- |
| Homepage | [https://download.gnome.org/sources/libepoxy/](https://download.gnome.org/sources/libepoxy/) |
| License | MIT |
| Versions | 1.5.9 |
| Architectures | arm64, x64, x86 |
| Definition | [libepoxy/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libepoxy/xmake.lua) |

##### Install command

```console
xrepo install libepoxy
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libepoxy")
```


### libevent (windows)


| Description | *libevent – an event notification library* |
| -- | -- |
| Homepage | [https://libevent.org/](https://libevent.org/) |
| License | BSD-3-Clause |
| Versions | 2.1.12 |
| Architectures | arm64, x64, x86 |
| Definition | [libevent/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libevent/xmake.lua) |

##### Install command

```console
xrepo install libevent
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libevent")
```


### libffi (windows)


| Description | *Portable Foreign Function Interface library.* |
| -- | -- |
| Homepage | [https://sourceware.org/libffi/](https://sourceware.org/libffi/) |
| Versions | 3.2.1, 3.3, 3.4.2 |
| Architectures | arm64, x64, x86 |
| Definition | [libffi/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libffi/xmake.lua) |

##### Install command

```console
xrepo install libffi
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libffi")
```


### libfive (windows)


| Description | *libfive is a software library and set of tools for solid modeling, especially suited for parametric and procedural design.* |
| -- | -- |
| Homepage | [https://libfive.com/](https://libfive.com/) |
| Versions | 2021.04.08, 2022.02.22 |
| Architectures | arm64, x64, x86 |
| Definition | [libfive/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libfive/xmake.lua) |

##### Install command

```console
xrepo install libfive
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libfive")
```


### libflac (windows)


| Description | *Free Lossless Audio Codec* |
| -- | -- |
| Homepage | [https://xiph.org/flac](https://xiph.org/flac) |
| License | BSD |
| Versions | 1.3.3 |
| Architectures | arm64, x64, x86 |
| Definition | [libflac/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libflac/xmake.lua) |

##### Install command

```console
xrepo install libflac
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libflac")
```


### libfreenect2 (windows)


| Description | *Open source drivers for the Kinect for Windows v2 device* |
| -- | -- |
| Homepage | [https://github.com/OpenKinect/libfreenect2](https://github.com/OpenKinect/libfreenect2) |
| License | GPL-2.0 |
| Versions | v0.2.0, v0.2.1 |
| Architectures | arm64, x64, x86 |
| Definition | [libfreenect2/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libfreenect2/xmake.lua) |

##### Install command

```console
xrepo install libfreenect2
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libfreenect2")
```


### libgd (windows)


| Description | *GD is an open source code library for the dynamic creation of images by programmers.* |
| -- | -- |
| Homepage | [http://libgd.org/](http://libgd.org/) |
| Versions | 2.3.2 |
| Architectures | arm64, x64, x86 |
| Definition | [libgd/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libgd/xmake.lua) |

##### Install command

```console
xrepo install libgd
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libgd")
```


### libgeotiff (windows)


| Description | *Libgeotiff is an open source library for reading and writing GeoTIFF information tags* |
| -- | -- |
| Homepage | [https://github.com/OSGeo/libgeotiff](https://github.com/OSGeo/libgeotiff) |
| License | MIT |
| Versions | 1.7.1 |
| Architectures | arm64, x64, x86 |
| Definition | [libgeotiff/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libgeotiff/xmake.lua) |

##### Install command

```console
xrepo install libgeotiff
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libgeotiff")
```


### libgit2 (windows)


| Description | *A cross-platform, linkable library implementation of Git that you can use in your application.* |
| -- | -- |
| Homepage | [https://libgit2.org/](https://libgit2.org/) |
| License | GPL-2.0-only |
| Versions | v1.3.0 |
| Architectures | arm64, x64, x86 |
| Definition | [libgit2/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libgit2/xmake.lua) |

##### Install command

```console
xrepo install libgit2
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libgit2")
```


### libharu (windows)


| Description | *libHaru is a free, cross platform, open source library for generating PDF files.* |
| -- | -- |
| Homepage | [http://libharu.org/](http://libharu.org/) |
| License | zlib |
| Versions | 2.3.0 |
| Architectures | arm64, x64, x86 |
| Definition | [libharu/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libharu/xmake.lua) |

##### Install command

```console
xrepo install libharu
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libharu")
```


### libheif (windows)


| Description | *libheif is an HEIF and AVIF file format decoder and encoder.* |
| -- | -- |
| Homepage | [https://github.com/strukturag/libheif](https://github.com/strukturag/libheif) |
| License | LGPL-3.0 |
| Versions | 1.12.0 |
| Architectures | arm64, x64, x86 |
| Definition | [libheif/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libheif/xmake.lua) |

##### Install command

```console
xrepo install libheif
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libheif")
```


### libhv (windows)


| Description | *Like libevent, libev, and libuv, libhv provides event-loop with non-blocking IO and timer, but simpler api and richer protocols.* |
| -- | -- |
| Homepage | [https://github.com/ithewei/libhv](https://github.com/ithewei/libhv) |
| Versions | 1.0.0, 1.1.0, 1.1.1, 1.2.1, 1.2.2, 1.2.3, 1.2.4, 1.2.6 |
| Architectures | arm64, x64, x86 |
| Definition | [libhv/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libhv/xmake.lua) |

##### Install command

```console
xrepo install libhv
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libhv")
```


### libiconv (windows)


| Description | *Character set conversion library.* |
| -- | -- |
| Homepage | [https://www.gnu.org/software/libiconv](https://www.gnu.org/software/libiconv) |
| Versions | 1.15, 1.16, 1.17 |
| Architectures | arm64, x64, x86 |
| Definition | [libiconv/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libiconv/xmake.lua) |

##### Install command

```console
xrepo install libiconv
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libiconv")
```


### libigl (windows)


| Description | *Simple C++ geometry processing library.* |
| -- | -- |
| Homepage | [https://libigl.github.io/](https://libigl.github.io/) |
| License | MPL-2.0 |
| Versions | v2.2.0, v2.3.0, v2.4.0 |
| Architectures | arm64, x64, x86 |
| Definition | [libigl/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libigl/xmake.lua) |

##### Install command

```console
xrepo install libigl
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libigl")
```


### libimagequant (windows)


| Description | *Small, portable C library for high-quality conversion of RGBA images to 8-bit indexed-color (palette) images.* |
| -- | -- |
| Homepage | [https://pngquant.org/lib/](https://pngquant.org/lib/) |
| License | GPL-3.0 |
| Versions | 2.15.1 |
| Architectures | arm64, x64, x86 |
| Definition | [libimagequant/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libimagequant/xmake.lua) |

##### Install command

```console
xrepo install libimagequant
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libimagequant")
```


### libintl (windows)


| Description | *GNU gettext runtime* |
| -- | -- |
| Homepage | [https://www.gnu.org/software/gettext/](https://www.gnu.org/software/gettext/) |
| Versions | 0.21 |
| Architectures | arm64, x64, x86 |
| Definition | [libintl/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libintl/xmake.lua) |

##### Install command

```console
xrepo install libintl
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libintl")
```


### libjpeg (windows)


| Description | *A widely used C library for reading and writing JPEG image files.* |
| -- | -- |
| Homepage | [http://ijg.org/](http://ijg.org/) |
| Versions | v9b, v9c, v9d, v9e |
| Architectures | arm64, x64, x86 |
| Definition | [libjpeg/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libjpeg/xmake.lua) |

##### Install command

```console
xrepo install libjpeg
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libjpeg")
```


### libjpeg-turbo (windows)


| Description | *A JPEG image codec that uses SIMD instructions (MMX, SSE2, AVX2, Neon, AltiVec) to accelerate baseline JPEG compression and decompression on x86, x86-64, Arm, and PowerPC systems.* |
| -- | -- |
| Homepage | [https://libjpeg-turbo.org/](https://libjpeg-turbo.org/) |
| License | BSD-3-Clause |
| Versions | 2.0.5, 2.0.6, 2.0.90, 2.1.0, 2.1.1, 2.1.2, 2.1.3, 2.1.4 |
| Architectures | arm64, x64, x86 |
| Definition | [libjpeg-turbo/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libjpeg-turbo/xmake.lua) |

##### Install command

```console
xrepo install libjpeg-turbo
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libjpeg-turbo")
```


### liblas (windows)


| Description | *libLAS - LAS 1.0/1.1/1.2 ASPRS LiDAR data translation toolset* |
| -- | -- |
| Homepage | [https://liblas.org/index.html](https://liblas.org/index.html) |
| License | BSD-3-Clause |
| Versions | 1.8.1 |
| Architectures | arm64, x64, x86 |
| Definition | [liblas/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/liblas/xmake.lua) |

##### Install command

```console
xrepo install liblas
```

##### Integration in the project (xmake.lua)

```lua
add_requires("liblas")
```


### libmng (windows)


| Description | *libmng - The reference library for reading, displaying, writing and examining Multiple-Image Network Graphics.* |
| -- | -- |
| Homepage | [https://libmng.com/](https://libmng.com/) |
| Versions | 2.0.3 |
| Architectures | arm64, x64, x86 |
| Definition | [libmng/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libmng/xmake.lua) |

##### Install command

```console
xrepo install libmng
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libmng")
```


### libmspack (windows)


| Description | *libmspack is a portable library for some loosely related Microsoft compression formats.* |
| -- | -- |
| Homepage | [https://www.cabextract.org.uk/libmspack/](https://www.cabextract.org.uk/libmspack/) |
| License | LGPL-2.0 |
| Versions | v0.10.1alpha |
| Architectures | arm64, x64, x86 |
| Definition | [libmspack/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libmspack/xmake.lua) |

##### Install command

```console
xrepo install libmspack
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libmspack")
```


### libogg (windows)


| Description | *Ogg Bitstream Library* |
| -- | -- |
| Homepage | [https://www.xiph.org/ogg/](https://www.xiph.org/ogg/) |
| Versions | v1.3.4 |
| Architectures | arm64, x64, x86 |
| Definition | [libogg/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libogg/xmake.lua) |

##### Install command

```console
xrepo install libogg
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libogg")
```


### libopus (windows)


| Description | *Modern audio compression for the internet.* |
| -- | -- |
| Homepage | [https://opus-codec.org](https://opus-codec.org) |
| Versions | 1.3.1 |
| Architectures | arm64, x64, x86 |
| Definition | [libopus/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libopus/xmake.lua) |

##### Install command

```console
xrepo install libopus
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libopus")
```


### libpng (windows)


| Description | *The official PNG reference library* |
| -- | -- |
| Homepage | [http://www.libpng.org/pub/png/libpng.html](http://www.libpng.org/pub/png/libpng.html) |
| License | libpng-2.0 |
| Versions | v1.6.34, v1.6.35, v1.6.36, v1.6.37 |
| Architectures | arm64, x64, x86 |
| Definition | [libpng/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libpng/xmake.lua) |

##### Install command

```console
xrepo install libpng
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libpng")
```


### libpsl (windows)


| Description | *C library to handle the Public Suffix List* |
| -- | -- |
| Homepage | [https://github.com/rockdaboot/libpsl](https://github.com/rockdaboot/libpsl) |
| License | MIT |
| Versions | 0.21.1 |
| Architectures | arm64, x64, x86 |
| Definition | [libpsl/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libpsl/xmake.lua) |

##### Install command

```console
xrepo install libpsl
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libpsl")
```


### libraw (windows)


| Description | *LibRaw is a library for reading RAW files from digital cameras.* |
| -- | -- |
| Homepage | [http://www.libraw.org](http://www.libraw.org) |
| License | LGPL-2.1 |
| Versions | 0.19.5, 0.20.2 |
| Architectures | arm64, x64, x86 |
| Definition | [libraw/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libraw/xmake.lua) |

##### Install command

```console
xrepo install libraw
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libraw")
```


### librdkafka (windows)


| Description | *The Apache Kafka C/C++ library* |
| -- | -- |
| Homepage | [https://github.com/edenhill/librdkafka](https://github.com/edenhill/librdkafka) |
| Versions | v1.6.2, v1.8.2-POST2 |
| Architectures | arm64, x64, x86 |
| Definition | [librdkafka/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/librdkafka/xmake.lua) |

##### Install command

```console
xrepo install librdkafka
```

##### Integration in the project (xmake.lua)

```lua
add_requires("librdkafka")
```


### libressl (windows)


| Description | *LibreSSL is a version of the TLS/crypto stack forked from OpenSSL in 2014, with goals of modernizing the codebase, improving security, and applying best practice development processes.* |
| -- | -- |
| Homepage | [https://www.libressl.org/](https://www.libressl.org/) |
| Versions | 3.4.2 |
| Architectures | arm64, x64, x86 |
| Definition | [libressl/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libressl/xmake.lua) |

##### Install command

```console
xrepo install libressl
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libressl")
```


### libsais (windows)


| Description | *libsais is a library for linear time suffix array, longest common prefix array and burrows wheeler transform construction based on induced sorting algorithm.* |
| -- | -- |
| Homepage | [https://github.com/IlyaGrebnov/libsais](https://github.com/IlyaGrebnov/libsais) |
| License | Apache-2.0 |
| Versions | v2.7.1 |
| Architectures | arm64, x64, x86 |
| Definition | [libsais/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libsais/xmake.lua) |

##### Install command

```console
xrepo install libsais
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libsais")
```


### libsdl (windows)


| Description | *Simple DirectMedia Layer* |
| -- | -- |
| Homepage | [https://www.libsdl.org/](https://www.libsdl.org/) |
| License | zlib |
| Versions | 2.0.12, 2.0.14, 2.0.16, 2.0.18, 2.0.20, 2.0.22, 2.0.8, 2.24.0, 2.24.2, 2.26.0, 2.26.1, 2.26.2 |
| Architectures | arm64, x64, x86 |
| Definition | [libsdl/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libsdl/xmake.lua) |

##### Install command

```console
xrepo install libsdl
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libsdl")
```


### libsdl_gfx (windows)


| Description | *Simple DirectMedia Layer primitives drawing library* |
| -- | -- |
| Homepage | [https://www.ferzkopp.net/wordpress/2016/01/02/sdl_gfx-sdl2_gfx/](https://www.ferzkopp.net/wordpress/2016/01/02/sdl_gfx-sdl2_gfx/) |
| Versions | 1.0.4 |
| Architectures | arm64, x64, x86 |
| Definition | [libsdl_gfx/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libsdl_gfx/xmake.lua) |

##### Install command

```console
xrepo install libsdl_gfx
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libsdl_gfx")
```


### libsdl_image (windows)


| Description | *Simple DirectMedia Layer image loading library* |
| -- | -- |
| Homepage | [http://www.libsdl.org/projects/SDL_image/](http://www.libsdl.org/projects/SDL_image/) |
| License | zlib |
| Versions | 2.0.5, 2.6.0, 2.6.1, 2.6.2 |
| Architectures | arm64, x64, x86 |
| Definition | [libsdl_image/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libsdl_image/xmake.lua) |

##### Install command

```console
xrepo install libsdl_image
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libsdl_image")
```


### libsdl_mixer (windows)


| Description | *Simple DirectMedia Layer mixer audio library* |
| -- | -- |
| Homepage | [https://www.libsdl.org/projects/SDL_mixer/](https://www.libsdl.org/projects/SDL_mixer/) |
| Versions | 2.0.4, 2.6.0, 2.6.1, 2.6.2 |
| Architectures | arm64, x64, x86 |
| Definition | [libsdl_mixer/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libsdl_mixer/xmake.lua) |

##### Install command

```console
xrepo install libsdl_mixer
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libsdl_mixer")
```


### libsdl_net (windows)


| Description | *Simple DirectMedia Layer networking library* |
| -- | -- |
| Homepage | [https://www.libsdl.org/projects/SDL_net/](https://www.libsdl.org/projects/SDL_net/) |
| Versions | 2.0.1, 2.2.0 |
| Architectures | arm64, x64, x86 |
| Definition | [libsdl_net/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libsdl_net/xmake.lua) |

##### Install command

```console
xrepo install libsdl_net
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libsdl_net")
```


### libsdl_ttf (windows)


| Description | *Simple DirectMedia Layer text rendering library* |
| -- | -- |
| Homepage | [https://www.libsdl.org/projects/SDL_ttf/](https://www.libsdl.org/projects/SDL_ttf/) |
| License | zlib |
| Versions | 2.0.15, 2.0.18, 2.20.0, 2.20.1 |
| Architectures | arm64, x64, x86 |
| Definition | [libsdl_ttf/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libsdl_ttf/xmake.lua) |

##### Install command

```console
xrepo install libsdl_ttf
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libsdl_ttf")
```


### libsimdpp (windows)


| Description | *Portable header-only C++ low level SIMD library* |
| -- | -- |
| Homepage | [https://github.com/p12tic/libsimdpp](https://github.com/p12tic/libsimdpp) |
| Versions | v2.1 |
| Architectures | arm64, x64, x86 |
| Definition | [libsimdpp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libsimdpp/xmake.lua) |

##### Install command

```console
xrepo install libsimdpp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libsimdpp")
```


### libsndfile (windows)


| Description | *A C library for reading and writing sound files containing sampled audio data.* |
| -- | -- |
| Homepage | [https://libsndfile.github.io/libsndfile/](https://libsndfile.github.io/libsndfile/) |
| License | LGPL-2.1 |
| Versions | 1.0.31, v1.0.30 |
| Architectures | arm64, x64, x86 |
| Definition | [libsndfile/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libsndfile/xmake.lua) |

##### Install command

```console
xrepo install libsndfile
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libsndfile")
```


### libsodium (windows)


| Description | *Sodium is a new, easy-to-use software library for encryption, decryption, signatures, password hashing and more.* |
| -- | -- |
| Homepage | [https://libsodium.org](https://libsodium.org) |
| Versions | 1.0.18 |
| Architectures | arm64, x64, x86 |
| Definition | [libsodium/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libsodium/xmake.lua) |

##### Install command

```console
xrepo install libsodium
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libsodium")
```


### libsoundio (windows)


| Description | *C library for cross-platform real-time audio input and output.* |
| -- | -- |
| Homepage | [http://libsound.io/](http://libsound.io/) |
| License | MIT |
| Versions | 2.0.0 |
| Architectures | arm64, x64, x86 |
| Definition | [libsoundio/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libsoundio/xmake.lua) |

##### Install command

```console
xrepo install libsoundio
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libsoundio")
```


### libspng (windows)


| Description | *Simple, modern libpng alternative* |
| -- | -- |
| Homepage | [https://libspng.org](https://libspng.org) |
| Versions | v0.7.1 |
| Architectures | arm64, x64, x86 |
| Definition | [libspng/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libspng/xmake.lua) |

##### Install command

```console
xrepo install libspng
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libspng")
```


### libsquish (windows)


| Description | *The libSquish library compresses images with the DXT standard (also known as S3TC).* |
| -- | -- |
| Homepage | [https://sourceforge.net/projects/libsquish/](https://sourceforge.net/projects/libsquish/) |
| License | MIT |
| Versions | 1.15 |
| Architectures | arm64, x64, x86 |
| Definition | [libsquish/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libsquish/xmake.lua) |

##### Install command

```console
xrepo install libsquish
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libsquish")
```


### libssh2 (windows)


| Description | *C library implementing the SSH2 protocol* |
| -- | -- |
| Homepage | [https://www.libssh2.org/](https://www.libssh2.org/) |
| License | BSD-3-Clause |
| Versions | 1.10.0 |
| Architectures | arm64, x64, x86 |
| Definition | [libssh2/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libssh2/xmake.lua) |

##### Install command

```console
xrepo install libssh2
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libssh2")
```


### libsv (windows)


| Description | *libsv - Public domain cross-platform semantic versioning in c99* |
| -- | -- |
| Homepage | [https://github.com/uael/sv](https://github.com/uael/sv) |
| Versions | 2021.11.27 |
| Architectures | arm64, x64, x86 |
| Definition | [libsv/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libsv/xmake.lua) |

##### Install command

```console
xrepo install libsv
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libsv")
```


### libsvm (windows)


| Description | *A simple, easy-to-use, and efficient software for SVM classification and regression* |
| -- | -- |
| Homepage | [https://github.com/cjlin1/libsvm](https://github.com/cjlin1/libsvm) |
| Versions | v325 |
| Architectures | arm64, x64, x86 |
| Definition | [libsvm/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libsvm/xmake.lua) |

##### Install command

```console
xrepo install libsvm
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libsvm")
```


### libtiff (windows)


| Description | *TIFF Library and Utilities.* |
| -- | -- |
| Homepage | [http://www.simplesystems.org/libtiff/](http://www.simplesystems.org/libtiff/) |
| Versions | v4.1.0, v4.2.0, v4.3.0, v4.4.0 |
| Architectures | arm64, x64, x86 |
| Definition | [libtiff/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libtiff/xmake.lua) |

##### Install command

```console
xrepo install libtiff
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libtiff")
```


### libtins (windows)


| Description | *High-level, multiplatform C++ network packet sniffing and crafting library.* |
| -- | -- |
| Homepage | [http://libtins.github.io/](http://libtins.github.io/) |
| License | BSD-2-Clause |
| Versions | v4.4 |
| Architectures | arm64, x64, x86 |
| Definition | [libtins/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libtins/xmake.lua) |

##### Install command

```console
xrepo install libtins
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libtins")
```


### libtool (windows)


| Description | *A generic library support script.* |
| -- | -- |
| Homepage | [https://www.gnu.org/software/libtool/](https://www.gnu.org/software/libtool/) |
| Versions | 2.4.5, 2.4.6 |
| Architectures | arm64, x64, x86 |
| Definition | [libtool/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libtool/xmake.lua) |

##### Install command

```console
xrepo install libtool
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libtool")
```


### libtorch (windows)


| Description | *An open source machine learning framework that accelerates the path from research prototyping to production deployment.* |
| -- | -- |
| Homepage | [https://pytorch.org/](https://pytorch.org/) |
| License | BSD-3-Clause |
| Versions | v1.11.0, v1.12.1, v1.8.0, v1.8.1, v1.8.2, v1.9.0, v1.9.1 |
| Architectures | x64 |
| Definition | [libtorch/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libtorch/xmake.lua) |

##### Install command

```console
xrepo install libtorch
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libtorch")
```


### libui (windows)


| Description | *A portable GUI library for C* |
| -- | -- |
| Homepage | [https://libui-ng.github.io/libui-ng/](https://libui-ng.github.io/libui-ng/) |
| Versions | 2022.12.3 |
| Architectures | arm64, x64, x86 |
| Definition | [libui/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libui/xmake.lua) |

##### Install command

```console
xrepo install libui
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libui")
```


### libusb (windows)


| Description | *A cross-platform library to access USB devices.* |
| -- | -- |
| Homepage | [https://libusb.info](https://libusb.info) |
| Versions | v1.0.24 |
| Architectures | arm64, x64, x86 |
| Definition | [libusb/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libusb/xmake.lua) |

##### Install command

```console
xrepo install libusb
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libusb")
```


### libuv (windows)


| Description | *A multi-platform support library with a focus on asynchronous I/O.* |
| -- | -- |
| Homepage | [http://libuv.org/](http://libuv.org/) |
| License | MIT |
| Versions | v1.22.0, v1.23.0, v1.23.1, v1.23.2, v1.24.0, v1.24.1, v1.25.0, v1.26.0, v1.27.0, v1.28.0, v1.40.0, v1.41.0, v1.42.0, v1.44.1 |
| Architectures | arm64, x64, x86 |
| Definition | [libuv/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libuv/xmake.lua) |

##### Install command

```console
xrepo install libuv
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libuv")
```


### libvorbis (windows)


| Description | *Reference implementation of the Ogg Vorbis audio format.* |
| -- | -- |
| Homepage | [https://xiph.org/vorbis](https://xiph.org/vorbis) |
| License | BSD-3 |
| Versions | 1.3.7 |
| Architectures | arm64, x64, x86 |
| Definition | [libvorbis/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libvorbis/xmake.lua) |

##### Install command

```console
xrepo install libvorbis
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libvorbis")
```


### libwebp (windows)


| Description | *Library to encode and decode images in WebP format.* |
| -- | -- |
| Homepage | [https://chromium.googlesource.com/webm/libwebp/](https://chromium.googlesource.com/webm/libwebp/) |
| License | BSD-3-Clause |
| Versions | v1.1.0, v1.2.2 |
| Architectures | arm64, x64, x86 |
| Definition | [libwebp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libwebp/xmake.lua) |

##### Install command

```console
xrepo install libwebp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libwebp")
```


### libwebsockets (windows)


| Description | *canonical libwebsockets.org websocket library* |
| -- | -- |
| Homepage | [https://github.com/warmcat/libwebsockets](https://github.com/warmcat/libwebsockets) |
| Versions | v4.1.6 |
| Architectures | arm64, x64, x86 |
| Definition | [libwebsockets/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libwebsockets/xmake.lua) |

##### Install command

```console
xrepo install libwebsockets
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libwebsockets")
```


### libxmake (windows)


| Description | *The c/c++ bindings of the xmake core engine* |
| -- | -- |
| Homepage | [https://xmake.io](https://xmake.io) |
| Versions | v2.5.9, v2.7.1 |
| Architectures | arm64, x64, x86 |
| Definition | [libxmake/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libxmake/xmake.lua) |

##### Install command

```console
xrepo install libxmake
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libxmake")
```


### libxml2 (windows)


| Description | *The XML C parser and toolkit of Gnome.* |
| -- | -- |
| Homepage | [http://xmlsoft.org/](http://xmlsoft.org/) |
| License | MIT |
| Versions | 2.9.10, 2.9.12, 2.9.9 |
| Architectures | arm64, x64, x86 |
| Definition | [libxml2/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libxml2/xmake.lua) |

##### Install command

```console
xrepo install libxml2
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libxml2")
```


### libxslt (windows)


| Description | *Libxslt is the XSLT C library developed for the GNOME project.* |
| -- | -- |
| Homepage | [http://xmlsoft.org/XSLT/](http://xmlsoft.org/XSLT/) |
| License | MIT |
| Versions | 1.1.34 |
| Architectures | arm64, x64, x86 |
| Definition | [libxslt/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libxslt/xmake.lua) |

##### Install command

```console
xrepo install libxslt
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libxslt")
```


### libyaml (windows)


| Description | *Canonical source repository for LibYAML.* |
| -- | -- |
| Homepage | [http://pyyaml.org/wiki/LibYAML](http://pyyaml.org/wiki/LibYAML) |
| License | MIT |
| Versions | 0.2.2, 0.2.5 |
| Architectures | arm64, x64, x86 |
| Definition | [libyaml/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libyaml/xmake.lua) |

##### Install command

```console
xrepo install libyaml
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libyaml")
```


### libzip (windows)


| Description | *A C library for reading, creating, and modifying zip archives.* |
| -- | -- |
| Homepage | [https://libzip.org/](https://libzip.org/) |
| License | BSD-3-Clause |
| Versions | v1.8.0, v1.9.2 |
| Architectures | arm64, x64, x86 |
| Definition | [libzip/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/libzip/xmake.lua) |

##### Install command

```console
xrepo install libzip
```

##### Integration in the project (xmake.lua)

```lua
add_requires("libzip")
```


### lief (windows)


| Description | *Library to Instrument Executable Formats.* |
| -- | -- |
| Homepage | [https://lief.quarkslab.com](https://lief.quarkslab.com) |
| License | Apache-2.0 |
| Versions | 0.10.1, 0.11.5 |
| Architectures | arm64, x64, x86 |
| Definition | [lief/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/lief/xmake.lua) |

##### Install command

```console
xrepo install lief
```

##### Integration in the project (xmake.lua)

```lua
add_requires("lief")
```


### lightgbm (windows)


| Description | *LightGBM is a gradient boosting framework that uses tree based learning algorithms.* |
| -- | -- |
| Homepage | [https://github.com/microsoft/LightGBM](https://github.com/microsoft/LightGBM) |
| License | MIT |
| Versions | 3.2.1 |
| Architectures | x64 |
| Definition | [lightgbm/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/lightgbm/xmake.lua) |

##### Install command

```console
xrepo install lightgbm
```

##### Integration in the project (xmake.lua)

```lua
add_requires("lightgbm")
```


### littlefs (windows)


| Description | *A little fail-safe filesystem designed for microcontrollers* |
| -- | -- |
| Homepage | [https://github.com/littlefs-project/littlefs](https://github.com/littlefs-project/littlefs) |
| Versions | v2.5.0 |
| Architectures | arm64, x64, x86 |
| Definition | [littlefs/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/littlefs/xmake.lua) |

##### Install command

```console
xrepo install littlefs
```

##### Integration in the project (xmake.lua)

```lua
add_requires("littlefs")
```


### llfio (windows)


| Description | *UTF8-CPP: UTF-8 with C++ in a Portable Way* |
| -- | -- |
| Homepage | [https://github.com/ned14/llfio](https://github.com/ned14/llfio) |
| License | Apache-2.0 |
| Versions | 2022.9.7 |
| Architectures | arm64, x64, x86 |
| Definition | [llfio/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/llfio/xmake.lua) |

##### Install command

```console
xrepo install llfio
```

##### Integration in the project (xmake.lua)

```lua
add_requires("llfio")
```


### llhttp (windows)


| Description | *Port of http_parser to llparse* |
| -- | -- |
| Homepage | [https://github.com/nodejs/llhttp](https://github.com/nodejs/llhttp) |
| License | MIT |
| Versions | v3.0.0 |
| Architectures | arm64, x64, x86 |
| Definition | [llhttp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/llhttp/xmake.lua) |

##### Install command

```console
xrepo install llhttp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("llhttp")
```


### llvm (windows)


| Description | *The LLVM Compiler Infrastructure* |
| -- | -- |
| Homepage | [https://llvm.org/](https://llvm.org/) |
| Versions | 11.0.0, 14.0.0 |
| Architectures | arm64, x64, x86 |
| Definition | [llvm/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/llvm/xmake.lua) |

##### Install command

```console
xrepo install llvm
```

##### Integration in the project (xmake.lua)

```lua
add_requires("llvm")
```


### llvm-mingw (windows)


| Description | *An LLVM/Clang/LLD based mingw-w64 toolchain* |
| -- | -- |
| Homepage | [https://github.com/mstorsjo/llvm-mingw](https://github.com/mstorsjo/llvm-mingw) |
| Versions | 20211002, 20220323 |
| Architectures | arm64, x64, x86 |
| Definition | [llvm-mingw/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/llvm-mingw/xmake.lua) |

##### Install command

```console
xrepo install llvm-mingw
```

##### Integration in the project (xmake.lua)

```lua
add_requires("llvm-mingw")
```


### lodepng (windows)


| Description | *PNG encoder and decoder in C and C++.* |
| -- | -- |
| Homepage | [https://lodev.org/lodepng/](https://lodev.org/lodepng/) |
| License | zlib |
| Versions |  |
| Architectures | arm64, x64, x86 |
| Definition | [lodepng/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/lodepng/xmake.lua) |

##### Install command

```console
xrepo install lodepng
```

##### Integration in the project (xmake.lua)

```lua
add_requires("lodepng")
```


### log4cplus (windows)


| Description | *log4cplus is a simple to use C++ logging API providing thread-safe, flexible, and arbitrarily granular control over log management and configuration.* |
| -- | -- |
| Homepage | [https://sourceforge.net/projects/log4cplus/](https://sourceforge.net/projects/log4cplus/) |
| License | BSD-2-Clause |
| Versions | 2.0.6, 2.0.7 |
| Architectures | arm64, x64, x86 |
| Definition | [log4cplus/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/log4cplus/xmake.lua) |

##### Install command

```console
xrepo install log4cplus
```

##### Integration in the project (xmake.lua)

```lua
add_requires("log4cplus")
```


### loguru (windows)


| Description | *A lightweight C++ logging library* |
| -- | -- |
| Homepage | [https://github.com/emilk/loguru](https://github.com/emilk/loguru) |
| Versions | v2.1.0 |
| Architectures | arm64, x64, x86 |
| Definition | [loguru/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/loguru/xmake.lua) |

##### Install command

```console
xrepo install loguru
```

##### Integration in the project (xmake.lua)

```lua
add_requires("loguru")
```


### lua (windows)


| Description | *A powerful, efficient, lightweight, embeddable scripting language.* |
| -- | -- |
| Homepage | [http://lua.org](http://lua.org) |
| Versions | v5.1.1, v5.1.5, v5.2.3, v5.3.6, v5.4.1, v5.4.2, v5.4.3, v5.4.4 |
| Architectures | arm64, x64, x86 |
| Definition | [lua/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/lua/xmake.lua) |

##### Install command

```console
xrepo install lua
```

##### Integration in the project (xmake.lua)

```lua
add_requires("lua")
```


### lua-format (windows)


| Description | *Code formatter for Lua* |
| -- | -- |
| Homepage | [https://github.com/Koihik/LuaFormatter](https://github.com/Koihik/LuaFormatter) |
| Versions | 1.3.5 |
| Architectures | arm64, x64, x86 |
| Definition | [lua-format/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/lua-format/xmake.lua) |

##### Install command

```console
xrepo install lua-format
```

##### Integration in the project (xmake.lua)

```lua
add_requires("lua-format")
```


### luajit (windows)


| Description | *A Just-In-Time Compiler (JIT) for the Lua programming language.* |
| -- | -- |
| Homepage | [http://luajit.org](http://luajit.org) |
| Versions | 2.1.0-beta3 |
| Architectures | arm64, x64, x86 |
| Definition | [luajit/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/luajit/xmake.lua) |

##### Install command

```console
xrepo install luajit
```

##### Integration in the project (xmake.lua)

```lua
add_requires("luajit")
```


### luau (windows)


| Description | *A fast, small, safe, gradually typed embeddable scripting language derived from Lua.* |
| -- | -- |
| Homepage | [https://luau-lang.org/](https://luau-lang.org/) |
| License | MIT |
| Versions | 0.538 |
| Architectures | arm64, x64, x86 |
| Definition | [luau/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/luau/xmake.lua) |

##### Install command

```console
xrepo install luau
```

##### Integration in the project (xmake.lua)

```lua
add_requires("luau")
```


### lvgl (windows)


| Description | *Light and Versatile Graphics Library* |
| -- | -- |
| Homepage | [https://lvgl.io](https://lvgl.io) |
| License | MIT |
| Versions | v8.0.2, v8.2.0 |
| Architectures | arm64, x64, x86 |
| Definition | [lvgl/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/lvgl/xmake.lua) |

##### Install command

```console
xrepo install lvgl
```

##### Integration in the project (xmake.lua)

```lua
add_requires("lvgl")
```


### lyra (windows)


| Description | *A simple to use, composable, command line parser for C++ 11 and beyond* |
| -- | -- |
| Homepage | [https://www.bfgroup.xyz/Lyra/](https://www.bfgroup.xyz/Lyra/) |
| License | BSL-1.0 |
| Versions | 1.5.1, 1.6 |
| Architectures | arm64, x64, x86 |
| Definition | [lyra/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/lyra/xmake.lua) |

##### Install command

```console
xrepo install lyra
```

##### Integration in the project (xmake.lua)

```lua
add_requires("lyra")
```


### lz4 (windows)


| Description | *LZ4 - Extremely fast compression* |
| -- | -- |
| Homepage | [https://www.lz4.org/](https://www.lz4.org/) |
| Versions | v1.9.3 |
| Architectures | arm64, x64, x86 |
| Definition | [lz4/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/lz4/xmake.lua) |

##### Install command

```console
xrepo install lz4
```

##### Integration in the project (xmake.lua)

```lua
add_requires("lz4")
```


### lzma (windows)


| Description | *LZMA SDK* |
| -- | -- |
| Homepage | [https://www.7-zip.org/sdk.html](https://www.7-zip.org/sdk.html) |
| Versions | 19.00, 22.01 |
| Architectures | arm64, x64, x86 |
| Definition | [lzma/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/lzma/xmake.lua) |

##### Install command

```console
xrepo install lzma
```

##### Integration in the project (xmake.lua)

```lua
add_requires("lzma")
```


### lzo (windows)


| Description | *LZO is a portable lossless data compression library written in ANSI C.* |
| -- | -- |
| Homepage | [http://www.oberhumer.com/opensource/lzo](http://www.oberhumer.com/opensource/lzo) |
| License | GPL-2.0 |
| Versions | 2.10 |
| Architectures | arm64, x64, x86 |
| Definition | [lzo/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/l/lzo/xmake.lua) |

##### Install command

```console
xrepo install lzo
```

##### Integration in the project (xmake.lua)

```lua
add_requires("lzo")
```



## m
### m4 (windows)


| Description | *Macro processing language* |
| -- | -- |
| Homepage | [https://www.gnu.org/software/m4](https://www.gnu.org/software/m4) |
| Versions | 1.4.18, 1.4.19 |
| Architectures | arm64, x64, x86 |
| Definition | [m4/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/m4/xmake.lua) |

##### Install command

```console
xrepo install m4
```

##### Integration in the project (xmake.lua)

```lua
add_requires("m4")
```


### magic_enum (windows)


| Description | *Static reflection for enums (to string, from string, iteration) for modern C++, work with any enum type without any macro or boilerplate code* |
| -- | -- |
| Homepage | [https://github.com/Neargye/magic_enum](https://github.com/Neargye/magic_enum) |
| License | MIT |
| Versions | v0.7.3, v0.8.0, v0.8.1 |
| Architectures | arm64, x64, x86 |
| Definition | [magic_enum/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/magic_enum/xmake.lua) |

##### Install command

```console
xrepo install magic_enum
```

##### Integration in the project (xmake.lua)

```lua
add_requires("magic_enum")
```


### magnum (windows)


| Description | *Lightweight and modular C++11/C++14 graphics middleware for games and data visualization.* |
| -- | -- |
| Homepage | [https://magnum.graphics/](https://magnum.graphics/) |
| License | MIT |
| Versions | v2020.06 |
| Architectures | arm64, x64, x86 |
| Definition | [magnum/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/magnum/xmake.lua) |

##### Install command

```console
xrepo install magnum
```

##### Integration in the project (xmake.lua)

```lua
add_requires("magnum")
```


### magnum-extras (windows)


| Description | *Extras for magnum, Lightweight and modular C++11/C++14 graphics middleware for games and data visualization.* |
| -- | -- |
| Homepage | [https://magnum.graphics/](https://magnum.graphics/) |
| License | MIT |
| Versions | v2020.06 |
| Architectures | arm64, x64, x86 |
| Definition | [magnum-extras/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/magnum-extras/xmake.lua) |

##### Install command

```console
xrepo install magnum-extras
```

##### Integration in the project (xmake.lua)

```lua
add_requires("magnum-extras")
```


### magnum-integration (windows)


| Description | *Integration libraries for magnum, Lightweight and modular C++11/C++14 graphics middleware for games and data visualization.* |
| -- | -- |
| Homepage | [https://magnum.graphics/](https://magnum.graphics/) |
| License | MIT |
| Versions | v2020.06 |
| Architectures | arm64, x64, x86 |
| Definition | [magnum-integration/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/magnum-integration/xmake.lua) |

##### Install command

```console
xrepo install magnum-integration
```

##### Integration in the project (xmake.lua)

```lua
add_requires("magnum-integration")
```


### magnum-plugins (windows)


| Description | *Plugins for magnum, C++11/C++14 graphics middleware for games and data visualization.* |
| -- | -- |
| Homepage | [https://magnum.graphics/](https://magnum.graphics/) |
| License | MIT |
| Versions | v2020.06 |
| Architectures | arm64, x64, x86 |
| Definition | [magnum-plugins/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/magnum-plugins/xmake.lua) |

##### Install command

```console
xrepo install magnum-plugins
```

##### Integration in the project (xmake.lua)

```lua
add_requires("magnum-plugins")
```


### make (windows)


| Description | *GNU make tool.* |
| -- | -- |
| Homepage | [https://www.gnu.org/software/make/](https://www.gnu.org/software/make/) |
| Versions | 4.2.1, 4.3 |
| Architectures | arm64, x64, x86 |
| Definition | [make/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/make/xmake.lua) |

##### Install command

```console
xrepo install make
```

##### Integration in the project (xmake.lua)

```lua
add_requires("make")
```


### mapbox_earcut (windows)


| Description | *A C++ port of earcut.js, a fast, header-only polygon triangulation library.* |
| -- | -- |
| Homepage | [https://github.com/mapbox/earcut.hpp](https://github.com/mapbox/earcut.hpp) |
| License | ISC |
| Versions | 2.2.3 |
| Architectures | arm64, x64, x86 |
| Definition | [mapbox_earcut/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/mapbox_earcut/xmake.lua) |

##### Install command

```console
xrepo install mapbox_earcut
```

##### Integration in the project (xmake.lua)

```lua
add_requires("mapbox_earcut")
```


### mapbox_eternal (windows)


| Description | *A C++14 compile-time/constexpr map and hash map with minimal binary footprint* |
| -- | -- |
| Homepage | [https://github.com/mapbox/eternal](https://github.com/mapbox/eternal) |
| License | ISC |
| Versions | v1.0.1 |
| Architectures | arm64, x64, x86 |
| Definition | [mapbox_eternal/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/mapbox_eternal/xmake.lua) |

##### Install command

```console
xrepo install mapbox_eternal
```

##### Integration in the project (xmake.lua)

```lua
add_requires("mapbox_eternal")
```


### mapbox_geometry (windows)


| Description | *Provides header-only, generic C++ interfaces for geometry types, geometry collections, and features.* |
| -- | -- |
| Homepage | [https://github.com/mapbox/geometry.hpp](https://github.com/mapbox/geometry.hpp) |
| License | ISC |
| Versions | 1.1.0, 2.0.3 |
| Architectures | arm64, x64, x86 |
| Definition | [mapbox_geometry/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/mapbox_geometry/xmake.lua) |

##### Install command

```console
xrepo install mapbox_geometry
```

##### Integration in the project (xmake.lua)

```lua
add_requires("mapbox_geometry")
```


### mapbox_variant (windows)


| Description | *C++11/C++14 Variant* |
| -- | -- |
| Homepage | [https://github.com/mapbox/variant](https://github.com/mapbox/variant) |
| License | BSD |
| Versions | v1.2.0 |
| Architectures | arm64, x64, x86 |
| Definition | [mapbox_variant/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/mapbox_variant/xmake.lua) |

##### Install command

```console
xrepo install mapbox_variant
```

##### Integration in the project (xmake.lua)

```lua
add_requires("mapbox_variant")
```


### mariadb-connector-c (windows)


| Description | *MariaDB Connector/C is used to connect applications developed in C/C++ to MariaDB and MySQL databases.* |
| -- | -- |
| Homepage | [https://github.com/mariadb-corporation/mariadb-connector-c](https://github.com/mariadb-corporation/mariadb-connector-c) |
| License | LGPL-2.1 |
| Versions | 3.1.13 |
| Architectures | arm64, x64, x86 |
| Definition | [mariadb-connector-c/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/mariadb-connector-c/xmake.lua) |

##### Install command

```console
xrepo install mariadb-connector-c
```

##### Integration in the project (xmake.lua)

```lua
add_requires("mariadb-connector-c")
```


### marisa (windows)


| Description | *Matching Algorithm with Recursively Implemented StorAge.* |
| -- | -- |
| Homepage | [https://github.com/s-yata/marisa-trie](https://github.com/s-yata/marisa-trie) |
| Versions | v0.2.6 |
| Architectures | arm64, x64, x86 |
| Definition | [marisa/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/marisa/xmake.lua) |

##### Install command

```console
xrepo install marisa
```

##### Integration in the project (xmake.lua)

```lua
add_requires("marisa")
```


### marl (windows)


| Description | *Marl is a hybrid thread / fiber task scheduler written in C++ 11.* |
| -- | -- |
| Homepage | [https://github.com/google/marl](https://github.com/google/marl) |
| Versions | 2021.8.18, 2022.3.02 |
| Architectures | arm64, x64, x86 |
| Definition | [marl/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/marl/xmake.lua) |

##### Install command

```console
xrepo install marl
```

##### Integration in the project (xmake.lua)

```lua
add_requires("marl")
```


### mathfu (windows)


| Description | *C++ math library developed primarily for games focused on simplicity and efficiency.* |
| -- | -- |
| Homepage | [http://google.github.io/mathfu](http://google.github.io/mathfu) |
| License | Apache-2.0 |
| Versions | 2022.5.10 |
| Architectures | arm64, x64, x86 |
| Definition | [mathfu/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/mathfu/xmake.lua) |

##### Install command

```console
xrepo install mathfu
```

##### Integration in the project (xmake.lua)

```lua
add_requires("mathfu")
```


### matplotplusplus (windows)


| Description | *A C++ Graphics Library for Data Visualization* |
| -- | -- |
| Homepage | [https://alandefreitas.github.io/matplotplusplus/](https://alandefreitas.github.io/matplotplusplus/) |
| License | MIT |
| Versions | v1.1.0 |
| Architectures | arm64, x64, x86 |
| Definition | [matplotplusplus/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/matplotplusplus/xmake.lua) |

##### Install command

```console
xrepo install matplotplusplus
```

##### Integration in the project (xmake.lua)

```lua
add_requires("matplotplusplus")
```


### mbedtls (windows)


| Description | *An SSL library* |
| -- | -- |
| Homepage | [https://tls.mbed.org](https://tls.mbed.org) |
| Versions | 2.13.0, 2.25.0, 2.7.6 |
| Architectures | arm64, x64, x86 |
| Definition | [mbedtls/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/mbedtls/xmake.lua) |

##### Install command

```console
xrepo install mbedtls
```

##### Integration in the project (xmake.lua)

```lua
add_requires("mbedtls")
```


### mem (windows)


| Description | *A collection of C++11 headers useful for reverse engineering* |
| -- | -- |
| Homepage | [https://github.com/0x1F9F1/mem](https://github.com/0x1F9F1/mem) |
| Versions | 1.0.0 |
| Architectures | arm64, x64, x86 |
| Definition | [mem/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/mem/xmake.lua) |

##### Install command

```console
xrepo install mem
```

##### Integration in the project (xmake.lua)

```lua
add_requires("mem")
```


### meowhash (windows)


| Description | *Official version of the Meow hash, an extremely fast level 1 hash* |
| -- | -- |
| Homepage | [https://mollyrocket.com/meowhash](https://mollyrocket.com/meowhash) |
| Versions | 1.0.0 |
| Architectures | arm64, x64, x86 |
| Definition | [meowhash/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/meowhash/xmake.lua) |

##### Install command

```console
xrepo install meowhash
```

##### Integration in the project (xmake.lua)

```lua
add_requires("meowhash")
```


### meshoptimizer (windows)


| Description | *Mesh optimization library that makes meshes smaller and faster to render* |
| -- | -- |
| Homepage | [https://github.com/zeux/meshoptimizer](https://github.com/zeux/meshoptimizer) |
| License | MIT |
| Versions | v0.18 |
| Architectures | arm64, x64, x86 |
| Definition | [meshoptimizer/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/meshoptimizer/xmake.lua) |

##### Install command

```console
xrepo install meshoptimizer
```

##### Integration in the project (xmake.lua)

```lua
add_requires("meshoptimizer")
```


### meson (windows)


| Description | *Fast and user friendly build system.* |
| -- | -- |
| Homepage | [https://mesonbuild.com/](https://mesonbuild.com/) |
| License | Apache-2.0 |
| Versions | 0.50.1, 0.56.0, 0.58.0, 0.58.1, 0.59.1, 0.59.2, 0.60.1, 0.61.2, 0.62.1 |
| Architectures | arm64, x64, x86 |
| Definition | [meson/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/meson/xmake.lua) |

##### Install command

```console
xrepo install meson
```

##### Integration in the project (xmake.lua)

```lua
add_requires("meson")
```


### metis (windows)


| Description | *Serial Graph Partitioning and Fill-reducing Matrix Ordering* |
| -- | -- |
| Homepage | [http://glaros.dtc.umn.edu/gkhome/metis/metis/overview](http://glaros.dtc.umn.edu/gkhome/metis/metis/overview) |
| Versions | 5.1.1 |
| Architectures | arm64, x64, x86 |
| Definition | [metis/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/metis/xmake.lua) |

##### Install command

```console
xrepo install metis
```

##### Integration in the project (xmake.lua)

```lua
add_requires("metis")
```


### mfast (windows)


| Description | *High performance C++ encoding/decoding library for FAST (FIX Adapted for STreaming) protocol.* |
| -- | -- |
| Homepage | [https://github.com/objectcomputing/mFAST](https://github.com/objectcomputing/mFAST) |
| License | BSD-3-Clause |
| Versions | v1.2.2 |
| Architectures | arm64, x64, x86 |
| Definition | [mfast/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/mfast/xmake.lua) |

##### Install command

```console
xrepo install mfast
```

##### Integration in the project (xmake.lua)

```lua
add_requires("mfast")
```


### mhook (windows)


| Description | *A Windows API hooking library * |
| -- | -- |
| Homepage | [https://github.com/martona/mhook](https://github.com/martona/mhook) |
| Versions | 2.5.1 |
| Architectures | arm64, x64, x86 |
| Definition | [mhook/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/mhook/xmake.lua) |

##### Install command

```console
xrepo install mhook
```

##### Integration in the project (xmake.lua)

```lua
add_requires("mhook")
```


### microsoft-gsl (windows)


| Description | *Guidelines Support Library* |
| -- | -- |
| Homepage | [https://github.com/microsoft/GSL](https://github.com/microsoft/GSL) |
| License | MIT |
| Versions | v3.1.0, v4.0.0 |
| Architectures | arm64, x64, x86 |
| Definition | [microsoft-gsl/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/microsoft-gsl/xmake.lua) |

##### Install command

```console
xrepo install microsoft-gsl
```

##### Integration in the project (xmake.lua)

```lua
add_requires("microsoft-gsl")
```


### mikktspace (windows)


| Description | *A common standard for tangent space used in baking tools to produce normal maps.* |
| -- | -- |
| Homepage | [http://www.mikktspace.com/](http://www.mikktspace.com/) |
| Versions | 2020.03.26 |
| Architectures | arm64, x64, x86 |
| Definition | [mikktspace/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/mikktspace/xmake.lua) |

##### Install command

```console
xrepo install mikktspace
```

##### Integration in the project (xmake.lua)

```lua
add_requires("mikktspace")
```


### mimalloc (windows)


| Description | *mimalloc (pronounced 'me-malloc') is a general purpose allocator with excellent performance characteristics.* |
| -- | -- |
| Homepage | [https://github.com/microsoft/mimalloc](https://github.com/microsoft/mimalloc) |
| License | MIT |
| Versions | 1.6.7, 1.7.0, 1.7.1, 1.7.2, 1.7.3, 1.7.6, 1.7.7, 2.0.1, 2.0.2, 2.0.3, 2.0.5, 2.0.6, 2.0.7 |
| Architectures | arm64, x64, x86 |
| Definition | [mimalloc/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/mimalloc/xmake.lua) |

##### Install command

```console
xrepo install mimalloc
```

##### Integration in the project (xmake.lua)

```lua
add_requires("mimalloc")
```


### minhook (windows)


| Description | *The Minimalistic x86/x64 API Hooking Library for Windows.* |
| -- | -- |
| Homepage | [https://github.com/TsudaKageyu/minhook](https://github.com/TsudaKageyu/minhook) |
| License | BSD-2-Clause |
| Versions | v1.3.3 |
| Architectures | arm64, x64, x86 |
| Definition | [minhook/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/minhook/xmake.lua) |

##### Install command

```console
xrepo install minhook
```

##### Integration in the project (xmake.lua)

```lua
add_requires("minhook")
```


### miniaudio (windows)


| Description | *Single file audio playback and capture library written in C.* |
| -- | -- |
| Homepage | [https://miniaud.io](https://miniaud.io) |
| Versions | 2021.12.31 |
| Architectures | arm64, x64, x86 |
| Definition | [miniaudio/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/miniaudio/xmake.lua) |

##### Install command

```console
xrepo install miniaudio
```

##### Integration in the project (xmake.lua)

```lua
add_requires("miniaudio")
```


### minifb (windows)


| Description | *MiniFB is a small cross platform library to create a frame buffer that you can draw pixels in* |
| -- | -- |
| Homepage | [https://github.com/emoon/minifb](https://github.com/emoon/minifb) |
| License | MIT |
| Versions | 2022.11.12 |
| Architectures | arm64, x64, x86 |
| Definition | [minifb/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/minifb/xmake.lua) |

##### Install command

```console
xrepo install minifb
```

##### Integration in the project (xmake.lua)

```lua
add_requires("minifb")
```


### minilzo (windows)


| Description | *A very lightweight subset of the LZO library intended for easy inclusion with your application* |
| -- | -- |
| Homepage | [http://www.oberhumer.com/opensource/lzo/#minilzo](http://www.oberhumer.com/opensource/lzo/#minilzo) |
| Versions | 2.10 |
| Architectures | arm64, x64, x86 |
| Definition | [minilzo/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/minilzo/xmake.lua) |

##### Install command

```console
xrepo install minilzo
```

##### Integration in the project (xmake.lua)

```lua
add_requires("minilzo")
```


### minimp3 (windows)


| Description | *Minimalistic MP3 decoder single header library* |
| -- | -- |
| Homepage | [https://github.com/lieff/minimp3](https://github.com/lieff/minimp3) |
| License | CC0 |
| Versions | 2021.05.29 |
| Architectures | arm64, x64, x86 |
| Definition | [minimp3/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/minimp3/xmake.lua) |

##### Install command

```console
xrepo install minimp3
```

##### Integration in the project (xmake.lua)

```lua
add_requires("minimp3")
```


### miniz (windows)


| Description | *miniz: Single C source file zlib-replacement library* |
| -- | -- |
| Homepage | [https://github.com/richgel999/miniz/](https://github.com/richgel999/miniz/) |
| License | MIT |
| Versions | 2.1.0, 2.2.0 |
| Architectures | arm64, x64, x86 |
| Definition | [miniz/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/miniz/xmake.lua) |

##### Install command

```console
xrepo install miniz
```

##### Integration in the project (xmake.lua)

```lua
add_requires("miniz")
```


### minizip (windows)


| Description | *Mini zip and unzip based on zlib* |
| -- | -- |
| Homepage | [https://www.zlib.net/](https://www.zlib.net/) |
| License | zlib |
| Versions | v1.2.10, v1.2.11, v1.2.12 |
| Architectures | arm64, x64, x86 |
| Definition | [minizip/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/minizip/xmake.lua) |

##### Install command

```console
xrepo install minizip
```

##### Integration in the project (xmake.lua)

```lua
add_requires("minizip")
```


### minizip-ng (windows)


| Description | *Fork of the popular zip manipulation library found in the zlib distribution.* |
| -- | -- |
| Homepage | [https://github.com/zlib-ng/minizip-ng](https://github.com/zlib-ng/minizip-ng) |
| License | zlib |
| Versions | 3.0.3, 3.0.5 |
| Architectures | arm64, x64, x86 |
| Definition | [minizip-ng/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/minizip-ng/xmake.lua) |

##### Install command

```console
xrepo install minizip-ng
```

##### Integration in the project (xmake.lua)

```lua
add_requires("minizip-ng")
```


### mjson (windows)


| Description | *C/C++ JSON parser, emitter, JSON-RPC engine for embedded systems* |
| -- | -- |
| Homepage | [https://github.com/cesanta/mjson](https://github.com/cesanta/mjson) |
| License | MIT |
| Versions | 1.2.6 |
| Architectures | arm64, x64, x86 |
| Definition | [mjson/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/mjson/xmake.lua) |

##### Install command

```console
xrepo install mjson
```

##### Integration in the project (xmake.lua)

```lua
add_requires("mjson")
```


### mkl (windows)


| Description | *Intel® oneAPI Math Kernel Library* |
| -- | -- |
| Homepage | [https://software.intel.com/content/www/us/en/develop/tools/oneapi/components/onemkl.html](https://software.intel.com/content/www/us/en/develop/tools/oneapi/components/onemkl.html) |
| Versions | 2021.2.0+296, 2021.3.0+520, 2022.1.0+223 |
| Architectures | arm64, x64, x86 |
| Definition | [mkl/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/mkl/xmake.lua) |

##### Install command

```console
xrepo install mkl
```

##### Integration in the project (xmake.lua)

```lua
add_requires("mkl")
```


### mma (windows)


| Description | *A self-contained C++ implementation of MMA and GCMMA.* |
| -- | -- |
| Homepage | [https://github.com/jdumas/mma](https://github.com/jdumas/mma) |
| License | MIT |
| Versions | 2018.08.01 |
| Architectures | arm64, x64, x86 |
| Definition | [mma/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/mma/xmake.lua) |

##### Install command

```console
xrepo install mma
```

##### Integration in the project (xmake.lua)

```lua
add_requires("mma")
```


### mnn (windows)


| Description | *MNN is a highly efficient and lightweight deep learning framework.* |
| -- | -- |
| Homepage | [https://www.mnn.zone/](https://www.mnn.zone/) |
| License | Apache-2.0 |
| Versions | 1.2.1, 1.2.2 |
| Architectures | arm64, x64, x86 |
| Definition | [mnn/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/mnn/xmake.lua) |

##### Install command

```console
xrepo install mnn
```

##### Integration in the project (xmake.lua)

```lua
add_requires("mnn")
```


### mongo-c-driver (windows)


| Description | *The MongoDB C Driver.* |
| -- | -- |
| Homepage | [http://mongoc.org/](http://mongoc.org/) |
| License | Apache-2.0 |
| Versions | 1.19.0, 1.20.1 |
| Architectures | arm64, x64, x86 |
| Definition | [mongo-c-driver/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/mongo-c-driver/xmake.lua) |

##### Install command

```console
xrepo install mongo-c-driver
```

##### Integration in the project (xmake.lua)

```lua
add_requires("mongo-c-driver")
```


### mongo-cxx-driver (windows)


| Description | *mongodb c++ driver* |
| -- | -- |
| Homepage | [https://github.com/mongodb/mongo-cxx-driver](https://github.com/mongodb/mongo-cxx-driver) |
| Versions | 3.6.6 |
| Architectures | arm64, x64, x86 |
| Definition | [mongo-cxx-driver/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/mongo-cxx-driver/xmake.lua) |

##### Install command

```console
xrepo install mongo-cxx-driver
```

##### Integration in the project (xmake.lua)

```lua
add_requires("mongo-cxx-driver")
```


### moonjit (windows)


| Description | *A Just-In-Time Compiler (JIT) for the Lua programming language.* |
| -- | -- |
| Homepage | [https://github.com/moonjit/moonjit](https://github.com/moonjit/moonjit) |
| Versions | 2.2.0 |
| Architectures | arm64, x64, x86 |
| Definition | [moonjit/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/moonjit/xmake.lua) |

##### Install command

```console
xrepo install moonjit
```

##### Integration in the project (xmake.lua)

```lua
add_requires("moonjit")
```


### mpdecimal (windows)


| Description | *mpdecimal is a package for correctly-rounded arbitrary precision decimal floating point arithmetic.* |
| -- | -- |
| Homepage | [https://www.bytereef.org/mpdecimal/index.html](https://www.bytereef.org/mpdecimal/index.html) |
| License | BSD-2-Clause |
| Versions | 2.5.1 |
| Architectures | arm64, x64, x86 |
| Definition | [mpdecimal/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/mpdecimal/xmake.lua) |

##### Install command

```console
xrepo install mpdecimal
```

##### Integration in the project (xmake.lua)

```lua
add_requires("mpdecimal")
```


### mpmcqueue (windows)


| Description | *A bounded multi-producer multi-consumer concurrent queue written in C++11* |
| -- | -- |
| Homepage | [https://github.com/rigtorp/MPMCQueue](https://github.com/rigtorp/MPMCQueue) |
| Versions | v1.0 |
| Architectures | arm64, x64, x86 |
| Definition | [mpmcqueue/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/mpmcqueue/xmake.lua) |

##### Install command

```console
xrepo install mpmcqueue
```

##### Integration in the project (xmake.lua)

```lua
add_requires("mpmcqueue")
```


### msgpack-c (windows)


| Description | *MessagePack implementation for C* |
| -- | -- |
| Homepage | [https://msgpack.org/](https://msgpack.org/) |
| License | BSL-1.0 |
| Versions | 4.0.0 |
| Architectures | arm64, x64, x86 |
| Definition | [msgpack-c/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/msgpack-c/xmake.lua) |

##### Install command

```console
xrepo install msgpack-c
```

##### Integration in the project (xmake.lua)

```lua
add_requires("msgpack-c")
```


### msgpack-cxx (windows)


| Description | *MessagePack implementation for C++* |
| -- | -- |
| Homepage | [https://msgpack.org/](https://msgpack.org/) |
| License | BSL-1.0 |
| Versions | 4.1.1 |
| Architectures | arm64, x64, x86 |
| Definition | [msgpack-cxx/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/msgpack-cxx/xmake.lua) |

##### Install command

```console
xrepo install msgpack-cxx
```

##### Integration in the project (xmake.lua)

```lua
add_requires("msgpack-cxx")
```


### muslcc (windows)


| Description | *static cross- and native- musl-based toolchains.* |
| -- | -- |
| Homepage | [https://musl.cc/](https://musl.cc/) |
| Versions | 20210202 |
| Architectures | arm64, x64, x86 |
| Definition | [muslcc/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/muslcc/xmake.lua) |

##### Install command

```console
xrepo install muslcc
```

##### Integration in the project (xmake.lua)

```lua
add_requires("muslcc")
```


### mxml (windows)


| Description | *Mini-XML is a tiny XML library that you can use to read and write XML and XML-like data files in your application without requiring large non-standard libraries.* |
| -- | -- |
| Homepage | [https://www.msweet.org/mxml/](https://www.msweet.org/mxml/) |
| License | Apache-2.0 |
| Versions | 3.3 |
| Architectures | arm64, x64, x86 |
| Definition | [mxml/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/m/mxml/xmake.lua) |

##### Install command

```console
xrepo install mxml
```

##### Integration in the project (xmake.lua)

```lua
add_requires("mxml")
```



## n
### named_type (windows)


| Description | *Implementation of strong types in C++.* |
| -- | -- |
| Homepage | [https://github.com/joboccara/NamedType](https://github.com/joboccara/NamedType) |
| License | MIT |
| Versions | v1.1.0.20210209 |
| Architectures | arm64, x64, x86 |
| Definition | [named_type/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/named_type/xmake.lua) |

##### Install command

```console
xrepo install named_type
```

##### Integration in the project (xmake.lua)

```lua
add_requires("named_type")
```


### nana (windows)


| Description | *A modern C++ GUI library.* |
| -- | -- |
| Homepage | [http://nanapro.org](http://nanapro.org) |
| Versions | 1.6.2, 1.7.2, 1.7.4 |
| Architectures | arm64, x64, x86 |
| Definition | [nana/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/nana/xmake.lua) |

##### Install command

```console
xrepo install nana
```

##### Integration in the project (xmake.lua)

```lua
add_requires("nana")
```


### nanoflann (windows)


| Description | *nanoflann: a C++11 header-only library for Nearest Neighbor (NN) search with KD-trees* |
| -- | -- |
| Homepage | [https://github.com/jlblancoc/nanoflann/](https://github.com/jlblancoc/nanoflann/) |
| License | BSD-2-Clause |
| Versions | v1.3.2, v1.4.2 |
| Architectures | arm64, x64, x86 |
| Definition | [nanoflann/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/nanoflann/xmake.lua) |

##### Install command

```console
xrepo install nanoflann
```

##### Integration in the project (xmake.lua)

```lua
add_requires("nanoflann")
```


### nanogui (windows)


| Description | *Minimalistic GUI library for OpenGL* |
| -- | -- |
| Homepage | [https://github.com/wjakob/nanogui](https://github.com/wjakob/nanogui) |
| Versions | 2019.9.23 |
| Architectures | arm64, x64, x86 |
| Definition | [nanogui/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/nanogui/xmake.lua) |

##### Install command

```console
xrepo install nanogui
```

##### Integration in the project (xmake.lua)

```lua
add_requires("nanogui")
```


### nanosvg (windows)


| Description | *Simple stupid SVG parser* |
| -- | -- |
| Homepage | [https://github.com/memononen/nanosvg](https://github.com/memononen/nanosvg) |
| License | zlib |
| Versions | 2022.07.09 |
| Architectures | arm64, x64, x86 |
| Definition | [nanosvg/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/nanosvg/xmake.lua) |

##### Install command

```console
xrepo install nanosvg
```

##### Integration in the project (xmake.lua)

```lua
add_requires("nanosvg")
```


### nanovdb (windows)


| Description | *Developed by NVIDIA, NanoVDB adds real-time rendering GPU support for OpenVDB.* |
| -- | -- |
| Homepage | [https://developer.nvidia.com/nanovdb](https://developer.nvidia.com/nanovdb) |
| Versions | 20201219 |
| Architectures | arm64, x64, x86 |
| Definition | [nanovdb/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/nanovdb/xmake.lua) |

##### Install command

```console
xrepo install nanovdb
```

##### Integration in the project (xmake.lua)

```lua
add_requires("nanovdb")
```


### nanovg (windows)


| Description | *Antialiased 2D vector drawing library on top of OpenGL for UI and visualizations.* |
| -- | -- |
| Homepage | [https://github.com/memononen/nanovg/](https://github.com/memononen/nanovg/) |
| License | zlib |
| Versions | 2021.11.2 |
| Architectures | arm64, x64, x86 |
| Definition | [nanovg/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/nanovg/xmake.lua) |

##### Install command

```console
xrepo install nanovg
```

##### Integration in the project (xmake.lua)

```lua
add_requires("nanovg")
```


### nasm (windows)


| Description | *Netwide Assembler (NASM) is an 80x86 assembler.* |
| -- | -- |
| Homepage | [https://www.nasm.us/](https://www.nasm.us/) |
| License | BSD-2-Clause |
| Versions | 2.13.03, 2.15.05 |
| Architectures | arm64, x64, x86 |
| Definition | [nasm/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/nasm/xmake.lua) |

##### Install command

```console
xrepo install nasm
```

##### Integration in the project (xmake.lua)

```lua
add_requires("nasm")
```


### nativefiledialog (windows)


| Description | *A tiny, neat C library that portably invokes native file open and save dialogs.* |
| -- | -- |
| Homepage | [https://github.com/mlabbe/nativefiledialog](https://github.com/mlabbe/nativefiledialog) |
| License | zlib |
| Versions | 1.1.6 |
| Architectures | arm64, x64, x86 |
| Definition | [nativefiledialog/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/nativefiledialog/xmake.lua) |

##### Install command

```console
xrepo install nativefiledialog
```

##### Integration in the project (xmake.lua)

```lua
add_requires("nativefiledialog")
```


### ndk (windows)


| Description | *Android NDK toolchain.* |
| -- | -- |
| Homepage | [https://developer.android.com/ndk](https://developer.android.com/ndk) |
| Versions | 21.0, 22.0 |
| Architectures | arm64, x64, x86 |
| Definition | [ndk/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/ndk/xmake.lua) |

##### Install command

```console
xrepo install ndk
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ndk")
```


### newtondynamics (windows)


| Description | *Newton Dynamics is an integrated solution for real time simulation of physics environments.* |
| -- | -- |
| Homepage | [http://newtondynamics.com](http://newtondynamics.com) |
| License | zlib |
| Versions | v3.14d |
| Architectures | arm64, x64, x86 |
| Definition | [newtondynamics/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/newtondynamics/xmake.lua) |

##### Install command

```console
xrepo install newtondynamics
```

##### Integration in the project (xmake.lua)

```lua
add_requires("newtondynamics")
```


### newtondynamics3 (windows)


| Description | *Newton Dynamics is an integrated solution for real time simulation of physics environments.* |
| -- | -- |
| Homepage | [http://newtondynamics.com](http://newtondynamics.com) |
| License | zlib |
| Versions | v3.14d |
| Architectures | arm64, x64, x86 |
| Definition | [newtondynamics3/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/newtondynamics3/xmake.lua) |

##### Install command

```console
xrepo install newtondynamics3
```

##### Integration in the project (xmake.lua)

```lua
add_requires("newtondynamics3")
```


### newtondynamics4 (windows)


| Description | *Newton Dynamics is an integrated solution for real time simulation of physics environments.* |
| -- | -- |
| Homepage | [http://newtondynamics.com](http://newtondynamics.com) |
| License | zlib |
| Versions | v4.01 |
| Architectures | arm64, x64, x86 |
| Definition | [newtondynamics4/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/newtondynamics4/xmake.lua) |

##### Install command

```console
xrepo install newtondynamics4
```

##### Integration in the project (xmake.lua)

```lua
add_requires("newtondynamics4")
```


### nghttp2 (windows)


| Description | *nghttp2 is an implementation of HTTP/2 and its header compression algorithm HPACK in C.* |
| -- | -- |
| Homepage | [http://nghttp2.org/](http://nghttp2.org/) |
| License | MIT |
| Versions | 1.46.0 |
| Architectures | arm64, x64, x86 |
| Definition | [nghttp2/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/nghttp2/xmake.lua) |

##### Install command

```console
xrepo install nghttp2
```

##### Integration in the project (xmake.lua)

```lua
add_requires("nghttp2")
```


### nghttp3 (windows)


| Description | *HTTP/3 library written in C* |
| -- | -- |
| Homepage | [https://github.com/ngtcp2/nghttp3](https://github.com/ngtcp2/nghttp3) |
| License | MIT |
| Versions | 2022.02.08 |
| Architectures | arm64, x64, x86 |
| Definition | [nghttp3/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/nghttp3/xmake.lua) |

##### Install command

```console
xrepo install nghttp3
```

##### Integration in the project (xmake.lua)

```lua
add_requires("nghttp3")
```


### ngtcp2 (windows)


| Description | *ngtcp2 project is an effort to implement IETF QUIC protocol* |
| -- | -- |
| Homepage | [https://github.com/ngtcp2/ngtcp2](https://github.com/ngtcp2/ngtcp2) |
| License | MIT |
| Versions | 0.1.0 |
| Architectures | arm64, x64, x86 |
| Definition | [ngtcp2/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/ngtcp2/xmake.lua) |

##### Install command

```console
xrepo install ngtcp2
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ngtcp2")
```


### niftiheader (windows)


| Description | *Header structure descriptions for the nifti1 and nifti2 file formats.* |
| -- | -- |
| Homepage | [https://nifti.nimh.nih.gov/](https://nifti.nimh.nih.gov/) |
| License | Public Domain |
| Versions | 0.0.1 |
| Architectures | arm64, x64, x86 |
| Definition | [niftiheader/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/niftiheader/xmake.lua) |

##### Install command

```console
xrepo install niftiheader
```

##### Integration in the project (xmake.lua)

```lua
add_requires("niftiheader")
```


### ninja (windows)


| Description | *Small build system for use with gyp or CMake.* |
| -- | -- |
| Homepage | [https://ninja-build.org/](https://ninja-build.org/) |
| Versions | 1.10.1, 1.10.2, 1.11.0, 1.11.1, 1.9.0 |
| Architectures | arm64, x64, x86 |
| Definition | [ninja/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/ninja/xmake.lua) |

##### Install command

```console
xrepo install ninja
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ninja")
```


### nlohmann_json (windows)


| Description | *JSON for Modern C++* |
| -- | -- |
| Homepage | [https://nlohmann.github.io/json/](https://nlohmann.github.io/json/) |
| License | MIT |
| Versions | v3.10.0, v3.10.5, v3.11.2, v3.9.1 |
| Architectures | arm64, x64, x86 |
| Definition | [nlohmann_json/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/nlohmann_json/xmake.lua) |

##### Install command

```console
xrepo install nlohmann_json
```

##### Integration in the project (xmake.lua)

```lua
add_requires("nlohmann_json")
```


### nlopt (windows)


| Description | *NLopt is a library for nonlinear local and global optimization, for functions with and without gradient information.* |
| -- | -- |
| Homepage | [https://github.com/stevengj/nlopt/](https://github.com/stevengj/nlopt/) |
| License | LGPL-2.1 |
| Versions | v2.7.0, v2.7.1 |
| Architectures | arm64, x64, x86 |
| Definition | [nlopt/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/nlopt/xmake.lua) |

##### Install command

```console
xrepo install nlopt
```

##### Integration in the project (xmake.lua)

```lua
add_requires("nlopt")
```


### nng (windows)


| Description | *NNG, like its predecessors nanomsg (and to some extent ZeroMQ), is a lightweight, broker-less library, offering a simple API to solve common recurring messaging problems.* |
| -- | -- |
| Homepage | [https://github.com/nanomsg/nng](https://github.com/nanomsg/nng) |
| Versions | 1.3.2, 1.4.0, 1.5.2 |
| Architectures | arm64, x64, x86 |
| Definition | [nng/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/nng/xmake.lua) |

##### Install command

```console
xrepo install nng
```

##### Integration in the project (xmake.lua)

```lua
add_requires("nng")
```


### nngpp (windows)


| Description | *C++ wrapper around the nanomsg NNG API.* |
| -- | -- |
| Homepage | [https://github.com/cwzx/nngpp](https://github.com/cwzx/nngpp) |
| Versions | v2020.10.30 |
| Architectures | arm64, x64, x86 |
| Definition | [nngpp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/nngpp/xmake.lua) |

##### Install command

```console
xrepo install nngpp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("nngpp")
```


### nod (windows)


| Description | *Small, header only signals and slots C++11 library.* |
| -- | -- |
| Homepage | [https://github.com/fr00b0/nod](https://github.com/fr00b0/nod) |
| License | MIT |
| Versions | v0.5.4 |
| Architectures | arm64, x64, x86 |
| Definition | [nod/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/nod/xmake.lua) |

##### Install command

```console
xrepo install nod
```

##### Integration in the project (xmake.lua)

```lua
add_requires("nod")
```


### nodeeditor (windows)


| Description | *Qt Node Editor. Dataflow programming framework* |
| -- | -- |
| Homepage | [https://github.com/paceholder/nodeeditor](https://github.com/paceholder/nodeeditor) |
| License | BSD-3 |
| Versions | 2.1.3, 2.2.2 |
| Architectures | arm64, x64, x86 |
| Definition | [nodeeditor/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/nodeeditor/xmake.lua) |

##### Install command

```console
xrepo install nodeeditor
```

##### Integration in the project (xmake.lua)

```lua
add_requires("nodeeditor")
```


### nodesoup (windows)


| Description | *Force-directed graph layout with Fruchterman-Reingold* |
| -- | -- |
| Homepage | [https://github.com/olvb/nodesoup](https://github.com/olvb/nodesoup) |
| Versions | 2020.09.05 |
| Architectures | arm64, x64, x86 |
| Definition | [nodesoup/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/nodesoup/xmake.lua) |

##### Install command

```console
xrepo install nodesoup
```

##### Integration in the project (xmake.lua)

```lua
add_requires("nodesoup")
```


### nowide_standalone (windows)


| Description | *C++ implementation of the Python Numpy library* |
| -- | -- |
| Homepage | [https://github.com/boostorg/nowide/tree/standalone](https://github.com/boostorg/nowide/tree/standalone) |
| License | Boost Software License, Version 1.0 |
| Versions | 11.2.0 |
| Architectures | arm64, x64, x86 |
| Definition | [nowide_standalone/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/nowide_standalone/xmake.lua) |

##### Install command

```console
xrepo install nowide_standalone
```

##### Integration in the project (xmake.lua)

```lua
add_requires("nowide_standalone")
```


### ntkernel-error-category (windows)


| Description | *A C++ 11 std::error_category for the NT kernel's NTSTATUS error codes * |
| -- | -- |
| Homepage | [https://github.com/ned14/ntkernel-error-category](https://github.com/ned14/ntkernel-error-category) |
| License | Apache-2.0 |
| Versions | v1.0.0 |
| Architectures | arm64, x64, x86 |
| Definition | [ntkernel-error-category/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/ntkernel-error-category/xmake.lua) |

##### Install command

```console
xrepo install ntkernel-error-category
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ntkernel-error-category")
```


### numcpp (windows)


| Description | *C++ implementation of the Python Numpy library* |
| -- | -- |
| Homepage | [https://github.com/dpilger26/NumCpp](https://github.com/dpilger26/NumCpp) |
| License | MIT |
| Versions | 2.4.2 |
| Architectures | arm64, x64, x86 |
| Definition | [numcpp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/numcpp/xmake.lua) |

##### Install command

```console
xrepo install numcpp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("numcpp")
```


### nvtt (windows)


| Description | *The NVIDIA Texture Tools is a collection of image processing and texture manipulation tools.* |
| -- | -- |
| Homepage | [https://developer.nvidia.com/legacy-texture-tools](https://developer.nvidia.com/legacy-texture-tools) |
| License | MIT |
| Versions | 2.1.2 |
| Architectures | arm64, x64, x86 |
| Definition | [nvtt/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/n/nvtt/xmake.lua) |

##### Install command

```console
xrepo install nvtt
```

##### Integration in the project (xmake.lua)

```lua
add_requires("nvtt")
```



## o
### oatpp (windows)


| Description | *Modern Web Framework for C++. High performance, simple API, cross platform, zero dependency.* |
| -- | -- |
| Homepage | [https://oatpp.io/](https://oatpp.io/) |
| License | Apache-2.0 |
| Versions | 1.0.0, 1.2.5, 1.3.0 |
| Architectures | x64 |
| Definition | [oatpp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/oatpp/xmake.lua) |

##### Install command

```console
xrepo install oatpp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("oatpp")
```


### octomap (windows)


| Description | *An Efficient Probabilistic 3D Mapping Framework Based on Octrees* |
| -- | -- |
| Homepage | [https://octomap.github.io/](https://octomap.github.io/) |
| License | BSD-3-Clause |
| Versions | v1.9.7 |
| Architectures | arm64, x64, x86 |
| Definition | [octomap/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/octomap/xmake.lua) |

##### Install command

```console
xrepo install octomap
```

##### Integration in the project (xmake.lua)

```lua
add_requires("octomap")
```


### ode (windows)


| Description | *ODE is an open source, high performance library for simulating rigid body dynamics.* |
| -- | -- |
| Homepage | [http://ode.org/](http://ode.org/) |
| License | BSD-3-Clause |
| Versions | 0.16.2 |
| Architectures | arm64, x64, x86 |
| Definition | [ode/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/ode/xmake.lua) |

##### Install command

```console
xrepo install ode
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ode")
```


### ogre-next (windows)


| Description | *scene-oriented, flexible 3D engine written in C++* |
| -- | -- |
| Homepage | [https://www.ogre3d.org/](https://www.ogre3d.org/) |
| License | MIT |
| Versions | v2.2.5 |
| Architectures | arm64, x64, x86 |
| Definition | [ogre-next/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/ogre-next/xmake.lua) |

##### Install command

```console
xrepo install ogre-next
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ogre-next")
```


### olive.c (windows)


| Description | *Simple 2D Graphics Library for C* |
| -- | -- |
| Homepage | [https://tsoding.github.io/olive.c/](https://tsoding.github.io/olive.c/) |
| License | MIT |
| Versions | 2022.12.14 |
| Architectures | arm64, x64, x86 |
| Definition | [olive.c/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/olive.c/xmake.lua) |

##### Install command

```console
xrepo install olive.c
```

##### Integration in the project (xmake.lua)

```lua
add_requires("olive.c")
```


### onednn (windows)


| Description | *oneAPI Deep Neural Network Library* |
| -- | -- |
| Homepage | [https://oneapi-src.github.io/oneDNN/](https://oneapi-src.github.io/oneDNN/) |
| License | Apache-2.0 |
| Versions | v2.5.4 |
| Architectures | x64 |
| Definition | [onednn/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/onednn/xmake.lua) |

##### Install command

```console
xrepo install onednn
```

##### Integration in the project (xmake.lua)

```lua
add_requires("onednn")
```


### onedpl (windows)


| Description | *oneAPI DPC++ Library* |
| -- | -- |
| Homepage | [https://www.intel.com/content/www/us/en/developer/tools/oneapi/dpc-library.html](https://www.intel.com/content/www/us/en/developer/tools/oneapi/dpc-library.html) |
| Versions | 2021.6.1 |
| Architectures | arm64, x64, x86 |
| Definition | [onedpl/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/onedpl/xmake.lua) |

##### Install command

```console
xrepo install onedpl
```

##### Integration in the project (xmake.lua)

```lua
add_requires("onedpl")
```


### onetbb (windows)


| Description | *Threading Building Blocks (TBB) lets you easily write parallel C++ programs that take full advantage of multicore performance, that are portable, composable and have future-proof scalability.* |
| -- | -- |
| Homepage | [https://software.intel.com/en-us/tbb/](https://software.intel.com/en-us/tbb/) |
| Versions | 2020.3, 2021.2.0, 2021.3.0, 2021.4.0, 2021.5.0, 2021.7.0 |
| Architectures | arm64, x64, x86 |
| Definition | [onetbb/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/onetbb/xmake.lua) |

##### Install command

```console
xrepo install onetbb
```

##### Integration in the project (xmake.lua)

```lua
add_requires("onetbb")
```


### onnx (windows)


| Description | *Open standard for machine learning interoperability* |
| -- | -- |
| Homepage | [https://onnx.ai/](https://onnx.ai/) |
| License | Apache-2.0 |
| Versions | v1.11.0, v1.12.0 |
| Architectures | x64, x86 |
| Definition | [onnx/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/onnx/xmake.lua) |

##### Install command

```console
xrepo install onnx
```

##### Integration in the project (xmake.lua)

```lua
add_requires("onnx")
```


### onnxruntime (windows)


| Description | *ONNX Runtime: cross-platform, high performance ML inferencing and training accelerator* |
| -- | -- |
| Homepage | [https://www.onnxruntime.ai](https://www.onnxruntime.ai) |
| License | MIT |
| Versions | 1.11.1 |
| Architectures | arm64, x64, x86 |
| Definition | [onnxruntime/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/onnxruntime/xmake.lua) |

##### Install command

```console
xrepo install onnxruntime
```

##### Integration in the project (xmake.lua)

```lua
add_requires("onnxruntime")
```


### open3d (windows)


| Description | *Open3D: A Modern Library for 3D Data Processing* |
| -- | -- |
| Homepage | [http://www.open3d.org/](http://www.open3d.org/) |
| License | MIT |
| Versions | v0.15.1 |
| Architectures | x64 |
| Definition | [open3d/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/open3d/xmake.lua) |

##### Install command

```console
xrepo install open3d
```

##### Integration in the project (xmake.lua)

```lua
add_requires("open3d")
```


### openal-soft (windows)


| Description | *OpenAL Soft is a software implementation of the OpenAL 3D audio API.* |
| -- | -- |
| Homepage | [https://openal-soft.org](https://openal-soft.org) |
| License | LGPL-2.0 |
| Versions | 1.21.1, 1.22.0, 1.22.2 |
| Architectures | arm64, x64, x86 |
| Definition | [openal-soft/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/openal-soft/xmake.lua) |

##### Install command

```console
xrepo install openal-soft
```

##### Integration in the project (xmake.lua)

```lua
add_requires("openal-soft")
```


### openblas (windows)


| Description | *OpenBLAS is an optimized BLAS library based on GotoBLAS2 1.13 BSD version.* |
| -- | -- |
| Homepage | [http://www.openblas.net/](http://www.openblas.net/) |
| License | BSD-3-Clause |
| Versions | 0.3.12, 0.3.13, 0.3.15, 0.3.17, 0.3.18, 0.3.19, 0.3.20, 0.3.21 |
| Architectures | arm64, x64, x86 |
| Definition | [openblas/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/openblas/xmake.lua) |

##### Install command

```console
xrepo install openblas
```

##### Integration in the project (xmake.lua)

```lua
add_requires("openblas")
```


### opencc (windows)


| Description | *Conversion between Traditional and Simplified Chinese.* |
| -- | -- |
| Homepage | [https://github.com/BYVoid/OpenCC](https://github.com/BYVoid/OpenCC) |
| Versions | 1.1.2 |
| Architectures | arm64, x64, x86 |
| Definition | [opencc/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/opencc/xmake.lua) |

##### Install command

```console
xrepo install opencc
```

##### Integration in the project (xmake.lua)

```lua
add_requires("opencc")
```


### opencl-clhpp (windows)


| Description | *OpenCL API C++ bindings* |
| -- | -- |
| Homepage | [https://github.com/KhronosGroup/OpenCL-CLHPP/](https://github.com/KhronosGroup/OpenCL-CLHPP/) |
| License | Apache-2.0 |
| Versions | 1.2.8, 2.0.15 |
| Architectures | arm64, x64, x86 |
| Definition | [opencl-clhpp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/opencl-clhpp/xmake.lua) |

##### Install command

```console
xrepo install opencl-clhpp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("opencl-clhpp")
```


### opencl-headers (windows)


| Description | *Khronos OpenCL-Headers* |
| -- | -- |
| Homepage | [https://github.com/KhronosGroup/OpenCL-Headers/](https://github.com/KhronosGroup/OpenCL-Headers/) |
| License | Apache-2.0 |
| Versions | v2021.06.30 |
| Architectures | arm64, x64, x86 |
| Definition | [opencl-headers/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/opencl-headers/xmake.lua) |

##### Install command

```console
xrepo install opencl-headers
```

##### Integration in the project (xmake.lua)

```lua
add_requires("opencl-headers")
```


### opencolorio (windows)


| Description | *A complete color management solution geared towards motion picture production with an emphasis on visual effects and computer animation.* |
| -- | -- |
| Homepage | [https://opencolorio.org/](https://opencolorio.org/) |
| License | BSD-3-Clause |
| Versions | v2.1.0, v2.1.1 |
| Architectures | arm64, x64, x86 |
| Definition | [opencolorio/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/opencolorio/xmake.lua) |

##### Install command

```console
xrepo install opencolorio
```

##### Integration in the project (xmake.lua)

```lua
add_requires("opencolorio")
```


### opencv (windows)


| Description | *A open source computer vision library.* |
| -- | -- |
| Homepage | [https://opencv.org/](https://opencv.org/) |
| License | Apache-2.0 |
| Versions | 3.4.9, 4.2.0, 4.5.1, 4.5.2, 4.5.3, 4.5.4, 4.5.5, 4.6.0 |
| Architectures | arm64, x64, x86 |
| Definition | [opencv/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/opencv/xmake.lua) |

##### Install command

```console
xrepo install opencv
```

##### Integration in the project (xmake.lua)

```lua
add_requires("opencv")
```


### openexr (windows)


| Description | *OpenEXR provides the specification and reference implementation of the EXR file format, the professional-grade image storage format of the motion picture industry.* |
| -- | -- |
| Homepage | [https://www.openexr.com/](https://www.openexr.com/) |
| Versions | 2.5.3, 2.5.5, 2.5.7, 3.1.0, 3.1.1, 3.1.3, 3.1.4, 3.1.5 |
| Architectures | arm64, x64, x86 |
| Definition | [openexr/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/openexr/xmake.lua) |

##### Install command

```console
xrepo install openexr
```

##### Integration in the project (xmake.lua)

```lua
add_requires("openexr")
```


### openh264 (windows)


| Description | *OpenH264 is a codec library which supports H.264 encoding and decoding.* |
| -- | -- |
| Homepage | [http://www.openh264.org/](http://www.openh264.org/) |
| License | BSD-2-Clause |
| Versions | v2.1.1 |
| Architectures | arm64, x64, x86 |
| Definition | [openh264/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/openh264/xmake.lua) |

##### Install command

```console
xrepo install openh264
```

##### Integration in the project (xmake.lua)

```lua
add_requires("openh264")
```


### openimageio (windows)


| Description | *OpenImageIO is a library for reading and writing images, and a bunch of related classes, utilities, and applications.* |
| -- | -- |
| Homepage | [https://sites.google.com/site/openimageio/home](https://sites.google.com/site/openimageio/home) |
| License | BSD-3-Clause |
| Versions | 2.2.19+0, 2.3.10+1, 2.3.13+0 |
| Architectures | arm64, x64, x86 |
| Definition | [openimageio/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/openimageio/xmake.lua) |

##### Install command

```console
xrepo install openimageio
```

##### Integration in the project (xmake.lua)

```lua
add_requires("openimageio")
```


### openjpeg (windows)


| Description | *OpenJPEG is an open-source JPEG 2000 codec written in C language.* |
| -- | -- |
| Homepage | [http://www.openjpeg.org/](http://www.openjpeg.org/) |
| License | BSD-2-Clause |
| Versions | v2.3.1, v2.4.0, v2.5.0 |
| Architectures | arm64, x64, x86 |
| Definition | [openjpeg/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/openjpeg/xmake.lua) |

##### Install command

```console
xrepo install openjpeg
```

##### Integration in the project (xmake.lua)

```lua
add_requires("openjpeg")
```


### openmesh (windows)


| Description | *OpenMesh is a generic and efficient data structure for representing and manipulating polygonal meshes.* |
| -- | -- |
| Homepage | [https://www.graphics.rwth-aachen.de/software/openmesh/](https://www.graphics.rwth-aachen.de/software/openmesh/) |
| License | BSD-3-Clause |
| Versions | 8.1, 9.0 |
| Architectures | arm64, x64, x86 |
| Definition | [openmesh/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/openmesh/xmake.lua) |

##### Install command

```console
xrepo install openmesh
```

##### Integration in the project (xmake.lua)

```lua
add_requires("openmesh")
```


### openmp (windows)


| Description | *The OpenMP API specification for parallel programming* |
| -- | -- |
| Homepage | [https://openmp.org/](https://openmp.org/) |
| Versions |  |
| Architectures | arm64, x64, x86 |
| Definition | [openmp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/openmp/xmake.lua) |

##### Install command

```console
xrepo install openmp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("openmp")
```


### openrestry-luajit (windows)


| Description | *OpenResty's Branch of LuaJIT 2* |
| -- | -- |
| Homepage | [https://github.com/openresty/luajit2](https://github.com/openresty/luajit2) |
| Versions | v2.1-20220310 |
| Architectures | arm64, x64, x86 |
| Definition | [openrestry-luajit/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/openrestry-luajit/xmake.lua) |

##### Install command

```console
xrepo install openrestry-luajit
```

##### Integration in the project (xmake.lua)

```lua
add_requires("openrestry-luajit")
```


### openscenegraph (windows)


| Description | *The OpenSceneGraph is an open source high performance 3D graphics toolkit.* |
| -- | -- |
| Homepage | [https://www.openscenegraph.com/](https://www.openscenegraph.com/) |
| Versions | 3.6.5 |
| Architectures | arm64, x64, x86 |
| Definition | [openscenegraph/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/openscenegraph/xmake.lua) |

##### Install command

```console
xrepo install openscenegraph
```

##### Integration in the project (xmake.lua)

```lua
add_requires("openscenegraph")
```


### openssl (windows)


| Description | *A robust, commercial-grade, and full-featured toolkit for TLS and SSL.* |
| -- | -- |
| Homepage | [https://www.openssl.org/](https://www.openssl.org/) |
| Versions | 1.0.0, 1.0.2-u, 1.1.0-l, 1.1.1-h, 1.1.1-k, 1.1.1-l, 1.1.1-m, 1.1.1-n, 1.1.1-o, 1.1.1-p, 1.1.1-q, 1.1.1-r, 1.1.1-s |
| Architectures | arm64, x64, x86 |
| Definition | [openssl/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/openssl/xmake.lua) |

##### Install command

```console
xrepo install openssl
```

##### Integration in the project (xmake.lua)

```lua
add_requires("openssl")
```


### openssl3 (windows)


| Description | *A robust, commercial-grade, and full-featured toolkit for TLS and SSL.* |
| -- | -- |
| Homepage | [https://www.openssl.org/](https://www.openssl.org/) |
| Versions | 3.0.0, 3.0.1, 3.0.2, 3.0.3, 3.0.4, 3.0.5, 3.0.6, 3.0.7 |
| Architectures | arm64, x64, x86 |
| Definition | [openssl3/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/openssl3/xmake.lua) |

##### Install command

```console
xrepo install openssl3
```

##### Integration in the project (xmake.lua)

```lua
add_requires("openssl3")
```


### opensubdiv (windows)


| Description | *OpenSubdiv is a set of open source libraries that implement high performance subdivision surface (subdiv) evaluation on massively parallel CPU and GPU architectures.* |
| -- | -- |
| Homepage | [https://graphics.pixar.com/opensubdiv/docs/intro.html](https://graphics.pixar.com/opensubdiv/docs/intro.html) |
| License | Apache-2.0 |
| Versions | 3.4.4, 3.5.0 |
| Architectures | arm64, x64, x86 |
| Definition | [opensubdiv/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/opensubdiv/xmake.lua) |

##### Install command

```console
xrepo install opensubdiv
```

##### Integration in the project (xmake.lua)

```lua
add_requires("opensubdiv")
```


### openvdb (windows)


| Description | *OpenVDB - Sparse volume data structure and tools* |
| -- | -- |
| Homepage | [https://www.openvdb.org/](https://www.openvdb.org/) |
| Versions | v10.0.1, v7.1.0, v8.0.1, v8.1.0, v8.2.0, v9.0.0, v9.1.0 |
| Architectures | x64, x86 |
| Definition | [openvdb/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/openvdb/xmake.lua) |

##### Install command

```console
xrepo install openvdb
```

##### Integration in the project (xmake.lua)

```lua
add_requires("openvdb")
```


### optick (windows)


| Description | *C++ Profiler For Games (API)* |
| -- | -- |
| Homepage | [https://optick.dev](https://optick.dev) |
| Versions | 1.3.1 |
| Architectures | arm64, x64, x86 |
| Definition | [optick/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/optick/xmake.lua) |

##### Install command

```console
xrepo install optick
```

##### Integration in the project (xmake.lua)

```lua
add_requires("optick")
```


### ordered_map (windows)


| Description | *C++ hash map and hash set which preserve the order of insertion* |
| -- | -- |
| Homepage | [https://github.com/Tessil/ordered-map](https://github.com/Tessil/ordered-map) |
| License | MIT |
| Versions | v1.0.0 |
| Architectures | arm64, x64, x86 |
| Definition | [ordered_map/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/ordered_map/xmake.lua) |

##### Install command

```console
xrepo install ordered_map
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ordered_map")
```


### osqp (windows)


| Description | *The Operator Splitting QP Solver* |
| -- | -- |
| Homepage | [https://osqp.org/](https://osqp.org/) |
| License | Apache-2.0 |
| Versions | v0.6.2 |
| Architectures | arm64, x64, x86 |
| Definition | [osqp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/osqp/xmake.lua) |

##### Install command

```console
xrepo install osqp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("osqp")
```


### out_ptr (windows)


| Description | *Repository for a C++11 implementation of std::out_ptr (p1132), as a standalone library!* |
| -- | -- |
| Homepage | [https://github.com/soasis/out_ptr](https://github.com/soasis/out_ptr) |
| License | Apache-2.0 |
| Versions | 2022.10.07 |
| Architectures | arm64, x64, x86 |
| Definition | [out_ptr/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/out_ptr/xmake.lua) |

##### Install command

```console
xrepo install out_ptr
```

##### Integration in the project (xmake.lua)

```lua
add_requires("out_ptr")
```


### outcome (windows)


| Description | *Provides very lightweight outcome<T> and result<T> (non-Boost edition)* |
| -- | -- |
| Homepage | [https://github.com/ned14/outcome](https://github.com/ned14/outcome) |
| License | Apache-2.0 |
| Versions | v2.2.4 |
| Architectures | arm64, x64, x86 |
| Definition | [outcome/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/o/outcome/xmake.lua) |

##### Install command

```console
xrepo install outcome
```

##### Integration in the project (xmake.lua)

```lua
add_requires("outcome")
```



## p
### pagmo (windows)


| Description | *pagmo is a C++ scientific library for massively parallel optimization.* |
| -- | -- |
| Homepage | [https://esa.github.io/pagmo2/index.html](https://esa.github.io/pagmo2/index.html) |
| License | LGPL-3.0 |
| Versions | v2.18.0 |
| Architectures | arm64, x64, x86 |
| Definition | [pagmo/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/pagmo/xmake.lua) |

##### Install command

```console
xrepo install pagmo
```

##### Integration in the project (xmake.lua)

```lua
add_requires("pagmo")
```


### parallel-hashmap (windows)


| Description | *A family of header-only, very fast and memory-friendly hashmap and btree containers.* |
| -- | -- |
| Homepage | [https://greg7mdp.github.io/parallel-hashmap/](https://greg7mdp.github.io/parallel-hashmap/) |
| License | Apache-2.0 |
| Versions | 1.33, 1.34, 1.35 |
| Architectures | arm64, x64, x86 |
| Definition | [parallel-hashmap/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/parallel-hashmap/xmake.lua) |

##### Install command

```console
xrepo install parallel-hashmap
```

##### Integration in the project (xmake.lua)

```lua
add_requires("parallel-hashmap")
```


### partio (windows)


| Description | *Partio is an open source C++ library for reading, writing and manipulating a variety of standard particle formats (GEO, BGEO, PTC, PDB, PDA).* |
| -- | -- |
| Homepage | [http://partio.us/](http://partio.us/) |
| License | BSD-3-Clause |
| Versions | v1.14.0 |
| Architectures | arm64, x64, x86 |
| Definition | [partio/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/partio/xmake.lua) |

##### Install command

```console
xrepo install partio
```

##### Integration in the project (xmake.lua)

```lua
add_requires("partio")
```


### patch (windows)


| Description | *GNU patch, which applies diff files to original files.* |
| -- | -- |
| Homepage | [http://www.gnu.org/software/patch/patch.html](http://www.gnu.org/software/patch/patch.html) |
| Versions | 2.7.6 |
| Architectures | arm64, x64, x86 |
| Definition | [patch/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/patch/xmake.lua) |

##### Install command

```console
xrepo install patch
```

##### Integration in the project (xmake.lua)

```lua
add_requires("patch")
```


### pcg32 (windows)


| Description | *Tiny self-contained C++ version of the PCG32 pseudorandom number generator* |
| -- | -- |
| Homepage | [https://github.com/wjakob/pcg32](https://github.com/wjakob/pcg32) |
| License | Apache-2.0 |
| Versions | 2016.06.07 |
| Architectures | arm64, x64, x86 |
| Definition | [pcg32/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/pcg32/xmake.lua) |

##### Install command

```console
xrepo install pcg32
```

##### Integration in the project (xmake.lua)

```lua
add_requires("pcg32")
```


### pcl (windows)


| Description | *The Point Cloud Library (PCL) is a standalone, large scale, open project for 2D/3D image and point cloud processing.* |
| -- | -- |
| Homepage | [https://pointclouds.org/](https://pointclouds.org/) |
| License | BSD-3-Clause |
| Versions | 1.12.0, 1.12.1 |
| Architectures | arm64, x64, x86 |
| Definition | [pcl/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/pcl/xmake.lua) |

##### Install command

```console
xrepo install pcl
```

##### Integration in the project (xmake.lua)

```lua
add_requires("pcl")
```


### pcre (windows)


| Description | *A Perl Compatible Regular Expressions Library* |
| -- | -- |
| Homepage | [https://www.pcre.org/](https://www.pcre.org/) |
| Versions | 8.45 |
| Architectures | arm64, x64, x86 |
| Definition | [pcre/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/pcre/xmake.lua) |

##### Install command

```console
xrepo install pcre
```

##### Integration in the project (xmake.lua)

```lua
add_requires("pcre")
```


### pcre2 (windows)


| Description | *A Perl Compatible Regular Expressions Library* |
| -- | -- |
| Homepage | [https://www.pcre.org/](https://www.pcre.org/) |
| Versions | 10.39, 10.40 |
| Architectures | arm64, x64, x86 |
| Definition | [pcre2/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/pcre2/xmake.lua) |

##### Install command

```console
xrepo install pcre2
```

##### Integration in the project (xmake.lua)

```lua
add_requires("pcre2")
```


### pdcurses (windows)


| Description | *PDCurses - a curses library for environments that don't fit the termcap/terminfo model.* |
| -- | -- |
| Homepage | [https://pdcurses.org/](https://pdcurses.org/) |
| Versions | 3.9 |
| Architectures | arm64, x64, x86 |
| Definition | [pdcurses/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/pdcurses/xmake.lua) |

##### Install command

```console
xrepo install pdcurses
```

##### Integration in the project (xmake.lua)

```lua
add_requires("pdcurses")
```


### pdcursesmod (windows)


| Description | *PDCurses Modified - a curses library modified and extended from the 'official' pdcurses* |
| -- | -- |
| Homepage | [https://projectpluto.com/win32a.htm](https://projectpluto.com/win32a.htm) |
| Versions | v4.3.4 |
| Architectures | arm64, x64, x86 |
| Definition | [pdcursesmod/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/pdcursesmod/xmake.lua) |

##### Install command

```console
xrepo install pdcursesmod
```

##### Integration in the project (xmake.lua)

```lua
add_requires("pdcursesmod")
```


### pdfhummus (windows)


| Description | *High performance library for creating, modiyfing and parsing PDF files in C++ * |
| -- | -- |
| Homepage | [https://www.pdfhummus.com/](https://www.pdfhummus.com/) |
| License | Apache-2.0 |
| Versions | 4.1 |
| Architectures | arm64, x64, x86 |
| Definition | [pdfhummus/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/pdfhummus/xmake.lua) |

##### Install command

```console
xrepo install pdfhummus
```

##### Integration in the project (xmake.lua)

```lua
add_requires("pdfhummus")
```


### pegtl (windows)


| Description | *Parsing Expression Grammar Template Library* |
| -- | -- |
| Homepage | [https://github.com/taocpp/PEGTL](https://github.com/taocpp/PEGTL) |
| License | BSL-1.0 |
| Versions | 3.2.2, 3.2.5 |
| Architectures | arm64, x64, x86 |
| Definition | [pegtl/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/pegtl/xmake.lua) |

##### Install command

```console
xrepo install pegtl
```

##### Integration in the project (xmake.lua)

```lua
add_requires("pegtl")
```


### picojson (windows)


| Description | *A header-file-only, JSON parser serializer in C++* |
| -- | -- |
| Homepage | [https://pocoproject.org/](https://pocoproject.org/) |
| License | BSD-2-Clause |
| Versions | v1.3.0 |
| Architectures | arm64, x64, x86 |
| Definition | [picojson/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/picojson/xmake.lua) |

##### Install command

```console
xrepo install picojson
```

##### Integration in the project (xmake.lua)

```lua
add_requires("picojson")
```


### piex (windows)


| Description | *Preview Image Extractor (PIEX)* |
| -- | -- |
| Homepage | [https://github.com/google/piex](https://github.com/google/piex) |
| License | Apache-2.0 |
| Versions | 20190530 |
| Architectures | arm64, x64, x86 |
| Definition | [piex/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/piex/xmake.lua) |

##### Install command

```console
xrepo install piex
```

##### Integration in the project (xmake.lua)

```lua
add_requires("piex")
```


### pixman (windows)


| Description | *Low-level library for pixel manipulation.* |
| -- | -- |
| Homepage | [https://cairographics.org/](https://cairographics.org/) |
| Versions | 0.40.0 |
| Architectures | arm64, x64, x86 |
| Definition | [pixman/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/pixman/xmake.lua) |

##### Install command

```console
xrepo install pixman
```

##### Integration in the project (xmake.lua)

```lua
add_requires("pixman")
```


### pkg-config (windows)


| Description | *A helper tool used when compiling applications and libraries.* |
| -- | -- |
| Homepage | [https://freedesktop.org/wiki/Software/pkg-config/](https://freedesktop.org/wiki/Software/pkg-config/) |
| Versions | 0.29.2 |
| Architectures | arm64, x64, x86 |
| Definition | [pkg-config/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/pkg-config/xmake.lua) |

##### Install command

```console
xrepo install pkg-config
```

##### Integration in the project (xmake.lua)

```lua
add_requires("pkg-config")
```


### pkgconf (windows)


| Description | *A program which helps to configure compiler and linker flags for development frameworks.* |
| -- | -- |
| Homepage | [http://pkgconf.org](http://pkgconf.org) |
| Versions | 1.7.4, 1.8.0, 1.9.3 |
| Architectures | arm64, x64, x86 |
| Definition | [pkgconf/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/pkgconf/xmake.lua) |

##### Install command

```console
xrepo install pkgconf
```

##### Integration in the project (xmake.lua)

```lua
add_requires("pkgconf")
```


### pmp (windows)


| Description | *The Polygon Mesh Processing Library* |
| -- | -- |
| Homepage | [http://www.pmp-library.org/](http://www.pmp-library.org/) |
| License | MIT |
| Versions | 1.2.1 |
| Architectures | arm64, x64, x86 |
| Definition | [pmp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/pmp/xmake.lua) |

##### Install command

```console
xrepo install pmp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("pmp")
```


### poco (windows)


| Description | *The POCO C++ Libraries are powerful cross-platform C++ libraries for building network- and internet-based applications that run on desktop, server, mobile, IoT, and embedded systems.* |
| -- | -- |
| Homepage | [https://pocoproject.org/](https://pocoproject.org/) |
| License | BSL-1.0 |
| Versions | 1.11.0, 1.11.1, 1.12.1, 1.12.2 |
| Architectures | arm64, x64, x86 |
| Definition | [poco/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/poco/xmake.lua) |

##### Install command

```console
xrepo install poco
```

##### Integration in the project (xmake.lua)

```lua
add_requires("poco")
```


### polyscope (windows)


| Description | *A C++ & Python viewer for 3D data like meshes and point clouds* |
| -- | -- |
| Homepage | [https://polyscope.run/](https://polyscope.run/) |
| License | MIT |
| Versions | v1.3.0 |
| Architectures | arm64, x64, x86 |
| Definition | [polyscope/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/polyscope/xmake.lua) |

##### Install command

```console
xrepo install polyscope
```

##### Integration in the project (xmake.lua)

```lua
add_requires("polyscope")
```


### poppler (windows)


| Description | *Poppler, a PDF rendering library* |
| -- | -- |
| Homepage | [https://poppler.freedesktop.org/](https://poppler.freedesktop.org/) |
| License | GPL-2.0 |
| Versions | 21.03.0 |
| Architectures | arm64, x64, x86 |
| Definition | [poppler/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/poppler/xmake.lua) |

##### Install command

```console
xrepo install poppler
```

##### Integration in the project (xmake.lua)

```lua
add_requires("poppler")
```


### pprint (windows)


| Description | *Pretty Printer for Modern C++* |
| -- | -- |
| Homepage | [https://github.com/p-ranav/pprint](https://github.com/p-ranav/pprint) |
| Versions | 2020.2.20 |
| Architectures | arm64, x64, x86 |
| Definition | [pprint/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/pprint/xmake.lua) |

##### Install command

```console
xrepo install pprint
```

##### Integration in the project (xmake.lua)

```lua
add_requires("pprint")
```


### pqp (windows)


| Description | *A Proximity Query Package* |
| -- | -- |
| Homepage | [http://gamma.cs.unc.edu/SSV/](http://gamma.cs.unc.edu/SSV/) |
| Versions | 1.3 |
| Architectures | arm64, x64, x86 |
| Definition | [pqp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/pqp/xmake.lua) |

##### Install command

```console
xrepo install pqp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("pqp")
```


### premake5 (windows)


| Description | *Premake - Powerfully simple build configuration* |
| -- | -- |
| Homepage | [https://premake.github.io/](https://premake.github.io/) |
| Versions | 2022.11.17 |
| Architectures | arm64, x64, x86 |
| Definition | [premake5/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/premake5/xmake.lua) |

##### Install command

```console
xrepo install premake5
```

##### Integration in the project (xmake.lua)

```lua
add_requires("premake5")
```


### proj (windows)


| Description | *PROJ is a generic coordinate transformation software that transforms geospatial coordinates from one coordinate reference system (CRS) to another.* |
| -- | -- |
| Homepage | [https://proj.org/index.html](https://proj.org/index.html) |
| License | MIT |
| Versions | 8.2.1, 9.0.1 |
| Architectures | arm64, x64, x86 |
| Definition | [proj/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/proj/xmake.lua) |

##### Install command

```console
xrepo install proj
```

##### Integration in the project (xmake.lua)

```lua
add_requires("proj")
```


### promise-cpp (windows)


| Description | *C++ promise/A+ library in Javascript style.* |
| -- | -- |
| Homepage | [https://github.com/xhawk18/promise-cpp](https://github.com/xhawk18/promise-cpp) |
| License | MIT |
| Versions | 2.1.3 |
| Architectures | arm64, x64, x86 |
| Definition | [promise-cpp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/promise-cpp/xmake.lua) |

##### Install command

```console
xrepo install promise-cpp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("promise-cpp")
```


### protobuf-c (windows)


| Description | *Google's data interchange format for c* |
| -- | -- |
| Homepage | [https://github.com/protobuf-c/protobuf-c](https://github.com/protobuf-c/protobuf-c) |
| Versions | 1.3.1 |
| Architectures | arm64, x64, x86 |
| Definition | [protobuf-c/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/protobuf-c/xmake.lua) |

##### Install command

```console
xrepo install protobuf-c
```

##### Integration in the project (xmake.lua)

```lua
add_requires("protobuf-c")
```


### protobuf-cpp (windows)


| Description | *Google's data interchange format for cpp* |
| -- | -- |
| Homepage | [https://developers.google.com/protocol-buffers/](https://developers.google.com/protocol-buffers/) |
| Versions | 3.12.3, 3.13.0, 3.14.0, 3.15.5, 3.15.8, 3.17.3, 3.19.4, 3.8.0 |
| Architectures | arm64, x64, x86 |
| Definition | [protobuf-cpp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/protobuf-cpp/xmake.lua) |

##### Install command

```console
xrepo install protobuf-cpp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("protobuf-cpp")
```


### protoc (windows)


| Description | *Google's data interchange format compiler* |
| -- | -- |
| Homepage | [https://developers.google.com/protocol-buffers/](https://developers.google.com/protocol-buffers/) |
| Versions | 3.8.0 |
| Architectures | arm64, x64, x86 |
| Definition | [protoc/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/protoc/xmake.lua) |

##### Install command

```console
xrepo install protoc
```

##### Integration in the project (xmake.lua)

```lua
add_requires("protoc")
```


### prvhash (windows)


| Description | *PRVHASH - Pseudo-Random-Value Hash* |
| -- | -- |
| Homepage | [https://github.com/avaneev/prvhash](https://github.com/avaneev/prvhash) |
| License | MIT |
| Versions | 4.0 |
| Architectures | arm64, x64, x86 |
| Definition | [prvhash/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/prvhash/xmake.lua) |

##### Install command

```console
xrepo install prvhash
```

##### Integration in the project (xmake.lua)

```lua
add_requires("prvhash")
```


### ptex (windows)


| Description | *Per-Face Texture Mapping for Production Rendering* |
| -- | -- |
| Homepage | [http://ptex.us/](http://ptex.us/) |
| License | BSD-3-Clause |
| Versions | v2.3.2, v2.4.1, v2.4.2 |
| Architectures | arm64, x64, x86 |
| Definition | [ptex/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/ptex/xmake.lua) |

##### Install command

```console
xrepo install ptex
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ptex")
```


### pthreadpool (windows)


| Description | *Portable (POSIX/Windows/Emscripten) thread pool for C/C++* |
| -- | -- |
| Homepage | [https://github.com/Maratyszcza/pthreadpool](https://github.com/Maratyszcza/pthreadpool) |
| License | BSD-2-Clause |
| Versions | 2021.05.08 |
| Architectures | arm64, x64, x86 |
| Definition | [pthreadpool/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/pthreadpool/xmake.lua) |

##### Install command

```console
xrepo install pthreadpool
```

##### Integration in the project (xmake.lua)

```lua
add_requires("pthreadpool")
```


### pthreads4w (windows)


| Description | *POSIX Threads for Win32* |
| -- | -- |
| Homepage | [https://sourceforge.net/projects/pthreads4w/](https://sourceforge.net/projects/pthreads4w/) |
| Versions | 3.0.0 |
| Architectures | arm64, x64, x86 |
| Definition | [pthreads4w/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/pthreads4w/xmake.lua) |

##### Install command

```console
xrepo install pthreads4w
```

##### Integration in the project (xmake.lua)

```lua
add_requires("pthreads4w")
```


### ptl (windows)


| Description | *Lightweight C++11 multithreading tasking system featuring thread-pool, task-groups, and lock-free task queue* |
| -- | -- |
| Homepage | [https://github.com/jrmadsen/PTL](https://github.com/jrmadsen/PTL) |
| License | MIT |
| Versions | v2.0.0 |
| Architectures | arm64, x64, x86 |
| Definition | [ptl/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/ptl/xmake.lua) |

##### Install command

```console
xrepo install ptl
```

##### Integration in the project (xmake.lua)

```lua
add_requires("ptl")
```


### pugixml (windows)


| Description | *Light-weight, simple and fast XML parser for C++ with XPath support* |
| -- | -- |
| Homepage | [https://pugixml.org/](https://pugixml.org/) |
| License | MIT |
| Versions | 1.11.4 |
| Architectures | arm64, x64, x86 |
| Definition | [pugixml/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/pugixml/xmake.lua) |

##### Install command

```console
xrepo install pugixml
```

##### Integration in the project (xmake.lua)

```lua
add_requires("pugixml")
```


### pybind11 (windows)


| Description | *Seamless operability between C++11 and Python.* |
| -- | -- |
| Homepage | [https://github.com/pybind/pybind11](https://github.com/pybind/pybind11) |
| License | BSD-3-Clause |
| Versions | v2.10.0, v2.5.0, v2.6.2, v2.7.1, v2.8.1, v2.9.1, v2.9.2 |
| Architectures | arm64, x64, x86 |
| Definition | [pybind11/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/pybind11/xmake.lua) |

##### Install command

```console
xrepo install pybind11
```

##### Integration in the project (xmake.lua)

```lua
add_requires("pybind11")
```


### pycdc (windows)


| Description | *C++ python bytecode disassembler and decompiler* |
| -- | -- |
| Homepage | [https://github.com/zrax/pycdc](https://github.com/zrax/pycdc) |
| Versions | 2022.10.04 |
| Architectures | arm64, x64, x86 |
| Definition | [pycdc/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/pycdc/xmake.lua) |

##### Install command

```console
xrepo install pycdc
```

##### Integration in the project (xmake.lua)

```lua
add_requires("pycdc")
```


### pystring (windows)


| Description | *Pystring is a collection of C++ functions which match the interface and behavior of python's string class methods using std::string.* |
| -- | -- |
| Homepage | [https://github.com/imageworks/pystring](https://github.com/imageworks/pystring) |
| Versions | 2020.02.04 |
| Architectures | arm64, x64, x86 |
| Definition | [pystring/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/pystring/xmake.lua) |

##### Install command

```console
xrepo install pystring
```

##### Integration in the project (xmake.lua)

```lua
add_requires("pystring")
```


### python (windows)


| Description | *The python programming language.* |
| -- | -- |
| Homepage | [https://www.python.org/](https://www.python.org/) |
| Versions | 2.7.18, 3.10.6, 3.7.9, 3.8.10, 3.9.10, 3.9.13, 3.9.5, 3.9.6 |
| Architectures | arm64, x64, x86 |
| Definition | [python/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/python/xmake.lua) |

##### Install command

```console
xrepo install python
```

##### Integration in the project (xmake.lua)

```lua
add_requires("python")
```


### python2 (windows)


| Description | *The python programming language.* |
| -- | -- |
| Homepage | [https://www.python.org/](https://www.python.org/) |
| Versions | 2.7.15, 2.7.18 |
| Architectures | arm64, x64, x86 |
| Definition | [python2/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/p/python2/xmake.lua) |

##### Install command

```console
xrepo install python2
```

##### Integration in the project (xmake.lua)

```lua
add_requires("python2")
```



## q
### qdcae (windows)


| Description | *qd python (and C++) library for CAE (currently mostly LS-Dyna) * |
| -- | -- |
| Homepage | [https://github.com/qd-cae/qd-cae-python](https://github.com/qd-cae/qd-cae-python) |
| Versions | 0.8.9 |
| Architectures | arm64, x64, x86 |
| Definition | [qdcae/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/q/qdcae/xmake.lua) |

##### Install command

```console
xrepo install qdcae
```

##### Integration in the project (xmake.lua)

```lua
add_requires("qdcae")
```


### qhull (windows)


| Description | *Qhull computes the convex hull, Delaunay triangulation, Voronoi diagram, halfspace intersection about a point, furthest-site Delaunay triangulation, and furthest-site Voronoi diagram.* |
| -- | -- |
| Homepage | [http://www.qhull.org/](http://www.qhull.org/) |
| Versions | 2020.2 |
| Architectures | arm64, x64, x86 |
| Definition | [qhull/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/q/qhull/xmake.lua) |

##### Install command

```console
xrepo install qhull
```

##### Integration in the project (xmake.lua)

```lua
add_requires("qhull")
```


### qoi (windows)


| Description | *The Quite OK Image Format for fast, lossless image compression* |
| -- | -- |
| Homepage | [https://qoiformat.org/](https://qoiformat.org/) |
| License | MIT |
| Versions | 2021.12.22, 2022.11.17 |
| Architectures | arm64, x64, x86 |
| Definition | [qoi/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/q/qoi/xmake.lua) |

##### Install command

```console
xrepo install qoi
```

##### Integration in the project (xmake.lua)

```lua
add_requires("qoi")
```


### qt5base (windows)


| Description | *Qt is the faster, smarter way to create innovative devices, modern UIs & applications for multiple screens. Cross-platform software development at its best.* |
| -- | -- |
| Homepage | [https://www.qt.io](https://www.qt.io) |
| License | LGPL-3 |
| Versions | 5.12.5, 5.15.2 |
| Architectures | arm64, x64, x86 |
| Definition | [qt5base/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/q/qt5base/xmake.lua) |

##### Install command

```console
xrepo install qt5base
```

##### Integration in the project (xmake.lua)

```lua
add_requires("qt5base")
```


### qt5core (windows)


| Description | *Qt is the faster, smarter way to create innovative devices, modern UIs & applications for multiple screens. Cross-platform software development at its best.* |
| -- | -- |
| Homepage | [https://www.qt.io](https://www.qt.io) |
| License | LGPL-3 |
| Versions | 5.12.5, 5.15.2 |
| Architectures | arm64, x64, x86 |
| Definition | [qt5core/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/q/qt5core/xmake.lua) |

##### Install command

```console
xrepo install qt5core
```

##### Integration in the project (xmake.lua)

```lua
add_requires("qt5core")
```


### qt5gui (windows)


| Description | *Qt is the faster, smarter way to create innovative devices, modern UIs & applications for multiple screens. Cross-platform software development at its best.* |
| -- | -- |
| Homepage | [https://www.qt.io](https://www.qt.io) |
| License | LGPL-3 |
| Versions | 5.12.5, 5.15.2 |
| Architectures | arm64, x64, x86 |
| Definition | [qt5gui/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/q/qt5gui/xmake.lua) |

##### Install command

```console
xrepo install qt5gui
```

##### Integration in the project (xmake.lua)

```lua
add_requires("qt5gui")
```


### qt5lib (windows)


| Description | *Qt is the faster, smarter way to create innovative devices, modern UIs & applications for multiple screens. Cross-platform software development at its best.* |
| -- | -- |
| Homepage | [https://www.qt.io](https://www.qt.io) |
| License | LGPL-3 |
| Versions | 5.12.5, 5.15.2 |
| Architectures | arm64, x64, x86 |
| Definition | [qt5lib/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/q/qt5lib/xmake.lua) |

##### Install command

```console
xrepo install qt5lib
```

##### Integration in the project (xmake.lua)

```lua
add_requires("qt5lib")
```


### qt5network (windows)


| Description | *Qt is the faster, smarter way to create innovative devices, modern UIs & applications for multiple screens. Cross-platform software development at its best.* |
| -- | -- |
| Homepage | [https://www.qt.io](https://www.qt.io) |
| License | LGPL-3 |
| Versions | 5.12.5, 5.15.2 |
| Architectures | arm64, x64, x86 |
| Definition | [qt5network/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/q/qt5network/xmake.lua) |

##### Install command

```console
xrepo install qt5network
```

##### Integration in the project (xmake.lua)

```lua
add_requires("qt5network")
```


### qt5widgets (windows)


| Description | *Qt is the faster, smarter way to create innovative devices, modern UIs & applications for multiple screens. Cross-platform software development at its best.* |
| -- | -- |
| Homepage | [https://www.qt.io](https://www.qt.io) |
| License | LGPL-3 |
| Versions | 5.12.5, 5.15.2 |
| Architectures | arm64, x64, x86 |
| Definition | [qt5widgets/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/q/qt5widgets/xmake.lua) |

##### Install command

```console
xrepo install qt5widgets
```

##### Integration in the project (xmake.lua)

```lua
add_requires("qt5widgets")
```


### quickcpplib (windows)


| Description | *Eliminate all the tedious hassle when making state-of-the-art C++ 14 - 23 libraries!* |
| -- | -- |
| Homepage | [https://github.com/ned14/quickcpplib](https://github.com/ned14/quickcpplib) |
| License | Apache-2.0 |
| Versions | 20221116 |
| Architectures | arm64, x64, x86 |
| Definition | [quickcpplib/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/q/quickcpplib/xmake.lua) |

##### Install command

```console
xrepo install quickcpplib
```

##### Integration in the project (xmake.lua)

```lua
add_requires("quickcpplib")
```


### quickjs (windows)


| Description | *QuickJS is a small and embeddable Javascript engine* |
| -- | -- |
| Homepage | [https://bellard.org/quickjs/](https://bellard.org/quickjs/) |
| Versions | 2021.03.27 |
| Architectures | arm64, x64, x86 |
| Definition | [quickjs/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/q/quickjs/xmake.lua) |

##### Install command

```console
xrepo install quickjs
```

##### Integration in the project (xmake.lua)

```lua
add_requires("quickjs")
```



## r
### range-v3 (windows)


| Description | *Range library for C++14/17/20, basis for C++20's std::ranges* |
| -- | -- |
| Homepage | [https://github.com/ericniebler/range-v3/](https://github.com/ericniebler/range-v3/) |
| License | BSL-1.0 |
| Versions | 0.11.0, 0.12.0 |
| Architectures | arm64, x64, x86 |
| Definition | [range-v3/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/range-v3/xmake.lua) |

##### Install command

```console
xrepo install range-v3
```

##### Integration in the project (xmake.lua)

```lua
add_requires("range-v3")
```


### rapidcsv (windows)


| Description | *C++ header-only library for CSV parsing (by d99kris)* |
| -- | -- |
| Homepage | [https://github.com/d99kris/rapidcsv](https://github.com/d99kris/rapidcsv) |
| Versions | 8.50 |
| Architectures | arm64, x64, x86 |
| Definition | [rapidcsv/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/rapidcsv/xmake.lua) |

##### Install command

```console
xrepo install rapidcsv
```

##### Integration in the project (xmake.lua)

```lua
add_requires("rapidcsv")
```


### rapidjson (windows)


| Description | *RapidJSON is a JSON parser and generator for C++.* |
| -- | -- |
| Homepage | [https://github.com/Tencent/rapidjson](https://github.com/Tencent/rapidjson) |
| Versions | 2022.7.20, v1.1.0, v1.1.0-arrow |
| Architectures | arm64, x64, x86 |
| Definition | [rapidjson/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/rapidjson/xmake.lua) |

##### Install command

```console
xrepo install rapidjson
```

##### Integration in the project (xmake.lua)

```lua
add_requires("rapidjson")
```


### raw_pdb (windows)


| Description | *A C++11 library for reading Microsoft Program DataBase PDB files* |
| -- | -- |
| Homepage | [https://github.com/MolecularMatters/raw_pdb](https://github.com/MolecularMatters/raw_pdb) |
| Versions | 2022.10.17 |
| Architectures | arm64, x64, x86 |
| Definition | [raw_pdb/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/raw_pdb/xmake.lua) |

##### Install command

```console
xrepo install raw_pdb
```

##### Integration in the project (xmake.lua)

```lua
add_requires("raw_pdb")
```


### raygui (windows)


| Description | *A simple and easy-to-use immediate-mode gui library* |
| -- | -- |
| Homepage | [https://github.com/raysan5/raygui](https://github.com/raysan5/raygui) |
| License | zlib |
| Versions | 3.0, 3.2 |
| Architectures | arm64, x64, x86 |
| Definition | [raygui/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/raygui/xmake.lua) |

##### Install command

```console
xrepo install raygui
```

##### Integration in the project (xmake.lua)

```lua
add_requires("raygui")
```


### raylib (windows)


| Description | *A simple and easy-to-use library to enjoy videogames programming.* |
| -- | -- |
| Homepage | [http://www.raylib.com](http://www.raylib.com) |
| Versions | 2.5.0, 3.0.0, 3.5.0, 3.7.0, 4.0.0, 4.2.0 |
| Architectures | arm64, x64, x86 |
| Definition | [raylib/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/raylib/xmake.lua) |

##### Install command

```console
xrepo install raylib
```

##### Integration in the project (xmake.lua)

```lua
add_requires("raylib")
```


### re2 (windows)


| Description | *RE2 is a fast, safe, thread-friendly alternative to backtracking regular expression engines like those used in PCRE, Perl, and Python. It is a C++ library.* |
| -- | -- |
| Homepage | [https://github.com/google/re2](https://github.com/google/re2) |
| License | BSD-3-Clause |
| Versions | 2020.11.01, 2021.06.01, 2021.08.01, 2021.11.01, 2022.02.01 |
| Architectures | arm64, x64, x86 |
| Definition | [re2/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/re2/xmake.lua) |

##### Install command

```console
xrepo install re2
```

##### Integration in the project (xmake.lua)

```lua
add_requires("re2")
```


### readerwriterqueue (windows)


| Description | *A fast single-producer, single-consumer lock-free queue for C++* |
| -- | -- |
| Homepage | [https://github.com/cameron314/readerwriterqueue](https://github.com/cameron314/readerwriterqueue) |
| License | BSD-3-Clause |
| Versions | v1.0.6 |
| Architectures | arm64, x64, x86 |
| Definition | [readerwriterqueue/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/readerwriterqueue/xmake.lua) |

##### Install command

```console
xrepo install readerwriterqueue
```

##### Integration in the project (xmake.lua)

```lua
add_requires("readerwriterqueue")
```


### recastnavigation (windows)


| Description | *Navigation-mesh Toolset for Games* |
| -- | -- |
| Homepage | [https://github.com/recastnavigation/recastnavigation](https://github.com/recastnavigation/recastnavigation) |
| License | zlib |
| Versions | 1.5.1 |
| Architectures | arm64, x64, x86 |
| Definition | [recastnavigation/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/recastnavigation/xmake.lua) |

##### Install command

```console
xrepo install recastnavigation
```

##### Integration in the project (xmake.lua)

```lua
add_requires("recastnavigation")
```


### redis-plus-plus (windows)


| Description | *Redis client written in C++* |
| -- | -- |
| Homepage | [https://github.com/sewenew/redis-plus-plus](https://github.com/sewenew/redis-plus-plus) |
| Versions | 1.3.5, 1.3.6, 1.3.7 |
| Architectures | arm64, x64, x86 |
| Definition | [redis-plus-plus/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/redis-plus-plus/xmake.lua) |

##### Install command

```console
xrepo install redis-plus-plus
```

##### Integration in the project (xmake.lua)

```lua
add_requires("redis-plus-plus")
```


### rendergraph (windows)


| Description | *Vulkan render graph management library. .* |
| -- | -- |
| Homepage | [https://github.com/DragonJoker/RenderGraph/](https://github.com/DragonJoker/RenderGraph/) |
| License | MIT |
| Versions | v1.0.0, v1.1.0, v1.2.0 |
| Architectures | x64 |
| Definition | [rendergraph/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/rendergraph/xmake.lua) |

##### Install command

```console
xrepo install rendergraph
```

##### Integration in the project (xmake.lua)

```lua
add_requires("rendergraph")
```


### reproc (windows)


| Description | *a cross-platform C/C++ library that simplifies starting, stopping and communicating with external programs.* |
| -- | -- |
| Homepage | [https://github.com/DaanDeMeyer/reproc](https://github.com/DaanDeMeyer/reproc) |
| License | MIT |
| Versions | v14.2.4 |
| Architectures | arm64, x64, x86 |
| Definition | [reproc/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/reproc/xmake.lua) |

##### Install command

```console
xrepo install reproc
```

##### Integration in the project (xmake.lua)

```lua
add_requires("reproc")
```


### robin-hood-hashing (windows)


| Description | *Fast & memory efficient hashtable based on robin hood hashing for C++11/14/17/20* |
| -- | -- |
| Homepage | [https://github.com/martinus/robin-hood-hashing](https://github.com/martinus/robin-hood-hashing) |
| License | MIT |
| Versions | 3.11.3, 3.11.5 |
| Architectures | arm64, x64, x86 |
| Definition | [robin-hood-hashing/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/robin-hood-hashing/xmake.lua) |

##### Install command

```console
xrepo install robin-hood-hashing
```

##### Integration in the project (xmake.lua)

```lua
add_requires("robin-hood-hashing")
```


### robin-map (windows)


| Description | *A C++ implementation of a fast hash map and hash set using robin hood hashing* |
| -- | -- |
| Homepage | [https://github.com/Tessil/robin-map](https://github.com/Tessil/robin-map) |
| License | MIT |
| Versions | v0.6.3 |
| Architectures | arm64, x64, x86 |
| Definition | [robin-map/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/robin-map/xmake.lua) |

##### Install command

```console
xrepo install robin-map
```

##### Integration in the project (xmake.lua)

```lua
add_requires("robin-map")
```


### robotstxt (windows)


| Description | *The repository contains Google's robots.txt parser and matcher as a C++ librar.* |
| -- | -- |
| Homepage | [https://github.com/google/robotstxt](https://github.com/google/robotstxt) |
| License | Apache-2.0 |
| Versions | 2021.11.24 |
| Architectures | arm64, x64, x86 |
| Definition | [robotstxt/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/robotstxt/xmake.lua) |

##### Install command

```console
xrepo install robotstxt
```

##### Integration in the project (xmake.lua)

```lua
add_requires("robotstxt")
```


### rpclib (windows)


| Description | *rpclib is a modern C++ msgpack-RPC server and client library* |
| -- | -- |
| Homepage | [http://rpclib.net](http://rpclib.net) |
| Versions | v2.3.0 |
| Architectures | arm64, x64, x86 |
| Definition | [rpclib/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/rpclib/xmake.lua) |

##### Install command

```console
xrepo install rpclib
```

##### Integration in the project (xmake.lua)

```lua
add_requires("rpclib")
```


### rply (windows)


| Description | *RPly is a library that lets applications read and write PLY files.* |
| -- | -- |
| Homepage | [http://w3.impa.br/~diego/software/rply/](http://w3.impa.br/~diego/software/rply/) |
| License | MIT |
| Versions | 1.1.4 |
| Architectures | arm64, x64, x86 |
| Definition | [rply/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/rply/xmake.lua) |

##### Install command

```console
xrepo install rply
```

##### Integration in the project (xmake.lua)

```lua
add_requires("rply")
```


### rpmalloc (windows)


| Description | *Public domain cross platform lock free thread caching 16-byte aligned memory allocator implemented in C* |
| -- | -- |
| Homepage | [https://github.com/mjansson/rpmalloc](https://github.com/mjansson/rpmalloc) |
| Versions | 1.4.4 |
| Architectures | arm64, x64, x86 |
| Definition | [rpmalloc/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/rpmalloc/xmake.lua) |

##### Install command

```console
xrepo install rpmalloc
```

##### Integration in the project (xmake.lua)

```lua
add_requires("rpmalloc")
```


### rtm (windows)


| Description | *Realtime Math* |
| -- | -- |
| Homepage | [https://github.com/nfrechette/rtm](https://github.com/nfrechette/rtm) |
| License | MIT |
| Versions | v2.1.5 |
| Architectures | arm64, x64, x86 |
| Definition | [rtm/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/rtm/xmake.lua) |

##### Install command

```console
xrepo install rtm
```

##### Integration in the project (xmake.lua)

```lua
add_requires("rtm")
```


### rttr (windows)


| Description | *rttr: An open source library, which adds reflection to C++.* |
| -- | -- |
| Homepage | [https://www.rttr.org](https://www.rttr.org) |
| License | MIT |
| Versions | 0.9.5, 0.9.6 |
| Architectures | arm64, x64, x86 |
| Definition | [rttr/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/r/rttr/xmake.lua) |

##### Install command

```console
xrepo install rttr
```

##### Integration in the project (xmake.lua)

```lua
add_requires("rttr")
```



## s
### scnlib (windows)


| Description | *scnlib is a modern C++ library for replacing scanf and std::istream* |
| -- | -- |
| Homepage | [https://scnlib.readthedocs.io/](https://scnlib.readthedocs.io/) |
| Versions | 0.4, 1.1.2 |
| Architectures | arm64, x64, x86 |
| Definition | [scnlib/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/scnlib/xmake.lua) |

##### Install command

```console
xrepo install scnlib
```

##### Integration in the project (xmake.lua)

```lua
add_requires("scnlib")
```


### scons (windows)


| Description | *A software construction tool* |
| -- | -- |
| Homepage | [https://scons.org](https://scons.org) |
| Versions | 4.1.0, 4.3.0 |
| Architectures | arm64, x64, x86 |
| Definition | [scons/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/scons/xmake.lua) |

##### Install command

```console
xrepo install scons
```

##### Integration in the project (xmake.lua)

```lua
add_requires("scons")
```


### sentencepiece (windows)


| Description | *Unsupervised text tokenizer for Neural Network-based text generation. .* |
| -- | -- |
| Homepage | [https://github.com/google/sentencepiece](https://github.com/google/sentencepiece) |
| License | Apache-2.0 |
| Versions | v0.1.97 |
| Architectures | arm64, x64, x86 |
| Definition | [sentencepiece/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/sentencepiece/xmake.lua) |

##### Install command

```console
xrepo install sentencepiece
```

##### Integration in the project (xmake.lua)

```lua
add_requires("sentencepiece")
```


### sentry-native (windows)


| Description | *Sentry SDK for C, C++ and native applications.* |
| -- | -- |
| Homepage | [https://sentry.io](https://sentry.io) |
| Versions | 0.4.15, 0.4.4, 0.5.0 |
| Architectures | arm64, x64, x86 |
| Definition | [sentry-native/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/sentry-native/xmake.lua) |

##### Install command

```console
xrepo install sentry-native
```

##### Integration in the project (xmake.lua)

```lua
add_requires("sentry-native")
```


### sfml (windows)


| Description | *Simple and Fast Multimedia Library* |
| -- | -- |
| Homepage | [https://www.sfml-dev.org](https://www.sfml-dev.org) |
| Versions | 2.5.1 |
| Architectures | arm64, x64, x86 |
| Definition | [sfml/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/sfml/xmake.lua) |

##### Install command

```console
xrepo install sfml
```

##### Integration in the project (xmake.lua)

```lua
add_requires("sfml")
```


### sfntly (windows)


| Description | *The sfntly project contains Java and C++ libraries for reading, editing, and writing sfnt container fonts (OpenType, TrueType, AAT/GX, and Graphite.)* |
| -- | -- |
| Homepage | [https://github.com/googlefonts/sfntly](https://github.com/googlefonts/sfntly) |
| Versions | 20190917 |
| Architectures | arm64, x64, x86 |
| Definition | [sfntly/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/sfntly/xmake.lua) |

##### Install command

```console
xrepo install sfntly
```

##### Integration in the project (xmake.lua)

```lua
add_requires("sfntly")
```


### shaderc (windows)


| Description | *A collection of tools, libraries, and tests for Vulkan shader compilation.* |
| -- | -- |
| Homepage | [https://github.com/google/shaderc](https://github.com/google/shaderc) |
| License | Apache-2.0 |
| Versions | v2022.2 |
| Architectures | arm64, x64, x86 |
| Definition | [shaderc/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/shaderc/xmake.lua) |

##### Install command

```console
xrepo install shaderc
```

##### Integration in the project (xmake.lua)

```lua
add_requires("shaderc")
```


### shaderwriter (windows)


| Description | *Library used to write shaders from C++, and export them in either GLSL, HLSL or SPIR-V.* |
| -- | -- |
| Homepage | [https://github.com/DragonJoker/ShaderWriter](https://github.com/DragonJoker/ShaderWriter) |
| Versions | 0.1, 1.0, 1.1, 2.0, 2.1, 2.2, 2.3, 2.4, 2.5 |
| Architectures | arm64, x64, x86 |
| Definition | [shaderwriter/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/shaderwriter/xmake.lua) |

##### Install command

```console
xrepo install shaderwriter
```

##### Integration in the project (xmake.lua)

```lua
add_requires("shaderwriter")
```


### simage (windows)


| Description | *Simage is a library capable of loading, manipulating and saving images, creating and saving movies (AVI and MPEG), and loading audio.* |
| -- | -- |
| Homepage | [https://coin3d.github.io/simage/html/](https://coin3d.github.io/simage/html/) |
| License | MIT |
| Versions | 1.8.1, 1.8.3 |
| Architectures | arm64, x64, x86 |
| Definition | [simage/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/simage/xmake.lua) |

##### Install command

```console
xrepo install simage
```

##### Integration in the project (xmake.lua)

```lua
add_requires("simage")
```


### simbody (windows)


| Description | *High-performance C++ multibody dynamics/physics library for simulating articulated biomechanical and mechanical systems like vehicles, robots, and the human skeleton.* |
| -- | -- |
| Homepage | [https://simtk.org/home/simbody](https://simtk.org/home/simbody) |
| License | Apache-2.0 |
| Versions | 3.7 |
| Architectures | arm64, x64, x86 |
| Definition | [simbody/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/simbody/xmake.lua) |

##### Install command

```console
xrepo install simbody
```

##### Integration in the project (xmake.lua)

```lua
add_requires("simbody")
```


### simde (windows)


| Description | *Implementations of SIMD instruction sets for systems which don't natively support them.* |
| -- | -- |
| Homepage | [simd-everywhere.github.io/blog/](simd-everywhere.github.io/blog/) |
| Versions | 0.7.2 |
| Architectures | arm64, x64, x86 |
| Definition | [simde/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/simde/xmake.lua) |

##### Install command

```console
xrepo install simde
```

##### Integration in the project (xmake.lua)

```lua
add_requires("simde")
```


### simdjson (windows)


| Description | *Ridiculously fast JSON parsing, UTF-8 validation and JSON minifying for popular 64 bit systems.* |
| -- | -- |
| Homepage | [https://simdjson.org](https://simdjson.org) |
| License | Apache-2.0 |
| Versions | v0.9.5, v0.9.7, v1.0.0, v1.1.0, v3.0.0 |
| Architectures | x64 |
| Definition | [simdjson/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/simdjson/xmake.lua) |

##### Install command

```console
xrepo install simdjson
```

##### Integration in the project (xmake.lua)

```lua
add_requires("simdjson")
```


### simplethreadpool (windows)


| Description | *Simple thread pooling library in C++* |
| -- | -- |
| Homepage | [https://github.com/romch007/simplethreadpool](https://github.com/romch007/simplethreadpool) |
| License | MIT |
| Versions | 2022.11.18 |
| Architectures | arm64, x64, x86 |
| Definition | [simplethreadpool/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/simplethreadpool/xmake.lua) |

##### Install command

```console
xrepo install simplethreadpool
```

##### Integration in the project (xmake.lua)

```lua
add_requires("simplethreadpool")
```


### skia (windows)


| Description | *A complete 2D graphic library for drawing Text, Geometries, and Images.* |
| -- | -- |
| Homepage | [https://skia.org/](https://skia.org/) |
| License | BSD-3-Clause |
| Versions | 88, 89, 90 |
| Architectures | arm64, x64, x86 |
| Definition | [skia/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/skia/xmake.lua) |

##### Install command

```console
xrepo install skia
```

##### Integration in the project (xmake.lua)

```lua
add_requires("skia")
```


### snappy (windows)


| Description | *A fast compressor/decompressor* |
| -- | -- |
| Homepage | [https://github.com/google/snappy](https://github.com/google/snappy) |
| Versions | 1.1.8, 1.1.9 |
| Architectures | arm64, x64, x86 |
| Definition | [snappy/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/snappy/xmake.lua) |

##### Install command

```console
xrepo install snappy
```

##### Integration in the project (xmake.lua)

```lua
add_requires("snappy")
```


### snmalloc (windows)


| Description | *Message passing based allocator* |
| -- | -- |
| Homepage | [https://github.com/microsoft/snmalloc](https://github.com/microsoft/snmalloc) |
| License | MIT |
| Versions | 0.6.0 |
| Architectures | arm64, x64, x86 |
| Definition | [snmalloc/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/snmalloc/xmake.lua) |

##### Install command

```console
xrepo install snmalloc
```

##### Integration in the project (xmake.lua)

```lua
add_requires("snmalloc")
```


### sokol (windows)


| Description | *Simple STB-style cross-platform libraries for C and C++, written in C.* |
| -- | -- |
| Homepage | [https://github.com/floooh/sokol](https://github.com/floooh/sokol) |
| License | zlib |
| Versions | 2022.02.10, 2023.01.27 |
| Architectures | arm64, x64, x86 |
| Definition | [sokol/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/sokol/xmake.lua) |

##### Install command

```console
xrepo install sokol
```

##### Integration in the project (xmake.lua)

```lua
add_requires("sokol")
```


### sol2 (windows)


| Description | *A C++ library binding to Lua.* |
| -- | -- |
| Homepage | [https://github.com/ThePhD/sol2](https://github.com/ThePhD/sol2) |
| Versions | v3.2.1, v3.2.2, v3.2.3, v3.3.0 |
| Architectures | arm64, x64, x86 |
| Definition | [sol2/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/sol2/xmake.lua) |

##### Install command

```console
xrepo install sol2
```

##### Integration in the project (xmake.lua)

```lua
add_requires("sol2")
```


### sophus (windows)


| Description | *Sophus - Lie groups for 2d/3d Geometry* |
| -- | -- |
| Homepage | [https://strasdat.github.io/Sophus/](https://strasdat.github.io/Sophus/) |
| License | MIT |
| Versions | v22.10 |
| Architectures | arm64, x64, x86 |
| Definition | [sophus/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/sophus/xmake.lua) |

##### Install command

```console
xrepo install sophus
```

##### Integration in the project (xmake.lua)

```lua
add_requires("sophus")
```


### soplex (windows)


| Description | *SoPlex is an optimization package for solving linear programming problems (LPs) based on an advanced implementation of the primal and dual revised simplex algorithm.* |
| -- | -- |
| Homepage | [https://soplex.zib.de/](https://soplex.zib.de/) |
| Versions | 5.0.2 |
| Architectures | arm64, x64, x86 |
| Definition | [soplex/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/soplex/xmake.lua) |

##### Install command

```console
xrepo install soplex
```

##### Integration in the project (xmake.lua)

```lua
add_requires("soplex")
```


### sparsepp (windows)


| Description | *A fast, memory efficient hash map for C++* |
| -- | -- |
| Homepage | [https://github.com/greg7mdp/sparsepp](https://github.com/greg7mdp/sparsepp) |
| Versions | 1.22 |
| Architectures | arm64, x64, x86 |
| Definition | [sparsepp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/sparsepp/xmake.lua) |

##### Install command

```console
xrepo install sparsepp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("sparsepp")
```


### spdlog (windows)


| Description | *Fast C++ logging library.* |
| -- | -- |
| Homepage | [https://github.com/gabime/spdlog](https://github.com/gabime/spdlog) |
| Versions | v1.10.0, v1.11.0, v1.3.1, v1.4.2, v1.5.0, v1.8.0, v1.8.1, v1.8.2, v1.8.5, v1.9.0, v1.9.1, v1.9.2 |
| Architectures | arm64, x64, x86 |
| Definition | [spdlog/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/spdlog/xmake.lua) |

##### Install command

```console
xrepo install spdlog
```

##### Integration in the project (xmake.lua)

```lua
add_requires("spdlog")
```


### spirv-cross (windows)


| Description | *SPIRV-Cross is a practical tool and library for performing reflection on SPIR-V and disassembling SPIR-V back to high level languages.* |
| -- | -- |
| Homepage | [https://github.com/KhronosGroup/SPIRV-Cross/](https://github.com/KhronosGroup/SPIRV-Cross/) |
| License | Apache-2.0 |
| Versions | 1.2.154+1, 1.2.162+0, 1.2.189+1, 1.3.231+1 |
| Architectures | arm64, x64, x86 |
| Definition | [spirv-cross/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/spirv-cross/xmake.lua) |

##### Install command

```console
xrepo install spirv-cross
```

##### Integration in the project (xmake.lua)

```lua
add_requires("spirv-cross")
```


### spirv-headers (windows)


| Description | *SPIR-V Headers* |
| -- | -- |
| Homepage | [https://github.com/KhronosGroup/SPIRV-Headers/](https://github.com/KhronosGroup/SPIRV-Headers/) |
| License | MIT |
| Versions | 1.2.198+0, 1.3.211+0, 1.3.231+1 |
| Architectures | arm64, x64, x86 |
| Definition | [spirv-headers/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/spirv-headers/xmake.lua) |

##### Install command

```console
xrepo install spirv-headers
```

##### Integration in the project (xmake.lua)

```lua
add_requires("spirv-headers")
```


### spirv-reflect (windows)


| Description | *SPIRV-Reflect is a lightweight library that provides a C/C++ reflection API for SPIR-V shader bytecode in Vulkan applications.* |
| -- | -- |
| Homepage | [https://github.com/KhronosGroup/SPIRV-Reflect](https://github.com/KhronosGroup/SPIRV-Reflect) |
| License | Apache-2.0 |
| Versions | 1.2.154+1, 1.2.162+0, 1.2.189+1, 1.3.231+1 |
| Architectures | arm64, x64, x86 |
| Definition | [spirv-reflect/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/spirv-reflect/xmake.lua) |

##### Install command

```console
xrepo install spirv-reflect
```

##### Integration in the project (xmake.lua)

```lua
add_requires("spirv-reflect")
```


### spirv-tools (windows)


| Description | *SPIR-V Tools* |
| -- | -- |
| Homepage | [https://github.com/KhronosGroup/SPIRV-Tools/](https://github.com/KhronosGroup/SPIRV-Tools/) |
| License | Apache-2.0 |
| Versions | 2020.5, 2020.6, 2021.3, 2021.4, 2022.2, 2022.4 |
| Architectures | arm64, x64, x86 |
| Definition | [spirv-tools/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/spirv-tools/xmake.lua) |

##### Install command

```console
xrepo install spirv-tools
```

##### Integration in the project (xmake.lua)

```lua
add_requires("spirv-tools")
```


### sqlite3 (windows)


| Description | *The most used database engine in the world* |
| -- | -- |
| Homepage | [https://sqlite.org/](https://sqlite.org/) |
| Versions | 3.23.0+0, 3.24.0+0, 3.34.0+100, 3.35.0+300, 3.35.0+400, 3.36.0+0, 3.37.0+200, 3.39.0+200 |
| Architectures | arm64, x64, x86 |
| Definition | [sqlite3/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/sqlite3/xmake.lua) |

##### Install command

```console
xrepo install sqlite3
```

##### Integration in the project (xmake.lua)

```lua
add_requires("sqlite3")
```


### sqlpp11 (windows)


| Description | *A type safe SQL template library for C++* |
| -- | -- |
| Homepage | [https://github.com/rbock/sqlpp11](https://github.com/rbock/sqlpp11) |
| Versions | 0.61 |
| Architectures | arm64, x64, x86 |
| Definition | [sqlpp11/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/sqlpp11/xmake.lua) |

##### Install command

```console
xrepo install sqlpp11
```

##### Integration in the project (xmake.lua)

```lua
add_requires("sqlpp11")
```


### stackwalker (windows)


| Description | *A library to walk the callstack in windows applications.* |
| -- | -- |
| Homepage | [https://github.com/JochenKalmbach/StackWalker](https://github.com/JochenKalmbach/StackWalker) |
| Versions | 1.20 |
| Architectures | arm64, x64, x86 |
| Definition | [stackwalker/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/stackwalker/xmake.lua) |

##### Install command

```console
xrepo install stackwalker
```

##### Integration in the project (xmake.lua)

```lua
add_requires("stackwalker")
```


### stb (windows)


| Description | *single-file public domain (or MIT licensed) libraries for C/C++* |
| -- | -- |
| Homepage | [https://github.com/nothings/stb](https://github.com/nothings/stb) |
| Versions | 2021.07.13, 2021.09.10, 2023.01.30 |
| Architectures | arm64, x64, x86 |
| Definition | [stb/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/stb/xmake.lua) |

##### Install command

```console
xrepo install stb
```

##### Integration in the project (xmake.lua)

```lua
add_requires("stb")
```


### stlab (windows)


| Description | *Adobe Source Libraries from Software Technology Lab* |
| -- | -- |
| Homepage | [https://stlab.cc/](https://stlab.cc/) |
| License | BSL-1.0 |
| Versions | v1.6.2 |
| Architectures | arm64, x64, x86 |
| Definition | [stlab/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/stlab/xmake.lua) |

##### Install command

```console
xrepo install stlab
```

##### Integration in the project (xmake.lua)

```lua
add_requires("stlab")
```


### string-view-lite (windows)


| Description | *string_view lite - A C++17-like string_view for C++98, C++11 and later in a single-file header-only library* |
| -- | -- |
| Homepage | [https://github.com/martinmoene/string-view-lite](https://github.com/martinmoene/string-view-lite) |
| License | BSL-1.0 |
| Versions | v1.7.0 |
| Architectures | arm64, x64, x86 |
| Definition | [string-view-lite/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/string-view-lite/xmake.lua) |

##### Install command

```console
xrepo install string-view-lite
```

##### Integration in the project (xmake.lua)

```lua
add_requires("string-view-lite")
```


### strtk (windows)


| Description | *C++ String Toolkit Library* |
| -- | -- |
| Homepage | [https://www.partow.net/programming/strtk/index.html](https://www.partow.net/programming/strtk/index.html) |
| License | MIT |
| Versions | 2020.01.01 |
| Architectures | arm64, x64, x86 |
| Definition | [strtk/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/strtk/xmake.lua) |

##### Install command

```console
xrepo install strtk
```

##### Integration in the project (xmake.lua)

```lua
add_requires("strtk")
```


### subprocess.h (windows)


| Description | *single header process launching solution for C and C++ * |
| -- | -- |
| Homepage | [https://github.com/sheredom/subprocess.h](https://github.com/sheredom/subprocess.h) |
| Versions | 2022.12.20 |
| Architectures | arm64, x64, x86 |
| Definition | [subprocess.h/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/subprocess.h/xmake.lua) |

##### Install command

```console
xrepo install subprocess.h
```

##### Integration in the project (xmake.lua)

```lua
add_requires("subprocess.h")
```


### suitesparse (windows)


| Description | *SuiteSparse is a suite of sparse matrix algorithms* |
| -- | -- |
| Homepage | [https://people.engr.tamu.edu/davis/suitesparse.html](https://people.engr.tamu.edu/davis/suitesparse.html) |
| Versions | v5.10.1, v5.12.0, v5.13.0 |
| Architectures | arm64, x64, x86 |
| Definition | [suitesparse/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/suitesparse/xmake.lua) |

##### Install command

```console
xrepo install suitesparse
```

##### Integration in the project (xmake.lua)

```lua
add_requires("suitesparse")
```


### superlu (windows)


| Description | *SuperLU is a general purpose library for the direct solution of large, sparse, nonsymmetric systems of linear equations.* |
| -- | -- |
| Homepage | [https://portal.nersc.gov/project/sparse/superlu/](https://portal.nersc.gov/project/sparse/superlu/) |
| License | BSD-3-Clause |
| Versions | v5.2.2, v5.3.0 |
| Architectures | arm64, x64, x86 |
| Definition | [superlu/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/superlu/xmake.lua) |

##### Install command

```console
xrepo install superlu
```

##### Integration in the project (xmake.lua)

```lua
add_requires("superlu")
```


### swig (windows)


| Description | *SWIG is a software development tool that connects programs written in C and C++ with a variety of high-level programming languages.* |
| -- | -- |
| Homepage | [http://swig.org/](http://swig.org/) |
| License | GPL-3.0 |
| Versions | 4.0.2 |
| Architectures | arm64, x64, x86 |
| Definition | [swig/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/swig/xmake.lua) |

##### Install command

```console
xrepo install swig
```

##### Integration in the project (xmake.lua)

```lua
add_requires("swig")
```


### szip (windows)


| Description | *Szip is an implementation of the extended-Rice lossless compression algorithm.* |
| -- | -- |
| Homepage | [https://support.hdfgroup.org/doc_resource/SZIP/](https://support.hdfgroup.org/doc_resource/SZIP/) |
| Versions | 2.1.1 |
| Architectures | x64 |
| Definition | [szip/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/s/szip/xmake.lua) |

##### Install command

```console
xrepo install szip
```

##### Integration in the project (xmake.lua)

```lua
add_requires("szip")
```



## t
### tabulate (windows)


| Description | *Header-only library for printing aligned, formatted and colorized tables in Modern C++* |
| -- | -- |
| Homepage | [https://github.com/p-ranav/tabulate](https://github.com/p-ranav/tabulate) |
| License | MIT |
| Versions | 1.4 |
| Architectures | arm64, x64, x86 |
| Definition | [tabulate/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tabulate/xmake.lua) |

##### Install command

```console
xrepo install tabulate
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tabulate")
```


### taskflow (windows)


| Description | *A fast C++ header-only library to help you quickly write parallel programs with complex task dependencies* |
| -- | -- |
| Homepage | [https://taskflow.github.io/](https://taskflow.github.io/) |
| License | MIT |
| Versions | v3.0.0, v3.1.0, v3.2.0, v3.3.0, v3.4.0 |
| Architectures | arm64, x64, x86 |
| Definition | [taskflow/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/taskflow/xmake.lua) |

##### Install command

```console
xrepo install taskflow
```

##### Integration in the project (xmake.lua)

```lua
add_requires("taskflow")
```


### taywee_args (windows)


| Description | *A simple header-only C++ argument parser library.* |
| -- | -- |
| Homepage | [https://taywee.github.io/args/](https://taywee.github.io/args/) |
| License | MIT |
| Versions | 6.3.0, 6.4.6 |
| Architectures | arm64, x64, x86 |
| Definition | [taywee_args/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/taywee_args/xmake.lua) |

##### Install command

```console
xrepo install taywee_args
```

##### Integration in the project (xmake.lua)

```lua
add_requires("taywee_args")
```


### tbb (windows)


| Description | *Threading Building Blocks (TBB) lets you easily write parallel C++ programs that take full advantage of multicore performance, that are portable, composable and have future-proof scalability.* |
| -- | -- |
| Homepage | [https://software.intel.com/en-us/tbb/](https://software.intel.com/en-us/tbb/) |
| Versions | 2020.3, 2021.2.0, 2021.3.0, 2021.4.0, 2021.5.0, 2021.7.0 |
| Architectures | arm64, x64, x86 |
| Definition | [tbb/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tbb/xmake.lua) |

##### Install command

```console
xrepo install tbb
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tbb")
```


### tbox (windows)


| Description | *A glib-like multi-platform c library* |
| -- | -- |
| Homepage | [https://tboox.org](https://tboox.org) |
| Versions | v1.6.2, v1.6.3, v1.6.4, v1.6.5, v1.6.6, v1.6.7, v1.6.9, v1.7.1 |
| Architectures | arm64, x64, x86 |
| Definition | [tbox/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tbox/xmake.lua) |

##### Install command

```console
xrepo install tbox
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tbox")
```


### tclap (windows)


| Description | *This is a simple templatized C++ library for parsing command line arguments.* |
| -- | -- |
| Homepage | [https://sourceforge.net/projects/tclap/](https://sourceforge.net/projects/tclap/) |
| License | MIT |
| Versions | 1.4.0-rc1 |
| Architectures | arm64, x64, x86 |
| Definition | [tclap/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tclap/xmake.lua) |

##### Install command

```console
xrepo install tclap
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tclap")
```


### termcolor (windows)


| Description | *Termcolor is a header-only C++ library for printing colored messages to the terminal. Written just for fun with a help of the Force.* |
| -- | -- |
| Homepage | [https://github.com/ikalnytskyi/termcolor](https://github.com/ikalnytskyi/termcolor) |
| License | BSD-3-Clause |
| Versions | 2.1.0 |
| Architectures | arm64, x64, x86 |
| Definition | [termcolor/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/termcolor/xmake.lua) |

##### Install command

```console
xrepo install termcolor
```

##### Integration in the project (xmake.lua)

```lua
add_requires("termcolor")
```


### tesseract (windows)


| Description | *Tesseract Open Source OCR Engine* |
| -- | -- |
| Homepage | [https://tesseract-ocr.github.io/](https://tesseract-ocr.github.io/) |
| License | Apache-2.0 |
| Versions | 4.1.1, 4.1.3, 5.0.1, 5.2.0 |
| Architectures | x64, x86 |
| Definition | [tesseract/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tesseract/xmake.lua) |

##### Install command

```console
xrepo install tesseract
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tesseract")
```


### thread-pool (windows)


| Description | *BS::thread_pool: a fast, lightweight, and easy-to-use C++17 thread pool library* |
| -- | -- |
| Homepage | [https://github.com/bshoshany/thread-pool](https://github.com/bshoshany/thread-pool) |
| License | MIT |
| Versions | v3.3.0 |
| Architectures | arm64, x64, x86 |
| Definition | [thread-pool/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/thread-pool/xmake.lua) |

##### Install command

```console
xrepo install thread-pool
```

##### Integration in the project (xmake.lua)

```lua
add_requires("thread-pool")
```


### thrust (windows)


| Description | *The C++ parallel algorithms library.* |
| -- | -- |
| Homepage | [https://github.com/NVIDIA/thrust](https://github.com/NVIDIA/thrust) |
| License | Apache-2.0 |
| Versions | 1.17.0 |
| Architectures | arm64, x64, x86 |
| Definition | [thrust/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/thrust/xmake.lua) |

##### Install command

```console
xrepo install thrust
```

##### Integration in the project (xmake.lua)

```lua
add_requires("thrust")
```


### tiltedcore (windows)


| Description | *Core library from Tilted Phoques* |
| -- | -- |
| Homepage | [https://github.com/tiltedphoques/TiltedCore](https://github.com/tiltedphoques/TiltedCore) |
| Versions | v0.1.3, v0.1.4, v0.1.5, v0.1.6, v0.2.0, v0.2.1, v0.2.2, v0.2.3, v0.2.4, v0.2.5, v0.2.6, v0.2.7 |
| Architectures | arm64, x64, x86 |
| Definition | [tiltedcore/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tiltedcore/xmake.lua) |

##### Install command

```console
xrepo install tiltedcore
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tiltedcore")
```


### tiny-process-library (windows)


| Description | *A small platform independent library making it simple to create and stop new processes in C++, as well as writing to stdin and reading from stdout and stderr of a new process* |
| -- | -- |
| Homepage | [https://gitlab.com/eidheim/tiny-process-library](https://gitlab.com/eidheim/tiny-process-library) |
| License | MIT |
| Versions | v2.0.4 |
| Architectures | arm64, x64, x86 |
| Definition | [tiny-process-library/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tiny-process-library/xmake.lua) |

##### Install command

```console
xrepo install tiny-process-library
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tiny-process-library")
```


### tinycbor (windows)


| Description | *Concise Binary Object Representation (CBOR) Library* |
| -- | -- |
| Homepage | [https://github.com/intel/tinycbor](https://github.com/intel/tinycbor) |
| License | MIT |
| Versions | v0.6.0 |
| Architectures | arm64, x64, x86 |
| Definition | [tinycbor/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tinycbor/xmake.lua) |

##### Install command

```console
xrepo install tinycbor
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tinycbor")
```


### tinycc (windows)


| Description | *Tiny C Compiler* |
| -- | -- |
| Homepage | [https://bellard.org/tcc/](https://bellard.org/tcc/) |
| Versions | 0.9.27 |
| Architectures | arm64, x64, x86 |
| Definition | [tinycc/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tinycc/xmake.lua) |

##### Install command

```console
xrepo install tinycc
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tinycc")
```


### tinycrypt (windows)


| Description | *TinyCrypt Cryptographic Library* |
| -- | -- |
| Homepage | [https://github.com/intel/tinycrypt](https://github.com/intel/tinycrypt) |
| Versions | 2019.9.18 |
| Architectures | arm64, x64, x86 |
| Definition | [tinycrypt/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tinycrypt/xmake.lua) |

##### Install command

```console
xrepo install tinycrypt
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tinycrypt")
```


### tinyexr (windows)


| Description | *Tiny OpenEXR image loader/saver library* |
| -- | -- |
| Homepage | [https://github.com/syoyo/tinyexr/](https://github.com/syoyo/tinyexr/) |
| License | BSD-3-Clause |
| Versions | v1.0.1 |
| Architectures | arm64, x64, x86 |
| Definition | [tinyexr/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tinyexr/xmake.lua) |

##### Install command

```console
xrepo install tinyexr
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tinyexr")
```


### tinyfiledialogs (windows)


| Description | *Native dialog library for WINDOWS MAC OSX GTK+ QT CONSOLE* |
| -- | -- |
| Homepage | [https://sourceforge.net/projects/tinyfiledialogs/](https://sourceforge.net/projects/tinyfiledialogs/) |
| License | zlib |
| Versions | 3.8.8 |
| Architectures | arm64, x64, x86 |
| Definition | [tinyfiledialogs/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tinyfiledialogs/xmake.lua) |

##### Install command

```console
xrepo install tinyfiledialogs
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tinyfiledialogs")
```


### tinyformat (windows)


| Description | *Minimal, type safe printf replacement library for C++* |
| -- | -- |
| Homepage | [https://github.com/c42f/tinyformat/](https://github.com/c42f/tinyformat/) |
| Versions | 2.3.0 |
| Architectures | arm64, x64, x86 |
| Definition | [tinyformat/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tinyformat/xmake.lua) |

##### Install command

```console
xrepo install tinyformat
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tinyformat")
```


### tinygltf (windows)


| Description | *Header only C++11 tiny glTF 2.0 library* |
| -- | -- |
| Homepage | [https://github.com/syoyo/tinygltf/](https://github.com/syoyo/tinygltf/) |
| License | MIT |
| Versions | v2.5.0, v2.6.3 |
| Architectures | arm64, x64, x86 |
| Definition | [tinygltf/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tinygltf/xmake.lua) |

##### Install command

```console
xrepo install tinygltf
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tinygltf")
```


### tinyobjloader (windows)


| Description | *Tiny but powerful single file wavefront obj loader* |
| -- | -- |
| Homepage | [https://github.com/tinyobjloader/tinyobjloader](https://github.com/tinyobjloader/tinyobjloader) |
| License | MIT |
| Versions | v1.0.7, v2.0.0rc10 |
| Architectures | arm64, x64, x86 |
| Definition | [tinyobjloader/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tinyobjloader/xmake.lua) |

##### Install command

```console
xrepo install tinyobjloader
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tinyobjloader")
```


### tinyxml (windows)


| Description | *TinyXML is a simple, small, minimal, C++ XML parser that can be easily integrating into other programs.* |
| -- | -- |
| Homepage | [https://sourceforge.net/projects/tinyxml/](https://sourceforge.net/projects/tinyxml/) |
| License | zlib |
| Versions | 2.6.2 |
| Architectures | arm64, x64, x86 |
| Definition | [tinyxml/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tinyxml/xmake.lua) |

##### Install command

```console
xrepo install tinyxml
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tinyxml")
```


### tinyxml2 (windows)


| Description | *simple, small, efficient, C++ XML parser that can be easily integrating into other programs.* |
| -- | -- |
| Homepage | [http://www.grinninglizard.com/tinyxml2/](http://www.grinninglizard.com/tinyxml2/) |
| Versions | 8.0.0, 9.0.0 |
| Architectures | arm64, x64, x86 |
| Definition | [tinyxml2/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tinyxml2/xmake.lua) |

##### Install command

```console
xrepo install tinyxml2
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tinyxml2")
```


### tl_expected (windows)


| Description | *C++11/14/17 std::expected with functional-style extensions* |
| -- | -- |
| Homepage | [https://github.com/TartanLlama/expected](https://github.com/TartanLlama/expected) |
| License | CC0 |
| Versions | v1.0.0 |
| Architectures | arm64, x64, x86 |
| Definition | [tl_expected/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tl_expected/xmake.lua) |

##### Install command

```console
xrepo install tl_expected
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tl_expected")
```


### tl_function_ref (windows)


| Description | *A lightweight, non-owning reference to a callable.* |
| -- | -- |
| Homepage | [https://github.com/TartanLlama/function_ref](https://github.com/TartanLlama/function_ref) |
| License | CC0 |
| Versions | v1.0.0 |
| Architectures | arm64, x64, x86 |
| Definition | [tl_function_ref/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tl_function_ref/xmake.lua) |

##### Install command

```console
xrepo install tl_function_ref
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tl_function_ref")
```


### tmxparser (windows)


| Description | *C++11 library for parsing the maps generated by Tiled Map Editor* |
| -- | -- |
| Homepage | [https://github.com/sainteos/tmxparser](https://github.com/sainteos/tmxparser) |
| Versions | 2.2.0 |
| Architectures | arm64, x64, x86 |
| Definition | [tmxparser/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tmxparser/xmake.lua) |

##### Install command

```console
xrepo install tmxparser
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tmxparser")
```


### toml++ (windows)


| Description | *toml++ is a header-only TOML config file parser and serializer for C++17 (and later!).* |
| -- | -- |
| Homepage | [https://marzer.github.io/tomlplusplus/](https://marzer.github.io/tomlplusplus/) |
| Versions | v2.5.0, v3.0.0, v3.1.0, v3.2.0 |
| Architectures | arm64, x64, x86 |
| Definition | [toml++/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/toml++/xmake.lua) |

##### Install command

```console
xrepo install toml++
```

##### Integration in the project (xmake.lua)

```lua
add_requires("toml++")
```


### toml11 (windows)


| Description | *TOML for Modern C++* |
| -- | -- |
| Homepage | [https://github.com/ToruNiina/toml11](https://github.com/ToruNiina/toml11) |
| License | MIT |
| Versions | v3.7.0 |
| Architectures | arm64, x64, x86 |
| Definition | [toml11/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/toml11/xmake.lua) |

##### Install command

```console
xrepo install toml11
```

##### Integration in the project (xmake.lua)

```lua
add_requires("toml11")
```


### tracy (windows)


| Description | *C++ frame profiler* |
| -- | -- |
| Homepage | [https://github.com/wolfpld/tracy](https://github.com/wolfpld/tracy) |
| Versions | v0.8.2, v0.9 |
| Architectures | x64 |
| Definition | [tracy/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/tracy/xmake.lua) |

##### Install command

```console
xrepo install tracy
```

##### Integration in the project (xmake.lua)

```lua
add_requires("tracy")
```


### trantor (windows)


| Description | *a non-blocking I/O tcp network lib based on c++14/17* |
| -- | -- |
| Homepage | [https://github.com/an-tao/trantor/](https://github.com/an-tao/trantor/) |
| License | BSD-3-Clause |
| Versions | v1.3.0, v1.4.1, v1.5.0, v1.5.2, v1.5.5, v1.5.6, v1.5.7, v1.5.8 |
| Architectures | arm64, x64, x86 |
| Definition | [trantor/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/trantor/xmake.lua) |

##### Install command

```console
xrepo install trantor
```

##### Integration in the project (xmake.lua)

```lua
add_requires("trantor")
```


### turbobase64 (windows)


| Description | *Turbo Base64 - Fastest Base64 SIMD/Neon/Altivec* |
| -- | -- |
| Homepage | [https://github.com/powturbo/Turbo-Base64](https://github.com/powturbo/Turbo-Base64) |
| License | GPL-3.0 |
| Versions | 2022.02.21 |
| Architectures | arm64, x64, x86 |
| Definition | [turbobase64/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/t/turbobase64/xmake.lua) |

##### Install command

```console
xrepo install turbobase64
```

##### Integration in the project (xmake.lua)

```lua
add_requires("turbobase64")
```



## u
### uchardet (windows)


| Description | *uchardet is an encoding detector library, which takes a sequence of bytes in an unknown character encoding without any additional information, and attempts to determine the encoding of the text. * |
| -- | -- |
| Homepage | [https://www.freedesktop.org/wiki/Software/uchardet/](https://www.freedesktop.org/wiki/Software/uchardet/) |
| Versions | 0.0.7 |
| Architectures | arm64, x64, x86 |
| Definition | [uchardet/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/u/uchardet/xmake.lua) |

##### Install command

```console
xrepo install uchardet
```

##### Integration in the project (xmake.lua)

```lua
add_requires("uchardet")
```


### unicorn (windows)


| Description | *Unicorn CPU emulator framework (ARM, AArch64, M68K, Mips, Sparc, PowerPC, RiscV, S390x, X86)* |
| -- | -- |
| Homepage | [http://www.unicorn-engine.org](http://www.unicorn-engine.org) |
| Versions | 2022.02.13 |
| Architectures | arm64, x64, x86 |
| Definition | [unicorn/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/u/unicorn/xmake.lua) |

##### Install command

```console
xrepo install unicorn
```

##### Integration in the project (xmake.lua)

```lua
add_requires("unicorn")
```


### unordered_dense (windows)


| Description | *A fast & densely stored hashmap and hashset based on robin-hood backward shift deletion.* |
| -- | -- |
| Homepage | [https://github.com/martinus/unordered_dense](https://github.com/martinus/unordered_dense) |
| License | MIT |
| Versions | v1.1.0, v1.4.0, v2.0.2, v3.0.0 |
| Architectures | arm64, x64, x86 |
| Definition | [unordered_dense/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/u/unordered_dense/xmake.lua) |

##### Install command

```console
xrepo install unordered_dense
```

##### Integration in the project (xmake.lua)

```lua
add_requires("unordered_dense")
```


### unqlite (windows)


| Description | *An Embedded NoSQL, Transactional Database Engine.* |
| -- | -- |
| Homepage | [https://unqlite.org](https://unqlite.org) |
| Versions | 1.1.9 |
| Architectures | arm64, x64, x86 |
| Definition | [unqlite/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/u/unqlite/xmake.lua) |

##### Install command

```console
xrepo install unqlite
```

##### Integration in the project (xmake.lua)

```lua
add_requires("unqlite")
```


### unzip (windows)


| Description | *UnZip is an extraction utility for archives compressed in .zip format.* |
| -- | -- |
| Homepage | [http://infozip.sourceforge.net/UnZip.html](http://infozip.sourceforge.net/UnZip.html) |
| Versions | 6.0 |
| Architectures | arm64, x64, x86 |
| Definition | [unzip/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/u/unzip/xmake.lua) |

##### Install command

```console
xrepo install unzip
```

##### Integration in the project (xmake.lua)

```lua
add_requires("unzip")
```


### urdfdom (windows)


| Description | *A C++ parser for the Unified Robot Description Format (URDF)* |
| -- | -- |
| Homepage | [https://wiki.ros.org/urdf](https://wiki.ros.org/urdf) |
| License | BSD-3-Clause |
| Versions | 1.0.4 |
| Architectures | arm64, x64, x86 |
| Definition | [urdfdom/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/u/urdfdom/xmake.lua) |

##### Install command

```console
xrepo install urdfdom
```

##### Integration in the project (xmake.lua)

```lua
add_requires("urdfdom")
```


### urdfdom-headers (windows)


| Description | *Headers for URDF parsers* |
| -- | -- |
| Homepage | [http://ros.org/wiki/urdf](http://ros.org/wiki/urdf) |
| License | BSD-3-Clause |
| Versions | 1.0.5 |
| Architectures | arm64, x64, x86 |
| Definition | [urdfdom-headers/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/u/urdfdom-headers/xmake.lua) |

##### Install command

```console
xrepo install urdfdom-headers
```

##### Integration in the project (xmake.lua)

```lua
add_requires("urdfdom-headers")
```


### uriparser (windows)


| Description | *uriparser is a strictly RFC 3986 compliant URI parsing and handling library written in C89.* |
| -- | -- |
| Homepage | [https://uriparser.github.io/](https://uriparser.github.io/) |
| License | BSD-3-Clause |
| Versions | 0.9.5, 0.9.6, 0.9.7 |
| Architectures | arm64, x64, x86 |
| Definition | [uriparser/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/u/uriparser/xmake.lua) |

##### Install command

```console
xrepo install uriparser
```

##### Integration in the project (xmake.lua)

```lua
add_requires("uriparser")
```


### usd (windows)


| Description | *Universal Scene Description* |
| -- | -- |
| Homepage | [http://www.openusd.org](http://www.openusd.org) |
| Versions | v22.11 |
| Architectures | x64 |
| Definition | [usd/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/u/usd/xmake.lua) |

##### Install command

```console
xrepo install usd
```

##### Integration in the project (xmake.lua)

```lua
add_requires("usd")
```


### utest.h (windows)


| Description | *single header unit testing framework for C and C++* |
| -- | -- |
| Homepage | [https://www.duskborn.com/utest_h/](https://www.duskborn.com/utest_h/) |
| Versions | 2022.09.01 |
| Architectures | arm64, x64, x86 |
| Definition | [utest.h/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/u/utest.h/xmake.lua) |

##### Install command

```console
xrepo install utest.h
```

##### Integration in the project (xmake.lua)

```lua
add_requires("utest.h")
```


### utf8.h (windows)


| Description | *single header utf8 string functions for C and C++* |
| -- | -- |
| Homepage | [https://github.com/sheredom/utf8.h](https://github.com/sheredom/utf8.h) |
| Versions | 2022.07.04 |
| Architectures | arm64, x64, x86 |
| Definition | [utf8.h/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/u/utf8.h/xmake.lua) |

##### Install command

```console
xrepo install utf8.h
```

##### Integration in the project (xmake.lua)

```lua
add_requires("utf8.h")
```


### utf8proc (windows)


| Description | *A clean C library for processing UTF-8 Unicode data* |
| -- | -- |
| Homepage | [https://juliastrings.github.io/utf8proc/](https://juliastrings.github.io/utf8proc/) |
| License | MIT |
| Versions | v2.7.0, v2.8.0 |
| Architectures | arm64, x64, x86 |
| Definition | [utf8proc/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/u/utf8proc/xmake.lua) |

##### Install command

```console
xrepo install utf8proc
```

##### Integration in the project (xmake.lua)

```lua
add_requires("utf8proc")
```


### utfcpp (windows)


| Description | *UTF8-CPP: UTF-8 with C++ in a Portable Way* |
| -- | -- |
| Homepage | [https://github.com/nemtrif/utfcpp](https://github.com/nemtrif/utfcpp) |
| License | BSL-1.0 |
| Versions | v3.2.1 |
| Architectures | arm64, x64, x86 |
| Definition | [utfcpp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/u/utfcpp/xmake.lua) |

##### Install command

```console
xrepo install utfcpp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("utfcpp")
```


### uvw (windows)


| Description | *Header-only, event based, tiny and easy to use libuv wrapper in modern C++* |
| -- | -- |
| Homepage | [https://github.com/skypjack/uvw](https://github.com/skypjack/uvw) |
| Versions | 2.10.0 |
| Architectures | arm64, x64, x86 |
| Definition | [uvw/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/u/uvw/xmake.lua) |

##### Install command

```console
xrepo install uvw
```

##### Integration in the project (xmake.lua)

```lua
add_requires("uvw")
```


### uvwasi (windows)


| Description | *WASI syscall API built atop libuv* |
| -- | -- |
| Homepage | [https://github.com/nodejs/uvwasi](https://github.com/nodejs/uvwasi) |
| License | MIT |
| Versions | v0.0.12 |
| Architectures | arm64, x64, x86 |
| Definition | [uvwasi/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/u/uvwasi/xmake.lua) |

##### Install command

```console
xrepo install uvwasi
```

##### Integration in the project (xmake.lua)

```lua
add_requires("uvwasi")
```



## v
### v8 (windows)


| Description | *V8 JavaScript Engine* |
| -- | -- |
| Homepage | [https://chromium.googlesource.com/v8/v8.git](https://chromium.googlesource.com/v8/v8.git) |
| Versions | 10.0.58 |
| Architectures | arm64, x64, x86 |
| Definition | [v8/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/v/v8/xmake.lua) |

##### Install command

```console
xrepo install v8
```

##### Integration in the project (xmake.lua)

```lua
add_requires("v8")
```


### vc (windows)


| Description | *SIMD Vector Classes for C++* |
| -- | -- |
| Homepage | [https://github.com/VcDevel/Vc](https://github.com/VcDevel/Vc) |
| License | BSD-3-Clause |
| Versions | 1.4.2 |
| Architectures | arm64, x64, x86 |
| Definition | [vc/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/v/vc/xmake.lua) |

##### Install command

```console
xrepo install vc
```

##### Integration in the project (xmake.lua)

```lua
add_requires("vc")
```


### vcglib (windows)


| Description | *The Visualization and Computer Graphics Library (VCG for short) is a open source portable C++ templated library for manipulation, processing and displaying with OpenGL of triangle and tetrahedral meshes.* |
| -- | -- |
| Homepage | [http://www.vcglib.net/](http://www.vcglib.net/) |
| License | GPL-3.0 |
| Versions | 2020.12, 2021.07, 2021.10, 2022.02 |
| Architectures | arm64, x64, x86 |
| Definition | [vcglib/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/v/vcglib/xmake.lua) |

##### Install command

```console
xrepo install vcglib
```

##### Integration in the project (xmake.lua)

```lua
add_requires("vcglib")
```


### vectorial (windows)


| Description | *Vector math library with NEON/SSE support* |
| -- | -- |
| Homepage | [https://github.com/scoopr/vectorial](https://github.com/scoopr/vectorial) |
| Versions | 2019.06.28 |
| Architectures | arm64, x64, x86 |
| Definition | [vectorial/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/v/vectorial/xmake.lua) |

##### Install command

```console
xrepo install vectorial
```

##### Integration in the project (xmake.lua)

```lua
add_requires("vectorial")
```


### verilator (windows)


| Description | *Verilator open-source SystemVerilog simulator and lint system* |
| -- | -- |
| Homepage | [https://verilator.org](https://verilator.org) |
| Versions | 2023.1.10 |
| Architectures | arm64, x64, x86 |
| Definition | [verilator/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/v/verilator/xmake.lua) |

##### Install command

```console
xrepo install verilator
```

##### Integration in the project (xmake.lua)

```lua
add_requires("verilator")
```


### vexcl (windows)


| Description | *VexCL is a C++ vector expression template library for OpenCL/CUDA/OpenMP* |
| -- | -- |
| Homepage | [https://github.com/ddemidov/vexcl](https://github.com/ddemidov/vexcl) |
| License | MIT |
| Versions | 1.4.2 |
| Architectures | arm64, x64, x86 |
| Definition | [vexcl/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/v/vexcl/xmake.lua) |

##### Install command

```console
xrepo install vexcl
```

##### Integration in the project (xmake.lua)

```lua
add_requires("vexcl")
```


### viennacl (windows)


| Description | *ViennaCL is a free open-source linear algebra library for computations on many-core architectures (GPUs, MIC) and multi-core CPUs.* |
| -- | -- |
| Homepage | [http://viennacl.sourceforge.net/](http://viennacl.sourceforge.net/) |
| License | MIT |
| Versions | 1.7.1 |
| Architectures | arm64, x64, x86 |
| Definition | [viennacl/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/v/viennacl/xmake.lua) |

##### Install command

```console
xrepo install viennacl
```

##### Integration in the project (xmake.lua)

```lua
add_requires("viennacl")
```


### vk-bootstrap (windows)


| Description | *Vulkan Bootstrapping Iibrary.* |
| -- | -- |
| Homepage | [https://github.com/charles-lunarg/vk-bootstrap](https://github.com/charles-lunarg/vk-bootstrap) |
| License | MIT |
| Versions | v0.5 |
| Architectures | arm64, x64, x86 |
| Definition | [vk-bootstrap/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/v/vk-bootstrap/xmake.lua) |

##### Install command

```console
xrepo install vk-bootstrap
```

##### Integration in the project (xmake.lua)

```lua
add_requires("vk-bootstrap")
```


### volk (windows)


| Description | *volk is a meta-loader for Vulkan* |
| -- | -- |
| Homepage | [https://github.com/zeux/volk](https://github.com/zeux/volk) |
| License | MIT |
| Versions | 1.2.162, 1.2.190, 1.3.204, 1.3.231+1 |
| Architectures | arm64, x64, x86 |
| Definition | [volk/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/v/volk/xmake.lua) |

##### Install command

```console
xrepo install volk
```

##### Integration in the project (xmake.lua)

```lua
add_requires("volk")
```


### vtk (windows)


| Description | *The Visualization Toolkit (VTK) is open source software for manipulating and displaying scientific data.* |
| -- | -- |
| Homepage | [https://vtk.org/](https://vtk.org/) |
| License | BSD-3-Clause |
| Versions | 9.0.1, 9.0.3, 9.1.0, 9.2.2 |
| Architectures | arm64, x64, x86 |
| Definition | [vtk/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/v/vtk/xmake.lua) |

##### Install command

```console
xrepo install vtk
```

##### Integration in the project (xmake.lua)

```lua
add_requires("vtk")
```


### vulkan-headers (windows)


| Description | *Vulkan Header files and API registry* |
| -- | -- |
| Homepage | [https://github.com/KhronosGroup/Vulkan-Headers/](https://github.com/KhronosGroup/Vulkan-Headers/) |
| License | Apache-2.0 |
| Versions | 1.2.154+0, 1.2.162+0, 1.2.182+0, 1.2.189+1, 1.2.198+0, 1.3.211+0, 1.3.231+1 |
| Architectures | arm64, x64, x86 |
| Definition | [vulkan-headers/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/v/vulkan-headers/xmake.lua) |

##### Install command

```console
xrepo install vulkan-headers
```

##### Integration in the project (xmake.lua)

```lua
add_requires("vulkan-headers")
```


### vulkan-hpp (windows)


| Description | *Open-Source Vulkan C++ API* |
| -- | -- |
| Homepage | [https://github.com/KhronosGroup/Vulkan-Hpp/](https://github.com/KhronosGroup/Vulkan-Hpp/) |
| License | Apache-2.0 |
| Versions | v1.2.180, v1.2.189, v1.2.198, v1.3.231 |
| Architectures | x64, x86 |
| Definition | [vulkan-hpp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/v/vulkan-hpp/xmake.lua) |

##### Install command

```console
xrepo install vulkan-hpp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("vulkan-hpp")
```


### vulkan-loader (windows)


| Description | *This project provides the Khronos official Vulkan ICD desktop loader for Windows, Linux, and MacOS.* |
| -- | -- |
| Homepage | [https://github.com/KhronosGroup/Vulkan-Loader](https://github.com/KhronosGroup/Vulkan-Loader) |
| License | Apache-2.0 |
| Versions | 1.2.154+1, 1.2.162+0, 1.2.182+0, 1.2.189+1, 1.2.198+0, 1.3.231+1 |
| Architectures | x64, x86 |
| Definition | [vulkan-loader/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/v/vulkan-loader/xmake.lua) |

##### Install command

```console
xrepo install vulkan-loader
```

##### Integration in the project (xmake.lua)

```lua
add_requires("vulkan-loader")
```


### vulkan-memory-allocator (windows)


| Description | *Easy to integrate Vulkan memory allocation library.* |
| -- | -- |
| Homepage | [https://gpuopen-librariesandsdks.github.io/VulkanMemoryAllocator/html/](https://gpuopen-librariesandsdks.github.io/VulkanMemoryAllocator/html/) |
| License | MIT |
| Versions | v3.0.0, v3.0.1 |
| Architectures | arm64, x64, x86 |
| Definition | [vulkan-memory-allocator/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/v/vulkan-memory-allocator/xmake.lua) |

##### Install command

```console
xrepo install vulkan-memory-allocator
```

##### Integration in the project (xmake.lua)

```lua
add_requires("vulkan-memory-allocator")
```


### vulkan-tools (windows)


| Description | *Vulkan Utilities and Tools* |
| -- | -- |
| Homepage | [https://github.com/KhronosGroup/Vulkan-Tools](https://github.com/KhronosGroup/Vulkan-Tools) |
| License | Apache-2.0 |
| Versions | 1.2.154+0, 1.2.162+0, 1.2.189+1, 1.2.198+0 |
| Architectures | x64 |
| Definition | [vulkan-tools/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/v/vulkan-tools/xmake.lua) |

##### Install command

```console
xrepo install vulkan-tools
```

##### Integration in the project (xmake.lua)

```lua
add_requires("vulkan-tools")
```


### vulkan-validationlayers (windows)


| Description | *Vulkan Validation Layers* |
| -- | -- |
| Homepage | [https://github.com/KhronosGroup/Vulkan-ValidationLayers/](https://github.com/KhronosGroup/Vulkan-ValidationLayers/) |
| License | Apache-2.0 |
| Versions | 1.2.154+0, 1.2.162+0, 1.2.182+0, 1.2.189+1, 1.2.198+0 |
| Architectures | arm64, x64, x86 |
| Definition | [vulkan-validationlayers/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/v/vulkan-validationlayers/xmake.lua) |

##### Install command

```console
xrepo install vulkan-validationlayers
```

##### Integration in the project (xmake.lua)

```lua
add_requires("vulkan-validationlayers")
```



## w
### websocketpp (windows)


| Description | *C++ websocket client/server library* |
| -- | -- |
| Homepage | [http://www.zaphoyd.com/websocketpp](http://www.zaphoyd.com/websocketpp) |
| Versions | 0.8.2 |
| Architectures | arm64, x64, x86 |
| Definition | [websocketpp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/w/websocketpp/xmake.lua) |

##### Install command

```console
xrepo install websocketpp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("websocketpp")
```


### wil (windows)


| Description | *The Windows Implementation Libraries (WIL) is a header-only C++ library created to make life easier for developers on Windows through readable type-safe C++ interfaces for common Windows coding patterns.* |
| -- | -- |
| Homepage | [https://github.com/microsoft/wil](https://github.com/microsoft/wil) |
| License | MIT |
| Versions | 2022.09.16 |
| Architectures | arm64, x64, x86 |
| Definition | [wil/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/w/wil/xmake.lua) |

##### Install command

```console
xrepo install wil
```

##### Integration in the project (xmake.lua)

```lua
add_requires("wil")
```


### winflexbison (windows)


| Description | *Win flex-bison is a windows port the Flex (the fast lexical analyser) and Bison (GNU parser generator)* |
| -- | -- |
| Homepage | [https://github.com/lexxmark/winflexbison](https://github.com/lexxmark/winflexbison) |
| License | GPL |
| Versions | v2.5.25 |
| Architectures | arm64, x64, x86 |
| Definition | [winflexbison/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/w/winflexbison/xmake.lua) |

##### Install command

```console
xrepo install winflexbison
```

##### Integration in the project (xmake.lua)

```lua
add_requires("winflexbison")
```


### wolfssl (windows)


| Description | *The wolfSSL library is a small, fast, portable implementation of TLS/SSL for embedded devices to the cloud.  wolfSSL supports up to TLS 1.3!* |
| -- | -- |
| Homepage | [https://www.wolfssl.com](https://www.wolfssl.com) |
| License | GPL-2.0 |
| Versions | v5.3.0-stable |
| Architectures | arm64, x64, x86 |
| Definition | [wolfssl/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/w/wolfssl/xmake.lua) |

##### Install command

```console
xrepo install wolfssl
```

##### Integration in the project (xmake.lua)

```lua
add_requires("wolfssl")
```


### wxwidgets (windows)


| Description | *Cross-Platform C++ GUI Library* |
| -- | -- |
| Homepage | [https://www.wxwidgets.org/](https://www.wxwidgets.org/) |
| Versions | v3.2.0 |
| Architectures | arm64, x64, x86 |
| Definition | [wxwidgets/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/w/wxwidgets/xmake.lua) |

##### Install command

```console
xrepo install wxwidgets
```

##### Integration in the project (xmake.lua)

```lua
add_requires("wxwidgets")
```



## x
### xbyak (windows)


| Description | *A JIT assembler for x86(IA-32)/x64(AMD64, x86-64) MMX/SSE/SSE2/SSE3/SSSE3/SSE4/FPU/AVX/AVX2/AVX-512 by C++ header* |
| -- | -- |
| Homepage | [https://github.com/herumi/xbyak](https://github.com/herumi/xbyak) |
| Versions | v6.02, v6.03 |
| Architectures | arm64, x64, x86 |
| Definition | [xbyak/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/x/xbyak/xmake.lua) |

##### Install command

```console
xrepo install xbyak
```

##### Integration in the project (xmake.lua)

```lua
add_requires("xbyak")
```


### xege (windows)


| Description | *Easy Graphics Engine, a lite graphics library in Windows* |
| -- | -- |
| Homepage | [https://xege.org](https://xege.org) |
| License | LGPL-2.1 |
| Versions | v2020.08.31 |
| Architectures | arm64, x64, x86 |
| Definition | [xege/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/x/xege/xmake.lua) |

##### Install command

```console
xrepo install xege
```

##### Integration in the project (xmake.lua)

```lua
add_requires("xege")
```


### xerces-c (windows)


| Description | *Xerces-C++ is a validating XML parser written in a portable subset of C++.* |
| -- | -- |
| Homepage | [https://xerces.apache.org/xerces-c/](https://xerces.apache.org/xerces-c/) |
| License | Apache-2.0 |
| Versions | 3.2.3 |
| Architectures | arm64, x64, x86 |
| Definition | [xerces-c/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/x/xerces-c/xmake.lua) |

##### Install command

```console
xrepo install xerces-c
```

##### Integration in the project (xmake.lua)

```lua
add_requires("xerces-c")
```


### xframe (windows)


| Description | *C++ multi-dimensional labeled arrays and dataframe based on xtensor* |
| -- | -- |
| Homepage | [https://github.com/xtensor-stack/xframe/](https://github.com/xtensor-stack/xframe/) |
| License | BSD-3-Clause |
| Versions | 0.2.0, 0.3.0 |
| Architectures | arm64, x64, x86 |
| Definition | [xframe/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/x/xframe/xmake.lua) |

##### Install command

```console
xrepo install xframe
```

##### Integration in the project (xmake.lua)

```lua
add_requires("xframe")
```


### xsimd (windows)


| Description | *C++ wrappers for SIMD intrinsics* |
| -- | -- |
| Homepage | [https://github.com/xtensor-stack/xsimd/](https://github.com/xtensor-stack/xsimd/) |
| License | BSD-3-Clause |
| Versions | 7.6.0, 8.0.3, 8.0.5, 9.0.1 |
| Architectures | arm64, x64, x86 |
| Definition | [xsimd/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/x/xsimd/xmake.lua) |

##### Install command

```console
xrepo install xsimd
```

##### Integration in the project (xmake.lua)

```lua
add_requires("xsimd")
```


### xtensor (windows)


| Description | *Multi-dimensional arrays with broadcasting and lazy computing* |
| -- | -- |
| Homepage | [https://github.com/xtensor-stack/xtensor/](https://github.com/xtensor-stack/xtensor/) |
| License | BSD-3-Clause |
| Versions | 0.23.10, 0.24.0, 0.24.1, 0.24.3 |
| Architectures | arm64, x64, x86 |
| Definition | [xtensor/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/x/xtensor/xmake.lua) |

##### Install command

```console
xrepo install xtensor
```

##### Integration in the project (xmake.lua)

```lua
add_requires("xtensor")
```


### xtensor-blas (windows)


| Description | *BLAS extension to xtensor* |
| -- | -- |
| Homepage | [https://github.com/xtensor-stack/xtensor-blas/](https://github.com/xtensor-stack/xtensor-blas/) |
| License | BSD-3-Clause |
| Versions | 0.19.1, 0.20.0 |
| Architectures | arm64, x64, x86 |
| Definition | [xtensor-blas/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/x/xtensor-blas/xmake.lua) |

##### Install command

```console
xrepo install xtensor-blas
```

##### Integration in the project (xmake.lua)

```lua
add_requires("xtensor-blas")
```


### xtensor-io (windows)


| Description | *xtensor plugin to read and write images, audio files, numpy (compressed) npz and HDF5* |
| -- | -- |
| Homepage | [https://github.com/xtensor-stack/xtensor-io](https://github.com/xtensor-stack/xtensor-io) |
| License | BSD-3-Clause |
| Versions | 0.13.0 |
| Architectures | arm64, x64, x86 |
| Definition | [xtensor-io/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/x/xtensor-io/xmake.lua) |

##### Install command

```console
xrepo install xtensor-io
```

##### Integration in the project (xmake.lua)

```lua
add_requires("xtensor-io")
```


### xtl (windows)


| Description | *Basic tools (containers, algorithms) used by other quantstack packages* |
| -- | -- |
| Homepage | [https://github.com/xtensor-stack/xtl/](https://github.com/xtensor-stack/xtl/) |
| License | BSD-3-Clause |
| Versions | 0.7.2, 0.7.3, 0.7.4 |
| Architectures | arm64, x64, x86 |
| Definition | [xtl/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/x/xtl/xmake.lua) |

##### Install command

```console
xrepo install xtl
```

##### Integration in the project (xmake.lua)

```lua
add_requires("xtl")
```


### xxhash (windows)


| Description | *xxHash is an extremely fast non-cryptographic hash algorithm, working at RAM speed limit.* |
| -- | -- |
| Homepage | [http://cyan4973.github.io/xxHash/](http://cyan4973.github.io/xxHash/) |
| License | BSD-2-Clause |
| Versions | v0.8.0, v0.8.1 |
| Architectures | arm64, x64, x86 |
| Definition | [xxhash/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/x/xxhash/xmake.lua) |

##### Install command

```console
xrepo install xxhash
```

##### Integration in the project (xmake.lua)

```lua
add_requires("xxhash")
```


### xz (windows)


| Description | *General-purpose data compression with high compression ratio.* |
| -- | -- |
| Homepage | [https://tukaani.org/xz/](https://tukaani.org/xz/) |
| Versions | 5.2.10, 5.2.5, 5.4.1 |
| Architectures | arm64, x64, x86 |
| Definition | [xz/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/x/xz/xmake.lua) |

##### Install command

```console
xrepo install xz
```

##### Integration in the project (xmake.lua)

```lua
add_requires("xz")
```



## y
### yaml-cpp (windows)


| Description | *A YAML parser and emitter in C++* |
| -- | -- |
| Homepage | [https://github.com/jbeder/yaml-cpp/](https://github.com/jbeder/yaml-cpp/) |
| License | MIT |
| Versions | 0.6.3, 0.7.0 |
| Architectures | arm64, x64, x86 |
| Definition | [yaml-cpp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/y/yaml-cpp/xmake.lua) |

##### Install command

```console
xrepo install yaml-cpp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("yaml-cpp")
```


### yasm (windows)


| Description | *Modular BSD reimplementation of NASM.* |
| -- | -- |
| Homepage | [https://yasm.tortall.net/](https://yasm.tortall.net/) |
| Versions | 1.3.0 |
| Architectures | arm64, x64, x86 |
| Definition | [yasm/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/y/yasm/xmake.lua) |

##### Install command

```console
xrepo install yasm
```

##### Integration in the project (xmake.lua)

```lua
add_requires("yasm")
```


### yyjson (windows)


| Description | *The fastest JSON library in C.* |
| -- | -- |
| Homepage | [https://github.com/ibireme/yyjson](https://github.com/ibireme/yyjson) |
| Versions | 0.2.0, 0.3.0, 0.4.0, 0.5.0, 0.5.1 |
| Architectures | arm64, x64, x86 |
| Definition | [yyjson/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/y/yyjson/xmake.lua) |

##### Install command

```console
xrepo install yyjson
```

##### Integration in the project (xmake.lua)

```lua
add_requires("yyjson")
```



## z
### z3 (windows)


| Description | *Z3 is a theorem prover from Microsoft Research.* |
| -- | -- |
| Homepage | [https://github.com/Z3Prover/z3](https://github.com/Z3Prover/z3) |
| License | MIT |
| Versions | 4.8.15 |
| Architectures | arm64, x64, x86 |
| Definition | [z3/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/z/z3/xmake.lua) |

##### Install command

```console
xrepo install z3
```

##### Integration in the project (xmake.lua)

```lua
add_requires("z3")
```


### zeromq (windows)


| Description | *High-performance, asynchronous messaging library* |
| -- | -- |
| Homepage | [https://zeromq.org/](https://zeromq.org/) |
| License | GPL-3.0 |
| Versions | 4.3.2, 4.3.4 |
| Architectures | arm64, x64, x86 |
| Definition | [zeromq/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/z/zeromq/xmake.lua) |

##### Install command

```console
xrepo install zeromq
```

##### Integration in the project (xmake.lua)

```lua
add_requires("zeromq")
```


### zfp (windows)


| Description | *zfp is a compressed format for representing multidimensional floating-point and integer arrays.* |
| -- | -- |
| Homepage | [https://computing.llnl.gov/projects/zfp](https://computing.llnl.gov/projects/zfp) |
| License | BSD-3-Clause |
| Versions | 0.5.5 |
| Architectures | arm64, x64, x86 |
| Definition | [zfp/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/z/zfp/xmake.lua) |

##### Install command

```console
xrepo install zfp
```

##### Integration in the project (xmake.lua)

```lua
add_requires("zfp")
```


### zig (windows)


| Description | *Zig is a general-purpose programming language and toolchain for maintaining robust, optimal, and reusable software.* |
| -- | -- |
| Homepage | [https://www.ziglang.org/](https://www.ziglang.org/) |
| Versions | 0.10.0, 0.7.1, 0.9.1 |
| Architectures | arm64, x64, x86 |
| Definition | [zig/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/z/zig/xmake.lua) |

##### Install command

```console
xrepo install zig
```

##### Integration in the project (xmake.lua)

```lua
add_requires("zig")
```


### zlib (windows)


| Description | *A Massively Spiffy Yet Delicately Unobtrusive Compression Library* |
| -- | -- |
| Homepage | [http://www.zlib.net](http://www.zlib.net) |
| Versions | v1.2.10, v1.2.11, v1.2.12, v1.2.13 |
| Architectures | arm64, x64, x86 |
| Definition | [zlib/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/z/zlib/xmake.lua) |

##### Install command

```console
xrepo install zlib
```

##### Integration in the project (xmake.lua)

```lua
add_requires("zlib")
```


### zlib-ng (windows)


| Description | *zlib replacement with optimizations for next generation systems.* |
| -- | -- |
| Homepage | [https://github.com/zlib-ng/zlib-ng](https://github.com/zlib-ng/zlib-ng) |
| License | zlib |
| Versions | 2.0.5, 2.0.6 |
| Architectures | arm64, x64, x86 |
| Definition | [zlib-ng/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/z/zlib-ng/xmake.lua) |

##### Install command

```console
xrepo install zlib-ng
```

##### Integration in the project (xmake.lua)

```lua
add_requires("zlib-ng")
```


### zlibcomplete (windows)


| Description | *C++ interface to the ZLib library supporting compression with FLUSH, decompression, and std::string. RAII* |
| -- | -- |
| Homepage | [https://github.com/rudi-cilibrasi/zlibcomplete](https://github.com/rudi-cilibrasi/zlibcomplete) |
| License | MIT |
| Versions | 1.0.5 |
| Architectures | arm64, x64, x86 |
| Definition | [zlibcomplete/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/z/zlibcomplete/xmake.lua) |

##### Install command

```console
xrepo install zlibcomplete
```

##### Integration in the project (xmake.lua)

```lua
add_requires("zlibcomplete")
```


### zstd (windows)


| Description | *Zstandard - Fast real-time compression algorithm* |
| -- | -- |
| Homepage | [https://www.zstd.net/](https://www.zstd.net/) |
| License | BSD-3-Clause |
| Versions | v1.4.5, v1.5.0, v1.5.2 |
| Architectures | arm64, x64, x86 |
| Definition | [zstd/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/z/zstd/xmake.lua) |

##### Install command

```console
xrepo install zstd
```

##### Integration in the project (xmake.lua)

```lua
add_requires("zstd")
```


### zycore-c (windows)


| Description | *Internal library providing platform independent types, macros and a fallback for environments without LibC.* |
| -- | -- |
| Homepage | [https://github.com/zyantific/zycore-c](https://github.com/zyantific/zycore-c) |
| License | MIT |
| Versions | v1.0.0, v1.1.0 |
| Architectures | arm64, x64, x86 |
| Definition | [zycore-c/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/z/zycore-c/xmake.lua) |

##### Install command

```console
xrepo install zycore-c
```

##### Integration in the project (xmake.lua)

```lua
add_requires("zycore-c")
```


### zydis (windows)


| Description | *Fast and lightweight x86/x86-64 disassembler and code generation library* |
| -- | -- |
| Homepage | [https://zydis.re](https://zydis.re) |
| License | MIT |
| Versions | v3.2.1 |
| Architectures | arm64, x64, x86 |
| Definition | [zydis/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/z/zydis/xmake.lua) |

##### Install command

```console
xrepo install zydis
```

##### Integration in the project (xmake.lua)

```lua
add_requires("zydis")
```


### zziplib (windows)


| Description | *The zziplib library is intentionally lightweight, it offers the ability to easily extract data from files archived in a single zip file.* |
| -- | -- |
| Homepage | [http://zziplib.sourceforge.net/](http://zziplib.sourceforge.net/) |
| License | GPL-2.0 |
| Versions | v0.13.72 |
| Architectures | arm64, x64, x86 |
| Definition | [zziplib/xmake.lua](https://github.com/xmake-io/xmake-repo/blob/master/packages/z/zziplib/xmake.lua) |

##### Install command

```console
xrepo install zziplib
```

##### Integration in the project (xmake.lua)

```lua
add_requires("zziplib")
```



